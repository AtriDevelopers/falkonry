﻿using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using FalkonryClient.Helper.Models;
using Falkonry.Integrator.PISystem.Helper;
using log4net;
using Newtonsoft.Json;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Data;
using System.Collections.Generic;
using System.Threading;
using System.Reflection;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace Falkonry.Integrator.PISystem
{
    internal class FalkonryEvent
    {
        // ReSharper disable once InconsistentNaming
        public string entity { get; set; }
        // ReSharper disable once InconsistentNaming
        public string time { get; set; }
        // ReSharper disable once InconsistentNaming
        public string value { get; set; }
        public override string ToString()
        {
            return $"{{time: '{time}', entity: '{entity}', value: '{value}'}}";
        }
    }

    public class FalkonryReceiver
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
       

        //private readonly FalkonryClient.Falkonry _falkonry;

        private Assessment _assessment;
        private readonly AfConfiguration _afConfiguration;
        private readonly FalkonryConfiguration _falkonryConfiguration;
        private readonly string _AssessmentIdForStreaming;
        private readonly int _assessmentIndexForStreaming;
        private Timer _timer;
        private IDisposable internalSubscriberFalkonryOutput;

        private Dictionary<string, EventSource> _eventSource = new Dictionary<string, EventSource>();
        public FalkonryReceiver(FalkonryConfiguration falkonryConfiguration, AfConfiguration afConfiguration, string AssessmentId)
        {
            _afConfiguration = afConfiguration;
            _falkonryConfiguration = falkonryConfiguration;
            _AssessmentIdForStreaming = AssessmentId;
            if (_AssessmentIdForStreaming != null && _AssessmentIdForStreaming != "") {
                _assessmentIndexForStreaming = Array.IndexOf(_falkonryConfiguration.AssessmentId, _AssessmentIdForStreaming);
            }
            else
                Log.Debug($" Creating Backfill Falkonry receiver for datastream : {falkonryConfiguration.Datastream} Id : {falkonryConfiguration.SourceId}...");

           // _falkonry = new FalkonryClient.Falkonry(_falkonryConfiguration.Host, _falkonryConfiguration.Token);
        }

        private static SortedDictionary<string, string> _options;
        private static SortedDictionary<string, string> _toptions;
        /**
         * 
         *   Need to Refactor this in phase 2
         * 
         */ 
        public void StartBackfill(DateTime startTime, DateTime endTime)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    var assessmentList = FalkonryHelper.GetAssessments(_falkonryConfiguration);
                    string[] assessmentArray = _falkonryConfiguration.AssessmentId;
                    int index = 0;
                    foreach (var AssessmentId in assessmentArray)
                    {
                        try
                        {
                            var _assessment = assessmentList.Find(item => item.Id == AssessmentId);
                            _options = new SortedDictionary<string, string>
                                {
                                    {"startTime", startTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")},
                                    {"endTime", endTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")},
                                    {"responseFormat", "application/json"}
                                };
                            Log.Debug($" Creating request for historical output. for datastream: {_falkonryConfiguration.Datastream}. Assessment : {_assessment.Name} Time Range : { startTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")} TO {endTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")} ...");
                            var response = FalkonryHelper.GetHistoricalOutput(_falkonryConfiguration,_assessment, _options);
                            ProcessHistoricalOutput(_assessment, response, index);
                        }
                        catch (Exception exception)
                        {
                            Log.Error($"  Error message in assessment Method : StartBackfill Error :  {exception.Message} for datastream: {_falkonryConfiguration.Datastream}.");
                        }
                        index++;
                    }
                        
                }
                catch (Exception exception)
                {
                    Log.Error($"Error processing StartBackfill {exception} for datastream: {_falkonryConfiguration.Datastream}.");
                }
            });
        }

        internal string getAssessmentId()
        {
            return _AssessmentIdForStreaming;
        }

        private void ProcessHistoricalOutput(Assessment assessment, HttpResponse response, int index)
        {
            
            if (response.StatusCode == 200)
            {
                var events = response.Response.Split('\n').ToList()
                    .Where(e => e != "")
                    .Select(JsonConvert.DeserializeObject<FalkonryEvent>)
                    .OrderBy(e => long.Parse(e.time))
                    .ToList();

                BackfillEvents(events, index);
                Log.Debug($"Successfully output generated for backfill request. for datastream: {_falkonryConfiguration.Datastream}  for assessment :{assessment.Name}");
                AfHelper.UpdateState(_afConfiguration.SystemName, _falkonryConfiguration.Datastream, "INFO", $"Successfully process historical output generated for backfill request for assessment :{assessment.Name}");
                return;
            }
            if (response.StatusCode == 202)
            {
                Log.Debug($" Response JSON : .GetHistoricalOutput response length - {response.Response.Length} for datastream: {_falkonryConfiguration.Datastream} for assessment :{assessment.Name}");
                var trackerResponse = JsonConvert.DeserializeObject<Tracker>(response.Response);

                if (trackerResponse.Id != null)
                {
                    var id = trackerResponse.Id;

                    Log.Debug($" Tracker Id: {id} for datastream: {_falkonryConfiguration.Datastream}");

                    _toptions = new SortedDictionary<string, string>
                    {
                        {"trackerId", id},
                        {"responseFormat", "application/json"}
                    };

                    Thread.Sleep(5000);
                    response = FalkonryHelper.GetHistoricalOutput(_falkonryConfiguration,assessment, _toptions);
                }
                else
                {
                    Thread.Sleep(5000);
                    response = FalkonryHelper.GetHistoricalOutput(_falkonryConfiguration,assessment, _toptions);
                }

                ProcessHistoricalOutput(assessment, response, index);

                return;
            }
            if (response.StatusCode > 400)
            {
                Log.Debug($" Response JSON : .GetHistoricalOutput {response.Response} for datastream: {_falkonryConfiguration.Datastream} for assessment :{assessment.Name}");
                //Thread.Sleep(15000);
                //response = _falkonry.GetHistoricalOutput(assessment, _options);
                //ProcessHistoricalOutput(assessment, response);
                Log.Error($"Could not process historical output for Assessment '{assessment.Name}' Datastream Id {_falkonryConfiguration.SourceId}.");
                AfHelper.UpdateState(_afConfiguration.SystemName, _falkonryConfiguration.Datastream, "ERROR", $"Could not process historical output for backfill request. Response: {response.Response}");
                return;
            }
        }


        public void StopListening()
        {
            try
            {
                if (_eventSource != null && _eventSource.ContainsKey(_assessment.Id))
                {
                    Log.Debug($"Stopping streaming output Listener for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Name : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming}");
                    //_assessment = FalkonryHelper.GetAssessments(_falkonryConfiguration).First(a => a.Name == _AssessmentIdForStreaming && a.Datastream == _falkonryConfiguration.SourceId);
                    _eventSource[_assessment.Id]?.Shutdown();
                    _eventSource[_assessment.Id]?.Dispose();
                    _eventSource.Remove(_assessment.Id);
                    _timer?.Dispose();
                }
                internalSubscriberFalkonryOutput?.Dispose();
            }
            catch (Exception e)
            {
                Log.Error($"Unexpected error while stopping streaming receiver for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Name : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming} \n StackTrace:{e.StackTrace}");
                return;
            }

        }
        public void StartListening()
        {
            internalSubscriberFalkonryOutput?.Dispose();
            internalSubscriberFalkonryOutput = Observable.Timer(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(60))
               .SubscribeOn(NewThreadScheduler.Default)
               .ObserveOn(NewThreadScheduler.Default)
               .Subscribe(x => {
                   try
                   {
                       try
                       {
                           Datastream updatedDatastream = FalkonryHelper.GetDatastream(_falkonryConfiguration);
                           _assessment = FalkonryHelper.GetAssessment(_falkonryConfiguration, _AssessmentIdForStreaming);
                           if (updatedDatastream.Live.ToLower() != "on")
                           {
                               if (_eventSource.ContainsKey(_assessment.Id))
                               {
                                   _eventSource[_assessment.Id]?.Shutdown();
                                   _eventSource[_assessment.Id]?.Dispose();
                                   _eventSource.Remove(_assessment.Id);
                                   internalSubscriberFalkonryOutput?.Dispose();
                               }
                               _timer?.Dispose();
                               return;
                           }
                           if (_assessment.Live.ToLower() != "on") return;
                           if (_eventSource.ContainsKey(_assessment.Id)) return;
                           int id = Thread.CurrentThread.ManagedThreadId;
                           Log.Info($"Live output listener thread id: {id} ");
                           Log.Debug($"************ Before GetOutput Call for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Name : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming}");
                           EventSource eventSource = FalkonryHelper.GetOutput(_falkonryConfiguration, _assessment.Id);
                           Log.Debug($"----------- After GetOutput Call for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Name : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming}");
                           eventSource.Message += EventSource_Message;
                           eventSource.Error += EventSource_NewError;
                           _eventSource.Add(_assessment.Id, eventSource);
                       }
                       catch (Exception exception)
                       {
                           Log.Error($"Error in fetching assessment : {_AssessmentIdForStreaming} Exception Message: {exception.Message} \n Exception StackTrace: {exception.StackTrace}");
                         
                           return;
                       }

                   }
                   catch (Exception e)
                   {
                       Log.Error($"Fatal issue processing Streaming Output. Exception Message: {e.Message} \n Exception StackTrace: {e.StackTrace}");
                     
                   }
               }, x => {
                   Log.Error($"Fatal issue processing Streaming Output. Exception Message: {x.Message} \n Exception StackTrace: {x.StackTrace}");
               });
           
        }

        private void EventSource_NewError(object sender, EventSource.ServerSentErrorEventArgs e)
        {
            int id = Thread.CurrentThread.ManagedThreadId;
            Log.Info($"Event Source Error Message thread id: {id} ");
            Log.Error($"Error received from Falkonry Assessment Output: {e.Exception.Message} for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Name : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming}");

            try
            {
                if(e.Exception is System.Net.WebException)
                {
                    if (((HttpWebResponse)((System.Net.WebException)e.Exception).Response).StatusCode == HttpStatusCode.NotFound && _eventSource.ContainsKey(_assessment.Id))
                    {
                        Log.Error($"Shutting down eventsource for datastream: {_falkonryConfiguration.Datastream} assessment id : {_assessment.Id} as Datastream is not available");
                        if (_eventSource.ContainsKey(_assessment.Id))
                        {
                            _eventSource[_assessment.Id]?.Shutdown();
                            _eventSource[_assessment.Id]?.Dispose();
                            _eventSource.Remove(_assessment.Id);
                            _timer?.Dispose();
                            internalSubscriberFalkonryOutput?.Dispose();
                        }
                    }
                }else if(e.Exception is System.TimeoutException)
                {
                    Log.Error($"Timeout Exception in eventsource for Datastream : {_falkonryConfiguration.Datastream} assessment id : {_assessment.Id} \n StackTrace: {e.Exception.StackTrace}");
                    if (_eventSource.ContainsKey(_assessment.Id))
                    {
                        _eventSource[_assessment.Id]?.Shutdown();
                        _eventSource[_assessment.Id]?.Dispose();
                        _eventSource.Remove(_assessment.Id);
                        _timer?.Dispose();
                    }
                    StartListening();
                }
                else
                {
                    Log.Error($"Unknown Exception in eventsource for Datastream : {_falkonryConfiguration.Datastream} assessment id : {_assessment.Id} \n StackTrace: {e.Exception.StackTrace}");
                    if (_eventSource.ContainsKey(_assessment.Id))
                    {
                        _eventSource[_assessment.Id]?.Shutdown();
                        _eventSource[_assessment.Id]?.Dispose();
                        _eventSource.Remove(_assessment.Id);
                        _timer?.Dispose();
                    }
                    StartListening();
                }
                
            }
            catch (Exception exception)
            {
                Log.Error($"Exception in handling the event source error : {exception.Message} for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Name : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming} \n StackTrace :{exception.StackTrace}");
                return;    
            }
                
        }


        private void BackfillEvents(List<FalkonryEvent> events, int index)
        {
            var elements = events.Select(e => e.entity).Distinct();
            foreach (var element in elements)
            {
                if (element == "") continue;

                var attribute = AfHelper.FindElementAttribute(Guid.Parse(element), _afConfiguration, index);
                if (attribute == null) continue;

                var elementEvents = events.Where(e => e.entity == element);
                var listOfAfValues = new AFValues();
                foreach (var elementEvent in elementEvents)
                {
                    var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    dtDateTime = dtDateTime.AddMilliseconds(long.Parse(elementEvent.time)).ToLocalTime();
                    listOfAfValues.Add(new AFValue(elementEvent.value, dtDateTime));
                }

                if (listOfAfValues.Count <= 0) continue;
                try
                {
                    attribute.Data.UpdateValues(listOfAfValues, AFUpdateOption.Replace);
                }
                catch (Exception exception)
                {
                    Log.Error($"Count not update the Attribute '{attribute.Name}'. Error: '{exception.Message}'");
                }
            }
        }

        private void EventSource_Message(object sender, EventSource.ServerSentEventArgs e)
        {
            int id = Thread.CurrentThread.ManagedThreadId;
            Log.Info($"Event Source Message thread id: {id} ");
            var falkonryEvent = JsonConvert.DeserializeObject<FalkonryEvent>(e.Data);

            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(long.Parse(falkonryEvent.time)).ToLocalTime();

            //if (dtDateTime < _latestEventDate) return;
            //_latestEventDate = dtDateTime;

            Log.Debug($"New event received from Falkonry with Id '{e.EventId}'.for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Id : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming} Event data: { falkonryEvent}");

            var attribute = AfHelper.FindElementAttribute(Guid.Parse(falkonryEvent.entity), _afConfiguration, _assessmentIndexForStreaming);
            if (attribute == null) return;

            var afValue = new AFValue(falkonryEvent.value, dtDateTime);
            try
            {
                attribute.Data.UpdateValue(afValue, AFUpdateOption.Replace);
            }
            catch (Exception exception)
            {
                Log.Error($"Count not update the Attribute '{attribute.Name}'. Error: '{exception.Message}'.for datastream: {_falkonryConfiguration.Datastream} Id : {_falkonryConfiguration.SourceId}... Assessment Id : {_AssessmentIdForStreaming}. Index : {_assessmentIndexForStreaming}");

            }
        }
    }
}
