﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Subscribers
{
    /**
     * 
     *   Generic Subscriber for Observables who emeit PiConfiguration event.
     * 
     */ 
    public class PiConfigurationSubscriber : IObserver<PiConfiguration>
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        public void OnCompleted()
        {
            Log.Info($"Subscriber On Completed Invoked");
        }

        public void OnError(Exception exception)
        {
            Log.Fatal($"Exception While Processing for DataStream ",exception);
        }

        public void OnNext(PiConfiguration value)
        {
          // Log.Info($"Subscriber On Next Invoked for DataStream {value.FalkonryConfiguration.Datastream} SourceID {value.FalkonryConfiguration.Datastream}");
        }
    }
}
