﻿using System;
using log4net.Layout;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;

namespace Falkonry.Integrator.PISystem.Helper
{
    class LogPatternLayout : PatternLayout
    {
        public override string Header
        {
            get
            {
                var dateString = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                return string.Format("[{0} Falkonry PI Agent version:  {1} ]\r\n", dateString, Application.ProductVersion);
            }
        }
    }
}