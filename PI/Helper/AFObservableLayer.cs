﻿using OSIsoft.AF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using OSIsoft.AF.Asset;
using System.Timers;
using System.Configuration;
using System.Reactive.Linq;
using System.Reactive;
using System.Reactive.Subjects;
using System.Reactive.Concurrency;
using Falkonry.Integrator.PISystem.Functors;
using System.Threading;

namespace Falkonry.Integrator.PISystem.Helper
{
    /**
     *
     * This is an observable layer over AF Database change events.
     * 
     */
    public class AFObservableLayer
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private double afChangeEventCollectionSeconds;
        /**
         * Observable to Query the AFDatabase at Regular Interval of time and Keep the Local Mem Sync with AFDatabase.
         */
        private IObservable<long> bootStraper;
        /**
         * Observable to Query the Falkonry Back end at Regular Interval of time and delete the Datastream
         * Which are not present at Falkonry Back end and delete them from AFDatabase.
         */
        private IObservable<long> falkonryDeleteObservable;
        /**
        * Observable to Query the Falkonry Back end at Regular Interval of time 
        private Subject<PiConfiguration> configAddedSubject;
        * and starts the Live Monitoring for the PiConfiguration.
        */
        private IObservable<long> watchlivestream;
        private Subject<PiConfiguration> configAddedSubject;
        private Subject<PiConfiguration> configDeletedSubject;
        private Subject<PiConfiguration> configUpdatedSubject;
        private Subject<PiConfiguration> pushHistoricalDataSubject;
        private Subject<PiConfiguration> FalkonrySyncDataSubject;
        
        private IObservable<PiConfiguration> configAdded;
        private IObservable<PiConfiguration> configDeleted;
        private IObservable<PiConfiguration> configUpdate;
        private IObservable<PiConfiguration> pushHistorical;

        private IDisposable liveStreamWatchSubscription;
        private IDisposable bootStrapSubscription;
        private IDisposable falkonrySyncSubscription;

        private IDisposable configAddedScheduler;
        private IDisposable configUpdateScheduler;
        private IDisposable configDeletedScheduler;
        private IDisposable pushHistoricalScheduler;
        private IDisposable liveStreamScheduler;
        private IDisposable falkonrySyncScheduler;
        private IDisposable bootStrapScheduler;

        public AFObservableLayer()
        {
           
        }
       
        private void initialize()
        {

            afChangeEventCollectionSeconds = 60.0;
            configAddedSubject = new Subject<PiConfiguration>();
            configDeletedSubject = new Subject<PiConfiguration>();
            configUpdatedSubject = new Subject<PiConfiguration>();
            pushHistoricalDataSubject = new Subject<PiConfiguration>();
            FalkonrySyncDataSubject = new Subject<PiConfiguration>();

            //configDeletedScheduler = new EventLoopScheduler();
            //configUpdateScheduler = new EventLoopScheduler();
            //configAddedScheduler = new EventLoopScheduler();
            //pushHistoricalScheduler = new EventLoopScheduler();

            configAdded = configAddedSubject.AsObservable()
                                    .SubscribeOn(NewThreadScheduler.Default)
                                    .ObserveOn(new EventLoopScheduler());
            configDeleted = configDeletedSubject.AsObservable().SubscribeOn(NewThreadScheduler.Default)
                                    .ObserveOn(new EventLoopScheduler());
            configUpdate = configUpdatedSubject.AsObservable().SubscribeOn(NewThreadScheduler.Default)
                                    .ObserveOn(new EventLoopScheduler());
            pushHistorical = pushHistoricalDataSubject.AsObservable().SubscribeOn(NewThreadScheduler.Default)
                                    .ObserveOn(new EventLoopScheduler());
          
          

        }


        /**
* 
*  Compulsorily need to call setup method in order pump AF Database Change event to the Observable Pipeline. 
*/
        public void setUp()
        {
            initialize();
            startBootStrapObservablePipeline();
            startFalkonrySyncObservablePipeLine();
            startLiveStreamWatchObservablePipeline();
        }

        private void startBootStrapObservablePipeline()
        {
          //  bootStrapScheduler = new EventLoopScheduler();
            bootStraper = Observable.Timer(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(afChangeEventCollectionSeconds))
                                    .SubscribeOn(NewThreadScheduler.Default)
                                    .ObserveOn(new EventLoopScheduler())
                                     .Select(x => {
                                         Log.Info($"Sync AF DB and Local Memcache started minute tick :{x}"); return x;
                                     });
            bootStrapSubscription = bootStraper
                       .Select(new FetchConfigurationAFElements().getDelegate())
                       .Select(new TrasformAElementListToPiConfiguration().getDelegate())
                       .Select(new PopulateDatastreamConfigValuesFromFalkonryServer().getDelegate())
                       .Select(new FilterDeletedPiConfigurationList(configDeletedSubject, pushHistoricalDataSubject).getDelegate())
                       .Select(new FilterCreatedPiConfigurationList(configAddedSubject, pushHistoricalDataSubject).getDelegate())
                       .Select(new FilterUpdatedPiConfigurationList(configUpdatedSubject, pushHistoricalDataSubject).getDelegate())
                       .Subscribe(new TimerSubscription(this));
        }

        private void startFalkonrySyncObservablePipeLine()
        {
            //falkonrySyncScheduler = new EventLoopScheduler();
            falkonryDeleteObservable = Observable.Timer(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(afChangeEventCollectionSeconds))
                                   .SubscribeOn(NewThreadScheduler.Default)
                                   .ObserveOn(new EventLoopScheduler())
                                    .Select(x => {
                                        Log.Info($"Falkonry Sync AF DB and Falkonry started minute tick :{x}"); return x;
                                    });
            falkonrySyncSubscription = falkonryDeleteObservable
                       .Select(new FetchConfigurationAFElements().getDelegate())
                       .Select(new TrasformAElementListToPiConfiguration().getDelegate())
                       .Select(new UpdateChangesInAFFromFalkonry().getDelegate())
                       .Select(new FindPiconfigurationToDelete().getDelegate())
                       .Select(new DeletePiconfigurationsFromPiSystem(configDeletedSubject).getDelegate())
                       .Subscribe(new FalknoryDeleteSyncSubscription(this));
        }

        private void startLiveStreamWatchObservablePipeline()
        {
            //liveStreamScheduler = new EventLoopScheduler();
            watchlivestream = Observable.Timer(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(afChangeEventCollectionSeconds))
                                   .SubscribeOn(NewThreadScheduler.Default)
                                   .ObserveOn(new EventLoopScheduler())
                                   .Select(x => {
                                       Log.Info($"Live Streaming Check minute tick :{x}"); return x;
                                   });
            liveStreamWatchSubscription = watchlivestream.Select(new WatchLiveStreamConfig(configUpdatedSubject).getDelegate())
                      .Subscribe(new WatchLiveSubscription(this));
        }

        public string FalkonryConfigurationRootElement()
        {
            return @"Falkonry\Integrator";
        }

        public IObservable<PiConfiguration> getObservableForConfigCreated()
        {
            return configAdded;
        }

        public IObservable<PiConfiguration> getObservableForConfigDeleted()
        {
            return configDeleted;
        }

        public IObservable<PiConfiguration> getObservableForConfigUpdated()
        {
            return configUpdate;
        }

        public IObservable<PiConfiguration> getObservableForHistoricalPush()
        {
            return pushHistorical;
        }

        public void tearDown()
        {
            if(liveStreamWatchSubscription!=null)
                liveStreamWatchSubscription.Dispose();
            if(bootStrapSubscription!=null)
                bootStrapSubscription.Dispose();
            if(falkonrySyncSubscription!=null)
                falkonrySyncSubscription.Dispose();
            //disposeSchedulers();

        }

        private void disposeSchedulers()
        {
            configAddedScheduler?.Dispose();
            configUpdateScheduler?.Dispose();
            configDeletedScheduler?.Dispose();
            pushHistoricalScheduler?.Dispose();
            bootStrapScheduler?.Dispose();
            liveStreamScheduler?.Dispose();
            falkonrySyncScheduler?.Dispose();
        }
        private class FalknoryDeleteSyncSubscription : IObserver<IList<PiConfiguration>>
        {
            AFObservableLayer layer;
            public FalknoryDeleteSyncSubscription(AFObservableLayer layer)
            {
                this.layer = layer;
            }
            public void OnCompleted()
            {
                Log.Info($" Falkonry Delete Sync Subscriber OnCompleted");
            }

            public void OnError(Exception error)
            {
                Log.Fatal($" Falkonry Delete Sync Subscriber Observable Stopped \n Stacktrace :{error.StackTrace}");
                if (error.Data.Contains("issue"))
                {
                    layer.startFalkonrySyncObservablePipeLine();
                }
            }

            public void OnNext(IList<PiConfiguration> value)
            {
                value.All(x =>
                {
                    Log.Info($"Deleted DataStream from PiSystem :: {x.FalkonryConfiguration.Datastream}");
                    return true;
                });
               
            }

        }

        private class TimerSubscription : IObserver<IList<PiConfiguration>>
        {
            AFObservableLayer layer;
            public TimerSubscription(AFObservableLayer layer)
            {
                this.layer = layer;
            }
            public void OnCompleted()
            {
                Log.Info($" AFObservable Layer OnCompleted");
            }

            public void OnError(Exception error)
            {
                Log.Fatal($" AFObservable Layer BootStrap Observable Stopped \n StackTrace :{error.StackTrace}");
                if (error.Data.Contains("issue"))
                {
                    layer.startBootStrapObservablePipeline();
                }
            }

            public void OnNext(IList<PiConfiguration> value)
            {
              
            }
        }

        private class WatchLiveSubscription : IObserver<IList<PiConfiguration>>
        {
             AFObservableLayer layer;

            public WatchLiveSubscription(AFObservableLayer layer)
            {
                this.layer = layer;
            }

            public void OnCompleted()
            {
                Log.Info($" AFObservable Layer OnCompleted");
            }

            public void OnError(Exception error)
            {
                Log.Fatal($" AFObservable Layer watchlivestream Observable Stopped \n Stacktrace :{error.StackTrace}");
                if (error.Data.Contains("issue"))
                {
                    layer.startLiveStreamWatchObservablePipeline();
                }
            }

            public void OnNext(IList<PiConfiguration> value)
            {
               
            }
        }
    }

    
}
