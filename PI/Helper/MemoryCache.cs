﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Helper
{
    /**
     *  Singleton class used to maintain in-memory copy of PiConfiguration stored in the AFDatabase
     * 
     * 
     */
    public sealed class MemoryCache
    {
        private static readonly MemoryCache instance = new MemoryCache();

        /**
         *   List to maintain the copy of PiConfiguration.
         *   Caution :: Do not expose this to outside world using getter.
         *   What ever operations you need to perform on this list create a method for it in this class.
         * 
         */
        private IList<PiConfiguration> piConfigurations;

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static MemoryCache()
        {
        }

        private MemoryCache()
        {
            piConfigurations = new List<PiConfiguration>();
        }

        public static MemoryCache Instance
        {
            get
            {
                return instance;
            }
        }

        public void addPiConfiguration(PiConfiguration config)
        {
            piConfigurations.Add(config);
        }

        public void removeConfiguration(PiConfiguration config)
        {
            piConfigurations.Remove(config);
        }

        //public void removeConfigurationList(IList<PiConfiguration> configlist)
        //{
        //    piConfigurations.Remove(configlist);
        //}

        public int getPiConfigurationCount()
        {
            return piConfigurations.Count;
        }

        public IList<PiConfiguration> getConfigurationsCopy()
        {
            lock (piConfigurations)
            {
                List<PiConfiguration> configList = new List<PiConfiguration>();
                piConfigurations.All(x =>
                {
                    configList.Add(x.CopyObject<PiConfiguration>());
                    return true;
                });
                return configList;
            }
        }
    }
}
