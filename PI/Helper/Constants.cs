﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Helper
{
    public class Constants
    {
        private Constants() { }
        public  const String CONFIGDBNAME = "Configuration";
        public const String CONFIGElementNAME = "Configurations";

        public const String CONFIGTemplateConfigurations = "Falkonry_Configurations";
        public const String CONFIGTemplateConfigurationItem = "Falkonry_ConfigurationItem";
        public const String CONFIGTemplateConfigurationItem_AF = "Falkonry_ConfigurationItem_AF";
        public const String CONFIGTemplateConfigurationItem_Falkonry = "Falkonry_ConfigurationItem_Falkonry";
        public const String CONFIGTemplateConfigurationItem_Historical = "Falkonry_ConfigurationItem_Historical";
        public const String CONFIGTemplateConfigurationItem_Streaming = "Falkonry_ConfigurationItem_Streaming";

        public sealed class ParentConfigConstant
        {
            private ParentConfigConstant() { }
            public const String Deleted = "Deleted";
            public const String Enabled = "Enabled";
            public const String ID = "Id";
            public const String Message = "Message";
            public const String Name = "Name";
            public const String Status = "Status";
        }
        public sealed class AFConfigConstants
        {
            private AFConfigConstants() { }
            public const String name = "AF";
            public const String AFServerTimeZone = "AFServerTimeZoneId";
            public const String DatabaseName = "DatabaseName";
            public const String EventFrameTemplateName = "EventFrameTemplateNames";
            public const String EventFrameUploadedTime = "EventFrameUploadedTime";
            public const String OutTemplateAttribute = "OutputTemplateAttribute";
            public const String SystemName = "SystemName";
            public const String TemplateAttributes = "TemplateAttributes";
            public const String TemplateElementPaths = "TemplateElementPaths";
            public const String TemplateName = "TemplateName";
        }

        public sealed class FalkonryConfigConstants
        {
            private FalkonryConfigConstants() { }
            public const String name = "Falkonry";
            public const String AssessmentId = "AssessmentId";
            public const String AssessmentIdForFacts = "AssessmentIdForFacts";
            public const String BackFill = "BackFill";
            public const String BatchLimit = "BatchLimit";
            public const String DataStream = "Datastream";
            public const String Host = "Host";
            public const String SourceId = "SourceId";
            public const String TimeFormat = "TimeFormat";
            public const String Token = "Token";
        }

        public sealed class HistoricalConfigConstants
        {
            private HistoricalConfigConstants() { }
            public const String name = "Historical";
            public const String Enabled = "Enabled";
            public const String EndTime = "EndTime";
            public const String Override = "Override";
            public const String PageSize = "PageSize";
            public const String StartTime = "StartTime";
        }

        public sealed class StremingConfigConstants
        {
            private StremingConfigConstants() { }
            public const String name = "Streaming";
            public const String CollectionInterval = "CollectionIntervalMs";
          
        }


    }
}
