﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FalkonryClient.Helper.Models;
using log4net;
using System.IO;
using Microsoft.Win32;
using FalkonryClient.Service;
using System.Net;
using Newtonsoft.Json;
using Msi.Tables;
using System.Diagnostics;
using System.Security.Principal;


namespace Falkonry.Integrator.PISystem.Helper
{
    public class Utils
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        public static DateTime defaultDate = DateTime.Parse("01-Jan-1970");

        public static string FalkonryConfigurationRootElement()
        {
            return @"Falkonry\Integrator";
        }


        public static DateTime ConvertMillisecondsToUTC(long millisecondsSince1970)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(millisecondsSince1970);
            return dtDateTime;
        }

        public static DateTime ConvertMillisecondsToLOCAL(long millisecondsSince1970)
        {
            //var dtDateTime = (new DateTime(1970, 1, 1)).AddMilliseconds(millisecondsSince1970).ToLocalTime();
            //var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            //dtDateTime = dtDateTime.AddMilliseconds(millisecondsSince1970);
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(millisecondsSince1970).ToLocalTime();
            return dtDateTime;
            //
        }

        public static long ConvertDateTimeToMilliseconds(DateTime dateTimeObj)
        {
            return Convert.ToInt64(dateTimeObj.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local)).TotalMilliseconds);

        }


        public static string WindowsToIana(string windowsZoneId)
        {
            if (windowsZoneId.Equals("UTC", StringComparison.Ordinal))
                return "Etc/UTC";

            var tzdbSource = NodaTime.TimeZones.TzdbDateTimeZoneSource.Default;
            var tzi = TimeZoneInfo.FindSystemTimeZoneById(windowsZoneId);
            if (tzi == null) return null;
            var tzid = tzdbSource.MapTimeZoneId(tzi);
            if (tzid == null) return null;
            return tzdbSource.CanonicalIdMap[tzid];
        }

        public static void WriteErrorToFile(String ErrorMessage)
        {
            var ErrorFileName = GetErrorLogsFileName();
            FileStream aFile;
            if (!File.Exists(ErrorFileName))
            {
                aFile = new FileStream(ErrorFileName, FileMode.Create, FileAccess.Write);
            }
            else
            {
                aFile = new FileStream(ErrorFileName, FileMode.Append, FileAccess.Write);
            }
            StreamWriter sw = new StreamWriter(aFile);
            sw.WriteLine(ErrorMessage);
            sw.Close();
            aFile.Close();
            return;
        }

        private static string GetErrorLogsFileName()
        {
            var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            if (location == null) return "";
            var errorFileName = location.ToLower().Replace(".exe", ".error.log");
            return errorFileName;
        }

        public static string Get45or451FromRegistry()
        {
            using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
            {
                int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                if (true)
                {
                    return CheckFor45DotVersion(releaseKey);
                }

            }
        }


        // Checking the version using >= will enable forward compatibility,  
        // however you should always compile your code on newer versions of 
        // the framework to ensure your app works the same. 
        private static string CheckFor45DotVersion(int releaseKey)
        {
            if (releaseKey >= 393273)
            {
                return "4.6 RC or later";
            }
            if ((releaseKey >= 379893))
            {
                return "4.5.2 or later";
            }
            if ((releaseKey >= 378675))
            {
                return "4.5.1 or later";
            }
            if ((releaseKey >= 378389))
            {
                return "4.5 or later";
            }
            // This line should never execute. A non-null release key should mean 
            // that 4.5 or later is installed. 
            return "No 4.5 or later version detected";
        }

        public static void ResultantTimeFrame(TimeFrame existingTimeFrame, TimeFrame newTimeFrame, TimeFrame resultantTimeFrame_1, TimeFrame resultantTimeFrame_2)
        {
            //Log.Debug($"ResultantTimeFrame: existingTimeFrame, Start Time - {existingTimeFrame.StartTime}, EndTime - {existingTimeFrame.EndTime}");
            //Log.Debug($"ResultantTimeFrame: newTimeFrame, Start Time - {newTimeFrame.StartTime}, EndTime - {newTimeFrame.EndTime}");

            if (newTimeFrame.StartTime < existingTimeFrame.StartTime)
            {
                resultantTimeFrame_1.StartTime = newTimeFrame.StartTime;
                if (newTimeFrame.EndTime >= existingTimeFrame.StartTime)
                {
                    resultantTimeFrame_1.EndTime = existingTimeFrame.StartTime.AddMilliseconds(-1);
                    if (newTimeFrame.EndTime > existingTimeFrame.EndTime)
                    {
                        resultantTimeFrame_2.StartTime = existingTimeFrame.EndTime.AddMilliseconds(1);
                        resultantTimeFrame_2.EndTime = newTimeFrame.EndTime;
                    }
                }
                else
                {
                    resultantTimeFrame_1.EndTime = newTimeFrame.EndTime;
                }
            }
            else
            {
                if (newTimeFrame.StartTime > existingTimeFrame.EndTime)
                {
                    resultantTimeFrame_1.StartTime = newTimeFrame.StartTime;
                }
                else
                {
                    resultantTimeFrame_1.StartTime = existingTimeFrame.EndTime.AddMilliseconds(1);
                }
                if (resultantTimeFrame_1.StartTime < newTimeFrame.EndTime)
                    resultantTimeFrame_1.EndTime = newTimeFrame.EndTime;
                else
                {
                    resultantTimeFrame_1.StartTime = Utils.defaultDate;
                    resultantTimeFrame_1.EndTime = Utils.defaultDate;
                }
            }
            Log.Debug($"ResultantTimeFrame: resultantTimeFrame_1, Start Time - {resultantTimeFrame_1.StartTime}, EndTime - {resultantTimeFrame_1.EndTime}");
            Log.Debug($"ResultantTimeFrame: resultantTimeFrame_2, Start Time - {resultantTimeFrame_2.StartTime}, EndTime - {resultantTimeFrame_2.EndTime}");
        }

        public static void AggregateTimeFrame(TimeFrame existingTimeFrame, TimeFrame newTimeFrame, TimeFrame resultantTimeFrame)
        {
           // Log.Debug($"AggregateTimeFrame: existingTimeFrame, Start Time - {existingTimeFrame.StartTime}, EndTime - {existingTimeFrame.EndTime}");
            //Log.Debug($"AggregateTimeFrame: newTimeFrame, Start Time - {newTimeFrame.StartTime}, EndTime - {newTimeFrame.EndTime}");

            if (newTimeFrame.StartTime < existingTimeFrame.StartTime)
            {
                resultantTimeFrame.StartTime = newTimeFrame.StartTime;
            }
            else
            {
                resultantTimeFrame.StartTime = existingTimeFrame.StartTime;
            }

            if (newTimeFrame.EndTime > existingTimeFrame.EndTime)
            {
                resultantTimeFrame.EndTime = newTimeFrame.EndTime;
            }
            else
            {
                resultantTimeFrame.EndTime = existingTimeFrame.EndTime;
            }
            Log.Debug($"AggregateTimeFrame: Start Time - {resultantTimeFrame.StartTime}, EndTime - {resultantTimeFrame.EndTime}");

        }


        public static string HandleGetReponse(HttpWebRequest request)
        {
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.Timeout)
                {
                    throw new FalkonryException("Request Time Out");
                }
                else if (e.Status == WebExceptionStatus.NameResolutionFailure || e.Status == WebExceptionStatus.ProxyNameResolutionFailure)
                {
                    throw new FalkonryException("Host unreachable");
                }
                response = (HttpWebResponse)e.Response;
            }
            try
            {
                var stream = response.GetResponseStream();
                if (stream != null)
                {
                    var resp = new StreamReader(stream).ReadToEnd();

                    if (Convert.ToInt32(response.StatusCode) == 401)
                    {
                        throw new FalkonryException("Unauthorized : Invalid token " + Convert.ToString(response.StatusCode));
                    }
                    else if (Convert.ToInt32(response.StatusCode) < 400 || Convert.ToInt32(response.StatusCode) == 409)
                    {
                        return resp;
                    }
                    else
                    {
                        try
                        {
                            ErrorMessage errMsgJson = new ErrorMessage();
                            errMsgJson = JsonConvert.DeserializeObject<ErrorMessage>(resp);
                            if (errMsgJson.Message.Length > 0)
                            {
                                throw new FalkonryException(Convert.ToString(errMsgJson.Message));
                            }
                            else
                            {
                                throw new FalkonryException("Internal Server Error.");
                            }
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    throw new FalkonryException("Internal Server Error.");
                }
            }
            catch (Exception exception1)
            {
                throw;
            }
        }



        public static bool CheckAgentUpdate(string installedVersionStr)
        {
            try
            {
                //DialogResult dialogResult = MessageBox.Show($"A new version of PI Agent is available, do you want to upgrade the PI Agent?", "Update Available", MessageBoxButtons.YesNo);
                Log.Debug("Inside CheckAgentUpdate");
                var url = Properties.Default.agentCloudURL;
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "application/json";
                GoogleCloudBucketObject msiObject = null;
                try
                {
                    var response = HandleGetReponse(request);
                    msiObject = JsonConvert.DeserializeObject<GoogleCloudBucketObject>(response);
                    Log.Debug($"CheckAgentUpdate: Google cloud MSI Object - {response}");
                }
                catch (Exception e)
                {
                    Log.Debug($"CheckAgentUpdate: Error in getting google cloud msi object - {e.Message}, Stack Trace - {e.StackTrace}");
                    return false;
                }

                string appFileName = Environment.GetCommandLineArgs()[0];
                string msiDirPath = Path.GetDirectoryName(appFileName);
                msiDirPath += "\\MSI";
                string msiPath = msiDirPath + '\\' + Properties.Default.falkonryAgentMSI;
                Dictionary<string, byte[]> fileList = new Dictionary<string, byte[]>();
                DirectoryInfo msiDir = Directory.CreateDirectory(msiDirPath);

                WebClient Client = new WebClient();
                Client.DownloadFile(msiObject.mediaLink, msiPath);
                Log.Debug($"CheckAgentUpdate: File downloaded successfully at msiPath - {msiPath}");

                if (!File.Exists(msiPath))
                {
                    Log.Debug($"File Does not exist");
                    return false;
                }

                string msiVersionStr = PropertyTable.Get(msiPath, "ProductVersion");
                var falkonryVersion = new Version(msiVersionStr);
                var installedVersion = new Version(installedVersionStr);
                Log.Debug($"CheckAgentUpdate: installedVersion - {installedVersionStr} ,falkonryVersion - {msiVersionStr} ");
                var result = falkonryVersion.CompareTo(installedVersion);
                if (result > 0)
                {
                    Log.Debug("CheckAgentUpdate: Cloud Version is greater");
                    return true;
                }
                else if (result < 0)
                {
                    Log.Debug("CheckAgentUpdate: installed Version is greater");
                    return false;
                }
                else
                {
                    Log.Debug("CheckAgentUpdate: installed Version is equal to Cloud Version");
                    return false;
                }
            }
            catch (Exception e)
            {
                Log.Warn($"CheckAgentUpdate: Exception caught - {e.Message}, StackTrace - {e.StackTrace}");
                return false;
            }
        }

        public static void InstallLatestAgent()
        {
            try
            {
                Log.Debug("Inside InstallLatestAgent");
                string appFileName = Environment.GetCommandLineArgs()[0];
                string msiPath = Path.GetDirectoryName(appFileName);
                msiPath += "\\MSI";
                Process.Start(msiPath + "\\" + Properties.Default.falkonryAgentMSI);
                Log.Debug("End of InstallLatestAgent");
                return;
            }
            catch (Exception exception)
            {
                Log.Warn($"Exception caught in InstallLatestAgent -  {exception.Message}");
                Log.Warn($"Exception caught Stack Trace -  {exception.StackTrace}");
            }
        }


        public static void DeleteAgentMsi()
        {
            try
            {
                Log.Debug("Inside DeleteAgentMsi");

                string appFileName = Environment.GetCommandLineArgs()[0];
                string msiPath = Path.GetDirectoryName(appFileName);
                msiPath += "\\MSI\\";
                msiPath += Properties.Default.falkonryAgentMSI;

                if (File.Exists(msiPath))
                {
                    File.Delete(msiPath);
                }
                Log.Debug("End of DeleteAgentMsi");
                return;
            }
            catch (Exception exception)
            {
                Log.Warn($"Exception caught in DeleteAgentMsi -  {exception.Message}");
                Log.Warn($"Exception caught Stack Trace -  {exception.StackTrace}");
            }
        }

        public static bool IsUserAdministrator()
        {
            bool isAdmin;
            try
            {
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
            }
            catch (Exception ex)
            {
                isAdmin = false;
            }
            return isAdmin;
        }

        public static bool ValidateAuthToken(string host, string token, SortedDictionary<string, string> _piOptions = null)
        {
            try
            {
                var url = host + "/";
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ServicePoint.Expect100Continue = false;
                request.Credentials = CredentialCache.DefaultCredentials;
                request.Headers.Add("Authorization", "Bearer " + token);
                request.Headers.Add("x-falkonry-source", _piOptions["header"]);
                request.Method = "GET";
                request.ContentType = "application/json";

                HandleGetReponse(request);
                return true;
            }
            catch (Exception exception)
            {
                if (exception.Message == "Unauthorized : Invalid token Unauthorized" || exception.Message == "Host unreachable" || exception.Message == "Invalid URI: The hostname could not be parsed.")
                {
                    return false;
                }
                throw;
            }
        }
    }
}