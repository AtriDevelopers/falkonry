﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FalkonryClient.Helper.Models;
using OSIsoft.AF.Asset;
using log4net;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using OSIsoft.AF.PI;


namespace Falkonry.Integrator.PISystem.Helper
{
    public class FalkonryHelper
    {
        private static SortedDictionary<string, string> _piOptions = new SortedDictionary<string, string>();
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);


        public static bool AuthenticateConnectionDetails(string host, string token)
        {
            try
            {
                _piOptions["header"] = "piagent-v" + Application.ProductVersion;
                return Utils.ValidateAuthToken(host, token, _piOptions);
            }
            catch (Exception exception)
            {
                Log.Error($"Error in Authentication Host: {host} Token:{token} AuthenticateConnectionDetails: Error - {exception.Message}  StackTrace: {exception.StackTrace}");
                return false;
            }
        }

        public static Datastream CreateDatastream(PiConfigurationItem serviceConfiguration)
        {
            Log.Debug($"CreateDatastream: Configuration Id: {serviceConfiguration.Id}. datastreamrequest name {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}");

            FalkonryConfiguration falkonryConfiguration = serviceConfiguration.Configuration.FalkonryConfiguration;
            AfConfiguration afConfiguration = serviceConfiguration.Configuration.AfConfiguration;

            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);

            var datasource = new Datasource
            {
                Type = "PI",
                Host = afConfiguration.SystemName,
                ElementTemplateName = afConfiguration.TemplateName
            };

            var signal = new Signal
            {
                SignalIdentifier = "signal",
                ValueIdentifier = "value",
            };


            string dsTimeZone = Utils.WindowsToIana(serviceConfiguration.Configuration.AfConfiguration.AFServerTimeZoneId);
            Log.Debug($"CreateDatastream: TimezoneId - {serviceConfiguration.Configuration.AfConfiguration.AFServerTimeZoneId}, TimeZone - {dsTimeZone}");
            var time = new Time
            {
                Zone = dsTimeZone,
                Identifier = "time",
                Format = falkonryConfiguration.TimeFormat
            };

            var Field = new Field
            {
                Time = time,
                Signal = signal,
                EntityIdentifier = "entity",
            };
            if (!serviceConfiguration.BatchIdentifier.Equals(""))
            {
                Field.BatchIdentifier = serviceConfiguration.BatchIdentifier;
            }
            var datastreamRequest = new DatastreamRequest()
            {
                Name = falkonryConfiguration.Datastream,
                Field = Field,
                DataSource = datasource,
                InputList = new List<Input>()
            };
            // datastreamRequest
            AFAttributeList attributeList = AfHelper.FindAttributes(serviceConfiguration.Configuration.AfConfiguration);
            foreach (AFAttribute attribute in attributeList)
            {
                List<string> annotationList = new List<string>();
                string pointType = "";
                try
                {
                    if (attribute.PIPoint != null)
                    {
                        var attributeProperties = attribute.PIPoint.GetAttributes();
                        foreach (var attributeProperty in attributeProperties)
                        {
                            if (attributeProperty.Key == PICommonPointAttributes.Compressing)
                            {
                                if (attributeProperty.Value.ToString().Equals("1"))
                                    annotationList.Add("COMPRESSED");
                            }
                        }
                        pointType = attribute.PIPoint.PointType.ToString();
                    }
                }
                catch (Exception exception)
                {
                    Log.Debug($"CreateDatastream:' PI Point Not available for Attribute : {attribute.Name} of Element : {attribute.Element.Name} ");
                }
                string value = "";
                if (attribute.Type.Name.ToLower().Equals("string") || pointType.ToLower().Equals("string"))
                {
                    value = "Categorical";
                }
                else
                {
                    value = "Numeric";
                }

                var inputType = new InputType
                {
                    Type = "Raw",
                    InputConf = null
                };

                var EventType = new EventType
                {
                    Type = "Samples"
                };

                var ValueType = new FalkonryClient.Helper.Models.ValueType
                {
                    Type = value
                };

                var Input = new Input    /// signals name from api input signals
                {
                    Name = attribute.Name,
                    Annotations = annotationList,
                    ValueType = ValueType,
                    EventType = EventType,
                    InputType = inputType,
                    Query = null
                };
                    datastreamRequest.InputList.Add(Input);
            }

            try
            {
                Datastream createdDatastream = falkonry.CreateDatastream(datastreamRequest);
                Log.Info($" Successfully created datastream {createdDatastream.Name}. Id : {createdDatastream.Id} Datastream Name : {createdDatastream.Name}");
                AfHelper.UpdateState(afConfiguration.SystemName, falkonryConfiguration.Datastream, "INFO", "Datastream Successfully Created");
                serviceConfiguration.Configuration.FalkonryConfiguration.SourceId = createdDatastream.Id;
                //AfHelper.SaveSingleConfig(serviceConfiguration);
                return createdDatastream;
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in create datastream {datastreamRequest.Name}. Received error from Falkonry Server. Error : {exception.Message}");
                AfHelper.UpdateState(afConfiguration.SystemName, falkonryConfiguration.Datastream, "ERROR", $"Error in creating datastream. error : {exception.Message}");
                throw;
            }
        }

        internal static bool syncWithFalkonry(PiConfigurationItem config)
        {
            try
            {
                //Log.Debug($"syncWithFalkonry: Configuration Id: {config.Id}. Datastream: {config.Configuration.FalkonryConfiguration.Datastream}. Id: {config.Configuration.FalkonryConfiguration.SourceId}");

                if (config.Configuration.FalkonryConfiguration.SourceId != "new_datastream_saved")
                {
                    Datastream currentDatastream = FalkonryHelper.GetDatastream(config.Configuration.FalkonryConfiguration);
                    bool isDsChanged = false;
                    string dsOldName = "";
                    //bool previousState = config.Configuration.StreamingDataAccess.Enabled;
                    if (currentDatastream.Live.ToLower() != "on" && config.Configuration.StreamingDataAccess.Enabled)
                    {
                        config.Configuration.StreamingDataAccess.Enabled = false;
                        isDsChanged = true;
                        Log.Info("syncWithFalkonry: Streaming turned off.");
                    }
                    else if (currentDatastream.Live.ToLower() == "on" && !config.Configuration.StreamingDataAccess.Enabled)
                    {
                        config.Configuration.StreamingDataAccess.Enabled = true;
                        isDsChanged = true;
                        Log.Info("syncWithFalkonry: Streaming turned on.");
                    }
                    if (currentDatastream.Name.ToLower() != config.Configuration.FalkonryConfiguration.Datastream.ToLower())
                    {
                        dsOldName = config.Configuration.FalkonryConfiguration.Datastream;
                        config.Configuration.FalkonryConfiguration.Datastream = currentDatastream.Name;
                        config.Name = currentDatastream.Name;
                        isDsChanged = true;
                        Log.Info($"syncWithFalkonry: Datastream name changed from {dsOldName} to {currentDatastream.Name}");
                    }
                    List<Assessment> assessments = GetAssessments(config.Configuration.FalkonryConfiguration);
                    List<string> AssessmentIdList = new List<string>(assessments.Count);
                    foreach (Assessment assessmentObj in assessments)
                    {
                        if (assessmentObj.Datastream.Equals(config.Configuration.FalkonryConfiguration.SourceId))
                        {
                            AssessmentIdList.Add(assessmentObj.Id);
                            //assessments.Add(assessmentObj);
                        }
                    }

                    List<string> assessmentListConfig = new List<string>(config.Configuration.FalkonryConfiguration.AssessmentId);
                    List<string> outputListConfig = new List<string>(config.Configuration.AfConfiguration.OutputTemplateAttribute);
                    List<string> eventframeListConfig = new List<string>(config.Configuration.AfConfiguration.EventFrameTemplateNames);
                    int index = 0;
                    
                    foreach (string AssessmentId in config.Configuration.FalkonryConfiguration.AssessmentId)
                    {
                        if (AssessmentId != "" && !AssessmentIdList.Contains(AssessmentId))
                        {
                            isDsChanged = true;
                            AfHelper.UpdateState(config.Configuration.AfConfiguration.SystemName, config.Name, "INFO", $"Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                            index = assessmentListConfig.IndexOf(AssessmentId);
                            assessmentListConfig.RemoveAt(index);
                            outputListConfig.RemoveAt(index);
                            eventframeListConfig.RemoveAt(index);
                            config.Enabled = true;
                            Log.Info($"syncWithFalkonry: Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                        }
                    }
                    config.Configuration.FalkonryConfiguration.AssessmentId = assessmentListConfig.ToArray();
                    config.Configuration.AfConfiguration.OutputTemplateAttribute = outputListConfig.ToArray();
                    config.Configuration.AfConfiguration.EventFrameTemplateNames = eventframeListConfig.ToArray();
                    config.Configuration.FalkonryConfiguration.AssessmentIdForFacts = assessmentListConfig.ToArray();

                    if (isDsChanged)
                    {
                        config.Enabled = true;
                        AfHelper.SaveSingleConfig(config, dsOldName);
                    }
                    //Log.Debug("End of syncWithFalkonry");
                    return true;
                }
                else
                {
                    //Log.Debug("End of syncWithFalkonry");
                    return true;
                }

            }
            catch (Exception e)
            {
                if (e.Message == "No such Datastream available")
                {
                    Log.Error($"syncWithFalkonry - {e.Message}");
                    return false;
                }
                else if (config.Deleted && e.Message == "Host unreachable")
                {
                    Log.Error($"syncWithFalkonry - {e.Message}");
                    return false;
                }
                else
                {
                    Log.Error($"Exception caught in syncWithFalkonry - {e.Message}");
                    Log.Error($"Exception - Stack Trace {e.StackTrace}");
                    return true;
                }

            }
        }

        public static Datastream GetDatastream(FalkonryConfiguration falkonryConfiguration)
        {
            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                Log.Debug($"Get Datastream from falkonry : 'Host {falkonryConfiguration.Host}, Token {falkonryConfiguration.Token}'");
                return falkonry.GetDatastream(falkonryConfiguration.SourceId);
            }
            catch (Exception exception)
            {
                Log.Error($"Could not fetch datastream {falkonryConfiguration.SourceId}. Received error from Falkonry Server. Error : {exception.Message}");
                if(exception.Message != "No such Datastream available")
                {
                    Log.Error($"  Exception Stack Trace : {exception.StackTrace}");
                }
                throw;
            }
        }

        public static List<Datastream> GetDatastreams(FalkonryConfiguration falkonryConfiguration)
        {
             var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                return falkonry.GetDatastreams();
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in GetDatastreams . Received error from Falkonry Server. Error : {exception.Message}");
                if(!exception.Message.Contains("Unauthorized"))
                {
                    Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                }
                throw;
            }
        }

        public static List<Datastream> GetDatastreams(string host,string token)
        {
            var falkonry = new FalkonryClient.Falkonry(host, token, _piOptions);
            try
            {
                return falkonry.GetDatastreams();
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in GetDatastreams . Received error from Falkonry Server. Error : {exception.Message}");
                if (!exception.Message.Contains("Unauthorized"))
                {
                    Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                }
                throw;
            }
        }
        public static void AddEntityInformation(FalkonryConfiguration falkonryConfiguration, AFAttributeList attributes = null)
        {
            try
            {
                Log.Debug($"AddEntityInformation: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");

                List<EntityMetaRequest> entitiesMetaData = new List<EntityMetaRequest>();
                var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);

                var elementNames = attributes.Select(a => a.Element).Select(p => p.Name).Distinct();

                foreach (var attribute in attributes)
                {
                    EntityMetaRequest entityMetaRequest = new EntityMetaRequest()
                    {
                        Label = elementNames.Count() == 1 ? attribute.Element.GetPath(attribute.Database) : attribute.Element.Name,
                        SourceId = attribute.Element.ID.ToString(),
                        Path = attribute.Element.GetPath()
                    };
                    if (!entitiesMetaData.Contains(entityMetaRequest))
                        entitiesMetaData.Add(entityMetaRequest);
                }
                try
                {
                    var datastream = falkonry.GetDatastreams().First(d => d.Name == falkonryConfiguration.Datastream);
                    var result = falkonry.PostEntityMeta(entitiesMetaData, datastream);
                }
                catch (Exception exception)
                {
                    Log.Error($"  Could not add entity info to datastream {falkonryConfiguration.SourceId}. Received error from Falkonry Server. Error : {exception.Message}");
                    throw;
                }
                Log.Debug(" End of AddEntityInformation ");
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in AddEntityInformation - Message {exception.StackTrace}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static void DeleteDatastream(FalkonryConfiguration falkonryConfiguration)
        {
            try
            {
                Log.Debug($"DeleteDatastream: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
                var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
                Log.Debug($"Deleting datastream:' Datastream Id :{falkonryConfiguration.SourceId} Datastream Name : {falkonryConfiguration.Datastream}'");
                falkonry.DeleteDatastream(falkonryConfiguration.SourceId);
                Log.Debug($"Datastream deleted successfully :' Datastream Id :{falkonryConfiguration.SourceId} Datastream Name : {falkonryConfiguration.Datastream}'");
            }
            catch (Exception exception)
            {
                Log.Error($"Could not delete datastream {falkonryConfiguration.SourceId}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static void offDatastream(FalkonryConfiguration falkonryConfiguration)
        {
            try
            {
                Log.Debug($"offDatastream: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
                var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
                falkonry.offDatastream(falkonryConfiguration.SourceId);
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in offDatastream, Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
                Log.Error($"Exception - Message {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static void onDatastream(FalkonryConfiguration falkonryConfiguration)
        {
            try
            {
                Log.Debug($"onDatastream: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
                var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
                falkonry.onDatastream(falkonryConfiguration.SourceId);
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in onDatastream, Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
                Log.Error($"Exception -Message {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static List<Assessment> GetAssessments(FalkonryConfiguration falkonryConfiguration)
        {
            // Log.Debug($"GetAssessments: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                return falkonry.GetAssessments();
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in GetAssessments for datastream: {falkonryConfiguration.SourceId}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }
        public static List<Assessment> GetAssessments(string host,string token)
        {
            // Log.Debug($"GetAssessments: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
            var falkonry = new FalkonryClient.Falkonry(host, token, _piOptions);
            try
            {
                return falkonry.GetAssessments();
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in GetAssessments for host: {host}  token: {token}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static Assessment GetAssessment(FalkonryConfiguration falkonryConfiguration, string assessmentId)
        {
            // Log.Debug($"GetAssessments: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");
            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                Assessment assessment = falkonry.GetAssessment(assessmentId);
                return assessment;
            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in GetAssessments for datastream: {falkonryConfiguration.SourceId}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static Assessment CreateAssessment(FalkonryConfiguration falkonryConfiguration, AssessmentRequest newAssessment)
        {
            Log.Debug($"CreateAssessment: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}. AssessmentName: {newAssessment.Name}");

            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                Log.Debug($"CreateAssessment called - Datastream - {newAssessment.Datastream} new Assessment Name - {newAssessment.Name}");
                return falkonry.CreateAssessment(newAssessment);
            }
            catch (Exception exception)
            {
                Log.Error($"  Could not create assessment {falkonryConfiguration.SourceId}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static HttpResponse GetHistoricalOutput(FalkonryConfiguration falkonryConfiguration, Assessment _assessment, SortedDictionary<string, string> _options)
        {
           // Log.Debug($"GetHistoricalOutput: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}. AssessmentName: {_assessment.Name}. Assessment Id: {_assessment.Id}");
            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                return falkonry.GetHistoricalOutput(_assessment, _options);
            }
            catch (Exception exception)
            {
                Log.Error($"  Could not get historical output {falkonryConfiguration.SourceId} assessment {_assessment.Id}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static EventSource GetOutput(FalkonryConfiguration falkonryConfiguration, string assessmentId)
        {
            Log.Debug($"GetOutput: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}. Assessment Id: {assessmentId}");
            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                return falkonry.GetOutput(assessmentId, null, null);
            }
            catch (Exception exception)
            {
                Log.Error($"Could not get output datastream {falkonryConfiguration.SourceId} assessment {assessmentId}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }



        public static InputStatus AddInput(FalkonryConfiguration falkonryConfiguration, string datastreamId, string jsonData, SortedDictionary<string, string> _options)
        {
            Log.Debug($"AddInput: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}.");

            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                return falkonry.AddInput(datastreamId, jsonData, _options);
            }
            catch (Exception exception)
            {
                Log.Error($"  Could not AddInput datastream {falkonryConfiguration.SourceId}  Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }

        public static InputStatus AddFacts(FalkonryConfiguration falkonryConfiguration, string assessmentId, string jsonData, SortedDictionary<string, string> options = null)
        {
            Log.Debug($"AddFacts: Datastream: {falkonryConfiguration.Datastream}. Id: {falkonryConfiguration.SourceId}. AssessmentId: {assessmentId}");

            var falkonry = new FalkonryClient.Falkonry(falkonryConfiguration.Host, falkonryConfiguration.Token, _piOptions);
            try
            {
                return falkonry.AddFacts(assessmentId, jsonData, options);
            }
            catch (Exception exception)
            {
                Log.Error($"  Could not AddFacts datastream {falkonryConfiguration.SourceId} assessment {assessmentId}. Received error from Falkonry Server. Error : {exception.Message}");
                Log.Error($"Exception - Stack Trace {exception.StackTrace}");
                throw;
            }
        }
    }
}