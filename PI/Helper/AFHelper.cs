﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.EventFrame;
using OSIsoft.AF.Search;
using log4net;
using FalkonryClient.Helper.Models;
using Newtonsoft.Json;
using System.Reflection;
using System.IO;

namespace Falkonry.Integrator.PISystem.Helper
{
    public class AfHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private static string Delimiter = "#@#";


        public static AFAttribute FindElementAttribute(Guid elementId, AfConfiguration afConfiguration, int index)
        {
            try
            {
                var af = new PISystems()[afConfiguration.SystemName]; af.Connect();
                var database = af.Databases[afConfiguration.DatabaseName];
                database.Refresh();
                return AFElement.FindElement(af, elementId).Attributes[afConfiguration.OutputTemplateAttribute.GetValue(index).ToString()];

            }
            catch (Exception)
            {
                Log.Error($" Error in finding element attribute for AF Template : { afConfiguration.TemplateName}");
                return null;
            }
        }
        public static AFAttributeList FindElementAttributes(AfConfiguration afConfiguration)
        {
            try
            {
                var af = new PISystems()[afConfiguration.SystemName]; af.Connect();
                var database = af.Databases[afConfiguration.DatabaseName];
                database.Refresh();
                var template = database.ElementTemplates[afConfiguration.TemplateName];

                var foundAttributes = AFAttribute.FindElementAttributes(
                    database,
                    null,
                    "*",
                    null,
                    template,
                    AFElementType.Any,
                    "*",
                    null,
                    TypeCode.Empty,
                    true,
                    AFSortField.Name,
                    AFSortOrder.Ascending,
                    100000);

                if (foundAttributes == null) return new AFAttributeList();
                if (foundAttributes.Count == 0) return foundAttributes;

                var justRequiredAttributes = new AFAttributeList(foundAttributes
                    .Where(a => afConfiguration.TemplateAttributes.Contains(a.Name))
                    .Where(a => afConfiguration.TemplateElementPaths.Contains(a.Element.GetPath(database))));

                return justRequiredAttributes;
            }
            catch (Exception exception)
            {
                Log.Error($" Error in fetching element attribute list for AF Template : { afConfiguration.TemplateName} Error : {exception.Message}");
                return new AFAttributeList();
            }
        }

        public static AFAttributeList FindAttributes(AfConfiguration afConfiguration)
        {
            try
            {
                var af = new PISystems()[afConfiguration.SystemName]; af.Connect();
                var database = af.Databases[afConfiguration.DatabaseName];
                database.Refresh();
                var template = database.ElementTemplates[afConfiguration.TemplateName];

                var foundAttributes = AFAttribute.FindElementAttributes(
                    database,
                    null,
                    "*",
                    null,
                    template,
                    AFElementType.Any,
                    "*",
                    null,
                    TypeCode.Empty,
                    true,
                    AFSortField.Name,
                    AFSortOrder.Ascending,
                    100000);

                if (foundAttributes == null) return new AFAttributeList();
                if (foundAttributes.Count == 0) return foundAttributes;

                Hashtable foundAttributesNameMap = new Hashtable();

                foreach (var aFAttribute in foundAttributes)
                {
                    if (!foundAttributesNameMap.Contains(aFAttribute.Name) && afConfiguration.TemplateElementPaths.Contains(aFAttribute.Element.GetPath(database)))
                    //if (!foundAttributesNameMap.Contains(aFAttribute.Name))
                    {
                        foundAttributesNameMap.Add(aFAttribute.Name, aFAttribute);
                    }

                }
                var foundAttributesDistinct = new AFAttributeList();
                foreach (DictionaryEntry assessmentHashPair in foundAttributesNameMap)
                {
                    foundAttributesDistinct.Add((AFAttribute)(assessmentHashPair.Value));
                }

                AFAttributeList justRequiredAttributes;
                if (template != null)
                {
                    justRequiredAttributes = new AFAttributeList(foundAttributesDistinct
                    .Where(a => afConfiguration.TemplateAttributes.Contains(a.Name))
                    .Where(a => afConfiguration.TemplateElementPaths.Contains(a.Element.GetPath(database))));
                }
                else
                {
                    justRequiredAttributes = new AFAttributeList(foundAttributesDistinct
                    .Where(a => afConfiguration.TemplateAttributes.Contains(a.Name)));
                }


                return justRequiredAttributes;
            }
            catch (Exception exception)
            {
                Log.Error($" Error in fetching element attribute list for AF Template : { afConfiguration.TemplateName} Error : {exception.Message}");
                return new AFAttributeList();
            }
        }

        public static AFAttributeList FindAllAttributes(AfConfiguration afConfiguration)
        {
            try
            {
                var af = new PISystems()[afConfiguration.SystemName]; af.Connect();
                var database = af.Databases[afConfiguration.DatabaseName];
                database.Refresh();
                var template = database.ElementTemplates[afConfiguration.TemplateName];

                var foundAttributes = AFAttribute.FindElementAttributes(
                    database,
                    null,
                    "*",
                    null,
                    template,
                    AFElementType.Any,
                    "*",
                    null,
                    TypeCode.Empty,
                    true,
                    AFSortField.Name,
                    AFSortOrder.Ascending,
                    100000);

                if (foundAttributes == null) return new AFAttributeList();
                if (foundAttributes.Count == 0) return foundAttributes;

                return foundAttributes;
            }
            catch (Exception exception)
            {
                Log.Error($" Error in fetching element attribute list for AF Template : { afConfiguration.TemplateName} Error : {exception.Message}");
                return new AFAttributeList();
            }
        }

        public static List<AFEventFrame> FindEventFrames1(AfConfiguration afConfiguration, string[] eventFramneTemplateNames, DateTime startTime, DateTime endTime)
        {
            try
            {
                var af = new PISystems()[afConfiguration.SystemName]; af.Connect();
                var database = af.Databases[afConfiguration.DatabaseName];

                var validEventFrames = new List<AFEventFrame>();

                foreach (var templateName in eventFramneTemplateNames)
                {
                    var search = new AFEventFrameSearch(database, $"FindEventsOfTemplate_{templateName}", $@"Template:'{templateName}' Start:>='{startTime:O}' End:<='{endTime:O}' ")
                    {
                        CacheTimeout = TimeSpan.FromMinutes(10)
                    };

                    validEventFrames.AddRange(search.FindEventFrames(fullLoad: true)
                        .Where(i => i.PrimaryReferencedElement != null)
                        .Where(item => afConfiguration.TemplateElementPaths.Contains(item.PrimaryReferencedElement.GetPath(database))));
                }

                return validEventFrames;
            }
            catch (Exception)
            {
                Log.Error($" Error in fetching event frames list for AF Database : { afConfiguration.DatabaseName}");
                return new List<AFEventFrame>();
            }
        }

        public static List<AFEventFrame> FindEventFrames(AfConfiguration afConfiguration, string[] eventFramneTemplateNames, string[] eventFrameDateTime, TimeFrame selectTimeFrame, bool overrideHistorianData)
        {
            try
            {
                var validEventFrames = new List<AFEventFrame>();
                Log.Debug($"Inside FindEventFrames");
                for (int eventIndex = 0; eventIndex < eventFramneTemplateNames.Count(); eventIndex++)
                {
                    DateTime startTime, endTime;
                    if (eventFrameDateTime[eventIndex] == "" || overrideHistorianData)
                    {
                        startTime = selectTimeFrame.StartTime;
                        endTime = selectTimeFrame.EndTime;
                    }
                    else
                    {
                        string[] timeFrame = eventFrameDateTime[eventIndex].Split('-');
                        if (timeFrame.Count() != 2)
                        {
                            startTime = selectTimeFrame.StartTime;
                            endTime = selectTimeFrame.EndTime;
                        }
                        else
                        {
                            startTime = (new DateTime(1970, 1, 1)).AddMilliseconds(Convert.ToInt64(timeFrame[0])).ToLocalTime();
                            endTime = (new DateTime(1970, 1, 1)).AddMilliseconds(Convert.ToInt64(timeFrame[1])).ToLocalTime();
                            TimeFrame
                                newTimeFrame = new TimeFrame(),
                                existingTimeFrame = new TimeFrame(),
                                resultantTimeFrame_1 = new TimeFrame(),
                                resultantTimeFrame_2 = new TimeFrame();

                            newTimeFrame.StartTime = selectTimeFrame.StartTime;
                            newTimeFrame.EndTime = selectTimeFrame.EndTime;

                            existingTimeFrame.StartTime = startTime;
                            existingTimeFrame.EndTime = endTime;

                            resultantTimeFrame_1.StartTime = Utils.defaultDate;
                            resultantTimeFrame_1.EndTime = Utils.defaultDate;

                            resultantTimeFrame_2.StartTime = Utils.defaultDate;
                            resultantTimeFrame_2.EndTime = Utils.defaultDate;


                            Utils.ResultantTimeFrame(existingTimeFrame, newTimeFrame, resultantTimeFrame_1, resultantTimeFrame_2);

                            startTime = resultantTimeFrame_1.StartTime;
                            endTime = resultantTimeFrame_1.EndTime;

                            if (resultantTimeFrame_2.StartTime != Utils.defaultDate && resultantTimeFrame_2.EndTime != Utils.defaultDate)
                            {
                                validEventFrames.AddRange(GetEventFrames(afConfiguration, eventFramneTemplateNames[eventIndex], resultantTimeFrame_2.StartTime, resultantTimeFrame_2.EndTime));
                            }
                        }
                    }
                    if (startTime != Utils.defaultDate && endTime != Utils.defaultDate)
                        validEventFrames.AddRange(GetEventFrames(afConfiguration, eventFramneTemplateNames[eventIndex], startTime, endTime));
                }
                Log.Debug("End of FindEventFrames");
                return validEventFrames;
            }
            catch (Exception exception)
            {
                Log.Error($" Error in fetching event frames list for AF Database : { afConfiguration.DatabaseName}, - {exception.Message}");
                Log.Error($" Error in fetching event frames list Stack Trace - {exception.StackTrace}");
                return new List<AFEventFrame>();
            }
        }

        public static List<AFEventFrame> GetEventFrames(AfConfiguration afConfiguration, string eventFrameName, DateTime startTime, DateTime endTime)
        {
            try
            {
                if (afConfiguration.AFServerTimeZoneId != TimeZoneInfo.Local.Id)
                {
                    Log.Debug($"GetEventFrames: Local time zone - {TimeZoneInfo.Local.Id} is different from AFservertTimeZone - {afConfiguration.AFServerTimeZoneId}");
                    Log.Debug($"GetEventFrames: Machine Time zone startTime - {startTime}");
                    Log.Debug($"GetEventFrames: Machine Time zone endTime - {endTime}");
                    var afServerzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(afConfiguration.AFServerTimeZoneId);
                    startTime = TimeZoneInfo.ConvertTime(startTime, afServerzTimeZone);
                    endTime = TimeZoneInfo.ConvertTime(endTime, afServerzTimeZone);
                    Log.Debug($"GetEventFrames: AFServer Time zone startTime - {startTime}");
                    Log.Debug($"GetEventFrames: AFServer Time zone endTime - {endTime}");
                }


                var af = new PISystems()[afConfiguration.SystemName]; af.Connect();
                var database = af.Databases[afConfiguration.DatabaseName];

                var validEventFrames = new List<AFEventFrame>();

                var search = new AFEventFrameSearch(database, $"FindEventsOfTemplate_{eventFrameName}", $@"Template:'{eventFrameName}' 
                                        Start:>='{startTime:O}' End:<='{endTime:O}' ")
                {
                    CacheTimeout = TimeSpan.FromMinutes(10)
                };

                validEventFrames.AddRange(search.FindEventFrames(fullLoad: true)
                    .Where(i => i.PrimaryReferencedElement != null)
                    .Where(item => afConfiguration.TemplateElementPaths.Contains(item.PrimaryReferencedElement.GetPath(database))));
                Log.Debug($"GetEventFrames: eventFrameName - {eventFrameName}, startTime-{startTime}, endTime-{endTime}, Event Count - {validEventFrames.Count} ");
                return validEventFrames;
            }
            catch (Exception)
            {
                Log.Error($" Error in fetching event frames list for AF Database : { afConfiguration.DatabaseName}");
                return new List<AFEventFrame>();
            }
        }

        public static void UpdateEnabledFlag(string piSystemName, string configurationName, bool enabledFlag)
        {
            try
            {
                var af = new PISystems()[piSystemName]; af.Connect();
                var database = af.Databases["Configuration"]; if (database == null) return;

                var rootElement = AFObject.FindObject(FalkonryConfigurationRootElement(), database) as AFElement;
                if (rootElement == null) return;
                if (!rootElement.HasChildren) return;

                var configurationsElement = rootElement.Elements["Configurations"];
                if (configurationsElement == null) return;
                if (!configurationsElement.HasChildren) return;

                var configElement = configurationsElement.Elements[configurationName];
                if (configElement == null) return;

                configElement.Attributes["Enabled"].SetValue(new AFValue(enabledFlag));
                database.CheckIn();
                database.Refresh();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void UpdateState(string piSystemName, string configurationName, string status, string errorMessage)
        {
            try
            {
                var af = new PISystems()[piSystemName]; af.Connect();
                var database = af.Databases["Configuration"]; if (database == null) return;

                var rootElement = AFObject.FindObject(FalkonryConfigurationRootElement(), database) as AFElement;
                if (rootElement == null) return;
                if (!rootElement.HasChildren) return;

                var configurationsElement = rootElement.Elements["Configurations"];
                if (configurationsElement == null) return;
                if (!configurationsElement.HasChildren) return;

                var configElement = configurationsElement.Elements[configurationName];
                if (configElement == null) return;

                var currentdate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

                configElement.Attributes["Status"].SetValue(new AFValue(status));
                configElement.Attributes["Message"].SetValue(new AFValue("[" + currentdate + "] " + status + " - " + errorMessage));
                if (errorMessage == "Error in Deleting datastream" && status == "ERROR")
                {
                    configElement.Attributes["Deleted"].SetValue(new AFValue(false));
                }
                database.CheckIn();
                database.Refresh();
            }
            catch (Exception e)
            {
                Log.Error($" Error in updating state of {configurationName} Status : {status} Message : {errorMessage} Error : {e.Message}");
                return;
            }
        }
        /*  public static void UpdateSourceID(string piSystemName, string configurationName, string sourceId)
          {
              try
              {
                  var af = new PISystems()[piSystemName]; af.Connect();
                  var database = af.Databases["Configuration"]; if (database == null) return;

                  var rootElement = AFObject.FindObject(FalkonryConfigurationRootElement(), database) as AFElement;
                  if (rootElement == null) return;
                  if (!rootElement.HasChildren) return;

                  var configurationsElement = rootElement.Elements["Configurations"];
                  if (configurationsElement == null) return;
                  if (!configurationsElement.HasChildren) return;

                  var configElement = configurationsElement.Elements[configurationName];
                  if (configElement == null) return;
                  var configItemTemplate = database.ElementTemplates["Falkonry_ConfigurationItem"];
                  foreach (var configurationItemElement in configurationsElement.Elements)
                  {
                      if (configurationItemElement.Template == configItemTemplate)
                      {
                          var configItemFalkonryElement = configurationItemElement.Elements["Falkonry"];
                          var configItemFalkonryElement = configElement.Elements["Falkonry"];
                          var configItemFalkonry = new FalkonryConfiguration()
                          {
                              AssessmentId = configItemFalkonryElement.Attributes["AssessmentId"].GetValue().ToString(),
                              AssessmentIdForFacts = configItemFalkonryElement.Attributes["AssessmentIdForFacts"].GetValue().ToString(),
                              Backfill = bool.Parse(configItemFalkonryElement.Attributes["Backfill"].GetValue().ToString()),
                              BatchLimit = int.Parse(configItemFalkonryElement.Attributes["BatchLimit"].GetValue().ToString()),
                              Datastream = configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString(),
                              Host = configItemFalkonryElement.Attributes["Host"].GetValue().ToString(),
                              TimeFormat = configItemFalkonryElement.Attributes["TimeFormat"].GetValue().ToString(),
                              Token = configItemFalkonryElement.Attributes["Token"].GetValue().ToString(),
                              SourceId = sourceId
                          };

                      }
                  }


                          configElement.Attributes["SourceId"].SetValue(new AFValue(sourceId));
              }
              catch (Exception e)
              {
                  Console.WriteLine(e.Message);
              }
          }*/
        public static string FalkonryConfigurationRootElement()
        {
            return @"Falkonry\Integrator";
        }
        public static Configurations RetrieveConfigurationFromPISystem(string piSystemName)
        {
            try
            {
                bool isAssessmentNameIdConversion = false;
                var configurations = new Configurations() { ConfigurationItems = new List<PiConfigurationItem>() };

                bool isDbBackupTaken = false;
                var af = new PISystems()[piSystemName];
                af.Connect();
                af.Refresh();
                var database = af.Databases["Configuration"]; if (database == null) return null;
                database.Refresh();
                var configsTemplate = database.ElementTemplates["Falkonry_Configurations"];
                var configItemTemplate = database.ElementTemplates["Falkonry_ConfigurationItem"];
                var configItemAFTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_AF"];
                var configItemFalkonryTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_Falkonry"];
                var configItemStreamingTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_Streaming"];
                var configItemHistoricalTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_Historical"];

                if (configsTemplate == null || configItemTemplate == null ||
                    configItemAFTemplate == null ||
                    configItemFalkonryTemplate == null || configItemStreamingTemplate == null ||
                    configItemHistoricalTemplate == null) return null;

                var rootElement = AFObject.FindObject(FalkonryConfigurationRootElement(), database) as AFElement;
                if (rootElement == null) return null;
                if (!rootElement.HasChildren) return null;

                var configurationsElement = rootElement.Elements["Configurations"];
                //configurationsElement.Changed
                if (configurationsElement == null) return null;
                //if (!(configurationsElement.Template == configsTemplate)) return null;
                Log.Debug($"RetrieveConfigurationFromPISystem: configurationsElement Elements Count - {configurationsElement.Elements.Count}");
                foreach (var configurationItemElement in configurationsElement.Elements)
                {
                    try
                    {
                        if (configurationItemElement.Template == configItemTemplate)
                        {

                            var configItemFalkonryElement = configurationItemElement.Elements["Falkonry"];
                            if (configItemFalkonryElement.Template != configItemFalkonryTemplate) return null;

                            var configItemAFElement = configurationItemElement.Elements["AF"];
                            if (configItemAFElement.Template != configItemAFTemplate) return null;

                            var configItemHistoricalElement = configurationItemElement.Elements["Historical"];
                            if (configItemHistoricalElement.Template != configItemHistoricalTemplate) return null;

                            FalkonryConfiguration falkonryConfig = new FalkonryConfiguration();
                            falkonryConfig.Host = configItemFalkonryElement.Attributes["Host"].GetValue().ToString();
                            falkonryConfig.Token = configItemFalkonryElement.Attributes["Token"].GetValue().ToString();

                            //////////////
                            string[] assessmentIdConfig;
                            string[] outputTemplateConfig;
                            string[] eventFrameConfig;
                            string[] diffEventFrameConfig;
                            string afServerTimeZoneId = "";
                            long startTimems, endTimems;



                            if (configItemFalkonryElement.Attributes["AssessmentName"] != null)
                            {
                                Log.Info($"RetrieveConfigurationFromPISystem: MIGRATION REQUIRED");
                                if (!isDbBackupTaken)
                                {
                                    Log.Info($"RetrieveConfigurationFromPISystem: Taking AF DATABASE BACKUP");
                                    string appFileName = Environment.GetCommandLineArgs()[0];
                                    string appPath = Path.GetDirectoryName(appFileName);
                                    appPath += "\\";
                                    string dbDirectory = appPath + "DbBackup";
                                    System.IO.Directory.CreateDirectory(dbDirectory);
                                    string afDatabaseXML = af.ExportXml(database, PIExportMode.AllReferences);
                                    System.IO.File.WriteAllText(dbDirectory + "\\afDatabase.xml", afDatabaseXML);
                                    isDbBackupTaken = true;
                                    Log.Info($"RetrieveConfigurationFromPISystem: AF DATABASE BACKUP SUCCESSFUL - {dbDirectory}\\afDatabase.xml");
                                }
                                Log.Info($"RetrieveConfigurationFromPISystem: *MIGRATION* AssessmentName  exist, converting it to AssessmentId for Datastream - {configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString()}");
                                isAssessmentNameIdConversion = true;
                                Hashtable assessmentMap = new Hashtable();

                                Log.Debug($"RetrieveConfigurationFromPISystem: Datastream - {configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString()} Assessment Names - {configItemFalkonryElement.Attributes["AssessmentName"].GetValue().ToString()}");
                                string[] assessmentNameConfig = configItemFalkonryElement.Attributes["AssessmentName"].GetValue().ToString().Split(new[] { Delimiter }, StringSplitOptions.None);
                                assessmentIdConfig = new string[assessmentNameConfig.Count()];
                                bool isHostReachable = true;
                                if (!FalkonryHelper.AuthenticateConnectionDetails(falkonryConfig.Host, falkonryConfig.Token))
                                {
                                    Log.Debug($"RetrieveConfigurationFromPISystem: Agent not able to connect to : '{falkonryConfig.Host}' Host :'{falkonryConfig.Token}', skipping {configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString()}");
                                    isHostReachable = false;
                                }
                                
                                if (isHostReachable)
                                {
                                    List<Assessment> assessments = FalkonryHelper.GetAssessments(falkonryConfig);
                                    Log.Info($"RetrieveConfigurationFromPISystem: Total Assessment count: {assessments.Count}");

                                    foreach (Assessment assessmentObj in assessments)
                                    {
                                        if (!assessmentMap.Contains(assessmentObj.Name) && assessmentObj.Datastream == configItemFalkonryElement.Attributes["SourceId"].GetValue().ToString())
                                            assessmentMap.Add(assessmentObj.Name, assessmentObj.Id);
                                    }


                                    for (int i = 0; i < assessmentNameConfig.Count(); i++)
                                    {
                                        assessmentIdConfig[i] = assessmentMap[assessmentNameConfig[i]].ToString();
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < assessmentNameConfig.Count(); i++)
                                    {
                                        assessmentIdConfig[i] = "";
                                    }
                                }
                                Log.Info($"RetrieveConfigurationFromPISystem: Datastream - {configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString()} Assessment count: {assessmentNameConfig.Count()}");
                                Log.Info($"RetrieveConfigurationFromPISystem :Datastream - {configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString()} OutputTemplateAttribute - {configItemAFElement.Attributes["OutputTemplateAttribute"].GetValue().ToString()}");
                                Log.Info($"RetrieveConfigurationFromPISystem :Datastream - {configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString()} EventFrameTemplateNames - {configItemAFElement.Attributes["EventFrameTemplateNames"].GetValue().ToString()}");
                                
                                //Log.Info($"RetrieveConfigurationFromPISystem :Datastream - {configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString()} EventFrameUploadedTime - {configItemAFElement.Attributes["EventFrameUploadedTime"].GetValue().ToString()}");

                                outputTemplateConfig = configItemAFElement.Attributes["OutputTemplateAttribute"].GetValue().ToString().Split(new[] { Delimiter }, StringSplitOptions.None);
                                eventFrameConfig = configItemAFElement.Attributes["EventFrameTemplateNames"].GetValue().ToString().Split(new[] { Delimiter }, StringSplitOptions.None);

                                DateTime StartTime = DateTime.Parse(configItemHistoricalElement.Attributes["StartTime"].GetValue().ToString());
                                DateTime EndTime = DateTime.Parse(configItemHistoricalElement.Attributes["EndTime"].GetValue().ToString());

                                startTimems = Utils.ConvertDateTimeToMilliseconds(StartTime);
                                endTimems = Utils.ConvertDateTimeToMilliseconds(EndTime);
                                diffEventFrameConfig = new string[eventFrameConfig.Count()];
                                for (int diffIndex = 0; diffIndex < diffEventFrameConfig.Count(); diffIndex++)
                                {
                                    diffEventFrameConfig[diffIndex] = "";

                                    if (eventFrameConfig[diffIndex] == "")
                                    {
                                        continue;
                                    }
                                    string[] assessmentEventFrameList = eventFrameConfig[diffIndex].Split(',');

                                    for (int eventFrameIndex = 0; eventFrameIndex < assessmentEventFrameList.Count(); eventFrameIndex++)
                                    {
                                        diffEventFrameConfig[diffIndex] += startTimems;
                                        diffEventFrameConfig[diffIndex] += "-";
                                        diffEventFrameConfig[diffIndex] += endTimems;
                                        if (eventFrameIndex + 1 != assessmentEventFrameList.Count())
                                        {
                                            diffEventFrameConfig[diffIndex] += ",";
                                        }
                                    }

                                }
                                afServerTimeZoneId = TimeZoneInfo.Local.Id;
                            }
                            else
                            {
                                assessmentIdConfig = ((System.Collections.IEnumerable)configItemFalkonryElement.Attributes["AssessmentId"].GetValue().Value).Cast<object>()
                                    .Where(x => x != null)
                                     .Select(x => x.ToString())
                                     .ToArray();
                                outputTemplateConfig = ((System.Collections.IEnumerable)configItemAFElement.Attributes["OutputTemplateAttribute"].GetValue().Value).Cast<object>()
                                    .Where(x => x != null)
                                    .Select(x => x.ToString())
                                      .ToArray();

                                eventFrameConfig = ((System.Collections.IEnumerable)configItemAFElement.Attributes["EventFrameTemplateNames"].GetValue().Value).Cast<object>()
                                    .Where(x => x != null)
                                    .Select(x => x.ToString())
                                     .ToArray();
                                diffEventFrameConfig = ((System.Collections.IEnumerable)configItemAFElement.Attributes["EventFrameUploadedTime"].GetValue().Value).Cast<object>()
                                    .Where(x => x != null)
                                     .Select(x => x.ToString())
                                     .ToArray();
                                startTimems = Convert.ToInt64(configItemHistoricalElement.Attributes["StartTime"].GetValue().ToString());
                                endTimems = Convert.ToInt64(configItemHistoricalElement.Attributes["EndTime"].GetValue().ToString());
                                afServerTimeZoneId = configItemAFElement.Attributes["AFServerTimeZoneId"].GetValue().ToString();
                            }

                            var configItemFalkonry = new FalkonryConfiguration()
                            {
                                AssessmentId = assessmentIdConfig,
                                AssessmentIdForFacts = assessmentIdConfig,
                                Backfill = bool.Parse(configItemFalkonryElement.Attributes["Backfill"].GetValue().ToString()),
                                BatchLimit = int.Parse(configItemFalkonryElement.Attributes["BatchLimit"].GetValue().ToString()),
                                Datastream = configItemFalkonryElement.Attributes["Datastream"].GetValue().ToString(),
                                Host = configItemFalkonryElement.Attributes["Host"].GetValue().ToString(),
                                TimeFormat = configItemFalkonryElement.Attributes["TimeFormat"].GetValue().ToString(),
                                Token = configItemFalkonryElement.Attributes["Token"].GetValue().ToString(),
                                SourceId = configItemFalkonryElement.Attributes["SourceId"].GetValue().ToString()
                            };


                            var configItemAF = new AfConfiguration()
                            {
                                SystemName = configItemAFElement.Attributes["SystemName"].GetValue().ToString(),
                                TemplateName = configItemAFElement.Attributes["TemplateName"].GetValue().ToString(),
                                DatabaseName = configItemAFElement.Attributes["DatabaseName"].GetValue().ToString(),
                                OutputTemplateAttribute = outputTemplateConfig,
                                TemplateAttributes = configItemAFElement.Attributes["TemplateAttributes"].GetValue().ToString().Split(','),
                                TemplateElementPaths = configItemAFElement.Attributes["TemplateElementPaths"].GetValue().ToString().Split(','),
                                EventFrameTemplateNames = eventFrameConfig,
                                EventFrameUploadedTime = diffEventFrameConfig,
                                AFServerTimeZoneId = afServerTimeZoneId
                            };

                            var configItemStreamingElement = configurationItemElement.Elements["Streaming"];
                            if (configItemStreamingElement.Template != configItemStreamingTemplate) return null;
                            var configItemStreaming = new StreamingConfiguration()
                            {
                                CollectionIntervalMs = int.Parse(configItemStreamingElement.Attributes["CollectionIntervalMs"].GetValue().ToString()),
                                //Enabled = bool.Parse(configItemStreamingElement.Attributes["Enabled"].GetValue().ToString()),
                            };

                            var configItemHistorical = new HistoricalConfiguration()
                            {
                                Enabled = bool.Parse(configItemHistoricalElement.Attributes["Enabled"].GetValue().ToString()),
                                PageSize = int.Parse(configItemHistoricalElement.Attributes["PageSize"].GetValue().ToString()),
                                //StartTime = DateTime.Parse(configItemHistoricalElement.Attributes["StartTime"].GetValue().ToString()),
                                //EndTime = DateTime.Parse(configItemHistoricalElement.Attributes["EndTime"].GetValue().ToString()),
                                StartTime = startTimems,
                                EndTime = endTimems,
                                Override = bool.Parse(configItemHistoricalElement.Attributes["Override"].GetValue().ToString()),
                            };

                            var configItemPI = new PiConfiguration()
                            {
                                AfConfiguration = configItemAF,
                                FalkonryConfiguration = configItemFalkonry,
                                StreamingDataAccess = configItemStreaming,
                                HistoricalDataAccess = configItemHistorical
                            };

                            var configItem = new PiConfigurationItem()
                            {
                                Id = Guid.Parse(configurationItemElement.Attributes["Id"].GetValue().ToString()),
                                Enabled = bool.Parse(configurationItemElement.Attributes["Enabled"].GetValue().ToString()),
                                Deleted = bool.Parse(configurationItemElement.Attributes["Deleted"].GetValue().ToString()),
                                Name = configurationItemElement.Name,
                                ErrorMessage = configurationItemElement.Attributes["Message"].GetValue().ToString(),
                                Status = configurationItemElement.Attributes["Status"].GetValue().ToString(),
                                Configuration = configItemPI
                            };

                            configurations.ConfigurationItems.Add(configItem);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error($"RetrieveConfigurationFromPISystem: Exception caught in fetching data for {configurationItemElement.Elements["Falkonry"].Elements["Datastream"]} from database. Error : {e.Message}");
                        Log.Error($" ExceptionStack Trace : {e.StackTrace}");
                        continue;
                    }

                }
                database.CheckIn();
                database.Refresh();
                if (configurations != null)
                {
                    //Log.Info($" Configuration items fetched from system : {piSystemName}. Pi system version : {af.ServerVersion}. Total Datastreams retrieved : {configurations.ConfigurationItems.Count}");
                    if (isAssessmentNameIdConversion)
                    {
                        Log.Debug($"RetrieveConfigurationFromPISystem, configuration migrated, storing it in PI DB");
                        StoreConfigurationForPISystem(piSystemName, configurations);
                    }
                }
                return configurations;
            }
            catch (Exception e)
            {
                Log.Error($" Exception caught in fetching config data from database. Error : {e.Message}");
                Log.Error($" ExceptionStack Trace : {e.StackTrace}");
                throw;
                //return null;
            };
        }

        public static Tuple<bool,Exception> StoreConfigurationForPISystem(string piSystemName, Configurations configurations, string dsOldName = "")
        {
            try
            {
                var af = new PISystems()[piSystemName]; af.Connect();
                var database = af.Databases["Configuration"]; if (database == null) return Tuple.Create<bool,Exception>(false,null);

                var configsTemplate = database.ElementTemplates["Falkonry_Configurations"];
                if (configsTemplate == null)
                {
                    configsTemplate = database.ElementTemplates.Add("Falkonry_Configurations");
                }

                var configItemTemplate = database.ElementTemplates["Falkonry_ConfigurationItem"];
                if (configItemTemplate == null)
                {
                    configItemTemplate = database.ElementTemplates.Add("Falkonry_ConfigurationItem");
                    var attId = configItemTemplate.AttributeTemplates.Add("Id");
                    attId.Type = typeof(string);
                    attId.IsConfigurationItem = false;

                    var attName = configItemTemplate.AttributeTemplates.Add("Name");
                    attName.Type = typeof(string);
                    attName.IsConfigurationItem = false;

                    var attEnabled = configItemTemplate.AttributeTemplates.Add("Enabled");
                    attEnabled.Type = typeof(bool);
                    attEnabled.IsConfigurationItem = false;

                    var attStatus = configItemTemplate.AttributeTemplates.Add("Status");
                    attStatus.Type = typeof(string);
                    attStatus.IsConfigurationItem = false;

                    var attError = configItemTemplate.AttributeTemplates.Add("Message");
                    attError.Type = typeof(string);
                    attError.IsConfigurationItem = false;

                    var attDelete = configItemTemplate.AttributeTemplates.Add("Deleted");
                    attDelete.Type = typeof(bool);
                    attDelete.IsConfigurationItem = false;

                    database.CheckIn();
                    database.Refresh();
                }

                var configItemAFTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_AF"];
                if (configItemAFTemplate == null)
                {
                    configItemAFTemplate = database.ElementTemplates.Add("Falkonry_ConfigurationItem_AF");
                    var att1 = configItemAFTemplate.AttributeTemplates.Add("SystemName");
                    att1.Type = typeof(string);
                    att1.IsConfigurationItem = false;

                    var att2 = configItemAFTemplate.AttributeTemplates.Add("TemplateName");
                    att2.Type = typeof(string);
                    att2.IsConfigurationItem = false;

                    var att3 = configItemAFTemplate.AttributeTemplates.Add("DatabaseName");
                    att3.Type = typeof(string);
                    att3.IsConfigurationItem = false;

                    var att4 = configItemAFTemplate.AttributeTemplates.Add("OutputTemplateAttribute");
                    att4.Type = typeof(String[]);
                    att4.IsConfigurationItem = false;

                    var att5 = configItemAFTemplate.AttributeTemplates.Add("TemplateAttributes");
                    att5.Type = typeof(string);
                    att5.IsConfigurationItem = false;

                    var att6 = configItemAFTemplate.AttributeTemplates.Add("TemplateElementPaths");
                    att6.Type = typeof(string);
                    att6.IsConfigurationItem = false;

                    var att7 = configItemAFTemplate.AttributeTemplates.Add("EventFrameTemplateNames");
                    att7.Type = typeof(String[]);
                    att7.IsConfigurationItem = false;

                    var att8 = configItemAFTemplate.AttributeTemplates.Add("EventFrameUploadedTime");
                    att8.Type = typeof(String[]);
                    att8.IsConfigurationItem = false;

                    var att9 = configItemAFTemplate.AttributeTemplates.Add("AFServerTimeZoneId");
                    att9.Type = typeof(string);
                    att9.IsConfigurationItem = false;

                    database.CheckIn();
                    database.Refresh();
                }
                else if (configItemAFTemplate.AttributeTemplates["OutputTemplateAttribute"] != null && configItemAFTemplate.AttributeTemplates["OutputTemplateAttribute"].Type == typeof(string))
                {
                    Log.Debug("StoreConfigurationForPISystem: *MIGRATION* changing typeof OutputTemplateAttribute and  EventFrameTemplateNames from string to String[] ");
                    configItemAFTemplate.AttributeTemplates["OutputTemplateAttribute"].Type = typeof(String[]);
                    configItemAFTemplate.AttributeTemplates["EventFrameTemplateNames"].Type = typeof(String[]);

                    Log.Debug("StoreConfigurationForPISystem: *MIGRATION* Creating EventFrameUploadedTime in AF Template");
                    var att1 = configItemAFTemplate.AttributeTemplates.Add("EventFrameUploadedTime");
                    att1.Type = typeof(String[]);
                    att1.IsConfigurationItem = false;

                    var att2 = configItemAFTemplate.AttributeTemplates.Add("AFServerTimeZoneId");
                    att2.Type = typeof(string);
                    att2.IsConfigurationItem = false;

                    database.CheckIn();
                    database.Refresh();
                }

                var configItemFalkonryTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_Falkonry"];
                if (configItemFalkonryTemplate == null)
                {
                    configItemFalkonryTemplate = database.ElementTemplates.Add("Falkonry_ConfigurationItem_Falkonry");
                    var att1 = configItemFalkonryTemplate.AttributeTemplates.Add("AssessmentId");
                    att1.Type = typeof(String[]);
                    att1.IsConfigurationItem = false;

                    var att2 = configItemFalkonryTemplate.AttributeTemplates.Add("AssessmentIdForFacts");
                    att2.Type = typeof(String[]);
                    att2.IsConfigurationItem = false;

                    var att3 = configItemFalkonryTemplate.AttributeTemplates.Add("Backfill");
                    att3.Type = typeof(bool);
                    att3.IsConfigurationItem = false;

                    var att4 = configItemFalkonryTemplate.AttributeTemplates.Add("BatchLimit");
                    att4.Type = typeof(int);
                    att4.IsConfigurationItem = false;

                    var att5 = configItemFalkonryTemplate.AttributeTemplates.Add("Datastream");
                    att5.Type = typeof(string);
                    att5.IsConfigurationItem = false;

                    var att6 = configItemFalkonryTemplate.AttributeTemplates.Add("Host");
                    att6.Type = typeof(string);
                    att6.IsConfigurationItem = false;

                    var att7 = configItemFalkonryTemplate.AttributeTemplates.Add("TimeFormat");
                    att7.Type = typeof(string);
                    att7.IsConfigurationItem = false;

                    var att8 = configItemFalkonryTemplate.AttributeTemplates.Add("Token");
                    att8.Type = typeof(string);
                    att8.IsConfigurationItem = false;

                    var att9 = configItemFalkonryTemplate.AttributeTemplates.Add("SourceId");
                    att9.Type = typeof(string);
                    att9.IsConfigurationItem = false;

                    database.CheckIn();
                    database.Refresh();
                }
                else if (configItemFalkonryTemplate.AttributeTemplates["AssessmentName"] != null)
                {
                    Log.Debug("StoreConfigurationForPISystem: *MIGRATION* Adding AssessmentId and AssessmentIdForFacts in Attribute Template ");
                    configItemFalkonryTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_Falkonry"];
                    var att1 = configItemFalkonryTemplate.AttributeTemplates.Add("AssessmentId");
                    att1.Type = typeof(String[]);
                    att1.IsConfigurationItem = false;

                    var att2 = configItemFalkonryTemplate.AttributeTemplates.Add("AssessmentIdForFacts");
                    att2.Type = typeof(String[]);
                    att2.IsConfigurationItem = false;

                    Log.Debug("StoreConfigurationForPISystem: *MIGRATION* Removing AssessmentName and AssessmentNameForFacts in Attribute Template ");
                    configItemFalkonryTemplate.AttributeTemplates.Remove("AssessmentName");
                    configItemFalkonryTemplate.AttributeTemplates.Remove("AssessmentNameForFacts");
                    database.CheckIn();
                    database.Refresh();
                }

                var configItemStreamingTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_Streaming"];
                if (configItemStreamingTemplate == null)
                {
                    configItemStreamingTemplate = database.ElementTemplates.Add("Falkonry_ConfigurationItem_Streaming");
                    var att1 = configItemStreamingTemplate.AttributeTemplates.Add("CollectionIntervalMs");
                    att1.Type = typeof(int);
                    att1.IsConfigurationItem = false;

                    var att2 = configItemStreamingTemplate.AttributeTemplates.Add("Enabled");
                    att2.Type = typeof(bool);
                    att2.IsConfigurationItem = false;

                    database.CheckIn();
                    database.Refresh();
                }

                var configItemHistoricalTemplate = database.ElementTemplates["Falkonry_ConfigurationItem_Historical"];
                if (configItemHistoricalTemplate == null)
                {
                    configItemHistoricalTemplate = database.ElementTemplates.Add("Falkonry_ConfigurationItem_Historical");
                    var att1 = configItemHistoricalTemplate.AttributeTemplates.Add("PageSize");
                    att1.Type = typeof(int);
                    att1.IsConfigurationItem = false;

                    var att2 = configItemHistoricalTemplate.AttributeTemplates.Add("Enabled");
                    att2.Type = typeof(bool);
                    att2.IsConfigurationItem = false;

                    //var att3 = configItemHistoricalTemplate.AttributeTemplates.Add("StartTime");
                    //att3.Type = typeof(DateTime);
                    //att3.IsConfigurationItem = false;

                    //var att4 = configItemHistoricalTemplate.AttributeTemplates.Add("EndTime");
                    //att4.Type = typeof(DateTime);
                    //att4.IsConfigurationItem = false;

                    var att3 = configItemHistoricalTemplate.AttributeTemplates.Add("StartTime");
                    att3.Type = typeof(long);
                    att3.IsConfigurationItem = false;

                    var att4 = configItemHistoricalTemplate.AttributeTemplates.Add("EndTime");
                    att4.Type = typeof(long);
                    att4.IsConfigurationItem = false;

                    var att5 = configItemHistoricalTemplate.AttributeTemplates.Add("Override");
                    att5.Type = typeof(bool);
                    att5.IsConfigurationItem = false;

                    database.CheckIn();
                    database.Refresh();
                }
                else if (configItemHistoricalTemplate.AttributeTemplates["StartTime"] != null && configItemHistoricalTemplate.AttributeTemplates["StartTime"].Type == typeof(DateTime)
                    && configItemHistoricalTemplate.AttributeTemplates["EndTime"] != null && configItemHistoricalTemplate.AttributeTemplates["EndTime"].Type == typeof(DateTime))
                {
                    configItemHistoricalTemplate.AttributeTemplates["StartTime"].Type = typeof(long);
                    configItemHistoricalTemplate.AttributeTemplates["EndTime"].Type = typeof(long);
                    database.CheckIn();
                    database.Refresh();
                }

                var rootElement = AFObject.FindObject(FalkonryConfigurationRootElement(), database) as AFElement;
                if (rootElement == null)
                {
                    var pathItems = FalkonryConfigurationRootElement().Split('\\');
                    var element = database.Elements.Add(pathItems[0]);
                    for (var i = 1; i < pathItems.Count(); i++)
                    {
                        element = element.Elements.Add(pathItems[i]);
                    }
                    rootElement = element;

                    database.CheckIn();
                    database.Refresh();
                }

                var configurationsElement = rootElement.Elements["Configurations"];
                if (configurationsElement == null)
                {
                    configurationsElement = rootElement.Elements.Add("Configurations", configsTemplate);
                    database.CheckIn();
                    database.Refresh();
                }
                bool allConfigSaved = true;
                Exception dbException = null;
                foreach (var configuationsItem in configurations.ConfigurationItems)
                {
                    try
                    {
                        string configName = "";
                        if (dsOldName != "")
                        {
                            configName = dsOldName;
                        }
                        else
                        {
                            configName = configuationsItem.Name;
                        }
                        var configElement = configurationsElement.Elements[configName];
                        if (configElement == null) configElement = configurationsElement.Elements.Add(configuationsItem.Name, configItemTemplate);

                        //Check if the configuration we are handing in the loop is removed
                        if (bool.Parse(configElement.Attributes["Deleted"].GetValue().ToString()) && configElement.Attributes["Message"].GetValue().ToString().Contains("Datastream Deleted"))
                        {
                            Log.Debug("Deleting from database - " + configuationsItem.Name);
                            configurationsElement.Elements.Remove(configElement.ID);
                            continue;
                        }

                        // Check for Configurations that need to be removed from the AF Database
                        //var itemNames = configurations.ConfigurationItems.Select(i => (i.ErrorMessage == "Deleted")i.Name.ToLower().Trim());
                        if (dsOldName != "")
                        {
                            configElement.Name = configuationsItem.Name;
                        }
                        configElement.Attributes["Id"].SetValue(new AFValue(configuationsItem.Id.ToString(), DateTime.Now));
                        configElement.Attributes["Enabled"].SetValue(new AFValue(configuationsItem.Enabled, DateTime.Now));
                        configElement.Attributes["Deleted"].SetValue(new AFValue(configuationsItem.Deleted, DateTime.Now));
                        configElement.Attributes["Name"].SetValue(new AFValue(configuationsItem.Name.ToString(), DateTime.Now));
                        
                        // AF
                        var configItemAFElement = configElement.Elements["AF"];
                        if (configItemAFElement == null)
                        {
                            configItemAFElement = configElement.Elements.Add("AF", configItemAFTemplate);
                        }
                        configItemAFElement.Attributes["SystemName"].SetValue(new AFValue(configuationsItem.Configuration.AfConfiguration.SystemName, DateTime.Now));
                        configItemAFElement.Attributes["TemplateName"].SetValue(new AFValue(configuationsItem.Configuration.AfConfiguration.TemplateName, DateTime.Now));
                        configItemAFElement.Attributes["DatabaseName"].SetValue(new AFValue(configuationsItem.Configuration.AfConfiguration.DatabaseName, DateTime.Now));
                        configItemAFElement.Attributes["OutputTemplateAttribute"].SetValue(new AFValue(configuationsItem.Configuration.AfConfiguration.OutputTemplateAttribute, DateTime.Now));

                        var tempAttributes = ""; configuationsItem.Configuration.AfConfiguration.TemplateAttributes.ToList().ForEach(t => tempAttributes += t + ","); if (tempAttributes.Length > 0) tempAttributes = tempAttributes.Substring(0, tempAttributes.Length - 1);
                        configItemAFElement.Attributes["TemplateAttributes"].SetValue(new AFValue(tempAttributes, DateTime.Now));

                        var tempEPaths = ""; configuationsItem.Configuration.AfConfiguration.TemplateElementPaths.ToList().ForEach(t => tempEPaths += t + ","); if (tempEPaths.Length > 0) tempEPaths = tempEPaths.Substring(0, tempEPaths.Length - 1);
                        configItemAFElement.Attributes["TemplateElementPaths"].SetValue(new AFValue(tempEPaths, DateTime.Now));

                        configItemAFElement.Attributes["EventFrameTemplateNames"].SetValue(new AFValue(configuationsItem.Configuration.AfConfiguration.EventFrameTemplateNames, DateTime.Now));
                        configItemAFElement.Attributes["EventFrameUploadedTime"].SetValue(new AFValue(configuationsItem.Configuration.AfConfiguration.EventFrameUploadedTime, DateTime.Now));
                        configItemAFElement.Attributes["AFServerTimeZoneId"].SetValue(new AFValue(configuationsItem.Configuration.AfConfiguration.AFServerTimeZoneId, DateTime.Now));


                        // Falkonry
                        var configItemFalkonryElement = configElement.Elements["Falkonry"];
                        if (configItemFalkonryElement == null)
                        {
                            configItemFalkonryElement = configElement.Elements.Add("Falkonry", configItemFalkonryTemplate);
                        }
                        configItemFalkonryElement.Attributes["AssessmentId"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.AssessmentId, DateTime.Now));
                        configItemFalkonryElement.Attributes["AssessmentIdForFacts"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.AssessmentIdForFacts, DateTime.Now));
                        configItemFalkonryElement.Attributes["Backfill"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.Backfill, DateTime.Now));
                        configItemFalkonryElement.Attributes["BatchLimit"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.BatchLimit, DateTime.Now));
                        configItemFalkonryElement.Attributes["Datastream"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.Datastream, DateTime.Now));
                        configItemFalkonryElement.Attributes["Host"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.Host, DateTime.Now));
                        configItemFalkonryElement.Attributes["TimeFormat"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.TimeFormat, DateTime.Now));
                        configItemFalkonryElement.Attributes["Token"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.Token, DateTime.Now));
                        configItemFalkonryElement.Attributes["SourceId"].SetValue(new AFValue(configuationsItem.Configuration.FalkonryConfiguration.SourceId, DateTime.Now));

                        // Historical
                        var configItemHistoricalElement = configElement.Elements["Historical"];
                        if (configItemHistoricalElement == null)
                        {
                            configItemHistoricalElement = configElement.Elements.Add("Historical", configItemHistoricalTemplate);
                        }
                        configItemHistoricalElement.Attributes["Enabled"].SetValue(new AFValue(configuationsItem.Configuration.HistoricalDataAccess.Enabled, DateTime.Now));
                        configItemHistoricalElement.Attributes["PageSize"].SetValue(new AFValue(configuationsItem.Configuration.HistoricalDataAccess.PageSize, DateTime.Now));
                        configItemHistoricalElement.Attributes["StartTime"].SetValue(new AFValue(configuationsItem.Configuration.HistoricalDataAccess.StartTime, DateTime.Now));
                        configItemHistoricalElement.Attributes["EndTime"].SetValue(new AFValue(configuationsItem.Configuration.HistoricalDataAccess.EndTime, DateTime.Now));
                        configItemHistoricalElement.Attributes["Override"].SetValue(new AFValue(configuationsItem.Configuration.HistoricalDataAccess.Override, DateTime.Now));


                        // Streaming
                        var configItemStreamingElement = configElement.Elements["Streaming"];
                        if (configItemStreamingElement == null)
                        {
                            configItemStreamingElement = configElement.Elements.Add("Streaming", configItemStreamingTemplate);
                        }
                        configItemStreamingElement.Attributes["Enabled"].SetValue(new AFValue(configuationsItem.Configuration.StreamingDataAccess.Enabled, DateTime.Now));
                        configItemStreamingElement.Attributes["CollectionIntervalMs"].SetValue(new AFValue(configuationsItem.Configuration.StreamingDataAccess.CollectionIntervalMs, DateTime.Now));

                        database.CheckIn();
                        database.Refresh();
                    }
                    catch (Exception e)
                    {
                        Log.Error($" Exception caught in storing config data {configuationsItem.Name} into database. Error :{ e.Message}");
                        Log.Error($" Exception caught in storing config data  {configuationsItem.Name} into database. Stack Trace : {e.StackTrace}");
                        allConfigSaved = false;
                        dbException = e;
                        continue;
                    }
                }
                database.CheckIn();
                database.Refresh();
                return Tuple.Create<bool,Exception>(allConfigSaved,dbException);
            }
            catch (Exception e)
            {
                Log.Error($" Exception caught in storing config data into database. Error : {e.Message}");
                Log.Error($" Exception caught in storing config data into database. Stack Trace : {e.StackTrace}");
                e.Data.Add("title", @"Not Saved.");
                e.Data.Add("msg", e.Message);
                throw;
            }
        }


        public static bool StoreupdateConfigurationPISystem(string piSystemName, PiConfigurationItem configurations, string dsOldName = "")
        {
            bool yes = true;
            var af = new PISystems()[piSystemName];
            af.Connect();
            af.Refresh();
            AFDatabase database = (AFDatabase)af.Databases[Constants.CONFIGDBNAME];
            database.Refresh();
            AFKeyedResults<String, AFElement> searchResult = AFElement.FindElementsByPath(new string[] { Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME }, database);
            AFElement rootConfigElement;
            searchResult.Results.TryGetValue(Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME, out rootConfigElement);
            rootConfigElement.Refresh();


            AFElement element = AFElement.FindElement(af, configurations.Configuration.Id);
            
            AFElement falkonryConfig = element.Elements.Where(x => x.Name.Equals(Constants.FalkonryConfigConstants.name)).First();

            falkonryConfig.Attributes["AssessmentId"].SetValue(new AFValue(configurations.Configuration.FalkonryConfiguration.AssessmentId, DateTime.Now));
            falkonryConfig.Attributes["AssessmentIdForFacts"].SetValue(new AFValue(configurations.Configuration.FalkonryConfiguration.AssessmentIdForFacts, DateTime.Now));

            database.CheckIn();
            return true;

        }

        public static bool SaveSingleConfig(PiConfigurationItem configItem, string dsOldName = "")
        {
            Log.Info($"Saving single Datastream config for datastream : {configItem.Name} . Config Id : {configItem.Id}. Datastream Id : {configItem.Configuration.FalkonryConfiguration.SourceId}");
            Configurations newConfig = new Configurations();
            newConfig.ConfigurationItems = new List<PiConfigurationItem>();
            newConfig.ConfigurationItems.Add(configItem);
            return Helper.AfHelper.StoreConfigurationForPISystem(configItem.Configuration.AfConfiguration.SystemName, newConfig, dsOldName).Item1;
        }

        public static bool SaveSingleupdateConfig(PiConfiguration configItem, string dsOldName = "")
        {
            Log.Info($"Saving single Datastream config for datastream : {configItem.Name} . Config Id : {configItem.Id}. Datastream Id : {configItem.FalkonryConfiguration.SourceId}");
            PiConfigurationItem _item = new PiConfigurationItem();
            _item.Configuration = configItem;
          
            return Helper.AfHelper.StoreupdateConfigurationPISystem(configItem.AfConfiguration.SystemName, _item, dsOldName);
        }

        public static PiConfiguration updateHistoricalElement(PiConfiguration piconfig)
        {
            try
            {
                var af = new PISystems()[piconfig.AfConfiguration.SystemName];
                af.Connect();
                af.Refresh();
                AFDatabase database = (AFDatabase)af.Databases[Constants.CONFIGDBNAME];
                database.Refresh();
                AFKeyedResults<String, AFElement> searchResult = AFElement.FindElementsByPath(new string[] { Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME }, database);
                AFElement rootConfigElement;
                searchResult.Results.TryGetValue(Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME, out rootConfigElement);
                rootConfigElement.Refresh();
                AFElement element = AFElement.FindElement(af, piconfig.Id);
                AFElement historicalConfig = element.Elements.Where(x => x.Name.Equals(Constants.HistoricalConfigConstants.name)).First();
                AFValue backfillValue = new AFValue("False");
                historicalConfig.Attributes[Constants.HistoricalConfigConstants.Override].SetValue(backfillValue);
                historicalConfig.Attributes[Constants.HistoricalConfigConstants.Enabled].SetValue(backfillValue);
                database.CheckIn();
            }

            catch(Exception ex)
            {
                Log.Fatal($"Exception caught in AFDatabase of updateHistorical {piconfig}");
                Log.Fatal($"Exception StackTrace {ex.StackTrace}");
            }
            return piconfig;
        }

        public static PiConfiguration updateFalkonryBackfill(PiConfiguration piconfig)
        {
            try
            {
                var af = new PISystems()[piconfig.AfConfiguration.SystemName];
                af.Connect();
                af.Refresh();
                AFDatabase database = (AFDatabase)af.Databases[Constants.CONFIGDBNAME];
                database.Refresh();
                AFKeyedResults<String, AFElement> searchResult = AFElement.FindElementsByPath(new string[] { Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME }, database);
                AFElement rootConfigElement;
                searchResult.Results.TryGetValue(Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME, out rootConfigElement);
                rootConfigElement.Refresh();
                AFElement element = AFElement.FindElement(af, piconfig.Id);
                AFElement falkonryConfig = element.Elements.Where(d => d.Name.Equals(Constants.FalkonryConfigConstants.name)).First();
                AFValue backfillValue = new AFValue("False");
                falkonryConfig.Attributes[Constants.FalkonryConfigConstants.BackFill].SetValue(backfillValue);

                database.CheckIn();
            }
            catch(Exception ex)
            {
                Log.Fatal($"Exception caught in AFDatabase of updateFalkonryBackfill {piconfig}");
                Log.Fatal($"Exception StackTrace {ex.StackTrace}");
            }
            return piconfig;
        }

        /**
         * Does not delte the actual confiuration only sets delete flag==true for the Configuration
         */ 
        public static void deleteConfigurationFromAFDatabase(PiConfiguration piconfig)
        {
            try
            {
                var systems = new OSIsoft.AF.PISystems();

                foreach (var system in systems)
                {
                    var systemname = system.Name;

                    var af = new PISystems()[systemname];
                    af.Connect();
                    af.Refresh();
                    AFDatabase database = (AFDatabase)af.Databases[Constants.CONFIGDBNAME];
                    database.Refresh();
                    AFKeyedResults<String, AFElement> searchResult = AFElement.FindElementsByPath(new string[] { Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME }, database);
                    AFElement rootConfigElement;
                    searchResult.Results.TryGetValue(Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME, out rootConfigElement);
                    rootConfigElement.Refresh();

                    AFElement config = AFElement.FindElement(af, piconfig.Id);

                    AFValue delete = new AFValue("True");
                    config.Attributes[Constants.ParentConfigConstant.Deleted].SetValue(delete);
                    database.CheckIn();
                }
            }
            catch (Exception error)
            {
                Log.Error($" Deleting from AFDatabase faild for Datastream: {piconfig.FalkonryConfiguration.Datastream} Id: {piconfig.FalkonryConfiguration.SourceId} Exception Message: {error.Message} \n Exception Stacktrace: {error.StackTrace}");
            }
        }

    }
}