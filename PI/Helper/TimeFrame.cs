﻿using System;

namespace Falkonry.Integrator.PISystem.Helper
{
    public class TimeFrame
    {
        public DateTime StartTime
        {
            get;
            set;
        }

        public DateTime EndTime
        {
            get;
            set;
        }
    }
}