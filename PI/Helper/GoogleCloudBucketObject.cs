﻿namespace Falkonry.Integrator.PISystem.Helper
{
    public class GoogleCloudBucketObject
    {
        public string kind
        {
            get;
            set;
        }

        public string id
        {
            get;
            set;
        }
        public string selfLink
        {
            get;
            set;
        }
        public string name
        {
            get;
            set;
        }
        public string bucket
        {
            get;
            set;
        }
        public string generation
        {
            get;
            set;
        }
        public string metageneration
        {
            get;
            set;
        }
        public string contentType
        {
            get;
            set;
        }
        public string timeCreated
        {
            get;
            set;
        }
        public string updated
        {
            get;
            set;
        }
        public string storageClass
        {
            get;
            set;
        }
        public string timeStorageClassUpdated
        {
            get;
            set;
        }
        public string size
        {
            get;
            set;
        }
        public string md5Hash
        {
            get;
            set;
        }
        public string mediaLink
        {
            get;
            set;
        }
        public string crc32c
        {
            get;
            set;
        }
        public string etag
        {
            get;
            set;
        }
    }
}
