﻿using System;
using System.Configuration;
using System.IO;
using System.Collections;
using System.IO.Compression;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FalkonryClient.Helper.Models;
using OSIsoft.AF.Asset;
using log4net;
using Microsoft.Win32;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;

namespace Falkonry.Integrator.PISystem.Helper
{
    public class EmailService
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static void sendMail(string account = "", string bodyAddon = "")
        {
            try
            {
                Log.Debug("Inside sendMail");
                string appFileName = Environment.GetCommandLineArgs()[0];
                string appPath = Path.GetDirectoryName(appFileName);
                appPath += "\\";
                string tempPath = "Logs\\Extracted Logs\\Temp";
                Dictionary<string, byte[]> fileList = new Dictionary<string, byte[]>();
                DirectoryInfo tempDir = Directory.CreateDirectory(appPath + tempPath);
                try
                {
                    Array.ForEach(Directory.GetFiles($@"{appPath}Logs\\Extracted Logs"), File.Delete);
                }
                catch(Exception)
                {
                    Log.Debug("sendMail: Exception caught while deleting the logs files");
                }
                var value = Properties.Default.mailPort;
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(Properties.Default.mailFrom);
                message.To.Add(new MailAddress(Properties.Default.mailTo));
                if(account != "")
                    message.Subject = $"PIAgent - Bug Report from Account: {account}, Machine Name: {Environment.MachineName.ToString()}";
                else
                    message.Subject = $"PIAgent - Bug Report from Machine Name: {Environment.MachineName.ToString()}";


                string body = $"PI Agent Version: {Application.ProductVersion} <br/>";
                body += $"Windows Operating System: { Environment.OSVersion.ToString()} <br/>";
                body += $"Machine Name: { Environment.MachineName.ToString()} <br/>";
                body += $"UTC Time:{DateTime.UtcNow.ToString("dd-MM-yyyy hh:mm:ss")} <br/>";
                body += $"Timezone: {TimeZoneInfo.Local.StandardName} <br/>";
                body += bodyAddon;

                Log.Debug($"Mail body: {body}");
                message.Body = body;
                var activityLog = appPath + "Logs\\Activity\\Activity.log";
                var piSystemLog = appPath + "Logs\\PISystem\\PISystem.log";

                Log.Debug("sendMail: Copying file to temp folder");
                File.Copy(activityLog, appPath + tempPath + "\\" + "Activity.log", true);
                fileList.Add("Activity.log", System.IO.File.ReadAllBytes(appPath + tempPath + "\\" + "Activity.log"));

                if(File.Exists(activityLog +".1"))
                {
                    File.Copy(activityLog + ".1", appPath + tempPath + "\\" + "Activity.log.1", true);
                    fileList.Add("Activity.log.1", System.IO.File.ReadAllBytes(appPath + tempPath + "\\" + "Activity.log.1"));
                }

                File.Copy(piSystemLog, appPath + tempPath + "\\" + "PISystem.log", true);
                fileList.Add("PISystem.log", System.IO.File.ReadAllBytes(appPath + tempPath + "\\" + "PISystem.log"));

                if (File.Exists(piSystemLog + ".1"))
                {
                    File.Copy(piSystemLog + ".1", appPath + tempPath + "\\" + "PISystem.log.1", true);
                    fileList.Add("PISystem.log.1", System.IO.File.ReadAllBytes(appPath + tempPath + "\\" + "PISystem.log.1"));
                }


                Log.Debug($"btnExportLogs_Click: Copied all the logs files into {appPath + tempPath} folder and streamed its data into fileList");
                string fileName = $@"{appPath}Logs\\Extracted Logs\\FalkonryLogs" + DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss") + ".zip";


                using (var memoryStream = new MemoryStream())
                {
                    using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (var file in fileList)
                        {
                            var demoFile = archive.CreateEntry(file.Key);

                            using (var entryStream = demoFile.Open())
                            using (var b = new BinaryWriter(entryStream))
                            {
                                b.Write(file.Value);
                            }
                        }
                    }

                    using (var fileStream = new FileStream(fileName, FileMode.Create))
                    {
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.CopyTo(fileStream);
                    }
                }
                Log.Debug($"btnExportLogs_Click: Zipped file successfully created {fileName}");
                tempDir.Delete(true);
                Log.Debug($"btnExportLogs_Click: Deleted {appPath + tempPath} folder");

                message.Attachments.Add(new Attachment(fileName));
                message.IsBodyHtml = true;
                smtp.Port = Properties.Default.mailPort;
                smtp.Host = Properties.Default.mailHost;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(Properties.Default.mailUserName, Properties.Default.mailPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
                MessageBox.Show("Logs have been exported successfully, somebody from falkonry team will contact you shortly.", "Thank you ", MessageBoxButtons.OK);
                return;
            }
            catch (Exception exception)
            {
                Log.Warn($"Exception caught in sendMail- {exception.Message}");
                Log.Warn($"Exception caught in sendMail, Stack Trace- {exception.StackTrace}");
                MessageBox.Show($"{exception.Message}", "Error in Sending mail", MessageBoxButtons.OK);
            }
        }

    }
}
