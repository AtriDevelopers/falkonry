﻿namespace PI_Utility
{
    partial class UtilityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UtilityForm));
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.btnImportFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.streamingGrpBox = new System.Windows.Forms.GroupBox();
            this.lblRemainingEventsValue = new System.Windows.Forms.Label();
            this.lblRemainingEvents = new System.Windows.Forms.Label();
            this.collectionIntervalMs = new System.Windows.Forms.NumericUpDown();
            this.collectionInterval = new System.Windows.Forms.NumericUpDown();
            this.lblCollectionInterval = new System.Windows.Forms.Label();
            this.pageSize = new System.Windows.Forms.NumericUpDown();
            this.pageSizeLabel = new System.Windows.Forms.Label();
            this.chkBoxLiveStreaming = new System.Windows.Forms.CheckBox();
            this.dsName = new System.Windows.Forms.Label();
            this.configurationGroup = new System.Windows.Forms.GroupBox();
            this.configPanel = new System.Windows.Forms.Panel();
            this.WhichAFDatabase = new OSIsoft.AF.UI.AFDatabasePicker();
            this.chkExistingDB = new System.Windows.Forms.CheckBox();
            this.txtDbName = new System.Windows.Forms.TextBox();
            this.lblHostName = new System.Windows.Forms.Label();
            this.WhichPISystem = new OSIsoft.AF.UI.PISystemPicker();
            this.lblSelectConfiguration = new System.Windows.Forms.Label();
            this.historyGroup = new System.Windows.Forms.GroupBox();
            this.txtCustomTz = new System.Windows.Forms.TextBox();
            this.chkCustomTz = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTimeFormat = new System.Windows.Forms.ComboBox();
            this.txtEntityIdentifier = new System.Windows.Forms.TextBox();
            this.chkMultipleThings = new System.Windows.Forms.CheckBox();
            this.cbElementTemplateList = new System.Windows.Forms.ComboBox();
            this.comboBoxTimeIdentifier = new System.Windows.Forms.ComboBox();
            this.comboBoxEntityIdentifier = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtElmentTemplateName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.mainPanel.SuspendLayout();
            this.streamingGrpBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.collectionIntervalMs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.collectionInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageSize)).BeginInit();
            this.configurationGroup.SuspendLayout();
            this.configPanel.SuspendLayout();
            this.historyGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtFileName
            // 
            this.txtFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtFileName.Location = new System.Drawing.Point(87, 15);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(353, 22);
            this.txtFileName.TabIndex = 1;
            // 
            // btnImportFile
            // 
            this.btnImportFile.BackColor = System.Drawing.Color.Silver;
            this.btnImportFile.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnImportFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnImportFile.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnImportFile.Location = new System.Drawing.Point(451, 14);
            this.btnImportFile.Name = "btnImportFile";
            this.btnImportFile.Size = new System.Drawing.Size(82, 23);
            this.btnImportFile.TabIndex = 2;
            this.btnImportFile.Text = "Browse";
            this.btnImportFile.UseVisualStyleBackColor = false;
            this.btnImportFile.Click += new System.EventHandler(this.btnImportFile_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.btnReset);
            this.mainPanel.Controls.Add(this.btnPause);
            this.mainPanel.Controls.Add(this.streamingGrpBox);
            this.mainPanel.Controls.Add(this.txtFileName);
            this.mainPanel.Controls.Add(this.btnImportFile);
            this.mainPanel.Controls.Add(this.dsName);
            this.mainPanel.Controls.Add(this.configurationGroup);
            this.mainPanel.Controls.Add(this.historyGroup);
            this.mainPanel.Controls.Add(this.btnSave);
            this.mainPanel.Location = new System.Drawing.Point(12, 12);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(655, 583);
            this.mainPanel.TabIndex = 59;
            // 
            // streamingGrpBox
            // 
            this.streamingGrpBox.Controls.Add(this.lblRemainingEventsValue);
            this.streamingGrpBox.Controls.Add(this.lblRemainingEvents);
            this.streamingGrpBox.Controls.Add(this.collectionIntervalMs);
            this.streamingGrpBox.Controls.Add(this.collectionInterval);
            this.streamingGrpBox.Controls.Add(this.lblCollectionInterval);
            this.streamingGrpBox.Controls.Add(this.pageSize);
            this.streamingGrpBox.Controls.Add(this.pageSizeLabel);
            this.streamingGrpBox.Controls.Add(this.chkBoxLiveStreaming);
            this.streamingGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.streamingGrpBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.streamingGrpBox.Location = new System.Drawing.Point(13, 426);
            this.streamingGrpBox.Name = "streamingGrpBox";
            this.streamingGrpBox.Size = new System.Drawing.Size(639, 105);
            this.streamingGrpBox.TabIndex = 55;
            this.streamingGrpBox.TabStop = false;
            this.streamingGrpBox.Text = "Streaming";
            this.streamingGrpBox.Enter += new System.EventHandler(this.streamingGrpBox_Enter);
            // 
            // lblRemainingEventsValue
            // 
            this.lblRemainingEventsValue.AutoSize = true;
            this.lblRemainingEventsValue.BackColor = System.Drawing.Color.Transparent;
            this.lblRemainingEventsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemainingEventsValue.Location = new System.Drawing.Point(197, 70);
            this.lblRemainingEventsValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRemainingEventsValue.Name = "lblRemainingEventsValue";
            this.lblRemainingEventsValue.Size = new System.Drawing.Size(0, 18);
            this.lblRemainingEventsValue.TabIndex = 42;
            this.lblRemainingEventsValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblRemainingEventsValue.Visible = false;
            // 
            // lblRemainingEvents
            // 
            this.lblRemainingEvents.AutoSize = true;
            this.lblRemainingEvents.BackColor = System.Drawing.Color.Transparent;
            this.lblRemainingEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemainingEvents.Location = new System.Drawing.Point(36, 70);
            this.lblRemainingEvents.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRemainingEvents.Name = "lblRemainingEvents";
            this.lblRemainingEvents.Size = new System.Drawing.Size(127, 18);
            this.lblRemainingEvents.TabIndex = 41;
            this.lblRemainingEvents.Text = "Remaining Events";
            this.lblRemainingEvents.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblRemainingEvents.Visible = false;
            // 
            // collectionIntervalMs
            // 
            this.collectionIntervalMs.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.collectionIntervalMs.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.collectionIntervalMs.Location = new System.Drawing.Point(529, 34);
            this.collectionIntervalMs.Margin = new System.Windows.Forms.Padding(2);
            this.collectionIntervalMs.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.collectionIntervalMs.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.collectionIntervalMs.Name = "collectionIntervalMs";
            this.collectionIntervalMs.Size = new System.Drawing.Size(90, 23);
            this.collectionIntervalMs.TabIndex = 40;
            this.collectionIntervalMs.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.collectionIntervalMs.Visible = false;
            // 
            // collectionInterval
            // 
            this.collectionInterval.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.collectionInterval.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.collectionInterval.Location = new System.Drawing.Point(728, 53);
            this.collectionInterval.Margin = new System.Windows.Forms.Padding(2);
            this.collectionInterval.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.collectionInterval.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.collectionInterval.Name = "collectionInterval";
            this.collectionInterval.Size = new System.Drawing.Size(90, 23);
            this.collectionInterval.TabIndex = 38;
            this.collectionInterval.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.collectionInterval.Visible = false;
            // 
            // lblCollectionInterval
            // 
            this.lblCollectionInterval.AutoSize = true;
            this.lblCollectionInterval.BackColor = System.Drawing.Color.Transparent;
            this.lblCollectionInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollectionInterval.Location = new System.Drawing.Point(408, 33);
            this.lblCollectionInterval.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCollectionInterval.Name = "lblCollectionInterval";
            this.lblCollectionInterval.Size = new System.Drawing.Size(112, 18);
            this.lblCollectionInterval.TabIndex = 39;
            this.lblCollectionInterval.Text = "Frequency (ms)";
            this.lblCollectionInterval.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCollectionInterval.Visible = false;
            // 
            // pageSize
            // 
            this.pageSize.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageSize.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.pageSize.Location = new System.Drawing.Point(292, 34);
            this.pageSize.Margin = new System.Windows.Forms.Padding(2);
            this.pageSize.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.pageSize.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.pageSize.Name = "pageSize";
            this.pageSize.Size = new System.Drawing.Size(90, 23);
            this.pageSize.TabIndex = 37;
            this.pageSize.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.pageSize.Visible = false;
            // 
            // pageSizeLabel
            // 
            this.pageSizeLabel.AutoSize = true;
            this.pageSizeLabel.BackColor = System.Drawing.Color.Transparent;
            this.pageSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageSizeLabel.Location = new System.Drawing.Point(189, 35);
            this.pageSizeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pageSizeLabel.Name = "pageSizeLabel";
            this.pageSizeLabel.Size = new System.Drawing.Size(98, 18);
            this.pageSizeLabel.TabIndex = 36;
            this.pageSizeLabel.Text = "No of Events ";
            this.pageSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.pageSizeLabel.Visible = false;
            // 
            // chkBoxLiveStreaming
            // 
            this.chkBoxLiveStreaming.AutoSize = true;
            this.chkBoxLiveStreaming.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxLiveStreaming.Location = new System.Drawing.Point(21, 33);
            this.chkBoxLiveStreaming.Name = "chkBoxLiveStreaming";
            this.chkBoxLiveStreaming.Size = new System.Drawing.Size(110, 22);
            this.chkBoxLiveStreaming.TabIndex = 0;
            this.chkBoxLiveStreaming.Text = "Stream Data";
            this.chkBoxLiveStreaming.UseVisualStyleBackColor = true;
            this.chkBoxLiveStreaming.CheckedChanged += new System.EventHandler(this.chkBoxLiveStreaming_CheckedChanged);
            // 
            // dsName
            // 
            this.dsName.AutoSize = true;
            this.dsName.BackColor = System.Drawing.Color.Transparent;
            this.dsName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dsName.ForeColor = System.Drawing.Color.White;
            this.dsName.Location = new System.Drawing.Point(10, 15);
            this.dsName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dsName.Name = "dsName";
            this.dsName.Size = new System.Drawing.Size(72, 18);
            this.dsName.TabIndex = 54;
            this.dsName.Text = "Import file";
            this.dsName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // configurationGroup
            // 
            this.configurationGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.configurationGroup.Controls.Add(this.configPanel);
            this.configurationGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationGroup.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.configurationGroup.Location = new System.Drawing.Point(13, 52);
            this.configurationGroup.Name = "configurationGroup";
            this.configurationGroup.Size = new System.Drawing.Size(639, 136);
            this.configurationGroup.TabIndex = 40;
            this.configurationGroup.TabStop = false;
            this.configurationGroup.Text = "System";
            // 
            // configPanel
            // 
            this.configPanel.AutoScroll = true;
            this.configPanel.Controls.Add(this.WhichAFDatabase);
            this.configPanel.Controls.Add(this.chkExistingDB);
            this.configPanel.Controls.Add(this.txtDbName);
            this.configPanel.Controls.Add(this.lblHostName);
            this.configPanel.Controls.Add(this.WhichPISystem);
            this.configPanel.Controls.Add(this.lblSelectConfiguration);
            this.configPanel.Location = new System.Drawing.Point(10, 22);
            this.configPanel.Name = "configPanel";
            this.configPanel.Size = new System.Drawing.Size(572, 110);
            this.configPanel.TabIndex = 37;
            // 
            // WhichAFDatabase
            // 
            this.WhichAFDatabase.AccessibleDescription = "Database Picker";
            this.WhichAFDatabase.AccessibleName = "Database Picker";
            this.WhichAFDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichAFDatabase.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.WhichAFDatabase.Location = new System.Drawing.Point(191, 68);
            this.WhichAFDatabase.Margin = new System.Windows.Forms.Padding(2);
            this.WhichAFDatabase.Name = "WhichAFDatabase";
            this.WhichAFDatabase.ShowBegin = false;
            this.WhichAFDatabase.ShowDelete = false;
            this.WhichAFDatabase.ShowEnd = false;
            this.WhichAFDatabase.ShowFind = false;
            this.WhichAFDatabase.ShowImages = false;
            this.WhichAFDatabase.ShowList = false;
            this.WhichAFDatabase.ShowNavigation = false;
            this.WhichAFDatabase.ShowNew = false;
            this.WhichAFDatabase.ShowNext = false;
            this.WhichAFDatabase.ShowNoEntries = false;
            this.WhichAFDatabase.ShowPrevious = false;
            this.WhichAFDatabase.ShowProperties = false;
            this.WhichAFDatabase.Size = new System.Drawing.Size(227, 25);
            this.WhichAFDatabase.TabIndex = 42;
            this.WhichAFDatabase.Visible = false;
            this.WhichAFDatabase.SelectionChange += new OSIsoft.AF.UI.SelectionChangeEventHandler(this.WhichAFDatabase_SelectionChange);
            // 
            // chkExistingDB
            // 
            this.chkExistingDB.AutoSize = true;
            this.chkExistingDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chkExistingDB.Enabled = false;
            this.chkExistingDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkExistingDB.Location = new System.Drawing.Point(324, 19);
            this.chkExistingDB.Name = "chkExistingDB";
            this.chkExistingDB.Size = new System.Drawing.Size(211, 22);
            this.chkExistingDB.TabIndex = 41;
            this.chkExistingDB.Text = "Upload to existing Database";
            this.chkExistingDB.UseVisualStyleBackColor = true;
            this.chkExistingDB.CheckedChanged += new System.EventHandler(this.chkExistingDB_CheckedChanged);
            // 
            // txtDbName
            // 
            this.txtDbName.BackColor = System.Drawing.SystemColors.Window;
            this.txtDbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDbName.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtDbName.Location = new System.Drawing.Point(192, 68);
            this.txtDbName.Margin = new System.Windows.Forms.Padding(2);
            this.txtDbName.Name = "txtDbName";
            this.txtDbName.Size = new System.Drawing.Size(226, 24);
            this.txtDbName.TabIndex = 39;
            // 
            // lblHostName
            // 
            this.lblHostName.AutoSize = true;
            this.lblHostName.BackColor = System.Drawing.Color.Transparent;
            this.lblHostName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHostName.ForeColor = System.Drawing.Color.White;
            this.lblHostName.Location = new System.Drawing.Point(8, 68);
            this.lblHostName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHostName.Name = "lblHostName";
            this.lblHostName.Size = new System.Drawing.Size(145, 18);
            this.lblHostName.TabIndex = 38;
            this.lblHostName.Text = "AF Database Name :";
            this.lblHostName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WhichPISystem
            // 
            this.WhichPISystem.AccessibleDescription = "PI System Picker";
            this.WhichPISystem.AccessibleName = "PI System Picker";
            this.WhichPISystem.Cursor = System.Windows.Forms.Cursors.Default;
            this.WhichPISystem.EnableDelete = false;
            this.WhichPISystem.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichPISystem.Location = new System.Drawing.Point(96, 16);
            this.WhichPISystem.LoginPromptSetting = OSIsoft.AF.UI.PISystemPicker.LoginPromptSettingOptions.Default;
            this.WhichPISystem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.WhichPISystem.Name = "WhichPISystem";
            this.WhichPISystem.ShowBegin = false;
            this.WhichPISystem.ShowDelete = false;
            this.WhichPISystem.ShowEnd = false;
            this.WhichPISystem.ShowFind = false;
            this.WhichPISystem.ShowImages = false;
            this.WhichPISystem.ShowNavigation = false;
            this.WhichPISystem.ShowNew = false;
            this.WhichPISystem.ShowNext = false;
            this.WhichPISystem.ShowPrevious = false;
            this.WhichPISystem.ShowProperties = false;
            this.WhichPISystem.Size = new System.Drawing.Size(181, 26);
            this.WhichPISystem.TabIndex = 37;
            this.WhichPISystem.ConnectionChange += new OSIsoft.AF.UI.ConnectionChangeEventHandler(this.WhichPISystem_ConnectionChange);
            this.WhichPISystem.Load += new System.EventHandler(this.WhichPISystem_Load);
            // 
            // lblSelectConfiguration
            // 
            this.lblSelectConfiguration.AutoSize = true;
            this.lblSelectConfiguration.BackColor = System.Drawing.Color.Transparent;
            this.lblSelectConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectConfiguration.ForeColor = System.Drawing.Color.White;
            this.lblSelectConfiguration.Location = new System.Drawing.Point(8, 16);
            this.lblSelectConfiguration.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSelectConfiguration.Name = "lblSelectConfiguration";
            this.lblSelectConfiguration.Size = new System.Drawing.Size(83, 18);
            this.lblSelectConfiguration.TabIndex = 21;
            this.lblSelectConfiguration.Text = "PI System :";
            this.lblSelectConfiguration.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // historyGroup
            // 
            this.historyGroup.AutoSize = true;
            this.historyGroup.Controls.Add(this.txtCustomTz);
            this.historyGroup.Controls.Add(this.chkCustomTz);
            this.historyGroup.Controls.Add(this.label2);
            this.historyGroup.Controls.Add(this.cbTimeFormat);
            this.historyGroup.Controls.Add(this.txtEntityIdentifier);
            this.historyGroup.Controls.Add(this.chkMultipleThings);
            this.historyGroup.Controls.Add(this.cbElementTemplateList);
            this.historyGroup.Controls.Add(this.comboBoxTimeIdentifier);
            this.historyGroup.Controls.Add(this.comboBoxEntityIdentifier);
            this.historyGroup.Controls.Add(this.label3);
            this.historyGroup.Controls.Add(this.label4);
            this.historyGroup.Controls.Add(this.txtElmentTemplateName);
            this.historyGroup.Controls.Add(this.label1);
            this.historyGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.historyGroup.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.historyGroup.Location = new System.Drawing.Point(13, 213);
            this.historyGroup.Name = "historyGroup";
            this.historyGroup.Size = new System.Drawing.Size(639, 194);
            this.historyGroup.TabIndex = 41;
            this.historyGroup.TabStop = false;
            this.historyGroup.Text = "Identifiers";
            // 
            // txtCustomTz
            // 
            this.txtCustomTz.Location = new System.Drawing.Point(202, 108);
            this.txtCustomTz.Name = "txtCustomTz";
            this.txtCustomTz.Size = new System.Drawing.Size(226, 23);
            this.txtCustomTz.TabIndex = 37;
            this.txtCustomTz.Visible = false;
            // 
            // chkCustomTz
            // 
            this.chkCustomTz.AutoSize = true;
            this.chkCustomTz.Location = new System.Drawing.Point(452, 111);
            this.chkCustomTz.Name = "chkCustomTz";
            this.chkCustomTz.Size = new System.Drawing.Size(157, 21);
            this.chkCustomTz.TabIndex = 36;
            this.chkCustomTz.Text = "Custom Time Format";
            this.chkCustomTz.UseVisualStyleBackColor = true;
            this.chkCustomTz.CheckedChanged += new System.EventHandler(this.chkCustomTz_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(18, 109);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 18);
            this.label2.TabIndex = 35;
            this.label2.Text = "Time Format :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbTimeFormat
            // 
            this.cbTimeFormat.FormattingEnabled = true;
            this.cbTimeFormat.Location = new System.Drawing.Point(202, 108);
            this.cbTimeFormat.Name = "cbTimeFormat";
            this.cbTimeFormat.Size = new System.Drawing.Size(225, 24);
            this.cbTimeFormat.TabIndex = 34;
            // 
            // txtEntityIdentifier
            // 
            this.txtEntityIdentifier.Location = new System.Drawing.Point(201, 144);
            this.txtEntityIdentifier.Name = "txtEntityIdentifier";
            this.txtEntityIdentifier.Size = new System.Drawing.Size(226, 23);
            this.txtEntityIdentifier.TabIndex = 33;
            this.txtEntityIdentifier.Visible = false;
            // 
            // chkMultipleThings
            // 
            this.chkMultipleThings.AutoSize = true;
            this.chkMultipleThings.Checked = true;
            this.chkMultipleThings.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMultipleThings.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkMultipleThings.Location = new System.Drawing.Point(451, 143);
            this.chkMultipleThings.Name = "chkMultipleThings";
            this.chkMultipleThings.Size = new System.Drawing.Size(127, 22);
            this.chkMultipleThings.TabIndex = 32;
            this.chkMultipleThings.Text = "Multiple entities";
            this.chkMultipleThings.UseVisualStyleBackColor = true;
            this.chkMultipleThings.CheckedChanged += new System.EventHandler(this.chkMultipleThings_CheckedChanged);
            // 
            // cbElementTemplateList
            // 
            this.cbElementTemplateList.FormattingEnabled = true;
            this.cbElementTemplateList.Location = new System.Drawing.Point(202, 34);
            this.cbElementTemplateList.Name = "cbElementTemplateList";
            this.cbElementTemplateList.Size = new System.Drawing.Size(226, 24);
            this.cbElementTemplateList.TabIndex = 31;
            this.cbElementTemplateList.Visible = false;
            // 
            // comboBoxTimeIdentifier
            // 
            this.comboBoxTimeIdentifier.FormattingEnabled = true;
            this.comboBoxTimeIdentifier.Location = new System.Drawing.Point(201, 71);
            this.comboBoxTimeIdentifier.Name = "comboBoxTimeIdentifier";
            this.comboBoxTimeIdentifier.Size = new System.Drawing.Size(226, 24);
            this.comboBoxTimeIdentifier.TabIndex = 30;
            this.comboBoxTimeIdentifier.SelectedIndexChanged += new System.EventHandler(this.comboBoxTimeIdentifier_SelectedIndexChanged);
            // 
            // comboBoxEntityIdentifier
            // 
            this.comboBoxEntityIdentifier.FormattingEnabled = true;
            this.comboBoxEntityIdentifier.Location = new System.Drawing.Point(201, 143);
            this.comboBoxEntityIdentifier.Name = "comboBoxEntityIdentifier";
            this.comboBoxEntityIdentifier.Size = new System.Drawing.Size(226, 24);
            this.comboBoxEntityIdentifier.TabIndex = 29;
            this.comboBoxEntityIdentifier.SelectedIndexChanged += new System.EventHandler(this.comboBoxEntityIdentifier_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(18, 139);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 18);
            this.label3.TabIndex = 25;
            this.label3.Text = "Entity Identifier :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(18, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 18);
            this.label4.TabIndex = 27;
            this.label4.Text = "Time Identifier :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtElmentTemplateName
            // 
            this.txtElmentTemplateName.BackColor = System.Drawing.SystemColors.Window;
            this.txtElmentTemplateName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtElmentTemplateName.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtElmentTemplateName.Location = new System.Drawing.Point(201, 34);
            this.txtElmentTemplateName.Margin = new System.Windows.Forms.Padding(2);
            this.txtElmentTemplateName.Name = "txtElmentTemplateName";
            this.txtElmentTemplateName.Size = new System.Drawing.Size(227, 24);
            this.txtElmentTemplateName.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(18, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 18);
            this.label1.TabIndex = 23;
            this.label1.Text = "Element Template Name: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Silver;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSave.Location = new System.Drawing.Point(13, 536);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(176, 39);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Upload";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.Color.Silver;
            this.btnPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPause.FlatAppearance.BorderSize = 0;
            this.btnPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPause.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPause.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPause.Location = new System.Drawing.Point(242, 536);
            this.btnPause.Margin = new System.Windows.Forms.Padding(2);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(176, 39);
            this.btnPause.TabIndex = 56;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = false;
            this.btnPause.Visible = false;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Silver;
            this.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Trebuchet MS", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReset.Location = new System.Drawing.Point(476, 536);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(176, 39);
            this.btnReset.TabIndex = 57;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // UtilityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(679, 598);
            this.Controls.Add(this.mainPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UtilityForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PI Data Uploader";
            this.Load += new System.EventHandler(this.Form_Load);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.streamingGrpBox.ResumeLayout(false);
            this.streamingGrpBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.collectionIntervalMs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.collectionInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageSize)).EndInit();
            this.configurationGroup.ResumeLayout(false);
            this.configPanel.ResumeLayout(false);
            this.configPanel.PerformLayout();
            this.historyGroup.ResumeLayout(false);
            this.historyGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Button btnImportFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Label dsName;
        private System.Windows.Forms.GroupBox configurationGroup;
        private System.Windows.Forms.GroupBox historyGroup;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel configPanel;
        private OSIsoft.AF.UI.PISystemPicker WhichPISystem;
        private System.Windows.Forms.Label lblSelectConfiguration;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtElmentTemplateName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDbName;
        private System.Windows.Forms.Label lblHostName;
        private System.Windows.Forms.ComboBox comboBoxTimeIdentifier;
        private System.Windows.Forms.ComboBox comboBoxEntityIdentifier;
        private System.Windows.Forms.CheckBox chkExistingDB;
        private OSIsoft.AF.UI.AFDatabasePicker WhichAFDatabase;
        private System.Windows.Forms.ComboBox cbElementTemplateList;
        private System.Windows.Forms.CheckBox chkMultipleThings;
        private System.Windows.Forms.TextBox txtEntityIdentifier;
        private System.Windows.Forms.ComboBox cbTimeFormat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCustomTz;
        private System.Windows.Forms.CheckBox chkCustomTz;
        private System.Windows.Forms.GroupBox streamingGrpBox;
        private System.Windows.Forms.NumericUpDown collectionInterval;
        private System.Windows.Forms.Label lblCollectionInterval;
        private System.Windows.Forms.NumericUpDown pageSize;
        private System.Windows.Forms.Label pageSizeLabel;
        private System.Windows.Forms.CheckBox chkBoxLiveStreaming;
        private System.Windows.Forms.NumericUpDown collectionIntervalMs;
        private System.Windows.Forms.Label lblRemainingEvents;
        private System.Windows.Forms.Label lblRemainingEventsValue;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnPause;
    }
}

