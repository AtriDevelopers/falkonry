﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PI_Utility
{
    class AttributeValue
    {
        public AttributeValue(DateTime Time, string Value)
        {
            this.Time = Time;
            this.Value = Value;
        }

        public DateTime Time
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }
    }
}
