﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.EventFrame;
using OSIsoft.AF.UI;
using OSIsoft.AF.Time;
using OSIsoft.AF.Data;
using OSIsoft.AF.PI;
using System.Globalization;
using System.Reflection;
using System.Collections;
using System.Threading;

namespace PI_Utility
{
    public partial class UtilityForm : Form
    {
        private static string[] headers;
        CsvReader csv;
        string dbName = "";
        int entityIdenColIndex = -1, timeIdenColIndex = -1;

        Hashtable liveStreamingProperties = new Hashtable();

        string[] timeFormatStrings = new string[] {
            "Unix Time Seconds",
            "Unix Time Milliseconds",
            "yyyy-MM-ddTHH:mm:ss",
            "yyyy-MM-ddTHH:mm:ssZ",
            "MM/dd/yyyy hh:mm:ss",
            "dd/MM/yyyy hh:mm:ss",
            "yyyy-MM-dd hh:mm:ss" };
        private static string Delimiter = "#@#";


        List<int> attributesColList = new List<int>();
        string[] firstRow;
        IDictionary<string, Assembly> _libs = new Dictionary<string, Assembly>();
        private volatile bool _shouldStop;

        Thread uploadStreamThread;
        ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        ManualResetEvent _pauseEvent = new ManualResetEvent(true);

        public UtilityForm()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            InitializeComponent();
        }

        // dll handler
        System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string keyName = new AssemblyName(args.Name).Name;

            // If DLL is loaded then don't load it again just return
            if (_libs.ContainsKey(keyName)) return _libs[keyName];

            using (Stream stream = Assembly.GetExecutingAssembly()
                   .GetManifestResourceStream(GetDllResourceName("LumenWorks.Framework.IO.dll")))  // <-- To find out the Namespace name go to Your Project >> Properties >> Application >> Default namespace
            {
                byte[] buffer = new BinaryReader(stream).ReadBytes((int)stream.Length);
                Assembly assembly = Assembly.Load(buffer);
                _libs[keyName] = assembly;
                return assembly;
            }
        }

        private string GetDllResourceName(string dllName)
        {
            string resourceName = string.Empty;
            foreach (string name in Assembly.GetExecutingAssembly().GetManifestResourceNames())
            {
                if (name.EndsWith(dllName))
                {
                    resourceName = name;
                    break;
                }
            }

            return resourceName;
        }

        private void Form_Load(object sender, EventArgs e)
        {

            for (int i = 0; i < timeFormatStrings.Count(); i++)
            {
                cbTimeFormat.Items.Add(timeFormatStrings[i]);
            }

        }

        private void btnImportFile_Click(object sender, EventArgs e)
        {
            comboBoxTimeIdentifier.Text = "";
            comboBoxEntityIdentifier.Text = "";
            cbElementTemplateList.Text = "";
            comboBoxEntityIdentifier.Items.Clear();
            comboBoxTimeIdentifier.Items.Clear();
            cbElementTemplateList.Items.Clear();


            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFileName.Text = openFileDialog1.FileName;
                csv = new CsvReader(new StreamReader(openFileDialog1.FileName), true);
                int fieldCount = csv.FieldCount;
                headers = csv.GetFieldHeaders();
                for (int i = 0; i < fieldCount; i++)
                {
                    comboBoxEntityIdentifier.Items.Add(headers[i]);
                    comboBoxTimeIdentifier.Items.Add(headers[i]);
                }
                if (txtDbName.Text != "" || WhichAFDatabase.Name != "")
                {
                    WhichAFDatabase_SelectionChange(null, null);
                }
            }
        }

        private void btnSave_ShowLoading()
        {
            if (chkBoxLiveStreaming.Checked)
            {
                btnSave.Text = "Streaming Data...";
            }
            else
            {
                btnSave.Text = "Uploading...";
            }
            btnSave.Enabled = false;
            btnPause.Show();
            btnReset.Show();

        }

        private void btnSave_HideLoading()
        {

            btnSave.Invoke((MethodInvoker)delegate
            {
                // Running on the UI thread
                btnSave.Text = "Upload";
                btnSave.Enabled = true;
                btnPause.Hide();
                btnReset.Hide();

            });

        }

        public void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string elementTemplateName = "", entityIdentifier = "";
                _shouldStop = false;
                btnPause.Text = "Pause";
                if (sender != null)
                {
                    liveStreamingProperties["isLiveStreaming"] = false;
                }
                bool result = false;
                //btnSave_ShowLoading();

                bool isExistingDB = false;
                WhichPISystem.Invoke((MethodInvoker)delegate
                {
                    if (!WhichPISystem.PISystem.ConnectionInfo.IsConnected)
                    {
                        MessageBox.Show("PI System not connected, please connect to PI System.");
                        //btnSave_HideLoading();
                        return;
                    }
                });

                if (chkExistingDB.Checked)
                {
                    isExistingDB = true;
                }

                if (isExistingDB)
                {
                    dbName = WhichAFDatabase.AFDatabase.Name;
                    elementTemplateName = cbElementTemplateList.Text;
                }
                else
                {
                    dbName = txtDbName.Text;
                    elementTemplateName = txtElmentTemplateName.Text;
                }
                if (dbName == "" || elementTemplateName == "" || comboBoxTimeIdentifier.Text == "" || (!chkCustomTz.Checked && cbTimeFormat.Text == "") || (chkMultipleThings.Checked && comboBoxEntityIdentifier.Text == ""))
                {
                    MessageBox.Show("Please fill all the inputs.");
                    btnSave_HideLoading();
                    return;
                }
                if (chkCustomTz.Checked)
                {
                    if (txtCustomTz.Text == "")
                    {
                        MessageBox.Show("Please fill all the inputs.");
                        btnSave_HideLoading();
                        return;
                    }
                }

                if (!chkMultipleThings.Checked)
                {
                    if (txtEntityIdentifier.Text == "")
                    {
                        MessageBox.Show("Please fill all the inputs.");
                        btnSave_HideLoading();
                        return;
                    }
                    else
                    {
                        entityIdentifier = txtEntityIdentifier.Text;
                    }
                }
                else
                {
                    entityIdentifier = comboBoxEntityIdentifier.Text;
                    if (entityIdenColIndex == -1)
                    {
                        MessageBox.Show("Entity Identifier is not available in file, please select another entity identifier");
                        btnSave_HideLoading();
                        return;
                    }
                }
                if (timeIdenColIndex == -1)
                {
                    MessageBox.Show("Selected Time identifer is not available in file, please select a different Time Identifier.");
                    btnSave_HideLoading();
                    return;
                }
                if (chkBoxLiveStreaming.Checked)
                {
                    if (!isExistingDB)
                    {
                        MessageBox.Show("For live streaming please selecting existing database option.");
                        btnSave_HideLoading();
                        return;
                    }
                    if (sender != null)
                    {

                        liveStreamingProperties["eventsToStream"] = (int)pageSize.Value;
                        liveStreamingProperties["frequencyOfEventsToStream"] = (int)collectionIntervalMs.Value; ;
                        liveStreamingProperties["completedStreamingRows"] = 0;
                        liveStreamingProperties["rowstoStreamAtaTime"] = 1;
                        liveStreamingProperties["currentTime"] = DateTime.ParseExact(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture), "yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);
                        liveStreamingProperties["isLiveStreaming"] = true;
                        lblRemainingEvents.Visible = false;
                        lblRemainingEventsValue.Visible = false;
                    }

                }

                var myPISystem = new PISystems()[WhichPISystem.PISystem.Name];
                myPISystem.Connect();
                AFDatabase myDB = myPISystem.Databases[dbName];
                if (myDB == null)
                {
                    myPISystem.Databases.Add(dbName);
                    myDB = myPISystem.Databases[dbName];
                    myDB.CheckIn();
                    myDB.Refresh();
                }


                // Create an Element Template
                AFElementTemplate myElemTemplate = myDB.ElementTemplates[elementTemplateName];
                if (myElemTemplate == null)
                {
                    myDB.ElementTemplates.Add(elementTemplateName);
                    myElemTemplate = myDB.ElementTemplates[elementTemplateName];
                    myDB.CheckIn();
                    myDB.Refresh();

                }
                string comboBoxTimeIdentifierText = comboBoxTimeIdentifier.Text;


                PIServers piServers = new PIServers();

                PIServer piServer = piServers[WhichPISystem.PISystem.Name];


                uploadStreamThread = new Thread(delegate ()
                {
                    uploadOrStream(sender, entityIdentifier, myElemTemplate, myDB, comboBoxTimeIdentifierText, piServer);
                });
                btnSave_ShowLoading();

                uploadStreamThread.Start();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

        }


        public void uploadOrStream(object sender, string entityIdentifier, AFElementTemplate myElemTemplate, AFDatabase myDB, string comboBoxTimeIdentifierText, PIServer piServer)
        {
            try
            {
                bool exitThread = false;
                int rowCount = 0;
                while (!_shouldStop)
                {
                    _pauseEvent.WaitOne(Timeout.Infinite);
                    bool result = false;
                    csv = new CsvReader(new StreamReader(openFileDialog1.FileName), true);
                    firstRow = new string[headers.Count()];
                    attributesColList.Clear();
                    Hashtable Thing_AttributeMap = new Hashtable();

                    if (csv.ReadNextRecord())
                    {
                        rowCount++;
                        for (int i = 0; i < headers.Count(); i++)
                        {
                            firstRow[i] = csv[i];
                            _pauseEvent.WaitOne(Timeout.Infinite);
                            if (_shouldStop)
                            {
                                exitThread = true;
                                break;
                            }
                            if (headers[i] != entityIdentifier && headers[i] != comboBoxTimeIdentifierText)
                            {
                                AFAttributeTemplate myAttrTemplate = myElemTemplate.AttributeTemplates[headers[i]];
                                if (myAttrTemplate == null)
                                {
                                    myElemTemplate.AttributeTemplates.Add(headers[i]);
                                    myAttrTemplate = myElemTemplate.AttributeTemplates[headers[i]];
                                    myAttrTemplate.Type = csv[i].GetType();
                                    double attValue = 0;
                                    myAttrTemplate.DataReferencePlugIn = myDB.PISystem.DataReferencePlugIns["PI Point"];
                                    if (Double.TryParse(csv[i], out attValue))
                                    {
                                        myAttrTemplate.Type = typeof(double);
                                        myAttrTemplate.ConfigString = $"{dbName}.%Element%.%Attribute%;ReadOnly=False;ptclassname=base;pointtype=Float64";
                                    }
                                    else
                                    {
                                        myAttrTemplate.Type = typeof(string);
                                        myAttrTemplate.ConfigString = $"{dbName}.%Element%.%Attribute%;ReadOnly=False;ptclassname=base;pointtype=String";
                                    }

                                    myAttrTemplate.ElementTemplate.CheckIn();
                                    myDB.Refresh();
                                }
                                attributesColList.Add(i);
                            }
                            if (headers[i] == comboBoxTimeIdentifierText)
                            {
                                DateTime stDateTime = DateTime.Now;

                                if (!parseDateString(firstRow[i], ref stDateTime, (bool)liveStreamingProperties["isLiveStreaming"]))
                                {
                                    btnSave_HideLoading();
                                    return;
                                }
                            }
                        }
                    }
                    
                    using (csv)
                    {
                        if ((bool)liveStreamingProperties["isLiveStreaming"])
                        {
                            liveStreamingProperties["completedStreamingRows"] = (int)liveStreamingProperties["completedStreamingRows"] + 1;
                            liveStreamingProperties["currentTime"] = ((DateTime)liveStreamingProperties["currentTime"]).AddMilliseconds((int)liveStreamingProperties["frequencyOfEventsToStream"]);
                        }
                        //FirstRowAfterHeader
                        int attributeCount = 0;
                        Thing_AttributeMap.Clear();

                        if (!updateAttribute(entityIdentifier, myElemTemplate, ref Thing_AttributeMap, ref attributeCount, ref liveStreamingProperties, true))
                        {
                            btnSave_HideLoading();
                            if (WhichAFDatabase.AFDatabase.IsDirty)
                                WhichAFDatabase.AFDatabase.UndoCheckOut(true);
                            return;
                        }
                        while (csv.ReadNextRecord())
                        {
                            rowCount++;
                            _pauseEvent.WaitOne();
                            if (_shouldStop)
                            {
                                exitThread = true;
                                break;
                            }

                            if ((bool)liveStreamingProperties["isLiveStreaming"])
                            {
                                liveStreamingProperties["completedStreamingRows"] = (int)liveStreamingProperties["completedStreamingRows"] + 1;
                                liveStreamingProperties["currentTime"] = ((DateTime)liveStreamingProperties["currentTime"]).AddMilliseconds((int)liveStreamingProperties["frequencyOfEventsToStream"]);
                            }
                            if (!updateAttribute(entityIdentifier, myElemTemplate, ref Thing_AttributeMap, ref attributeCount, ref liveStreamingProperties, false))
                            {
                                btnSave_HideLoading();
                                if (WhichAFDatabase.AFDatabase.IsDirty)
                                    WhichAFDatabase.AFDatabase.UndoCheckOut(true);
                                return;
                            }
                            if (attributeCount > 10000 || ((bool)liveStreamingProperties["isLiveStreaming"] && attributeCount >= (int)liveStreamingProperties["rowstoStreamAtaTime"]))
                            {
                                if (!updateAttributesFromMap(myElemTemplate, ref Thing_AttributeMap, myDB, piServer))
                                {
                                    result = false;
                                    break;
                                }
                                else
                                {
                                    if (_shouldStop)
                                    {
                                        exitThread = true;
                                        break;
                                    }
                                    attributeCount = 0;
                                    result = true;
                                    if ((bool)liveStreamingProperties["isLiveStreaming"])
                                    {
                                        lblRemainingEventsValue.Invoke((MethodInvoker)delegate
                                        {
                                            lblRemainingEventsValue.Text = ((int)liveStreamingProperties["eventsToStream"] - (int)liveStreamingProperties["completedStreamingRows"]).ToString();
                                        });
                                        if (rowCount >= (int)liveStreamingProperties["eventsToStream"])
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (_shouldStop)
                        {
                            exitThread = true;
                            break;
                        }
                    }
                    if (Thing_AttributeMap.Count > 0)
                    {
                        if (!updateAttributesFromMap(myElemTemplate, ref Thing_AttributeMap, myDB, piServer))
                            result = false;
                        else
                            result = true;
                    }

                    if (!_shouldStop && result)
                    {
                        if (!(bool)liveStreamingProperties["isLiveStreaming"])
                        {
                            MessageBox.Show($"Data has been successfully uploaded to {dbName} Database");
                            exitThread = true;
                            btnSave_HideLoading();
                            break;
                        }
                        else
                        {
                            MessageBox.Show($"Data has been successfully streamed to {dbName} Database");
                            exitThread = true;
                            btnSave_HideLoading();
                            break;


                            //if (streamingLimitReached)
                            //{
                            //    MessageBox.Show($"Data has been successfully streamed to {dbName} Database");
                            //    exitThread = true;
                            //    btnSave_HideLoading();
                            //    break;
                            //}
                            //else
                            //{
                            //    btnSave_Click(null, null);
                            //}
                        }

                    }

                    if (_shutdownEvent.WaitOne(0) || exitThread)
                        _shouldStop = true;

                    // Do the work..
                    Console.WriteLine("Thread is running");
                }

                Console.WriteLine("Exited from thread.");
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Exception - {exception.Message}");
                btnSave_HideLoading();
            }

        }



        public bool updateAttributesFromMap(AFElementTemplate myElemTemplate, ref Hashtable Thing_AttributeMap, AFDatabase myDB, PIServer piServer)
        {
            String catchString = "";
            try
            {
                bool result = true;
                var listOfAfValues = new AFValues();

                foreach (DictionaryEntry AttributeMapPair in Thing_AttributeMap)
                {
                    string mapKey = (string)AttributeMapPair.Key;
                    string[] mapKeyStrArray = mapKey.Split(new[] { Delimiter }, StringSplitOptions.None);
                    string elementName = mapKeyStrArray[0];
                    string attributeName = mapKeyStrArray[1];

                    AFElement myElement = myDB.Elements[elementName];
                    if (myElement == null)
                    {
                        myElement = myDB.Elements.Add(elementName, myElemTemplate);
                        myDB.CheckIn();
                        myDB.Refresh();
                    }

                    List<AttributeValue> mapValue = (List<AttributeValue>)AttributeMapPair.Value;
                    foreach (AttributeValue attributeValueObject in mapValue)
                    {
                        listOfAfValues.Add(new AFValue(attributeValueObject.Value, attributeValueObject.Time));
                    }

                    //*****************
                    AFAttributeTemplate attributeTemplate = myDB.ElementTemplates[myElemTemplate.Name].AttributeTemplates[attributeName];
                    if (attributeTemplate == null)
                    {
                        attributeTemplate = myDB.ElementTemplates[myElemTemplate.Name].AttributeTemplates.Add(attributeName);
                    }

                    attributeTemplate.DataReferencePlugIn = myDB.PISystem.DataReferencePlugIns["PI Point"];

                    int i = 0;
                    while (listOfAfValues[i].Value.ToString() == "")
                    {
                        i++;
                    }
                    if (listOfAfValues.Count > 0)
                    {
                        double n;
                        var isFloat = Double.TryParse(listOfAfValues[i].Value.ToString(), out n);
                        if (isFloat)
                        {
                            attributeTemplate.ConfigString = $"{dbName}.%Element%.%Attribute%;ReadOnly=False;ptclassname=base;pointtype=Float64";
                        }
                        else
                        {
                            attributeTemplate.ConfigString = $"{dbName}.%Element%.%Attribute%;ReadOnly=False;ptclassname=base;pointtype=String";
                        }
                        catchString = listOfAfValues[i].Value.ToString();
                        listOfAfValues.Clear();
                    }
                    else
                    {
                        continue;
                    }
                    attributeTemplate.ElementTemplate.CheckIn();

                    myDB.Refresh();
                    var foundAttributes = AFElement.FindElementsByTemplate(myDB, null, attributeTemplate.ElementTemplate, true, AFSortField.Name, AFSortOrder.Ascending, 10000).Select(elem => elem.Attributes[attributeTemplate.Name]);
                    if (foundAttributes.Count() == 0)
                    {
                        MessageBox.Show("Did not find any instances of the AF Attribute created, cannot create underlying PI Points.", "Unable to create PI Points.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return result;
                    }
                    for (int j = 0; j < foundAttributes.Count(); j++)
                    {
                        try
                        {
                            foundAttributes.ElementAt(j).DataReference.CreateConfig();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }

                    myDB.CheckIn();
                    string attributeTag = dbName + "." + elementName + "." + attributeName;

                    IList<PIPoint> points = PIPoint.FindPIPoints(piServer, new[] { attributeTag });
                    PIPoint digitalPIPoint = points[0];

                    digitalPIPoint.SetAttribute(PICommonPointAttributes.Compressing, false);
                    digitalPIPoint.SetAttribute(PICommonPointAttributes.Step, true);
                    digitalPIPoint.SetAttribute(PICommonPointAttributes.Archiving, true);
                    digitalPIPoint.SetAttribute(PICommonPointAttributes.ExceptionDeviation, 0);
                    digitalPIPoint.SetAttribute(PICommonPointAttributes.CompressionDeviation, 0.23);
                    digitalPIPoint.SaveAttributes();

                    AFElement.LoadAttributes(new[] { myElement }, new[] { attributeTemplate });

                    IList<AFValue> valuesToWrite = new List<AFValue>();
                    foreach (AttributeValue attributeValueObject in mapValue)
                    {
                        AFValue afValueDigital = new AFValue(attributeValueObject.Value, attributeValueObject.Time);
                        afValueDigital.PIPoint = digitalPIPoint;
                        valuesToWrite.Add(afValueDigital);
                    }
                    _pauseEvent.WaitOne(Timeout.Infinite);
                    if (_shouldStop)
                    {
                        break;
                    }
                    piServer.UpdateValues(valuesToWrite, AFUpdateOption.InsertNoCompression, AFBufferOption.BufferIfPossible);
                    //*****************
                }
                Thing_AttributeMap.Clear();

                return result;
            }
            catch (Exception exception)
            {

                MessageBox.Show($"Exception while updating attribute from map, Message - {catchString}{exception.Message}");
                return false;
            }
        }

        public bool parseDateString(string dateString, ref DateTime dtDateTime, bool isLive)
        {
            if (isLive)
            {
                try
                {
                    dtDateTime = DateTime.Parse(dateString);
                }
                catch (Exception)
                {


                }
                return true;
            }
            bool result = false;
            bool timeFormatMatched = false;
            if (chkCustomTz.Checked)
            {
                if (DateTime.TryParseExact(dateString, txtCustomTz.Text, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtDateTime))
                {
                    result = true;
                }
                else
                {
                    MessageBox.Show("Time format is different from file, please select proper time format");
                }
                return result;
            }
            string timeFormat = "";
            cbTimeFormat.Invoke((MethodInvoker)delegate
            {
                timeFormat = cbTimeFormat.Text;
            });
            if (timeFormat == timeFormatStrings[0])
            {
                long lngDateTime = 0;
                timeFormatMatched = true;
                if (long.TryParse(dateString, out lngDateTime))
                {
                    var sdDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    lngDateTime *= 1000;
                    dtDateTime = sdDateTime.AddMilliseconds(lngDateTime).ToLocalTime();
                    result = true;
                    return result;
                }
                else
                {
                    MessageBox.Show("Time format is different from file, please select proper time format");
                }
            }
            else if (timeFormat == timeFormatStrings[1])
            {
                long lngDateTime = 0;
                timeFormatMatched = true;
                if (long.TryParse(dateString, out lngDateTime))
                {
                    var sdDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    dtDateTime = sdDateTime.AddMilliseconds(lngDateTime).ToLocalTime();
                    result = true;
                    return result;
                }
                else
                {
                    MessageBox.Show("Time format is different from file, please select proper time format");

                }
            }
            else
            {
                for (int it = 2; it < timeFormatStrings.Count(); it++)
                {
                    if (timeFormat == timeFormatStrings[it])
                    {
                        timeFormatMatched = true;
                        if (DateTime.TryParseExact(dateString, timeFormatStrings[it], CultureInfo.InvariantCulture, DateTimeStyles.None, out dtDateTime))
                        {
                            result = true;
                        }
                        else
                        {
                            MessageBox.Show("Time format is different from file, please select proper time format");
                        }
                        break;
                    }
                }
            }
            if (!timeFormatMatched)
                MessageBox.Show("Time format is different from file, please select proper time format");
            return result;
        }

        public bool ParseRequestDate(string dateString, ref string dateTimeFormat, ref DateTime dtDateTime)
        {
            bool result = false;
            // Scenario #1
            long lngDateTime = 0;
            if (long.TryParse(dateString, out lngDateTime))
            {
                var sdDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

                if (dateString.Count() < 11)
                {
                    lngDateTime *= 1000;
                    dateTimeFormat = timeFormatStrings[1];

                }
                else
                {
                    dateTimeFormat = timeFormatStrings[2];
                }
                dtDateTime = sdDateTime.AddMilliseconds(lngDateTime).ToLocalTime();
                result = true;
                return result;
            }
            // Scenario #2
            for (int it = 2; it < timeFormatStrings.Count(); it++)
            {
                if (DateTime.TryParseExact(dateString, timeFormatStrings[it], CultureInfo.InvariantCulture, DateTimeStyles.None, out dtDateTime))
                {
                    dateTimeFormat = timeFormatStrings[it];
                    return result;
                }
            }

            return result;

        }

        private bool updateAttribute(string entityIdentifier, AFElementTemplate myElemTemplate, ref Hashtable Thing_AttributeMap, ref int attributeCount, ref Hashtable liveStreamingProperties, bool isFirstRow = false)
        {
            string
                elementName = "",
                elementNameValue = "",
                time = "",
                attributeValue = "";
            bool result = true;
            try
            {

                if (chkMultipleThings.Checked)
                {
                    if (isFirstRow)
                    {
                        elementNameValue = firstRow[entityIdenColIndex];
                        time = firstRow[timeIdenColIndex];
                    }
                    else
                    {
                        elementNameValue = csv[entityIdenColIndex];
                        time = csv[timeIdenColIndex];
                    }
                    elementName = elementNameValue;
                }
                else
                {
                    elementName = entityIdentifier;
                    if (isFirstRow)
                    {
                        time = firstRow[timeIdenColIndex];
                    }
                    else
                    {
                        time = csv[timeIdenColIndex];
                    }
                }
                if ((bool)liveStreamingProperties["isLiveStreaming"])
                {
                    // change time value
                    time = ((DateTime)liveStreamingProperties["currentTime"]).ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                }
                var stDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                if (!parseDateString(time, ref stDateTime, (bool)liveStreamingProperties["isLiveStreaming"]))
                {
                    result = false;
                    return result;
                }
                for (int i = 0; i < attributesColList.Count; i++)
                {
                    if (isFirstRow)
                        attributeValue = firstRow[attributesColList[i]];
                    else
                        attributeValue = csv[attributesColList[i]];

                    AttributeValue attributeValueObj = new AttributeValue(Time: stDateTime, Value: attributeValue);

                    if (Thing_AttributeMap.ContainsKey(elementName + "#@#" + headers[attributesColList[i]]))
                    {
                        ((List<AttributeValue>)Thing_AttributeMap[elementName + "#@#" + headers[attributesColList[i]]]).Add(attributeValueObj);
                    }
                    else
                    {
                        List<AttributeValue> MapValueAttributeList = new List<AttributeValue>();
                        MapValueAttributeList.Add(attributeValueObj);
                        Thing_AttributeMap.Add(elementName + "#@#" + headers[attributesColList[i]], MapValueAttributeList);
                    }
                    attributeCount++;
                }
                if ((bool)liveStreamingProperties["isLiveStreaming"])
                {
                    Thread.Sleep((int)liveStreamingProperties["frequencyOfEventsToStream"]);
                }
                return result;
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Exception while updating value in attribute, Message - {exception.Message}");
                result = false;
                return result;
            }

        }

        private void clearForm()
        {
            txtFileName.Text = "";
            txtEntityIdentifier.Text = "";
            txtElmentTemplateName.Text = "";
            txtDbName.Text = "";
            comboBoxTimeIdentifier.Text = "";
            comboBoxEntityIdentifier.Text = "";
            cbElementTemplateList.Text = "";
            comboBoxEntityIdentifier.Items.Clear();
            comboBoxTimeIdentifier.Items.Clear();
            cbElementTemplateList.Items.Clear();
            attributesColList.Clear();
            cbTimeFormat.Text = "";
            txtCustomTz.Text = "";
            WhichAFDatabase.ClearCollection();
            chkExistingDB_CheckedChanged(null, null);
            entityIdenColIndex = -1;
            timeIdenColIndex = -1;
        }

        private void comboBoxEntityIdentifier_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxTimeIdentifier.Items.Clear();
            entityIdenColIndex = -1;
            for (int i = 0; i < headers.Count(); i++)
            {
                if (headers[i] != comboBoxEntityIdentifier.SelectedItem.ToString())
                {
                    comboBoxTimeIdentifier.Items.Add(headers[i]);
                }
                else
                {
                    entityIdenColIndex = i;
                }
            }
            if (entityIdenColIndex == -1)
            {
                MessageBox.Show("Selected Entity identifer is not available in file, please select a different Entity Identifier.");
            }
        }

        private void chkMultipleThings_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMultipleThings.Checked)
            {
                txtEntityIdentifier.Visible = false;
                comboBoxEntityIdentifier.Visible = true;
            }
            else
            {
                txtEntityIdentifier.Visible = true;
                comboBoxEntityIdentifier.Visible = false;
            }
        }

        private void comboBoxTimeIdentifier_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxEntityIdentifier.Items.Clear();
            timeIdenColIndex = -1;
            for (int i = 0; i < headers.Count(); i++)
            {
                if (headers[i] != comboBoxTimeIdentifier.SelectedItem.ToString())
                {
                    comboBoxEntityIdentifier.Items.Add(headers[i]);
                }
                else
                {
                    timeIdenColIndex = i;
                }
            }
            if (timeIdenColIndex == -1)
            {
                MessageBox.Show("Selected Time identifer is not available in file, please select a different Time Identifier.");
            }
        }

        private void CheckPiSystemConnection()
        {
            WhichPISystem.Refresh();
            if (!WhichPISystem.PISystem.ConnectionInfo.IsConnected)
                WhichPISystem.PISystem.Connect(true, Owner);
            WhichPISystem.PISystem.Refresh();
            WhichAFDatabase.PISystem = WhichPISystem.PISystem;
            WhichAFDatabase.Refresh();
        }
        private void chkExistingDB_CheckedChanged(object sender, EventArgs e)
        {
            if (!WhichPISystem.PISystem.ConnectionInfo.IsConnected)
            {
                MessageBox.Show("PI System not connected, please connect to PI System.");
                chkExistingDB.Enabled = false;
                return;
            }

            if (chkExistingDB.Checked)
            {
                WhichAFDatabase.Visible = true;
                txtDbName.Visible = false;
                WhichAFDatabase.PISystem = WhichPISystem.PISystem;
                WhichAFDatabase.Refresh();
                //dbName = WhichPISystem.PISystems.DefaultPISystem.Databases.DefaultDatabase.Name;
                WhichAFDatabase.AFDatabase = WhichAFDatabase.AFDatabases[WhichPISystem.PISystems.DefaultPISystem.Databases.DefaultDatabase.Name];
                cbElementTemplateList.Visible = true;
                txtElmentTemplateName.Visible = false;
                cbElementTemplateList.Items.Clear();
                foreach (var elementTemplate in WhichAFDatabase.AFDatabase.ElementTemplates)
                {
                    cbElementTemplateList.Items.Add(elementTemplate);
                }
            }
            else
            {
                WhichAFDatabase.Visible = false;
                txtDbName.Visible = true;
                // dbName = txtDbName.Text;
                cbElementTemplateList.Visible = false;
                txtElmentTemplateName.Visible = true;
            }
        }

        private void WhichPISystem_ConnectionChange(object sender, SelectionChangeEventArgs e)
        {
            chkExistingDB.Enabled = false;
            if (!WhichPISystem.PISystem.ConnectionInfo.IsConnected)
            {
                WhichPISystem.PISystem.Connect(true, Owner);
                chkExistingDB.Enabled = true;
            }
            else
            {
                chkExistingDB.Enabled = true;
            }
        }

        private void WhichPISystem_Load(object sender, EventArgs e)
        {

        }

        private void chkCustomTz_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkCustomTz.Checked)
            {
                txtCustomTz.Visible = false;
                cbTimeFormat.Visible = true;
            }
            else
            {
                txtCustomTz.Visible = true;
                cbTimeFormat.Visible = false;
            }
        }

        private void chkBoxLiveStreaming_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkBoxLiveStreaming.Checked)
            {
                pageSizeLabel.Visible = false;
                lblCollectionInterval.Visible = false;
                pageSize.Visible = false;
                collectionIntervalMs.Visible = false;
                lblRemainingEvents.Visible = false;
                lblRemainingEventsValue.Visible = false;

            }
            else
            {
                pageSizeLabel.Visible = true;
                lblCollectionInterval.Visible = true;
                pageSize.Visible = true;
                collectionIntervalMs.Visible = true;
                lblRemainingEvents.Visible = false;
                lblRemainingEventsValue.Visible = false;
            }
        }

        private void streamingGrpBox_Enter(object sender, EventArgs e)
        {

        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            try
            {
                btnSave.Enabled = false;
                if (btnPause.Text == "Pause")
                {
                    btnPause.Text = "Resume";
                    _pauseEvent.Reset();

                }
                else
                {
                    btnPause.Text = "Pause";
                    _pauseEvent.Set();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Exception - {ex.Message}");
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                _shouldStop = true;

                if (uploadStreamThread.IsAlive)
                {

                }
                // Signal the shutdown event
                _shutdownEvent.Set();
                Console.WriteLine("Thread Stopped ");

                // Make sure to resume any paused threads
                _pauseEvent.Set();

                // Wait for the thread to exit
                uploadStreamThread.Join();
                btnPause.Hide();
                btnReset.Hide();
                btnSave.Text = "Upload";
                btnSave.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Exception - {ex.Message}");
            }
        }

        private void WhichAFDatabase_SelectionChange(object sender, SelectionChangeEventArgs e)
        {
            try
            {
                cbElementTemplateList.Items.Clear();
                if (WhichAFDatabase.AFDatabase == null || WhichAFDatabase.AFDatabase.Equals("")) return;
                foreach (var elementTemplate in WhichAFDatabase.AFDatabase.ElementTemplates)
                {
                    cbElementTemplateList.Items.Add(elementTemplate);
                }

            }
            catch (Exception exception)
            {
                return;
            }
        }
    }
}
