﻿using Falkonry.Integrator.PISystem.Helper;
using FalkonryClient.Helper.Models;
using log4net;
using OSIsoft.AF.Asset;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

namespace Falkonry.Integrator.PISystem
{
    public class Configurations
    {
        public List<PiConfigurationItem> ConfigurationItems {
            get; set;
        }
    }
    public class PiConfigurationItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
        public string Status { get; set; }
        public Boolean Deleted { get; set; }
        public string ErrorMessage { get; set; }
        public PiConfiguration Configuration { get; set; }
        public string BatchIdentifier { get; set; }
        public PiConfigurationItem()
        {
            BatchIdentifier = "";
        }
        public string getString()
        {
            string objString = " ConfigDetails ====";
            objString += "Enabled : " + this.Enabled;
            objString += "Deleted : " + this.Deleted;
            objString += "Message : " + this.ErrorMessage;
            objString += " AFDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.Configuration.AfConfiguration))
            {
                objString += " "  + descriptor.Name + " : "  + descriptor.GetValue(this.Configuration.AfConfiguration);
            }
            objString += " FalkonryDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.Configuration.FalkonryConfiguration))
            {
                objString += " " +  descriptor.Name + " : " + descriptor.GetValue(this.Configuration.FalkonryConfiguration);
            }
            objString += " HistoricalDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.Configuration.HistoricalDataAccess))
            {
                objString += " " +  descriptor.Name + " : " + descriptor.GetValue(this.Configuration.HistoricalDataAccess);
            }
            objString += " StreamingDataDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.Configuration.StreamingDataAccess))
            {
                objString += " " + descriptor.Name + " : " + descriptor.GetValue(this.Configuration.StreamingDataAccess);
            }
            return objString;
        }
    }
    [Serializable]
    public class PiConfiguration
    {
        /**
        * Unique Id of the AFDatabase for this element configuration.
        */
        public Guid Id  { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
        public string Status { get; set; }
        public Boolean Deleted { get; set; }
        public bool isGood { get; set; }
        public string ErrorMessage { get; set; }
        public string BatchIdentifier { get; set; }
        public FalkonryConfiguration FalkonryConfiguration { get; set; }
        public AfConfiguration AfConfiguration { get; set; }
        public HistoricalConfiguration HistoricalDataAccess { get; set; }
        public StreamingConfiguration StreamingDataAccess { get; set; }
        [IgnoreDataMemberAttribute]
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public PiConfiguration populateConfigFromAFElement(AFElement element)
        {
            try
            {
                isGood = false;
                Id = element.ID;
                Deleted = (bool)element.Attributes[Constants.ParentConfigConstant.Deleted].GetValue().Value;
                Enabled = (bool)element.Attributes[Constants.ParentConfigConstant.Enabled].GetValue().Value;
                ErrorMessage = (string)element.Attributes[Constants.ParentConfigConstant.Message].GetValue().Value;
                Name = (string)element.Attributes[Constants.ParentConfigConstant.Name].GetValue().Value;
                Status = (string)element.Attributes[Constants.ParentConfigConstant.Status].GetValue().Value;

                AFElement afConfig = element.Elements.Where(x => x.Name.Equals(Constants.AFConfigConstants.name)).First();
                AFElement falkonryConfig = element.Elements.Where(x => x.Name.Equals(Constants.FalkonryConfigConstants.name)).First();
                AFElement streamingConfig = element.Elements.Where(x => x.Name.Equals(Constants.StremingConfigConstants.name)).First();
                AFElement historicalConfig = element.Elements.Where(x => x.Name.Equals(Constants.HistoricalConfigConstants.name)).First();

                AfConfiguration afConfigPoco = new AfConfiguration();

                afConfigPoco.SystemName = (String)afConfig.Attributes[Constants.AFConfigConstants.SystemName].GetValue().Value;

                afConfigPoco.DatabaseName = (String)afConfig.Attributes[Constants.AFConfigConstants.DatabaseName].GetValue().Value;
                afConfigPoco.EventFrameTemplateNames = (String[])afConfig
                                           .Attributes[Constants.AFConfigConstants.EventFrameTemplateName]
                                                        .GetValue().Value;
                afConfigPoco.EventFrameUploadedTime = (String[])afConfig.Attributes[Constants.AFConfigConstants.EventFrameUploadedTime]
                                                        .GetValue().Value;
                afConfigPoco.OutputTemplateAttribute = (String[])afConfig.Attributes[Constants.AFConfigConstants.OutTemplateAttribute]
                                                        .GetValue().Value;
                afConfigPoco.AFServerTimeZoneId = (String)afConfig.Attributes[Constants.AFConfigConstants.AFServerTimeZone]
                                                        .GetValue().Value;
                afConfigPoco.TemplateAttributes = ((String)afConfig.Attributes[Constants.AFConfigConstants.TemplateAttributes]
                                                    .GetValue().Value).Split(',');
                afConfigPoco.TemplateElementPaths = ((String)afConfig.Attributes[Constants.AFConfigConstants.TemplateElementPaths]
                                                        .GetValue().Value).Split(',');
                afConfigPoco.TemplateName = (String)afConfig.Attributes[Constants.AFConfigConstants.TemplateName]
                                                        .GetValue().Value;


                FalkonryConfiguration falkonryPoco = new FalkonryConfiguration
                {
                    AssessmentId = (String[])falkonryConfig.Attributes[Constants.FalkonryConfigConstants.AssessmentId]
                                                       .GetValue().Value,
                    AssessmentIdForFacts = (String[])falkonryConfig.Attributes[Constants.FalkonryConfigConstants.AssessmentIdForFacts]
                                                       .GetValue().Value,
                    Backfill = (Boolean)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.BackFill]
                                                       .GetValue().Value,
                    BatchLimit = (int)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.BatchLimit]
                                                       .GetValue().Value,
                    Datastream = (String)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.DataStream]
                                                       .GetValue().Value,
                    Host = (String)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.Host]
                                                       .GetValue().Value,
                    SourceId = ((String)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.SourceId]
                                                       .GetValue().Value),
                    TimeFormat = ((String)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.TimeFormat]
                                                       .GetValue().Value),
                    Token = (String)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.Token]
                                                       .GetValue().Value,
                };

                HistoricalConfiguration historicalPoco = new HistoricalConfiguration
                {
                    Enabled = (bool)historicalConfig.Attributes[Constants.HistoricalConfigConstants.Enabled]
                                                       .GetValue().Value,
                    EndTime = (long)historicalConfig.Attributes[Constants.HistoricalConfigConstants.EndTime]
                                                       .GetValue().Value,
                    Override = (Boolean)historicalConfig.Attributes[Constants.HistoricalConfigConstants.Override]
                                                       .GetValue().Value,
                    PageSize = (int)historicalConfig.Attributes[Constants.HistoricalConfigConstants.PageSize]
                                                       .GetValue().Value,
                    StartTime = (long)historicalConfig.Attributes[Constants.HistoricalConfigConstants.StartTime]
                                                       .GetValue().Value,
                };

                StreamingConfiguration streamingConfigPoco = new StreamingConfiguration
                {
                    CollectionIntervalMs = (int)streamingConfig.Attributes[Constants.StremingConfigConstants.CollectionInterval]
                                                       .GetValue().Value,
                };
                this.AfConfiguration = afConfigPoco;
                this.FalkonryConfiguration = falkonryPoco;
                this.HistoricalDataAccess = historicalPoco;
                this.StreamingDataAccess = streamingConfigPoco;
                isGood = true;
            }
            catch(Exception e)
            {
                Log.Fatal($"Exception in transforming AFElement to PiConfiguration ", e);
            }
            return this;
        }

        public override bool Equals(object obj)
        {
            bool yes = false;
            if (obj is PiConfiguration && obj != null && ((PiConfiguration)obj).Id==Id)
            {
                yes = true;
            }
            return yes;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public string getString()
        {
            string objString = " ConfigDetails ====";
            objString += "Enabled : " + this.Enabled;
            objString += "Deleted : " + this.Deleted;
            objString += "Message : " + this.ErrorMessage;
            objString += " AFDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.AfConfiguration))
            {
                objString += " " + descriptor.Name + " : " + descriptor.GetValue(this.AfConfiguration);
            }
            objString += " FalkonryDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.FalkonryConfiguration))
            {
                objString += " " + descriptor.Name + " : " + descriptor.GetValue(this.FalkonryConfiguration);
            }
            objString += " HistoricalDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.HistoricalDataAccess))
            {
                objString += " " + descriptor.Name + " : " + descriptor.GetValue(this.HistoricalDataAccess);
            }
            objString += " StreamingDataDetails ===== ";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.StreamingDataAccess))
            {
                objString += " " + descriptor.Name + " : " + descriptor.GetValue(this.StreamingDataAccess);
            }
            return objString;
        }
    }
    [Serializable]
    public class FalkonryConfiguration
    {
        public string SourceId { get; set; }
        public string Host { get; set; }
        public string Token { get; set; }
        public string TimeFormat { get; set; }
        public string Datastream { get; set; }
        public int BatchLimit { get; set; }
        public string[] AssessmentId {  get; set; }
        public string[] AssessmentIdForFacts { get; set; }
        public bool Backfill { get; set; }
        [IgnoreDataMemberAttribute]
        public List<Assessment> assessmentList { get; set; }
    }
    [Serializable]
    public class AfConfiguration
    {
        public string SystemName { get; set; }
        public string DatabaseName { get; set; }
        public string TemplateName { get; set; }
        public string[] TemplateAttributes { get; set; }
        public string[] TemplateElementPaths { get; set; }
        public string[] OutputTemplateAttribute { get; set; }
        public string[] EventFrameTemplateNames { get; set; }
        public string[] EventFrameUploadedTime { get; set; }
        public string AFServerTimeZoneId { get; set; }
    }
    [Serializable]
    public class HistoricalConfiguration
    {
        public bool Enabled { get; set; }
        public long StartTime { get; set; }
        public long EndTime { get; set; }
        public int PageSize { get; set; }
        public bool Override { get; set; }
        
    }
    [Serializable]
    public class StreamingConfiguration
    {
        public bool Enabled { get; set; }
        public int CollectionIntervalMs { get; set; }
    }
}
