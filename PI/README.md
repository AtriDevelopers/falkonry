# PI
This Falkonry.Integrator.PISystem contains 3 projects
- Falkonry.Integrator.PIsystem
- Falkonry.Integrator.PISystem.Configurator
- Falkonry.Integrator.PISystem.Installer

### Falkonry.Integrator.PIsystem
This is the main service that performs the main data extract from the PI System and the data push into Falkonry. 
It expects a configuration file to be present in the same directory as the executable. The files are:
- Falkonry.Integrator.PIsystem.exe
- Falkonry.Integrator.PIsystem.Config.json

An example configuration file looks like the following example:
```
{
	"ConfigurationItems": [{
		"Id": "d412fc56-2847-4c8b-bf1b-3ebed583f69a",
		"Name": "First Config",
		"Enabled": true,
		"Configuration": {
			"FalkonryConfiguration": {
				"Host": "https://dev.falkonry.ai",
				"Token": "ly2vs6ilccxn43qb1yemtoo7jjhecs9s",
				"TimeFormat": "iso_8601",
				"EventBuffer": "AllFacts",
				"BatchLimit": 25000,
				"AssessmentName": "AllFacts"
			},
			"AfConfiguration": {
				"SystemName": "TGG-PI-DEV",
				"DatabaseName": "BMReports-Configuration",
				"TemplateName": "Physical Data Template",
				"TemplateAttributes": ["MEL", "PN"],
				"TemplateElementPaths": ["Grid Reference\\BMU\\T_LAGA-1\\Physical Data", "Grid Reference\\BMU\\T_SHBA-2\\Physical Data", "Grid Reference\\BMU\\T_SPLN-1\\Physical Data", "Grid Reference\\BMU\\T_SHBA-1\\Physical Data", "Grid Reference\\BMU\\T_ABTH7\\Physical Data", "Grid Reference\\BMU\\T_ABTH8\\Physical Data", "Grid Reference\\BMU\\T_ABTH9\\Physical Data", "Grid Reference\\BMU\\T_BAGE-1\\Physical Data", "Grid Reference\\BMU\\T_BAGE-2\\Physical Data", "Grid Reference\\BMU\\T_COTPS-1\\Physical Data", "Grid Reference\\BMU\\T_COTPS-2\\Physical Data", "Grid Reference\\BMU\\T_COTPS-3\\Physical Data", "Grid Reference\\BMU\\T_COTPS-4\\Physical Data", "Grid Reference\\BMU\\T_DRAXX-1\\Physical Data", "Grid Reference\\BMU\\T_DRAXX-2\\Physical Data", "Grid Reference\\BMU\\T_DRAXX-3\\Physical Data", "Grid Reference\\BMU\\T_DRAXX-4\\Physical Data", "Grid Reference\\BMU\\T_DRAXX-5\\Physical Data", "Grid Reference\\BMU\\T_DRAXX-6\\Physical Data", "Grid Reference\\BMU\\T_WBURB-1\\Physical Data", "Grid Reference\\BMU\\T_WBURB-2\\Physical Data", "Grid Reference\\BMU\\T_WBURB-3\\Physical Data"],
				"OutputTemplateAttribute": null,
				"EventFrameTemplateNames": ["Available Not Dispatched", "Failed Start", "Full Capacity", "MEL Down", "Not Commercially Available", "Turn Up Capability", "Unit Trip"]
			},
			"HistoricalDataAccess": {
				"Enabled": true,
				"StartTime": "2016-11-01T21:21:45",
				"EndTime": "2017-02-14T21:21:46",
				"PageSize": 1000000
			},
			"StreamingDataAccess": {
				"Enabled": false,
				"CollectionIntervalMs": 250
			}
		}
	}]
}
```

In order to run the data collection simply run the Falkonry.Integrator.PISystem.exe file via a cmd utility which will look for a config file in the same directory.

### Falkonry.Integrator.PISystem.Configurator
To facilitate updating the configuration file this project provide a GUI configuration utility to modify the contents. 
To launch the utility run the Falkonry.Integrator.PISystem.Configurator.exe file.

Click on the "Select" button of the "Select Integraor File" section at the top of the GUI. Browse to and select the Falkonry.Integrator.PISystem.Config.json file that sits in the same directory as the main executable.

The list of Datastream Configurations will then be populated in the "Select Datastream Configuration" dropdown. Simply select the desired Datastream configuration.

Upon selecting a Datastream Configuration the AF Configuration will be parsed and an attempt made to collect the information from AF and populate the form controls.
- Selecting a different PI System will update the list of AF Databases.
- Selecting a different AF Database will update the list of AF Element Templates and AF Event Frame Templates (essentially the same thing within the AF SDK).
- Selecting an AF Element Template will provide a list of instantiated objects from the Template. Ctrl click to select which Elements should have data collected for them.
- Selecting an AF Element Template will provide a list of the Attribute Templates that belong to the Element Template. Ctrl click to select which Attributes should have data collected for them.


### Supporting Operating System:
- Windows 7
- Windows Server 2012
