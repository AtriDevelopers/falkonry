﻿using System;
using log4net;
using System.Windows.Forms;
using System.ServiceProcess;
using Falkonry.Integrator.PISystem.Helper;

namespace Falkonry.Integrator.PISystem
{
    internal class Program
    {
        private static PiService _mainService;
        public static string ServiceName = typeof(Program).Namespace;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        

        public class PIServiceWrapper : ServiceBase
        {
            public PIServiceWrapper()
            {
                ServiceName = Program.ServiceName;
            }

            protected override void OnStart(string[] args)
            {
                Log.Info($" ======================= Entering On Start of Pi Service Wrapper ======================= ");
                Program.Start();
                Log.Info($" ======================= Leaving On Start of Pi Service Wrapper ======================= ");
            }

            protected override void OnStop()
            {
                Log.Info($" ======================= On Stop of Pi Service Wrapper ======================= ");
                Program.Stop();
            }
        }

        // ReSharper disable once UnusedParameter.Local
         static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            if (!Environment.UserInteractive)
            {
                Log.Info($" ======================= Starting up as a service. ======================= ");
                try
                {
                    Log.Info($" Windows Operating system: {Environment.OSVersion.ToString()} ");
                    Log.Info($" Falkonry Agent Version: {Application.ProductVersion} ");
                    Log.Info($".Net version installed on the machine is : {Utils.Get45or451FromRegistry()}");
                    Log.Info($" Machine Name: {Environment.MachineName.ToString()} ");
                    Log.Info($" Timezone: {TimeZoneInfo.Local.StandardName} ");
                }
                catch (Exception exception)
                {
                    Log.Error($"Error in fetching machine details : " + exception);    
                }
                using (var service = new PIServiceWrapper())
                {
                    Log.Info($"Above ServiceBase.Run");
                    ServiceBase.Run(service);
                    Log.Info($" Below ServiceBase.Run ");
                }
                   
            }
            else
            {
                Log.Info($" ======================= Starting up as a console app  =======================");
                Start();
                Console.WriteLine("Press any key to stop...");
                Log.Info(" ======================= Stopping the console app  =======================");
                Console.ReadKey(true);
                Stop();
            }
        }

        private static void Start()
        {
            Log.Info($" ======================= Entering  Start of Program ======================= ");
            _mainService = new PiService();
            _mainService.Start();
            Log.Info($" ======================= Leaving Start of Program ======================= ");
        }

        private static void Stop()
        {
            Log.Info($" ======================= Stopping the service app  =======================");
            _mainService.Stop();
        }
    }

    

    
    
}
