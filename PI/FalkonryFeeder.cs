﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OSIsoft.AF.Asset;
//using FalkonryClient.Helper.Models;
using log4net;
using OSIsoft.AF.EventFrame;
using System.Threading;
using Falkonry.Integrator.PISystem.Helper;
using FalkonryClient.Helper.Models;

namespace Falkonry.Integrator.PISystem
{
    internal class FalkonryFeeder
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private const string SignalDelim = "__";
        private const string TimeFormat = "yyyy-MM-ddTHH:mm:ss.fffZ";
        //private readonly FalkonryClient.Falkonry _falkonry;
        private readonly string _configTimeZoneId;
        private readonly string _systemName;
        private readonly FalkonryConfiguration _configuration;
        private readonly AfConfiguration _afConfiguration;
        private readonly Datastream _datastream;
        private readonly SortedDictionary<string, string> _options = new SortedDictionary<string, string>();
        private readonly string _piHost;
        private readonly string _templateName;
        private readonly string _afDatabaseName;
       

        public FalkonryFeeder(FalkonryConfiguration falkonryConfiguration,  bool streaming, AfConfiguration afConfiguration)
        {
            _configuration = falkonryConfiguration;
            _piHost = afConfiguration.SystemName;
            _templateName = afConfiguration.TemplateName;
            _afDatabaseName = afConfiguration.DatabaseName;
            _systemName = _piHost;
            _configTimeZoneId = afConfiguration.AFServerTimeZoneId;
            _afConfiguration = afConfiguration;
            string configTimeZone = Utils.WindowsToIana(_configTimeZoneId);
            Log.Debug($"Creating Falkonry Feeder client... Host :{_configuration.Host} Streaming : {streaming.ToString()}, datastream : {falkonryConfiguration.Datastream}, Id : {falkonryConfiguration.SourceId}");
            _options.Add("timeIdentifier", "time");
            _options.Add("timeFormat", _configuration.TimeFormat);
            _options.Add("fileFormat", "json");
            _options.Add("timeZone", configTimeZone);
            _options.Add("streaming", streaming.ToString().ToLower());
            _options.Add("hasMoreData", (!streaming).ToString().ToLower());
            _options.Add("signalIdentifier", "signal");
            _options.Add("entityIdentifier", "entity");
            _options.Add("valueIdentifier", "value");
            try
            {
                if (_configuration.SourceId != null && _configuration.SourceId != "new_datastream_saved")
                {
                    _datastream = FalkonryHelper.GetDatastream(_configuration);
                    if(_datastream.Field.BatchIdentifier!=null && !_datastream.Field.BatchIdentifier.Equals(""))
                    {
                        _options.Add("batchIdentifier", _datastream.Field.BatchIdentifier);
                    }
                   
                }
            }
            catch (Exception exception)
            {
                Log.Error($"Could not create datastream. Cannot process. Error : {exception.Message}");
            }
            
        }

        public void SendEvents(IEnumerable<AFValues> piEvents, int logBatchId=-1)
        {
            
            if(logBatchId != -1)
                Log.Debug($"Sending inputs for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId} BatchId : {logBatchId}");
            else
                Log.Debug($"Sending inputs for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId}");

            try
            {
                var eventsList = piEvents as AFValues[] ?? piEvents.ToArray();
                Log.Debug($"Sending Events, Attribute Count = '{eventsList.Length}' for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId}");
                if (!eventsList.Any()) return;

                var batchCount = 0; var batchLimit = _configuration.BatchLimit;
                var jsonData = new StringBuilder();
                
                AFAttribute batchAttribute = null;
                if (_datastream.Field.BatchIdentifier != null && !_datastream.Field.BatchIdentifier.Equals(""))
                {
                    try
                    {

                        var attributes = AfHelper.FindAllAttributes(_afConfiguration);
                        if (attributes != null && attributes.Count > 0)
                        {
                            batchAttribute = attributes.Where(x => x.Name.Equals(_datastream.Field.BatchIdentifier)).First();
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Fatal($"Exception in getting the Batch AFAttribute Datastream:  {_configuration.Datastream}. Id: {_configuration.SourceId} \n StackTrace: {e.StackTrace}");
                    }
                }
               
                // Refactor this nested for loop code in reactive style

                foreach (var attributeEvents in eventsList)
                {
                    if (attributeEvents.Count <= 0) continue;
                    Log.Debug($"Processing '{attributeEvents.Count}' values for all Attributes  for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId}");
                    bool breakFromLoop = false;

                    foreach (var attributeEvent in attributeEvents)
                    {
                       
                        if (!attributeEvent.IsGood) continue;
                        if (_afDatabaseName != attributeEvent.Attribute.Database.Name)
                        {
                            Log.Debug($"Data Ingestion skipped as database name mismatch found. Datastream Db : {_afDatabaseName} and attribute DB : { attributeEvent.Attribute.Database.Name}, for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId}");
                            break;
                        }

                        var path = attributeEvent.Attribute.Element.ID + SignalDelim + attributeEvent.Attribute.Name;
                        var batchValue = "";
                        try
                        {
                            if (batchAttribute != null)
                            {
                                AFValue batchValueAf = batchAttribute.GetValue(attributeEvent.Timestamp);
                                batchValue = "" + batchValueAf.Value;
                            }
                           
                        }
                        catch (Exception e)
                        {
                            Log.Fatal($"Exeption in getting the Batch {_datastream.Field.BatchIdentifier}");
                        }
                        var theValue = (attributeEvent.Value is string) ? "\"" + attributeEvent.Value + "\"" : attributeEvent.Value;
                        var data = "";
                        if (batchValue.Equals(""))
                        {
                             data = "{\"time\" :\"" + attributeEvent.Timestamp.UtcTime.ToString(TimeFormat) +
                                   "\", \"signal\": \"" + attributeEvent.Attribute.Name +
                                   "\",  \"entity\": \"" + attributeEvent.Attribute.Element.ID +
                                   "\", \"value\" : " + theValue +
                                   "}";
                            //Log.Debug($"Ingest data with no batch identifier {data}");
                        }
                        else
                        {
                            data = "{\"time\" :\"" + attributeEvent.Timestamp.UtcTime.ToString(TimeFormat) +
                                   "\", \"signal\": \"" + attributeEvent.Attribute.Name +
                                   "\",  \"entity\": \"" + attributeEvent.Attribute.Element.ID +
                                   "\", \"value\" : " + theValue +
                                   ", "+"\""+ _datastream.Field.BatchIdentifier +"\""+ " :"+"\""+ batchValue +
                                   "\""+"}";




                            //Log.Debug($"Ingest data with batch Identifier {data}");
                        }
                        
                       
                        if (jsonData.Length != 0) jsonData.Append("\n");
                  
                        jsonData.Append(data);

                        batchCount++;
                        if (batchCount <= batchLimit) continue;
                        Log.Debug($"Batch Limit of '{batchLimit}' events reached. Flushing to Falkonry...");
                        try
                        {
                            var response = FalkonryHelper.AddInput(_configuration,_datastream.Id, jsonData.ToString(), _options);
                            Log.Debug($"Response from Falkonry: {response.Status} number of events sent : {batchCount}");
                            jsonData = new StringBuilder();
                            batchCount = 0;
                            Thread.Sleep(10000);
                        }
                        catch (Exception exception)
                        {
                           Log.Error($"Error in ingesting data. Datastream id :{_datastream.Id}. Response from Falkonry Server : {exception.Message}");
                            if(exception.Message == "No such Datastream available")
                            {
                                breakFromLoop = true;
                                break;
                            }
                            if(exception.Message == "Datastream is not live, streaming data cannot be accepted.")
                            {
                                breakFromLoop = true;
                                throw exception;
                            }
                            if (_options["streaming"] != "true")
                                AfHelper.UpdateState(_systemName, _configuration.Datastream, "ERROR", $"Error in ingesting data.Error : {exception.Message}");
                           Thread.Sleep(10000);
                        }
                    }
                    if(breakFromLoop)
                    {
                        break;
                    }
                }

                if (batchCount <= 0) return;
                _options["hasMoreData"] = "false";
                try
                {
                    // Log.Debug("TIMELOG Start Time for AddInput : " + DateTime.Now.ToString());
                    var finalResponse = FalkonryHelper.AddInput(_configuration,_datastream.Id, jsonData.ToString(), _options);
                    // Log.Debug("TIMELOG End Time for AddInput : " + DateTime.Now.ToString());
                    Log.Debug($"Response from Falkonry: {finalResponse.Status} number of events sent : {batchCount}");
                    AfHelper.UpdateState(_systemName, _configuration.Datastream, "INFO", $"Successfully ingested data for datastream : {_configuration.Datastream}.");
                }
                catch (Exception exception)
                {
                    Log.Error($"Error in ingesting data in last batch. Datastream id :{_datastream.Id}. Response from Falkonry Server : {exception.Message}");
                  
                    if (exception.Message == "Datastream is not live, streaming data cannot be accepted.")
                    {
                        throw exception;
                    }
                    if(_options["streaming"] != "true")
                        AfHelper.UpdateState(_systemName, _configuration.Datastream, "ERROR", $"Error in ingesting historical data. Error : {exception.Message}");
                }
            }
            catch (Exception exception)
            {
                Log.Error($"Unable to Send Events: {exception}");
                //Log.Debug("TIMELOG End Time for AddInput : " + DateTime.Now.ToString());
                if (exception.Message == "Datastream is not live, streaming data cannot be accepted.")
                {
                    throw exception;
                }
            }
            return;
        }

        public void SendFacts(IEnumerable<AFEventFrame> piEventFrames, int index)
        {
            Log.Debug($"Sending facts for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId}. For Assessment : {_configuration.AssessmentId.GetValue(index)}");
            try
            {
                var jsonData = new StringBuilder();
                foreach (var eventFrame in piEventFrames)
                {
                    if (jsonData.Length != 0) jsonData.Append("\n");
                    //var data = "{\"time\" :" + eventFrame.StartTime.UtcTime.ToString(TimeFormat)  + ", \"end\":" + (eventFrame.EndTime.IsEmpty ? "" : eventFrame.EndTime.UtcTime.ToString(TimeFormat)) + ", \"entity\": \"" + eventFrame.PrimaryReferencedElement.ID + "\", \"value\" : \"" + eventFrame.Template.Name + "\"}";
                    //var data = "{\"time\" :\"" + eventFrame.StartTime.ToString("O") + "\", \"end\": \"" + (eventFrame.EndTime.IsEmpty ? "" : eventFrame.EndTime.ToString("O")) + "\", \"entity\": \"" + eventFrame.PrimaryReferencedElement.ID + "\", \"value\" : \"" + eventFrame.Template.Name + "\"}";
                    var data = "{\"time\" :\"" + eventFrame.StartTime.UtcTime.ToString(TimeFormat) + "\", \"end\": \"" + (eventFrame.EndTime.IsEmpty ? "" : eventFrame.EndTime.UtcTime.ToString(TimeFormat)) + "\", \"entity\": \"" + eventFrame.PrimaryReferencedElement.ID + "\", \"value\" : \"" + eventFrame.Template.Name + "\"}";
                    jsonData.Append(data);
                }

                try
                {
                   string configTimeZone = Utils.WindowsToIana(_configTimeZoneId);
                   var assessment = FalkonryHelper.GetAssessment(_configuration, _configuration.AssessmentId.GetValue(index).ToString());
                   // var assessment = FalkonryHelper.GetAssessments(_configuration).First(a => a.Id == (_configuration.AssessmentId.GetValue(index).ToString()));

                   var queryParams = new SortedDictionary<string, string>
                    {
                        {"startTimeIdentifier", "time"},
                        {"endTimeIdentifier", "end"},
                        {"timeFormat", _configuration.TimeFormat},
                        {"timeZone",configTimeZone},
                        { "entityIdentifier", "entity"},
                        { "valueIdentifier" , "value"}
                    };
                    FalkonryHelper.AddFacts(_configuration, assessment.Id, jsonData.ToString(), queryParams);
                    Log.Debug($"Response from Falkonry: Successfully ingested facts data  for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId}");
                    AfHelper.UpdateState(_systemName, _configuration.Datastream, "INFO", $"Successfully ingested facts data");
                }
                catch (Exception exception)
                {
                    Log.Error($"Error Response from Falkonry while fetching assessment method :SendFacts. for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId} error : {exception.Message}");
                    AfHelper.UpdateState(_systemName, _configuration.Datastream, "ERROR", $"Error in ingesting facts data. Error : {exception.Message}");
                }
            }
            catch (Exception exception)
            {
                Log.Error($"Unable to Send Facts: {exception} for datastream : {_configuration.Datastream}. Id: {_configuration.SourceId}");
            }
            
        }
    }
}
