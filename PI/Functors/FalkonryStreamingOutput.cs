﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Functors
{
    /**
     *  Fills the live output from the Falkonry to the PI System
     * 
     */ 
    public class FalkonryStreamingOutput
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private AFSubscriptions aFSubscriptions;

        public FalkonryStreamingOutput(AFSubscriptions aFSubscriptions)
        {
            this.aFSubscriptions = aFSubscriptions;
        }

        public Func<PiConfiguration, PiConfiguration> getDelegate()
        {
            return new Func<PiConfiguration, PiConfiguration>(falkonryStream);
        }

        public PiConfiguration falkonryStream(PiConfiguration config)
        {
            if (config.StreamingDataAccess.Enabled)
            {
               
                FalkonryStreamingObserver observer = new FalkonryStreamingObserver(config, aFSubscriptions);
                Observable.ToObservable(config.FalkonryConfiguration.AssessmentId.AsEnumerable())
                    .SubscribeOn(NewThreadScheduler.Default)
                    .ObserveOn(NewThreadScheduler.Default)
                    .Where(x => x!="")
                    .Select(x=> {
                        
                        FalkonryReceiver receiver = new FalkonryReceiver(config.FalkonryConfiguration, config.AfConfiguration, x);
                        int assessmentIndex =  Array.IndexOf(config.FalkonryConfiguration.AssessmentId, x);
                        receiver.StartListening();
                        Log.Debug($"Live output receiver created for datastream: {config.FalkonryConfiguration.Datastream} Id : {config.FalkonryConfiguration.SourceId}... Assessment Id : {x}. Index : {assessmentIndex}");
                        return receiver;
                    })
                    .Subscribe(observer);
            }
            return config;
        }

    }

    class FalkonryStreamingObserver : IObserver<FalkonryReceiver>
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private PiConfiguration configuration;
        private AFSubscriptions afSubscriptions;
        public FalkonryStreamingObserver(PiConfiguration config,AFSubscriptions subscriptions)
        {
            this.configuration = config;
            this.afSubscriptions = subscriptions;
        }
        public void OnCompleted()
        {
            
        }

        public void OnError(Exception error)
        {
            Log.Fatal($"Error in Creating Falkonry Receiver ", error);
        }

        public void OnNext(FalkonryReceiver value)
        {
            Log.Fatal($"Falkonry Receiver created for Datastream :{configuration.FalkonryConfiguration.Datastream} DSId: {configuration.FalkonryConfiguration.SourceId} Assessment id {value.getAssessmentId()}");
            afSubscriptions.AddFalkonryReceiverAgainstAssessmentId(Tuple.Create(configuration, value), value.getAssessmentId());
        }
    }
}
