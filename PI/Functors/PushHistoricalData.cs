﻿using Falkonry.Integrator.PISystem.Helper;
using FalkonryClient.Helper.Models;
using log4net;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Functors
{
    /**
     *  
     *  Pushes the Historical Data for the config to the Falkonry Back end.
     * 
     */ 
    public class PushHistoricalData 
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<PiConfiguration,PiConfiguration> getDelegate()
        {
            return new Func<PiConfiguration, PiConfiguration>(uploadHistoricalData);
        }
        private PiConfiguration uploadHistoricalData(PiConfiguration piConfiguration)
        {
           
            var attributes = AfHelper.FindElementAttributes(piConfiguration.AfConfiguration);
            Log.Debug($"Attributes found = '{attributes.Count}'' Datastream Id :{piConfiguration.FalkonryConfiguration.SourceId} Datastream Name : {piConfiguration.FalkonryConfiguration.Datastream}'");
            try
            {
                Datastream dataStream = FalkonryHelper.GetDatastream(piConfiguration.FalkonryConfiguration);
               
                if (attributes.Count > 0 && piConfiguration.HistoricalDataAccess != null)
                {
                    HistoricalPi _historical = new HistoricalPi(piConfiguration.HistoricalDataAccess,piConfiguration.FalkonryConfiguration,attributes);
                    
                    if (piConfiguration.HistoricalDataAccess.Enabled)
                    {
                        bool historianDataAcess = piConfiguration.HistoricalDataAccess.Enabled;
                        bool overrideHistorianData = piConfiguration.HistoricalDataAccess.Override;
                        bool addEntityMeta = false;
                        piConfiguration.HistoricalDataAccess.Enabled = false;
                        piConfiguration.HistoricalDataAccess.Override = false;

                        IEnumerable<PiConfiguration> results = MemoryCache.Instance.getConfigurationsCopy()
                                 .Where(y => y.Id == piConfiguration.Id).ToList();

                        results.All(z =>
                        {
                            z.HistoricalDataAccess.Enabled = false;
                            z.HistoricalDataAccess.Override = false;
                            return true;
                        });

                        AfHelper.updateHistoricalElement(piConfiguration);
                        Log.Debug($"Creating Historical PI data access... for  ' Config Id :Datastream Id :{piConfiguration.FalkonryConfiguration.SourceId} Datastream Name : {piConfiguration.FalkonryConfiguration.Datastream}'");
                        _historical = new HistoricalPi(piConfiguration.HistoricalDataAccess, piConfiguration.FalkonryConfiguration, attributes);
                        // Input for Falkonry
                        // Check that there isn't already data present...
                        DateTime startTime; DateTime endTime;
                        DateTime startTimeEvents = DateTime.Parse("01-Jan-1970");
                        DateTime endTimeEvents = DateTime.Parse("01-Jan-1970");

                        if (!overrideHistorianData)
                        {
                            try
                            {
                                startTimeEvents = Utils.ConvertMillisecondsToUTC(dataStream.Stats.EarliestDataPoint);
                            }
                            catch (Exception)
                            {
                            }

                            try
                            {
                                endTimeEvents = Utils.ConvertMillisecondsToUTC(dataStream.Stats.LatestDataPoint);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        else
                        {
                            startTime = Utils.ConvertMillisecondsToLOCAL(piConfiguration.HistoricalDataAccess.StartTime);
                            endTime = Utils.ConvertMillisecondsToLOCAL(piConfiguration.HistoricalDataAccess.EndTime);

                            Log.Debug($"Historical PI data access... Override : true. Selected time Range : {startTime} TO {endTime}. Data Present in Falkonry : {startTimeEvents} TO {endTimeEvents} for  '  Datastream Id :{piConfiguration.FalkonryConfiguration.SourceId} Datastream Name : {piConfiguration.FalkonryConfiguration.Datastream}'");
                            addEntityMeta = true;
                            if (addEntityMeta)
                                FalkonryHelper.AddEntityInformation(piConfiguration.FalkonryConfiguration, attributes);
                            _historical.StartBackfilling(
                                new FalkonryFeeder(
                                    piConfiguration.FalkonryConfiguration,
                                    false,
                                    piConfiguration.AfConfiguration
                                    ),
                                piConfiguration.AfConfiguration,
                                piConfiguration.FalkonryConfiguration.AssessmentId.Length > 0,
                                startTime,
                                endTime);
                           
                        }

                        TimeFrame
                            datastreamTimeEvents = new TimeFrame(),
                            configurationTimeEvents = new TimeFrame(),
                            resultantTimeFrame_1 = new TimeFrame(),
                            resultantTimeFrame_2 = new TimeFrame();

                        datastreamTimeEvents.StartTime = startTimeEvents;
                        datastreamTimeEvents.EndTime = endTimeEvents;

                        configurationTimeEvents.StartTime = (new DateTime(1970, 1, 1)).AddMilliseconds(piConfiguration.HistoricalDataAccess.StartTime).ToLocalTime();
                        configurationTimeEvents.EndTime = (new DateTime(1970, 1, 1)).AddMilliseconds(piConfiguration.HistoricalDataAccess.EndTime).ToLocalTime();

                        resultantTimeFrame_1.StartTime = Utils.defaultDate;
                        resultantTimeFrame_1.EndTime = Utils.defaultDate;

                        resultantTimeFrame_2.StartTime = Utils.defaultDate;
                        resultantTimeFrame_2.EndTime = Utils.defaultDate;

                        Utils.ResultantTimeFrame(datastreamTimeEvents, configurationTimeEvents, resultantTimeFrame_1, resultantTimeFrame_2);

                      
                        if (!overrideHistorianData)
                        {
                            Log.Debug($"Historical PI data access... Override : false. Selected time Range : {piConfiguration.HistoricalDataAccess.StartTime} TO {piConfiguration.HistoricalDataAccess.EndTime}. Data Present in Falkonry : {startTimeEvents} TO {endTimeEvents} for  ' Config Id : Datastream Id :{piConfiguration.FalkonryConfiguration.SourceId} Datastream Name : {piConfiguration.FalkonryConfiguration.Datastream}'");
                            if (resultantTimeFrame_1.StartTime != Utils.defaultDate && resultantTimeFrame_1.EndTime != Utils.defaultDate)
                            {
                                Log.Debug($"Historical PI data access... Override : false. Resultant Time Range {resultantTimeFrame_1.StartTime} To {resultantTimeFrame_1.EndTime} for  '  Datastream Id :{piConfiguration.FalkonryConfiguration.SourceId} Datastream Name : {piConfiguration.FalkonryConfiguration.Datastream}'");
                                addEntityMeta = true;
                                if (addEntityMeta)
                                    FalkonryHelper.AddEntityInformation(piConfiguration.FalkonryConfiguration, attributes);
                                _historical.StartBackfilling(
                                    new FalkonryFeeder(
                                        piConfiguration.FalkonryConfiguration,
                                        false,
                                        piConfiguration.AfConfiguration),
                                    piConfiguration.AfConfiguration,
                                    piConfiguration.FalkonryConfiguration.AssessmentId.Length > 0,
                                    resultantTimeFrame_1.StartTime,
                                    resultantTimeFrame_1.EndTime);

                            }
                            if (resultantTimeFrame_2.StartTime != Utils.defaultDate && resultantTimeFrame_2.EndTime != Utils.defaultDate)
                            {
                                Log.Debug($"Historical PI data access... Override : false. Resultant Time Range {resultantTimeFrame_2.StartTime} To {resultantTimeFrame_2.EndTime} for  ' Datastream Id :{piConfiguration.FalkonryConfiguration.SourceId} Datastream Name : {piConfiguration.FalkonryConfiguration.Datastream}'");
                                addEntityMeta = true;
                                if (addEntityMeta)
                                    FalkonryHelper.AddEntityInformation(piConfiguration.FalkonryConfiguration, attributes);
                                _historical.StartBackfilling(
                                    new FalkonryFeeder(
                                        piConfiguration.FalkonryConfiguration,
                                        false,
                                        piConfiguration.AfConfiguration),
                                    piConfiguration.AfConfiguration,
                                    piConfiguration.FalkonryConfiguration.AssessmentId.Length > 0,
                                    resultantTimeFrame_2.StartTime,
                                    resultantTimeFrame_2.EndTime);
                            }
                        }

                        if (piConfiguration.FalkonryConfiguration.AssessmentId.Length > 0)
                        {
                            _historical.StartUploadingFacts(new FalkonryFeeder(
                                        piConfiguration.FalkonryConfiguration,
                                        false,
                                        piConfiguration.AfConfiguration),
                                        piConfiguration.AfConfiguration,
                                         Utils.ConvertMillisecondsToLOCAL(piConfiguration.HistoricalDataAccess.StartTime),
                                         Utils.ConvertMillisecondsToLOCAL(piConfiguration.HistoricalDataAccess.EndTime), overrideHistorianData);

                        }
                       
                    }
                }

                updateAFDataBase(piConfiguration);
            }
            catch (Exception error)
            {
                Log.Error($" Error in Uploading Historical Data Datastream Id :{piConfiguration.FalkonryConfiguration.SourceId} Datastream Name : {piConfiguration.FalkonryConfiguration.Datastream}'");
                Log.Error($"Stacktrace ",error);
            }

            return piConfiguration;
        }

        private void updateAFDataBase(PiConfiguration config)
        {
            try
            {
                for (int k = 0; k < config.AfConfiguration.EventFrameUploadedTime.Count(); k++)
                {
                    string[] eventTimeFrameList = config.AfConfiguration.EventFrameUploadedTime[k].Split(',');
                    string[] eventFrameList = config.AfConfiguration.EventFrameTemplateNames[k].Split(',');
                    for (int eventIndex = 0; eventIndex < eventTimeFrameList.Count(); eventIndex++)
                    {
                        if (eventFrameList[eventIndex] == "")
                        {
                            eventTimeFrameList[eventIndex] = "";
                        }
                        else if (eventTimeFrameList[eventIndex] == "")
                        {
                            eventTimeFrameList[eventIndex] = config.HistoricalDataAccess.StartTime.ToString();
                            eventTimeFrameList[eventIndex] += "-";
                            eventTimeFrameList[eventIndex] += config.HistoricalDataAccess.EndTime.ToString();
                        }
                        else
                        {
                            string[] timeFrame = eventTimeFrameList[eventIndex].Split('-');
                            DateTime startTime = Utils.ConvertMillisecondsToLOCAL(Convert.ToInt64(timeFrame[0]));
                            DateTime endTime = Utils.ConvertMillisecondsToLOCAL(Convert.ToInt64(timeFrame[1]));
                            TimeFrame
                                newTimeFrame = new TimeFrame(),
                                existingTimeFrame = new TimeFrame(),
                                resultantTimeFrame = new TimeFrame();

                            newTimeFrame.StartTime = Utils.ConvertMillisecondsToLOCAL(config.HistoricalDataAccess.StartTime);
                            newTimeFrame.EndTime = Utils.ConvertMillisecondsToLOCAL(config.HistoricalDataAccess.EndTime);

                            existingTimeFrame.StartTime = startTime;
                            existingTimeFrame.EndTime = endTime;

                            Utils.AggregateTimeFrame(existingTimeFrame, newTimeFrame, resultantTimeFrame);

                            eventTimeFrameList[eventIndex] = Utils.ConvertDateTimeToMilliseconds(resultantTimeFrame.StartTime).ToString();
                            eventTimeFrameList[eventIndex] += "-";
                            eventTimeFrameList[eventIndex] += Utils.ConvertDateTimeToMilliseconds(resultantTimeFrame.EndTime).ToString();
                        }
                    }
                    config.AfConfiguration.EventFrameUploadedTime[k] = string.Join(",", eventTimeFrameList);
                }

                PiConfigurationItem configItem = new PiConfigurationItem();
                configItem.Id = config.Id;
                configItem.Name = config.Name;
                configItem.Enabled = config.Enabled;
                configItem.Status = config.Status;
                configItem.Deleted = config.Deleted;
                configItem.Configuration = config;

                AfHelper.SaveSingleConfig(configItem);
            }
            catch (Exception e)
            {
                Log.Error($"Could not write config to database.  ' Config Id : {config.Id} Datastream Id :{config.FalkonryConfiguration.SourceId} Datastream Name : {config.FalkonryConfiguration.Datastream}' Error : {e.Message}");
                Log.Error($"Exception Stack Trace - . {e.StackTrace}");
            }
        }
    }

   
    
}
