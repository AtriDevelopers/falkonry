﻿using Falkonry.Integrator.PISystem.Helper;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Functors
{
    /**
     *  Pushing Live Data to Falkonry.
     * 
     */
    class LiveStreamToFalkonry
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private StreamingPi _streaming;
        private AFSubscriptions aFSubscriptions;

        public LiveStreamToFalkonry(AFSubscriptions aFSubscriptions)
        {
            this.aFSubscriptions = aFSubscriptions;
        }

        public Func<PiConfiguration, PiConfiguration> getDelegate()
        {
            return new Func<PiConfiguration, PiConfiguration>(startLiveStreaming);
        }

        public PiConfiguration startLiveStreaming(PiConfiguration config)
        {
            try
            {
                if (config.StreamingDataAccess.Enabled)
                {
                    var attributes = AfHelper.FindElementAttributes(
                                   config.AfConfiguration);
                    _streaming = new StreamingPi(config.StreamingDataAccess, config.FalkonryConfiguration, attributes, config.AfConfiguration);
                    _streaming.StartListening();
                    this.aFSubscriptions.addLiveStreamInDictionary(this, config);
                    Log.Info($"Live Streaming ON for Datastream: {config.FalkonryConfiguration.Datastream} Id:{config.FalkonryConfiguration.SourceId}");
                }
                else
                {
                    //Log.Info($"Live Streaming OFF for Datastream: {config.FalkonryConfiguration.Datastream} Id:{config.FalkonryConfiguration.SourceId}");
                }

            }
            catch (Exception e)
            {
                Log.Fatal($"Error in Live Stream Delegate \n {e.StackTrace}");
            }
            return config;
        }

        internal void stopStreaming()
        {
            _streaming.Stop();
        }
    }
}
