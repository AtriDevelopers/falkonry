﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Functors
{
    class StopLiveStreaming
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private AFSubscriptions aFSubscriptions;

        public StopLiveStreaming(AFSubscriptions aFSubscriptions)
        {
            this.aFSubscriptions = aFSubscriptions;
        }

        public Func<PiConfiguration, PiConfiguration> getDelegate()
        {
            return new Func<PiConfiguration, PiConfiguration>(stopLiveStreaming);
        }

        public PiConfiguration stopLiveStreaming(PiConfiguration config)
        {
            aFSubscriptions.removeLiveStream(config);
            return config;
        }
    }
}
