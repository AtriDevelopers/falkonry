﻿using Falkonry.Integrator.PISystem.Helper;
using FalkonryClient.Helper.Models;
using log4net;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Functors
{
    /**
     * Gets the Configuration Elements from the Database.
     */ 
    public class FetchConfigurationAFElements
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<long, IList<AFElement>> getDelegate()
        {
            return new Func<long, IList<AFElement>>(getConfigElements);
        }

        /**
         *  i/p long
         *  o/p List Of AFElements Present in the PISystem.
         */ 
        public IList<AFElement> getConfigElements(long tick)
        {
            IList<AFElement> elements = new List<AFElement>();
            try
            {
                var systems = new OSIsoft.AF.PISystems();
               
                foreach (var system in systems)
                {
                    var systemname = system.Name;

                    var af = new PISystems()[systemname];
                    af.Connect();
                    af.Refresh();

                    AFDatabase database = (AFDatabase)af.Databases[Constants.CONFIGDBNAME];
                    database.Refresh();
                    AFKeyedResults<String, AFElement> searchResult = AFElement.FindElementsByPath(new string[] { Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME }, database);
                    AFElement rootConfigElement;
                    searchResult.Results.TryGetValue(Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME, out rootConfigElement);
                    rootConfigElement.Refresh();
                   
                    elements = rootConfigElement.Elements.ToList()
                        .Where(x=> {
                            return !(bool)x.Attributes[Constants.ParentConfigConstant.Deleted].GetValue().Value;
                            }
                        ).ToList();
                    if (elements == null)
                    {
                        elements = new List<AFElement>();
                    }
                    Helper.AfHelper.StoreConfigurationForPISystem(systemname, new Configurations() { ConfigurationItems = new List<PiConfigurationItem>() });
                }
            }
            catch (Exception error)
            {
                Log.Fatal($" GetConfig Elements Failed ", error);
            }
            return elements ;
        }
    }

    /**
     * Transforms AFElement to PiConfiguration POCO.
     * Also calls falkonry api to get the livestream status of the Datastream.
     */ 
    public class TrasformAElementListToPiConfiguration
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<IList<AFElement>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<AFElement>, IList<PiConfiguration>>(transform);
        }

        public IList<PiConfiguration> transform(IList<AFElement> elements)
        {
            
            if (elements.Count == 0)
            {
                return new List<PiConfiguration>();
            }
            else
            {
                List<PiConfiguration> configurationList = elements.Select(getPiConfiguration).Where(x => x.isGood).ToList();
                
                return configurationList;

            }
           
        }

        private PiConfiguration getPiConfiguration(AFElement element)
        {
            // This code can make PiConfiguration properties uninitilized since there is an try-catch inside populateConfigFromElement
            PiConfiguration configuration = new PiConfiguration().populateConfigFromAFElement(element);
            //try
            //{
            //    // getting Livstream status from the falkonry server for the particular configuration.
            //    if (configuration.isGood)
            //    {
            //        IList<Datastream> datastream = FalkonryHelper.GetDatastreams(configuration.FalkonryConfiguration);
            //        Datastream stream = datastream.Where(y =>
            //        {
            //            return configuration.FalkonryConfiguration.SourceId == y.Id;
            //        }).FirstOrDefault();
            //        configuration.StreamingDataAccess.Enabled = stream.Live.ToLower().Equals("on") ? true : false;
            //        configuration.BatchIdentifier = stream.Field.BatchIdentifier;
            //    }
            //}
            //catch (Exception error)
            //{ 
            //   // Log.Fatal($"Get datastream failed Datastream name : {configuration.FalkonryConfiguration.Datastream} Token : {configuration.FalkonryConfiguration.Token} Host : {configuration.FalkonryConfiguration.Host} Trace : { error.StackTrace }");
            //}
            return configuration;
        }
       
    }

    /**
     * i/p list of pi configuration from af database
     * o/p same list is return but list items will be updated with livestream and batchidentifier
     */
    public class PopulateDatastreamConfigValuesFromFalkonryServer
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<IList<PiConfiguration>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<PiConfiguration>, IList<PiConfiguration>>(transform);
        }

        public IList<PiConfiguration> transform(IList<PiConfiguration> elements)
        {
           
            if (elements.Count == 0)
            {
                return new List<PiConfiguration>();
            }
            else
            {
                List<PiConfiguration> configurationList = elements.ToList();
                List<PiConfiguration> finalList = new List<PiConfiguration>();
                if (configurationList.Count > 0)
                {
                    Dictionary<string, List<PiConfiguration>> dictionary = new Dictionary<string, List<PiConfiguration>>();
                    configurationList.All(x =>
                    {
                        // Segreggating the PiConfigurations for different host and tokens
                        if (!dictionary.ContainsKey(x.FalkonryConfiguration.Host + "#" + x.FalkonryConfiguration.Token))
                        {
                            List<PiConfiguration> tmplist = configurationList.Where(y =>
                            {
                                return (y.FalkonryConfiguration.Host + "#" + y.FalkonryConfiguration.Token) == (x.FalkonryConfiguration.Host + "#" + x.FalkonryConfiguration.Token);
                            }).ToList();
                            dictionary.Add(x.FalkonryConfiguration.Host + "#" + x.FalkonryConfiguration.Token, tmplist);
                        }
                        return true;
                    });
                    dictionary.All(x =>
                    {
                        string[] key = x.Key.Split('#');
                        if (key.Length > 0)
                        {
                            try
                            {
                                IList<Datastream> datastream = FalkonryHelper.GetDatastreams(key[0], key[1]);
                                x.Value.All(y =>
                                {
                                    try
                                    {
                                        // popoulating the streaming data accesss and batch identifier values for each local datastream
                                        List<Datastream> streams = datastream.Where(z =>
                                        {
                                            return z.Id == y.FalkonryConfiguration.SourceId;
                                        }).ToList();
                                        if (streams.Count > 0)
                                        {
                                            Datastream stream = streams.FirstOrDefault();
                                            y.StreamingDataAccess.Enabled = stream.Live.ToLower().Equals("on") ? true : false;
                                            y.BatchIdentifier = stream.Field.BatchIdentifier;
                                            finalList.Add(y);
                                        }
                                        else
                                        {
                                            AfHelper.deleteConfigurationFromAFDatabase(y);
                                        }
                                       
                                    }
                                    catch (Exception error)
                                    {
                                        Log.Fatal($"Exception in setting Datastream BatchIdentifier and Streaming Data access Exception Message: {error.Message} \nException Stack Trace : {error.StackTrace}");
                                    }
                                    return true;
                                });
                            }
                            catch (Exception error)
                            {
                                Log.Fatal($"Exception in getting Datastreams  Exception Stack Trace : {error.StackTrace}");

                            }
                        }
                        return true;
                    });
                }
                Exception infraError = new Exception("Either Internet is down or the Falkonry back end not responding. Check preceding logs for more details");
                if (finalList.Count == 0)
                {
                    infraError.Data.Add("issue", "infra");
                    throw infraError;
                }
                return finalList;

            }

        }
    }

    /**
     * Compares the PiConfiguration from the AFDatabase and Falkonry back end.
     * Finds the PiConfiguration which need to be deleted from AFDatabase.
     */
    class FindPiconfigurationToDelete
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        
        public Func<IList<PiConfiguration>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<PiConfiguration>, IList<PiConfiguration>>(filterList);
        }
        /**
         *  i/p List of PiConfiguration Present in AFDatabase.
         *  o/p List of PiConfiguration Need to Deleted from AFDatabase.
         */ 
        public IList<PiConfiguration> filterList(IList<PiConfiguration> piconfig)
        {
            IList<PiConfiguration> results = new List<PiConfiguration>();
            if (piconfig.Count == 0)
            {
                return new List<PiConfiguration>();
            }
            else
            {
                List<PiConfiguration> configurationList = piconfig.ToList();
                List<PiConfiguration> finalList = new List<PiConfiguration>();
                Dictionary<string, List<PiConfiguration>> dictionary = new Dictionary<string, List<PiConfiguration>>();
                Log.Debug($"Entering into search of deleted datastreams");
                configurationList.All(x =>
                {
                    // Segreggating the PiConfigurations for different host and tokens
                    if (!dictionary.ContainsKey(x.FalkonryConfiguration.Host + "#" + x.FalkonryConfiguration.Token))
                    {
                        List<PiConfiguration> tmplist = configurationList.Where(y =>
                        {
                            return (y.FalkonryConfiguration.Host + "#" + y.FalkonryConfiguration.Token) == (x.FalkonryConfiguration.Host + "#" + x.FalkonryConfiguration.Token);
                        }).ToList();
                        dictionary.Add(x.FalkonryConfiguration.Host + "#" + x.FalkonryConfiguration.Token, tmplist);
                    }
                    return true;
                });
                dictionary.All(x =>
                {
                    string[] key = x.Key.Split('#');
                    if (key.Length > 0)
                    {
                        try
                        {
                            // Calling getDatastreams for each unique host+token combination pair
                            IList<Datastream> datastream = FalkonryHelper.GetDatastreams(key[0], key[1]);
                            x.Value.All(y =>
                            {
                                if (datastream.Where(z => z.Id == y.FalkonryConfiguration.SourceId).Count() > 0)
                                {
                                    // Datastream present on falkonry server
                                }
                                else
                                {
                                    // Datastream deleted from falkonry server
                                    Log.Debug($"Datastream Deleted from Falkonry Server Datastream: {y.FalkonryConfiguration.Datastream} Id:{y.FalkonryConfiguration.SourceId}");
                                    finalList.Add(y);
                                   
                                }
                                return true;
                            });
                        }
                        catch (Exception error)
                        {
                            Log.Fatal($"Exception in getting Datastreams  Exception Message: {error.Message} \n Exception Stack Trace : {error.StackTrace}");

                        }
                    }
                    return true;
                });
                Log.Debug($"Leaving from search of deleted datastreams delete Datastream Count: {finalList.Count}");
                return finalList;

            }
        }
            
           
        
    }
    

    class UpdateChangesInAFFromFalkonry
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<IList<PiConfiguration>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<PiConfiguration>, IList<PiConfiguration>>(updateList);
        }

        public IList<PiConfiguration> updateList(IList<PiConfiguration> piconfig)
        {
           
            piconfig.All(x =>
            {
                try
                {
                    Datastream datastream = FalkonryHelper.GetDatastream(x.FalkonryConfiguration);
                    UpdateDatastreamName(x, datastream);
                    /**
                     *   removed below call since we will not Create Assessment Binding in AF Database if any new assessment created at 
                     *   Falkonry back end
                     */ 
                    //getCreateAssessmentid(x, datastream);
                    DeleteAssessmentid(x, datastream);
                    return true;
                }
                catch (Exception e)
                {
                    Log.Fatal($"Get Datastream Exception :: {e.StackTrace}");
                    return false;
                }


            });

            return piconfig;
        }

        public Datastream UpdateDatastreamName(PiConfiguration config, Datastream datastream)
        {
            string dsOldName = "";
            if (datastream.Name.ToLower() != config.FalkonryConfiguration.Datastream.ToLower())
            {
                Log.Info($"syncWithFalkonry: Datastream name changed from {dsOldName} to {datastream.Name}");

                var af = new PISystems()[config.AfConfiguration.SystemName];
                af.Connect();
                af.Refresh();
                AFDatabase database = (AFDatabase)af.Databases[Constants.CONFIGDBNAME];
                database.Refresh();
                AFKeyedResults<String, AFElement> searchResult = AFElement.FindElementsByPath(new string[] { Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME }, database);
                AFElement rootConfigElement;
                searchResult.Results.TryGetValue(Utils.FalkonryConfigurationRootElement() + "\\" + Constants.CONFIGElementNAME, out rootConfigElement);
                rootConfigElement.Refresh();

                AFElement element = AFElement.FindElement(af, config.Id);

                AFElement falkonryConfig = element.Elements.Where(x => x.Name.Equals(Constants.FalkonryConfigConstants.name)).First();

                dsOldName = config.FalkonryConfiguration.Datastream;
                config.FalkonryConfiguration.Datastream = datastream.Name;
                config.Name = datastream.Name;
                element.Name = datastream.Name;

                var DSName = (string)element.Attributes[Constants.ParentConfigConstant.Name].GetValue().Value;
                DSName = datastream.Name;
                var falkonryds = (string)falkonryConfig.Attributes[Constants.FalkonryConfigConstants.DataStream]
                                                        .GetValue().Value;
                Log.Debug($"{falkonryds}");

                AFValue falkonrydss = new AFValue(datastream.Name);

                element.Attributes[Constants.ParentConfigConstant.Name].SetValue(falkonrydss);

                falkonryConfig.Attributes[Constants.FalkonryConfigConstants.DataStream]
                                                         .SetValue(falkonrydss);
                database.CheckIn();


                Log.Debug($"falkonryConfig : {falkonryConfig}");



            }
            return datastream;
        }

        public List<string> DeleteAssessmentid(PiConfiguration config, Datastream datastream)
        {
            try
            {
                List<Assessment> assessments = FalkonryHelper.GetAssessments(config.FalkonryConfiguration);
                List<string> AssessmentIdList = new List<string>(assessments.Count);
                assessments.All(z =>
                {
                    if (z.Datastream.Equals(config.FalkonryConfiguration.SourceId))
                    {
                        AssessmentIdList.Add(z.Id);
                    }
                    return true;
                });
                IList<PiConfiguration> piconfig = MemoryCache.Instance.getConfigurationsCopy();

                IEnumerable<PiConfiguration> result = MemoryCache.Instance.getConfigurationsCopy()
                   .Where(x => x.FalkonryConfiguration.SourceId == datastream.Id);

                List<string> assessmentListConfig = new List<string>(config.FalkonryConfiguration.AssessmentId);

                int index = 0;

                if (AssessmentIdList.Count < assessmentListConfig.Count)
                {
                    Log.Debug($"{assessmentListConfig.Count}");

                    foreach (string AssessmentId in assessmentListConfig.ToList())
                    {
                        if (AssessmentId != "" && !AssessmentIdList.Contains(AssessmentId))
                        {
                            AfHelper.UpdateState(config.AfConfiguration.SystemName, config.Name, "INFO", $"Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                            index = assessmentListConfig.IndexOf(AssessmentId);
                            assessmentListConfig.RemoveAt(index);
                            config.Enabled = true;
                            Log.Info($"syncWithFalkonry: Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                        }
                    }
                    config.FalkonryConfiguration.AssessmentId = assessmentListConfig.ToArray();
                    config.FalkonryConfiguration.AssessmentIdForFacts = assessmentListConfig.ToArray();

                    AfHelper.SaveSingleupdateConfig(config, datastream.Name);
                }
                return assessmentListConfig;
            }
            catch (Exception ex)
            {
                Log.Error($"Exception caught in DeleteAssessmentid {config}");
                Log.Error($"Exception StackTrace {ex.StackTrace}");
                return null;
            }
            //TODO : delete the assessment
            
        }

        public List<string> getCreateAssessmentid(PiConfiguration config, Datastream datastream)
        {
            try
            {
                List<Assessment> assessments = FalkonryHelper.GetAssessments(config.FalkonryConfiguration);
                List<string> AssessmentIdList = new List<string>(assessments.Count);
                assessments.All(z =>
                {
                    if (z.Datastream.Equals(config.FalkonryConfiguration.SourceId))
                    {
                        AssessmentIdList.Add(z.Id);
                    }
                    return true;
                });
                List<string> assessmentListConfig = new List<string>(config.FalkonryConfiguration.AssessmentId);

                int index = 0;
                // backend                   // af database
                if (AssessmentIdList.Count > assessmentListConfig.Count)
                {
                    foreach (string AssessmentId in AssessmentIdList)
                    {
                        if (AssessmentId != "" && !assessmentListConfig.Contains(AssessmentId))
                        {
                            AfHelper.UpdateState(config.AfConfiguration.SystemName, config.Name, "INFO", $"Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                            index = assessmentListConfig.IndexOf(AssessmentId);
                            assessmentListConfig.Add(AssessmentId);
                            config.Enabled = true;
                            Log.Info($"syncWithFalkonry: Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                        }
                    }
                    config.FalkonryConfiguration.AssessmentId = assessmentListConfig.ToArray();
                    config.FalkonryConfiguration.AssessmentIdForFacts = assessmentListConfig.ToArray();

                    AfHelper.SaveSingleupdateConfig(config, datastream.Name);
                    //TODO : Add the new assessment
                }

                return assessmentListConfig;
            }
            catch (Exception ex)
            {
                Log.Error($"Exception in Create Assessment {config}");
                Log.Error($"Exception StackTrace {ex.StackTrace}");
                return null;
            }
        }

    }


    /***
     * Deletes the List of PiConfigurations from the AFDatabase.
     */
    class DeletePiconfigurationsFromPiSystem
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private Subject<PiConfiguration> configDeletedSubject;

        public DeletePiconfigurationsFromPiSystem(Subject<PiConfiguration> deletesubject)
        {
            this.configDeletedSubject = deletesubject;

        }
        public Func<IList<PiConfiguration>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<PiConfiguration>, IList<PiConfiguration>>(getConfigElements);
        }

        /**
         * i/p Filtered list of the PiConfigurations need to be deleted from the AFDatabase.
         * o/p same List returned with no changes.
         */
        public IList<PiConfiguration> getConfigElements(IList<PiConfiguration> piconfig)
        {
           
            IList<AFElement> elements = new List<AFElement>();

            try
            {
                    piconfig.All(x =>
                    {
                        AfHelper.deleteConfigurationFromAFDatabase(x);
                        return true;


                    });
                    piconfig.All(x =>
                    {

                        configDeletedSubject.OnNext(x);
                        Log.Debug($"Config deleted notified observer {x.FalkonryConfiguration.Datastream}");
                        return true;
                    });
                
            }
            catch (Exception error)
            {
                Log.Error($"Exception in Reactive Delegate while Deletion Exception Message:{error.Message} \n Exception StackTrack: {error.StackTrace}");
            }
            return piconfig;
        }

    }

    /**
     * Compares the Livestream status of Configurations stored in the Local Memory cache
     * with Falkonry Server
     */
    class WatchLiveStreamConfig
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private Subject<PiConfiguration> liveconfig;
        public WatchLiveStreamConfig(Subject<PiConfiguration> watchconfig)
        {
            this.liveconfig = watchconfig;
        }
        public Func<long, IList<PiConfiguration>> getDelegate()
        {
            return new Func<long, IList<PiConfiguration>>(getDatastream);
        }

        private IList<PiConfiguration> getDatastream(long tick)
        {
            
            IList<PiConfiguration> config = MemoryCache.Instance.getConfigurationsCopy();
            try
            {
                // memory cache list
                config.All(x =>
                {
                    try
                    {
                        Datastream stream = FalkonryHelper.GetDatastream(x.FalkonryConfiguration);
                        if (x.StreamingDataAccess.Enabled != (stream.Live.ToLower().Equals("on") ? true : false))
                        {
                            x.StreamingDataAccess.Enabled = stream.Live.ToLower().Equals("on") ? true : false;
                            MemoryCache.Instance.removeConfiguration(x);
                            MemoryCache.Instance.addPiConfiguration(x);
                            /**
                             * this is the typical case of leaking references to the outer world.
                             * If any serious bugs then refactor this to deep clone of object.
                             */
                            this.liveconfig.OnNext(x);
                            Log.Info($"Config Live Stream Changed at Falkonry Backend {x.FalkonryConfiguration.Datastream} Live stream is {x.StreamingDataAccess.Enabled}");
                        }
                        else
                        {
                        }
                    }
                    catch (Exception error)
                    {
                        Log.Fatal($"Get datastream failed Datastream name : {x.FalkonryConfiguration.Datastream} Token : {x.FalkonryConfiguration.Token} Host : {x.FalkonryConfiguration.Host} Trace : { error.StackTrace }");
                    }
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Fatal($" GetConfig Elements Failed ", e);
            }

            return config;
        }
    }


    class FilterUpdatedPiConfigurationList
    {
        private Subject<PiConfiguration> configSuject;
        private Subject<PiConfiguration> historicalPushSubject;
        public FilterUpdatedPiConfigurationList(Subject<PiConfiguration> subject, Subject<PiConfiguration> historicalPush)
        {
            this.configSuject = subject;
            this.historicalPushSubject = historicalPush;
        }
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<IList<PiConfiguration>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<PiConfiguration>, IList<PiConfiguration>>(filter);
        }
        /**
         *  i/p List of PiConfiguration Present in AF Database.
         *  o/p List of PiConfiguration Updated in AF  AFDatabase.
         */
        public IList<PiConfiguration> filter(IList<PiConfiguration> configs)
        {
            try
            {
                IEnumerable<PiConfiguration> updateConfigs = configs.Where(new FindUpdatedConfig().getDelegate());
                if (updateConfigs.ToList().Count > 0)
                {
                    // Config Updated.
                    // 1. Find out which config is updated.
                    // 2. Remove that config from the local mem cache.
                    // 3. Add updated config to the local mem cache.
                    // 4. Notify DataStream Updated Observers;
                    updateConfigs.All(x =>
                    {
                        configSuject.OnNext(x);

                        // TODO add check for Config for which historical data processing is going on
                        // add check do we really need to push historical data for this input
                        historicalPushSubject.OnNext(x);
                        Log.Debug($"Config updated notified all observers {x.FalkonryConfiguration.Datastream}");
                        return true;
                    });

                    updateConfigs.All(x =>
                    {
                        MemoryCache.Instance.removeConfiguration(x);
                        MemoryCache.Instance.addPiConfiguration(x);
                        return true;
                    });

                }
            }
            catch (Exception e)
            {
                Log.Fatal("Exception in FilterUpdatedPiConfigurationList ", e);
            }
            return configs;
        }


    }

    class FilterCreatedPiConfigurationList
    {
        private Subject<PiConfiguration> configSuject;
        private Subject<PiConfiguration> historicalPushSubject;
        public FilterCreatedPiConfigurationList(Subject<PiConfiguration> subject, Subject<PiConfiguration> historicalPush)
        {
            this.historicalPushSubject = historicalPush;
            this.configSuject = subject;
        }
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<IList<PiConfiguration>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<PiConfiguration>, IList<PiConfiguration>>(filter);
        }

        /**
         * i/p List of PiConfigurations from AFDatabase/PiSystem
         * o/p  Same List is returned with no change.
         */
        public IList<PiConfiguration> filter(IList<PiConfiguration> configs)
        {
            try
            {
                IEnumerable<PiConfiguration> createdConfigs = configs.Where(new FindDeletedOrCreatedConfig(MemoryCache.Instance.getConfigurationsCopy()).getDelegate());
                if (createdConfigs.Count() > 0)
                {
                    // Config Created
                    // 1. Find Out Which Config is Created.
                    // 2. Add that config to local mem cache.
                    // 3. Notify DataStream Created Observers
                    createdConfigs.All(x =>
                    {
                        MemoryCache.Instance.addPiConfiguration(x);
                        return true;
                    });
                    createdConfigs.All(x =>
                    {
                        configSuject.OnNext(x);
                        // TODO Need to Check if historical push is already scheduled for this PIConfiguration

                        // add check do we really need to push historical data for this input

                        historicalPushSubject.OnNext(x);
                        Log.Info($"Config created Notifying {x.FalkonryConfiguration.Datastream}");
                        return true;
                    });
                }
            }
            catch (Exception e)
            {
                Log.Fatal("Exception in FilterCreatedPiConfigurationList", e);
            }

            return configs;
        }

    }

    /**
     * Compares the PiConfiguration from the AFDatabase and MemoryCache
     * Finds the PiConfiguration which need to be deleted from MemoryCache.
     */
    class FilterDeletedPiConfigurationList
    {
        private Subject<PiConfiguration> configSuject;
        private Subject<PiConfiguration> historicalPushSubject;
        public FilterDeletedPiConfigurationList(Subject<PiConfiguration> subject, Subject<PiConfiguration> historicalSubject)
        {
            this.configSuject = subject;
            this.historicalPushSubject = historicalSubject;
        }
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public Func<IList<PiConfiguration>, IList<PiConfiguration>> getDelegate()
        {
            return new Func<IList<PiConfiguration>, IList<PiConfiguration>>(filter);
        }

        /**
         * i/p List Of PiConfiguration from  AFDatabase/PiSystem
         * o/p Same List is returned with no change.
         */
        public IList<PiConfiguration> filter(IList<PiConfiguration> configs)
        {

            try
            {
                IEnumerable<PiConfiguration> deletedConfigs = MemoryCache.Instance.getConfigurationsCopy().Where(new FindDeletedOrCreatedConfig(configs).getDelegate());

                if (deletedConfigs.Count() > 0)
                {
                    // Config Deleted
                    // 1. Find out which config is deleted.
                    // 2. Remove that config from the local mem cache.
                    // 3. Notify DataStream Deleted Observers

                    deletedConfigs.All(x =>
                    {
                        configSuject.OnNext(x);
                        Log.Info($"Config deleted notified all observers {x.FalkonryConfiguration.Datastream}");
                        return true;
                    });

                    deletedConfigs.All(x =>
                    {
                        MemoryCache.Instance.removeConfiguration(x);
                        return true;
                    });


                }

            }
            catch (Exception e)
            {
                Log.Fatal("Exception in  FilterDeletedPiConfigurationList ", e);
            }

            return configs;
        }
    }

    /**
     *  Helper to find the Newly Created Configs in AFDatabase or Deleted Configs in AFDatabase
     */
    class FindDeletedOrCreatedConfig
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        public FindDeletedOrCreatedConfig(IList<PiConfiguration> configs)
        {
            masterList = configs;
        }
        /**
         * While Finding Created Config we are initializing 
         * master list with Local Mem Cache and 
         * For Finding Deleted Config we are initializing 
         * master list with AF Database
         */
        private IList<PiConfiguration> masterList;
        private bool findConfigs(PiConfiguration config)
        {
            bool yes = false;
            IEnumerable<PiConfiguration> result = masterList
                .Where(x => x.Id == config.Id);
            if (result.Count() > 0)
            {
                yes = false;
                // Log.Debug($"Config present {config.FalkonryConfiguration.Datastream}");
            }
            else
            {
                yes = true;
                //  Log.Debug($"Config not present {config.FalkonryConfiguration.Datastream}");
            }
            return yes;
        }

        public Func<PiConfiguration, bool> getDelegate()
        {
            return new Func<PiConfiguration, bool>(findConfigs);
        }
    }

    /**
     *  Helper to find the updated configs in the AFDatabase. Which need to be reflected in the Local mem cache.
     */
    class FindUpdatedConfig
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private bool findConfigs(PiConfiguration config)
        {
            bool updated = false;
            IEnumerable<PiConfiguration> result = MemoryCache.Instance.getConfigurationsCopy()
                .Where(x => x.Id == config.Id);
            if (result.Count() > 0)
            {
                // Config Present in Local MemCache
                PiConfiguration localMemCache = result.ElementAt(0);
                // Falkonry  comaprison
                if (localMemCache.FalkonryConfiguration.BatchLimit != config.FalkonryConfiguration.BatchLimit
                    || localMemCache.FalkonryConfiguration.Backfill != config.FalkonryConfiguration.Backfill
                    || localMemCache.FalkonryConfiguration.AssessmentId.Count() != config.FalkonryConfiguration.AssessmentId.Count()
                    || localMemCache.FalkonryConfiguration.AssessmentIdForFacts.Count() != config.FalkonryConfiguration.AssessmentIdForFacts.Count())
                {
                    updated = true;
                   
                    Log.Debug($"Falkonry element changed for Datastream: {config.FalkonryConfiguration.Datastream} Id:{config.FalkonryConfiguration.SourceId}"
                        +"\n"
                        + $"MemCache Batch Limit: {localMemCache.FalkonryConfiguration.BatchLimit} AFDatabase Batch Limit: {config.FalkonryConfiguration.BatchLimit}"
                        +"\n"
                        + $"MemCache Back Fill: {localMemCache.FalkonryConfiguration.Backfill} AFDatabase Back Fill: {config.FalkonryConfiguration.Backfill}"
                        + "\n"
                        + $"MemCache Assessment ID Count: {localMemCache.FalkonryConfiguration.AssessmentId.Count()} AFDatabase Assessment ID Count: {config.FalkonryConfiguration.AssessmentId.Count()}"
                        + "\n"
                        + $"MemCache Assessment Facts ID Count: {localMemCache.FalkonryConfiguration.AssessmentIdForFacts.Count()} AFDatabase Assessment Facts ID Count: {config.FalkonryConfiguration.AssessmentIdForFacts.Count()}"
                        );
                }
                // AF Comparison
                if (localMemCache.AfConfiguration.EventFrameTemplateNames.Count() != config.AfConfiguration.EventFrameTemplateNames.Count()
                    || localMemCache.AfConfiguration.OutputTemplateAttribute.Count() != config.AfConfiguration.OutputTemplateAttribute.Count())
                {
                    updated = true;
                    Log.Debug($"AF element changed Datastream: {config.FalkonryConfiguration.Datastream} Id:{config.FalkonryConfiguration.SourceId}"
                         + "\n"
                        + $"MemCache EventFrameTemplate Count: {localMemCache.AfConfiguration.EventFrameTemplateNames.Count()} AFDatabase EventFrameTemplate Count: {config.AfConfiguration.EventFrameTemplateNames.Count()}"
                         + "\n"
                        + $"MemCache OutputTemplateAttribute Count: {localMemCache.AfConfiguration.OutputTemplateAttribute.Count()} AFDatabase OutputTemplateAttribute Count: {config.AfConfiguration.OutputTemplateAttribute.Count()}"
                        );
                }
                // Historical Comparison
                if (localMemCache.HistoricalDataAccess.EndTime != config.HistoricalDataAccess.EndTime
                    || localMemCache.HistoricalDataAccess.StartTime != config.HistoricalDataAccess.StartTime
                    || localMemCache.HistoricalDataAccess.PageSize != config.HistoricalDataAccess.PageSize
                    || localMemCache.HistoricalDataAccess.Override != config.HistoricalDataAccess.Override)
                {
                    updated = true;
                    Log.Debug($"Historical element changed Datastream: {config.FalkonryConfiguration.Datastream} Id:{config.FalkonryConfiguration.SourceId}"
                         + "\n"
                        + $"MemCache Start Time: {localMemCache.HistoricalDataAccess.StartTime} AFDatabase Start Time: {config.HistoricalDataAccess.StartTime}"
                        + "\n"
                        + $"MemCache End  Time: {localMemCache.HistoricalDataAccess.EndTime} AFDatabase End  Time: {config.HistoricalDataAccess.EndTime}"
                        + "\n"
                        + $"MemCache PageSize: {localMemCache.HistoricalDataAccess.PageSize} AFDatabase PageSize: {config.HistoricalDataAccess.PageSize}"
                        + "\n"
                        + $"MemCache Override Flag: {localMemCache.HistoricalDataAccess.Override} AFDatabaseOverride Flag: {config.HistoricalDataAccess.Override}"
                        );
                }
                if (localMemCache.StreamingDataAccess.CollectionIntervalMs != localMemCache.StreamingDataAccess.CollectionIntervalMs)
                {
                    updated = true;
                    Log.Debug($"Live Streaming element changed Datastream: {config.FalkonryConfiguration.Datastream} Id:{config.FalkonryConfiguration.SourceId}"
                        + "\n"
                        + $"MemCache Collection Interval Flag: {localMemCache.StreamingDataAccess.CollectionIntervalMs} AFDatabase Collection Interval Flag: {config.StreamingDataAccess.CollectionIntervalMs}"
                        );
                }
            }
            else
            {
                // Config Not Present in Local MemCache.
            }
            return updated;
        }

        public Func<PiConfiguration, bool> getDelegate()
        {
            return new Func<PiConfiguration, bool>(findConfigs);
        }
    }
}
