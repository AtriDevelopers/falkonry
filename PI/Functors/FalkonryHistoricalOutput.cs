﻿using Falkonry.Integrator.PISystem.Helper;
using FalkonryClient.Helper.Models;
using log4net;
using Newtonsoft.Json;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem.Functors
{
    /**
     * 
     *   Back Fills the Falkonry Historical Output to the PI System Database
     * 
     */ 
    public class FalkonryHistoricalOutput
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private PiConfiguration config;
        private static SortedDictionary<string, string> _options;
        private static SortedDictionary<string, string> _toptions;
        public Func<PiConfiguration, PiConfiguration> getDelegate()
        {
            return new Func<PiConfiguration, PiConfiguration>(BackFillHistoricalOutput);
        }

        public PiConfiguration BackFillHistoricalOutput(PiConfiguration config)
        {
            try
            {
               
                this.config = config;
                // Historical Output
                if (config.FalkonryConfiguration.Backfill)
                {
                    // TODO :: Need to refactor StartBackFill from tasks to observables.
                    //var receiver = new FalkonryReceiver(config.FalkonryConfiguration, config.AfConfiguration, null);
                    //receiver.StartBackfill(Utils.ConvertMillisecondsToUTC(config.HistoricalDataAccess.StartTime),
                    //    Utils.ConvertMillisecondsToUTC(config.HistoricalDataAccess.EndTime));
                    FalkonryHistoricalObserver observer = new FalkonryHistoricalObserver(config, null);
                    // TODO :: Need to remove this dependecy after Reactive Refactor
                    int index = 0;
                    Observable.ToObservable(config.FalkonryConfiguration.AssessmentId)
                        .SubscribeOn(new EventLoopScheduler())
                        .ObserveOn(new EventLoopScheduler())
                        .Select(x =>

                        {
                            try
                            {
                                
                                var assessmentList = FalkonryHelper.GetAssessments(config.FalkonryConfiguration);
                                var startTime = Utils.ConvertMillisecondsToUTC(config.HistoricalDataAccess.StartTime);
                                var endTime = Utils.ConvertMillisecondsToUTC(config.HistoricalDataAccess.EndTime);
                                var _assessment = assessmentList.Find(item => item.Id == x);
                                _options = new SortedDictionary<string, string>
                                    {
                                    {"startTime", startTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")},
                                    {"endTime", endTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")},
                                    {"responseFormat", "application/json"}
                                    };
                                Log.Debug($" Creating request for historical output. for datastream: {config.FalkonryConfiguration.Datastream}. Assessment : {_assessment.Name} Time Range : { startTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")} TO {endTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffff")} ...");
                                var response = FalkonryHelper.GetHistoricalOutput(config.FalkonryConfiguration, _assessment, _options);
                                ProcessHistoricalOutput(_assessment, response, index);
                                AfHelper.updateFalkonryBackfill(config.CopyObject<PiConfiguration>());
                                Log.Debug($"BackFill Process Completed for Datastream: {config.FalkonryConfiguration.Datastream} Id: {config.FalkonryConfiguration.SourceId}");

                            }
                            catch (Exception exception)
                            {
                                Log.Error($"  Error message in assessment Method : StartBackfill Error :  {exception.Message} for datastream: {config.FalkonryConfiguration.Datastream}.");
                            }
                            index++;
                            return x;
                        })
                        .Subscribe(observer);
                       
                }
            }

            catch(Exception ex)
            {
                Log.Error($" Exception caught in BackHistoricaloutput stacktrace {ex.StackTrace}");
                Log.Error($" Exception caught in BackHistoricaloutput {config}");
            }
            return config;

        }

        private void ProcessHistoricalOutput(Assessment assessment, HttpResponse response, int index)
        {
            Log.Debug($" Response Code : .GetHistoricalOutput {response.StatusCode} for datastream: {config.FalkonryConfiguration.Datastream} for assessment :{assessment.Name}");

            if (response.StatusCode == 200)
            {
                var events = response.Response.Split('\n').ToList()
                    .Where(e => e != "")
                    .Select(JsonConvert.DeserializeObject<FalkonryEvent>)
                    .OrderBy(e => long.Parse(e.time))
                    .ToList();

                BackfillEvents(events, index);
                Log.Debug($"Successfully output generated for backfill request. for datastream: {config.FalkonryConfiguration.Datastream}  for assessment :{assessment.Name}");
                AfHelper.UpdateState(config.AfConfiguration.SystemName, config.FalkonryConfiguration.Datastream, "INFO", $"Successfully process historical output generated for backfill request for assessment :{assessment.Name}");
                return;
            }
            if (response.StatusCode == 202)
            {
                Log.Debug($" Response JSON : .GetHistoricalOutput response length - {response.Response.Length} for datastream: {config.FalkonryConfiguration.Datastream} for assessment :{assessment.Name}");
                var trackerResponse = JsonConvert.DeserializeObject<Tracker>(response.Response);

                if (trackerResponse.Id != null)
                {
                    var id = trackerResponse.Id;

                    Log.Debug($" Tracker Id: {id} for datastream: {config.FalkonryConfiguration.Datastream}");

                    _toptions = new SortedDictionary<string, string>
                    {
                        {"trackerId", id},
                        {"responseFormat", "application/json"}
                    };

                    Thread.Sleep(5000);
                    response = FalkonryHelper.GetHistoricalOutput(config.FalkonryConfiguration, assessment, _toptions);
                }
                else
                {
                    Thread.Sleep(5000);
                    response = FalkonryHelper.GetHistoricalOutput(config.FalkonryConfiguration, assessment, _toptions);
                }

                ProcessHistoricalOutput(assessment, response, index);

                return;
            }
            if (response.StatusCode > 400)
            {
                Log.Debug($" Response JSON : .GetHistoricalOutput {response.Response} for datastream: {config.FalkonryConfiguration.Datastream} for assessment :{assessment.Name}");
                //Thread.Sleep(15000);
                //response = _falkonry.GetHistoricalOutput(assessment, _options);
                //ProcessHistoricalOutput(assessment, response);
                Log.Error($"Could not process historical output for Assessment '{assessment.Name}' Datastream Id {config.FalkonryConfiguration.SourceId}.");
                AfHelper.UpdateState(config.AfConfiguration.SystemName, config.FalkonryConfiguration.Datastream, "ERROR", $"Could not process historical output for backfill request. Response: {response.Response}");

                return;
            }
        }


        private void BackfillEvents(List<FalkonryEvent> events, int index)
        {
            var elements = events.Select(e => e.entity).Distinct();
            foreach (var element in elements)
            {
                if (element == "") continue;

                var attribute = AfHelper.FindElementAttribute(Guid.Parse(element), config.AfConfiguration, index);
                if (attribute == null) continue;

                var elementEvents = events.Where(e => e.entity == element);
                var listOfAfValues = new AFValues();
                foreach (var elementEvent in elementEvents)
                {
                    var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    dtDateTime = dtDateTime.AddMilliseconds(long.Parse(elementEvent.time)).ToLocalTime();
                    listOfAfValues.Add(new AFValue(elementEvent.value, dtDateTime));
                }

                if (listOfAfValues.Count <= 0) continue;
                try
                {
                    attribute.Data.UpdateValues(listOfAfValues, AFUpdateOption.Replace);
                }
                catch (Exception exception)
                {
                    Log.Error($"Count not update the Attribute '{attribute.Name}'. Error: '{exception.Message}'");
                }
            }
        }
    }

    class FalkonryHistoricalObserver : IObserver<string>
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private PiConfiguration configuration;
        private AFSubscriptions afSubscriptions;
        public FalkonryHistoricalObserver(PiConfiguration config, AFSubscriptions subscriptions)
        {
            this.configuration = config;
            this.afSubscriptions = subscriptions;
        }
        public void OnCompleted()
        {
            Log.Debug($"OnComplete of the Historical Output for DataStream {configuration.FalkonryConfiguration.Datastream}");
        }

        public void OnError(Exception error)
        {
            Log.Fatal($"Error in Creating Falkonry Receiver ", error);
        }

        public void OnNext(string value)
        {
            Log.Debug($"Histofical Back Fill Completed For DataStream {configuration.FalkonryConfiguration.Datastream} and AssessmentID {value}");
        }
    }
}
