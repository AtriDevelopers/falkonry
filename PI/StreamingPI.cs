﻿using System;
using System.Collections.Generic;
using System.Linq;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Data;
using System.Threading;
using log4net;
using System.Reflection;
using FalkonryClient;
using FalkonryClient.Helper.Models;
using Falkonry.Integrator.PISystem.Helper;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace Falkonry.Integrator.PISystem
{
    public class StreamingPi: IDisposable
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
       
        
        private DataPipesHelper _mainPipe;
        

        private StreamingConfiguration Configuration { get; }
        private FalkonryConfiguration FConfiguration { get; }
        private AfConfiguration AConfiguration { get; }
        private AFAttributeList ConfiguredAttributes { get; }

        public StreamingPi(StreamingConfiguration streamingConfiguation, FalkonryConfiguration falkonryConfiguration, AFAttributeList attributes, AfConfiguration afConfiguration)
        {
            Log.Debug($"Creating Streaming PI object for Datastream : {falkonryConfiguration.Datastream} Id : {falkonryConfiguration.SourceId}");
            Log.Debug($"Configuration = {streamingConfiguation}");
            Log.Debug($"AFAttributes Count = {attributes.Count}");
            Configuration = streamingConfiguation;
            FConfiguration = falkonryConfiguration;
            AConfiguration = afConfiguration;
            ConfiguredAttributes = attributes;
        }
        public void StartListening()
        {
            Log.Debug($"Start Streaming Listening for Datastream : {FConfiguration.Datastream} Id : {FConfiguration.SourceId}");
            Log.Debug("Creating AF Data Pipe Helper");
            _mainPipe = new DataPipesHelper(FConfiguration, AConfiguration, Configuration, this);
            Log.Debug($"Signing up AFAttributes, Count={ConfiguredAttributes.Count}");

            _mainPipe.AddSignup(ConfiguredAttributes);
            Log.Debug($"Listening for new Data Pipe Events on Interval {Configuration.CollectionIntervalMs}");

            _mainPipe.StartListening(Configuration.CollectionIntervalMs);
        }
        public void Stop()
        {
            Log.Debug($"Stop Streaming Listening for Datastream : {FConfiguration.Datastream} Id : {FConfiguration.SourceId}");
            Log.Debug("Stopping Data Pipe Helper");
            _mainPipe.StopListening();
            _mainPipe.Dispose();
        }

        public void Dispose()
        {
            Log.Debug($"Disposing Data Pipe for Datastream '{FConfiguration.Datastream}'");
            try { _mainPipe.Dispose(); }
            catch (Exception) { }
        }
    }

    public class DataPipesHelper : IDisposable
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        

        private readonly AFDataPipe _afDataPipe = new AFDataPipe();
        private Timer _timer;
        private StreamingPi referenceToStreaminPI;
        private FalkonryStreamingDataReceiver afDataReceiver;

        public DataPipesHelper(FalkonryConfiguration configuration, AfConfiguration afConfiguration, StreamingConfiguration streamingConfiguration, StreamingPi streamingPI)
        {
            Log.Debug("Creating Data Pipe and adding subscription for Falkonry");
            referenceToStreaminPI = streamingPI;
            afDataReceiver = new FalkonryStreamingDataReceiver(configuration, afConfiguration, streamingConfiguration, referenceToStreaminPI);
            _afDataPipe.Subscribe(afDataReceiver);
            
        }

        public void StartListening(int checkInterval)
        {
            Log.Debug("Listening for new events");
            if (_timer == null)
                _timer = new Timer(CheckForData, null, 0, checkInterval);
            afDataReceiver.internalStartListening();
        }

        public void StopListening()
        {
            Log.Debug("Stopping listening for new events");
            _timer?.Dispose();
            afDataReceiver.internalStopListening();
        }

        public void AddSignup(IList<AFAttribute> attributes)
        {
            Log.Debug("Adding new AFAttributes");
            _afDataPipe.AddSignups(attributes);
        }

        private void CheckForData(object o)
        {
            //log.Debug("Checking for new data...");
            bool hasMoreEvents;
            _afDataPipe.GetObserverEvents(out hasMoreEvents);
        }

        public void Dispose()
        {
            Log.Debug("Disposing the Data Pipe Helper");
            StopListening();
            _afDataPipe.Dispose();
        }
    }
    public class FalkonryStreamingDataReceiver 
        : IObserver<AFDataPipeEvent>
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private readonly FalkonryFeeder _falkonry;
        private readonly FalkonryConfiguration _falkonryConfiguration;
        private readonly AfConfiguration _afConfiguration;
        private readonly int _flushTimer;
        private IDisposable internalSubscriberForSendEvents;
        private IObservable<long> internalObservableForSendEvents; 

        public List<AFDataPipeEvent> _events;
        public int _logBatchId { get; private set; }
        public bool isSendingData =false;
        

        public FalkonryStreamingDataReceiver(FalkonryConfiguration configuration, AfConfiguration afConfiguration, StreamingConfiguration streamingConfiguration, StreamingPi referenceToStreaminPI)
        {
            Log.Debug($"Creating new FalkonryFeeder for  Datastream : {configuration.Datastream} Id : {configuration.SourceId}");
            _logBatchId = 0;
            _flushTimer = streamingConfiguration.CollectionIntervalMs; // 10 sec
            _falkonryConfiguration = configuration;
            _afConfiguration = afConfiguration;
            _events = new List<AFDataPipeEvent>();
            _falkonry = new FalkonryFeeder(configuration, true, afConfiguration);
            isSendingData = false;
            internalObservableForSendEvents = Observable.Timer(TimeSpan.FromSeconds(2), TimeSpan.FromMilliseconds(_flushTimer))
                .SubscribeOn(NewThreadScheduler.Default)
                .ObserveOn(new EventLoopScheduler());
          
        }
        public void internalStopListening()
        {
            internalSubscriberForSendEvents?.Dispose();
        }
        public void internalStartListening()
        {
            internalSubscriberForSendEvents?.Dispose();
            var values = new AFValues();
            internalSubscriberForSendEvents = internalObservableForSendEvents.Subscribe(x =>
              {
                  if (_events.Count > 0)
                  {
                      _logBatchId++;
                      int newId = Thread.CurrentThread.ManagedThreadId;
                      Log.Info($"ingestion thread id: {newId} ");
                      if (isSendingData)
                      {
                          Log.Debug("Data ingestion is already in progress.. This data will be sent in next batch. Current batch id : " + _logBatchId + " for Datastream : " + _falkonryConfiguration.Datastream);
                          return;
                      }
                      isSendingData = true;
                      Log.Debug("is Sending Data Made True Current batch id : " + _logBatchId + " for Datastream : " + _falkonryConfiguration.Datastream);
                      values = new AFValues();
                      lock (_events)
                      {
                          values.AddRange(_events.Select(afDataPipeEvent => afDataPipeEvent.Value));
                          _events = new List<AFDataPipeEvent>();
                      }
                      var events = new List<AFValues> { values };
                      try
                      {
                          Log.Debug("Send Events Call Initiated Current batch id : " + _logBatchId + " for Datastream : " + _falkonryConfiguration.Datastream);
                          _falkonry.SendEvents(events, _logBatchId);
                          Log.Debug("Send Events Call Completed Current batch id : " + _logBatchId + " for Datastream : " + _falkonryConfiguration.Datastream);
                          isSendingData = false;
                      }
                      catch (Exception e)
                      {
                          Log.Error($"Received error while fetching datastream : {_falkonryConfiguration.Datastream}. error is : {e.Message}");
                          isSendingData = false;
                      }
                  }
              }, x =>
              {
                  Log.Fatal($"Exception in settting observable timer for FalkonryStreamingReceiver Exception Message: {x.Message} \n Exception StackTrace: {x.StackTrace}");
              });
        }

        public void OnCompleted()
        {
           
        }

        public void OnError(Exception error)
        {
            Log.Fatal($"Exception in DataPipe Observer Exception Message: {error.Message} \n Exception StackTrace: {error.StackTrace}");
        }

        public void OnNext(AFDataPipeEvent pipeEvent)
        {
            lock (_events)
            {
               _events.Add(pipeEvent);
            }
        
        }
    }
}
