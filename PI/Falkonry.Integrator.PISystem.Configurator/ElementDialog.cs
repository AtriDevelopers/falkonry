﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Falkonry.Integrator.PISystem.Configurator
{
   

    public partial class ElementDialog : Form
    {
        public ConfigurationForm form_parent;
        public Label Element;

        public ElementDialog(ConfigurationForm form, Label label)
        {
            InitializeComponent();
            form_parent = form;
            Element = label;
        }

        private void ElementDialog_Load(object sender, EventArgs e)
        {
            textBox1.Text = Element.Text;
            this.textBox1.ForeColor = Color.Red;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
