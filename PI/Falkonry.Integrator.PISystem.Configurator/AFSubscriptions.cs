﻿using Falkonry.Integrator.PISystem.Functors;
using Falkonry.Integrator.PISystem.Helper;
using Falkonry.Integrator.PISystem.Subscribers;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Falkonry.Integrator.PISystem.Configurator
{
    public class AFSubscriptions
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private AFObservableLayer aflayer;
       
        private IDisposable configUpdatedSubscription;
        private ConfigurationForm configurationForm;

        public AFSubscriptions(AFObservableLayer layer, ConfigurationForm configurationForm) 
        {
            this.configurationForm = configurationForm;
            aflayer = layer;
        }

        public void setUpObservablePipeLine()
        {
            setUpConfigUpdatedPipeline(); 
        }
        


        private void setUpConfigUpdatedPipeline()
        {
            if (configUpdatedSubscription == null)
            {
                configUpdatedSubscription = aflayer.getObservableForConfigUpdated()
                     .ObserveOn(Dispatcher.CurrentDispatcher)
                    .Subscribe(new PiConfigLiveStreamChangeSubscribter(configurationForm));
            }
        }

        public void clearAllSubscriptions()
        {
          
            if (configUpdatedSubscription != null)
                configUpdatedSubscription.Dispose();

          
        }

        private class PiConfigLiveStreamChangeSubscribter : IObserver<PiConfiguration>
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
            private ConfigurationForm configurationForm;

            public PiConfigLiveStreamChangeSubscribter(ConfigurationForm configurationForm)
            {
                this.configurationForm = configurationForm;
            }

            public void OnCompleted()
        {
            Log.Info($"Subscriber On Completed Invoked");
        }

        public void OnError(Exception exception)
        {
            Log.Fatal($"Exception While Processing for DataStream ", exception);
        }

        public void OnNext(PiConfiguration value)
        {
                configurationForm.checkUpdatedConfig(value);
        }
    }

}
}
