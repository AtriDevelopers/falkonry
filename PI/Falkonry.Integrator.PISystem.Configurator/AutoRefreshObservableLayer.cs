﻿using Falkonry.Integrator.PISystem.Functors;
using Falkonry.Integrator.PISystem.Helper;
using FalkonryClient.Helper.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Falkonry.Integrator.PISystem.Configurator
{
    class AutoRefreshObservableLayer
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private double afChangeEventCollectionSeconds;
        /**
         * Observable to Query the AFDatabase at Regular Interval of time and Refresh the Configurator UI
         */
        private IObservable<long> bootStraper;


        private IDisposable bootStrapSubscription;

        private ConfigurationForm form;

        private IDisposable autoRefreshEventLoopScheduler;


        public AutoRefreshObservableLayer(ConfigurationForm form)
        {
            this.form = form;
        }

        private void initialize()
        {

            afChangeEventCollectionSeconds = 30.0;
            //autoRefreshEventLoopScheduler = new EventLoopScheduler();
            bootStraper = Observable.Timer(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(afChangeEventCollectionSeconds))
                                    .SubscribeOn(NewThreadScheduler.Default)
                                    .ObserveOn(new EventLoopScheduler())
                                     .Select(x => {
                                         Log.Info($"AutoRefreshLayer Sync AF DB and Local Memcache started minute tick :{x}"); return x;
                                     });

        }


        /**
    * 
    *  Compulsorily need to call setup method in order pump AF Database Change event to the Observable Pipeline. 
*/
        public void setUp()
        {
            initialize();
            bootStrapSubscription = bootStraper
                        .Select(new FetchConfigurationAFElements().getDelegate())
                        .Select(new TrasformAElementListToPiConfiguration().getDelegate())
                        .Select(new PopulateDatastreamConfigValuesFromFalkonryServer().getDelegate())
                        .Select(x =>
                        {
                            IList<PiConfigurationItem> oldPocoItem = new List<PiConfigurationItem>();
                            Dictionary<string, List<Assessment>> dictionary = new Dictionary<string, List<Assessment>>();
                            x.All(y =>
                            {
                                try
                                {
                                    
                                    // Populating dictionary with list of assessments for each unique host + token unique combinations.
                                    if (!dictionary.ContainsKey(y.FalkonryConfiguration.Host +"#"+ y.FalkonryConfiguration.Token))
                                    {
                                        List<Assessment> tmpAssessment = FalkonryHelper.GetAssessments(y.FalkonryConfiguration.Host, y.FalkonryConfiguration.Token);
                                        dictionary.Add(y.FalkonryConfiguration.Host + "#" + y.FalkonryConfiguration.Token, tmpAssessment);
                                    }
                                }
                                catch (Exception error)
                                {
                                    Log.Info($"Exception in populating assessment dictionary stacktrace {error.StackTrace}");
                                }
                                return true;
                            });
                            
                            x.All(y =>
                            {
                                Log.Info($"Total Datastream Count {x.Count}");
                                try
                                {
                                    if (dictionary.ContainsKey(y.FalkonryConfiguration.Host + "#" + y.FalkonryConfiguration.Token))
                                    {
                                        y.FalkonryConfiguration.assessmentList = new List<Assessment>();
                                        if(dictionary[y.FalkonryConfiguration.Host + "#" + y.FalkonryConfiguration.Token].Count > 0)
                                        {
                                            y.FalkonryConfiguration.assessmentList.AddRange(dictionary[y.FalkonryConfiguration.Host + "#" + y.FalkonryConfiguration.Token].Where(z => z.Datastream.Equals(y.FalkonryConfiguration.SourceId)));
                                        }
                                        DeleteAssessmentid(y, y.FalkonryConfiguration.assessmentList);
                                        PiConfigurationItem item = new PiConfigurationItem()
                                        {
                                            BatchIdentifier = y.BatchIdentifier,
                                            Configuration = y,
                                            Deleted = y.Deleted,
                                            Enabled = y.Enabled,
                                            ErrorMessage = y.ErrorMessage,
                                            Name = y.Name,
                                            Id = y.Id,
                                            Status = y.Status
                                        };
                                            if (!y.Deleted)
                                                oldPocoItem.Add(item);
                                        }
                                   
                                }
                                catch (Exception e)
                                {
                                    Log.Fatal($"Exception in fetching Assessments for Datastream: {y.FalkonryConfiguration.Datastream} Id: {y.FalkonryConfiguration.SourceId} ", e);
                                }
                               
                                return true;
                            });

                            Exception infraError = new Exception("Either Internet is down or the Falkonry back end not responding. Check preceding logs for more details");
                            if (x.Count>0 && oldPocoItem.Count == 0)
                            {
                                infraError.Data.Add("issue", "infra");
                                throw infraError;
                            }
                            return oldPocoItem;
                        })
                        .ObserveOn(Dispatcher.CurrentDispatcher)
                        .Subscribe(new TimerSubscription(this));



        }



        public string FalkonryConfigurationRootElement()
        {
            return @"Falkonry\Integrator";
        }

        public void DeleteAssessmentid(PiConfiguration config, List<Assessment> assessmentList)
        {
            try
            {
                List<string> AssessmentIdList = new List<string>(assessmentList.Count);
                foreach(Assessment item in assessmentList)
                {
                    AssessmentIdList.Add(item.Id);
                }
                List<string> assessmentListConfig = new List<string>(config.FalkonryConfiguration.AssessmentId);

                int index = 0;

                if (AssessmentIdList.Count < assessmentListConfig.Count)
                {
                    foreach (string AssessmentId in assessmentListConfig.ToList())
                    {
                        if (AssessmentId != "" && !AssessmentIdList.Contains(AssessmentId))
                        {
                            AfHelper.UpdateState(config.AfConfiguration.SystemName, config.Name, "INFO", $"Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                            index = assessmentListConfig.IndexOf(AssessmentId);
                            assessmentListConfig.RemoveAt(index);
                            config.Enabled = true;
                            Log.Info($"Datastream: {config.FalkonryConfiguration.Datastream} Id: {config.FalkonryConfiguration.SourceId} Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                        }
                    }
                    config.FalkonryConfiguration.AssessmentId = assessmentListConfig.ToArray();
                    config.FalkonryConfiguration.AssessmentIdForFacts = assessmentListConfig.ToArray();

                    AfHelper.SaveSingleupdateConfig(config, config.FalkonryConfiguration.Datastream);
                }


            }
            catch (Exception ex)
            {
                Log.Error($"Exception caught in DeleteAssessmentid {config}");
                Log.Error($"Exception StackTrace {ex.StackTrace}");
               
            }
            //TODO : delete the assessment

        }

        public void tearDown()
        {

            if (bootStrapSubscription != null)
                bootStrapSubscription.Dispose();

            //autoRefreshEventLoopScheduler?.Dispose();
        }


        private class TimerSubscription : IObserver<IList<PiConfigurationItem>>
        {
            AutoRefreshObservableLayer layer;
            public TimerSubscription(AutoRefreshObservableLayer layer)
            {
                this.layer = layer;
            }
            public void OnCompleted()
            {
                Log.Info($" Auto Refresh Layer Observable Completed");
            }

            public void OnError(Exception error)
            {
                Log.Fatal($" AFObservable Layer BootStrap Observable Stopped \n StackTrace :{error.StackTrace}");
                /**
                 * This is an internet connectivity issue or the falkonry back end is not responding.
                 * Restart obserable pipeline
                 */ 
                if (error.Data.Contains("issue"))
                {
                    layer.tearDown();
                    layer.setUp();
                }
            }

            public void OnNext(IList<PiConfigurationItem> value)
            {
                Log.Info($"Auto Refresh layer  on next Configuration count is {value.Count}");
                layer.form.refreshConfigurationlist(value);
            }
        }
    }
}

 


