﻿using System;
using System.IO;
using System.Collections;
using System.IO.Compression;
using System.Linq;
using System.ServiceProcess;
using System.Windows.Forms;
using Newtonsoft.Json;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.EventFrame;
using OSIsoft.AF.UI;
using CheckComboBoxTest;
using System.Drawing;
using FalkonryClient.Helper.Models;
using System.Collections.Generic;
using System.Globalization;
using Falkonry.Integrator.PISystem.Helper;
using log4net;
using System.Reflection;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Drawing.Text;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Falkonry.Integrator.PISystem.Configurator
{

    public partial class ConfigurationForm : Form
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        //log4net.Config.XmlConfigurator.Configure();
        private readonly object syncLock = new object();
        private readonly string piServiceName = "Falkonry PI Agent Service";
        private AutoRefreshObservableLayer refreshLayer;
        string[] EventFrameText = new string[0];
        bool test;
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
            IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        string oldSelectedDSName = "";

        FontFamily ff;
        Font font;
        bool backFillInitiationInProgress = false;

        private void LoadFont()
        {
            byte[] fontarray = Falkonry.Integrator.PISystem.Configurator.Resources.Roboto_Regular;
            int datalength = Falkonry.Integrator.PISystem.Configurator.Resources.Roboto_Regular.Length;
            IntPtr ptr = Marshal.AllocCoTaskMem((int)datalength);

            Marshal.Copy(fontarray, 0, ptr, datalength);
            uint CfFont = 0;
            AddFontMemResourceEx(ptr, (uint)fontarray.Length, IntPtr.Zero, ref CfFont);
            PrivateFontCollection pfc = new PrivateFontCollection();
            pfc.AddMemoryFont(ptr, datalength);
            Marshal.FreeCoTaskMem(ptr);
            ff = pfc.Families[0];
            Font = new Font(ff, 10f, FontStyle.Regular);
        }

        public void alloc_font(Font f, Control c, double size)
        {
            FontStyle style = FontStyle.Regular;
            c.Font = new Font(ff, (float)size, style);
        }
        public ConfigurationForm()
        {

            InitializeComponent();
            labelFooter.Text = "© 2018 Falkonry Inc. | Version: " + Application.ProductVersion;
            observableLayer = new AFObservableLayer();
            observableLayer.setUp();
            refreshLayer = new AutoRefreshObservableLayer(this);
            refreshLayer.setUp();
            // TODO :: Need to refactor this when we migrate to WPF
            subscriptions = new AFSubscriptions(observableLayer, this);

            // InitTimer();
            saving = false;
        }


        private int assessmentCount = 1;

        private bool isTimeSettingChanged = false;
        private int assessmentIndexInAction = 1;


        private string piHttpHeader = "pi-agent-" + Application.ProductVersion;
        private readonly SortedDictionary<string, string> _piOptions = new SortedDictionary<string, string>();
        private bool saving;
        private const string DYNAASSESSDROPDOWN = "lstDynamicAssessmentDropdown";
        private const string ADDNEWASSESSMENT = "picDynamicAddNewAssessment";
        private const string ATTRIBUTEOUTDROPDOWN = "lstDynamicWriteBackAttribute";
        private const string ADDNEWATTRIBUTE = "picDynamicAddNewOutput";
        private const string ASSESSMENTLOADER = "dynamicAssessmentLoader";
        private const string EVENTFRAMETEMPDROPDOWN = "ccbDynamicEventFrameTemplate";
        private const string DELETEASSESSMENT = "btnDynamicDeleteNewAssessment";
        
        AFNamedCollectionList<AFElement> afallElements;
        Hashtable assessmentMap = new Hashtable();
        AFObservableLayer observableLayer;
        AFSubscriptions subscriptions;


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            subscriptions.clearAllSubscriptions();
            observableLayer.tearDown();
            refreshLayer.tearDown();
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(0);
            }

        }
        private Boolean checkFalkonryStatus()
        {
            try
            {
                if (FalkonryHelper.AuthenticateConnectionDetails(txtFalkonryHost.Text, txtFalkonryToken.Text))
                {
                    //Log.Debug($"Successfully validated token : '{txtFalkonryToken.Text}' Host :'{txtFalkonryHost.Text}'");
                    falkonryConnected.Show();
                    falkonryDisconnected.Hide();
                    return true;
                }
                else
                {
                    Log.Debug($"checkFalkonryStatus: issue in connecting to Falkonry ");
                    falkonryConnected.Hide();
                    falkonryDisconnected.Show();
                    return false;
                }

            }
            catch (Exception)
            {
                Log.Warn($"checkFalkonryStatus: caught an exception, issue in connecting to Falkonry ");
                falkonryConnected.Hide();
                falkonryDisconnected.Show();
                return false;
            }
        }

        private String checkPiServiceStatus()
        {
            try
            {
                ServiceController sc = new ServiceController(piServiceName);
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        return "Running";
                    case ServiceControllerStatus.Stopped:
                        return "Stopped";
                    case ServiceControllerStatus.Paused:
                        return "Paused";
                    case ServiceControllerStatus.StopPending:
                        return "Stopping";
                    case ServiceControllerStatus.StartPending:
                        return "Starting";
                    default:
                        return "Status Changing";
                }
            }
            catch (Exception exception)
            {
                Log.Error($"Error in checking PI Service piServiceName : '{piServiceName}'. Error: {exception.Message}");
                return "Stopped";
            }
        }

        private void WhichPISystem_ConnectionChange(object sender, SelectionChangeEventArgs e)
        {
            CheckPiSystemConnection();
        }
        private void CheckPiSystemConnection()
        {
            WhichPISystem.Refresh();
            if (!WhichPISystem.PISystem.ConnectionInfo.IsConnected)
                WhichPISystem.PISystem.Connect(true, Owner);
            WhichPISystem.PISystem.Refresh();
            WhichAFDatabase.PISystem = WhichPISystem.PISystem;
            WhichAFDatabase.Refresh();
        }

        private void WhichAFDatabase_SelectionChange(object sender, SelectionChangeEventArgs e)
        {
            try
            {
                var dataBase = (e.SelectedObject as dynamic);
                if (dataBase == null)
                {
                    oldSelectedDSName = "";
                }
                if (dataBase!=null)
                {
                    oldSelectedDSName = dataBase.Name;
                    WhichElementTemplate.DataSource = null;
                    WhichAttributeTemplates.Items.Clear();
                    WhichAttributeTemplates.Text = "";
                    WhichElements.Items.Clear();
                    WhichElements.Text = "";
                    lstWriteBackAttribute.Items.Clear();
                    lstWriteBackAttribute.Text = "";
                    WhichEventFrameTemplate.Items.Clear();
                    WhichEventFrameTemplate.Text = "";

                    if (WhichAFDatabase.AFDatabase == null || WhichAFDatabase.AFDatabase.Equals("")) return;
                    var eTemplates = WhichAFDatabase.AFDatabase.ElementTemplates.Where(t => t.InstanceType == typeof(AFElement));
                    var eTemplatesNames = eTemplates.ToList().Select(et => et.Name).ToList();
                    eTemplatesNames.Add("No Template");
                    if (eTemplates.Count() > 0)
                    {
                        WhichElementTemplate.DataSource = eTemplatesNames;
                        WhichElementTemplate.SelectedIndex = 0;
                    }
                    var efTemplates = WhichAFDatabase.AFDatabase.ElementTemplates.Where(t => t.InstanceType == typeof(AFEventFrame) && t.Name != null);
                    int j = 0;
                    if (efTemplates.Count() > 0)
                    {
                        CCBoxItem selectAll = new CCBoxItem("Select All", 0);
                        WhichEventFrameTemplate.Items.Add(selectAll);
                        efTemplates.ToList().ForEach(i => WhichEventFrameTemplate.Items.Add(new CCBoxItem(i.Name, j++)));
                        WhichEventFrameTemplate.DisplayMember = "Name";
                        WhichEventFrameTemplate.MaxDropDownItems = 10;
                        WhichEventFrameTemplate.ValueSeparator = ", ";
                    }
                    afallElements = AFElement.FindElements(WhichAFDatabase.AFDatabase, null, "*", AFSearchField.Name, true, AFSortField.Name, AFSortOrder.Ascending, int.MaxValue);

                }

            }
            catch (Exception exception)
            {
                Log.Error($"WhichAFDatabase_SelectionChange: Exception caught - {exception.Message}");
                Log.Error($"WhichAFDatabase_SelectionChange: Exception caught, Stack Trace - {exception.StackTrace}");
                return;
            }
        }
        private void StartDatePicker_ValueChanged(object sender, EventArgs e)
        {
            isTimeSettingChanged = true;
            if (StartDatePicker.Value < EndDatePicker.Value)
            {
                enableSaveUpdate();
                return;
            }
            if (!_addingNewConfig) MessageBox.Show(
                $@"The Start Date '{StartDatePicker.Value}' must be before the End Date '{EndDatePicker.Value}'!",
                @"Invalid Start Date!", MessageBoxButtons.OK);
            StartDatePicker.Value = EndDatePicker.Value.AddSeconds(-1);

        }

        private void EndDatePicker_ValueChanged(object sender, EventArgs e)
        {
            isTimeSettingChanged = true;
            if (EndDatePicker.Value > StartDatePicker.Value)
            {
                enableSaveUpdate();
                return;
            }
            if (!_addingNewConfig) MessageBox.Show($@"The End Date '{EndDatePicker.Value}' must be after the Start Date '{StartDatePicker.Value}'!", @"Invalid End Date!", MessageBoxButtons.OK);
            EndDatePicker.Value = StartDatePicker.Value.AddSeconds(1);
            enableSaveUpdate();
        }

        private Control FindDynamicControlByName(string controlName)
        {
            return Controls.Find(controlName, true).FirstOrDefault();
        }

        private void saveObservalePipeline()
        {
            string dataStreamName = txtFalkonryDatastream.Text;
            string host = txtFalkonryHost.Text;
            string token = txtFalkonryToken.Text;
            int i = 0, j = 0;
            string[] AssessmentIdText = new string[assessmentCount];
            string[] lstWriteBackAttributeText = new string[assessmentCount];
            EventFrameText = new string[assessmentCount];
            string[] EventFrameUploadedTimeText = new string[assessmentCount];
            lstWriteBackAttributeText[0] = lstWriteBackAttribute.Text;
            TimeZoneInfo afServerTimeZone = WhichPISystem.PISystem.ServerTimeZone;
            int batchLimit = int.Parse(pageSize.Value.ToString(CultureInfo.InvariantCulture));
            int collectInteval = (int)collectionInterval.Value;
            long historicalStartTime = Utils.ConvertDateTimeToMilliseconds(StartDatePicker.Value);
            long historicalEndTime = Utils.ConvertDateTimeToMilliseconds(EndDatePicker.Value);
            int historicalPageSize = (int)pageSize.Value;
            bool isOverrideHistoricalData = chkOverride.Checked;
            string piSystemName = WhichPISystem.PISystem.Name;
            var piConfiguration = new PiConfiguration
            {
                AfConfiguration = new AfConfiguration
                {
                    SystemName = WhichPISystem.PISystem.Name,
                    DatabaseName = WhichAFDatabase.AFDatabase.Name,
                    TemplateName = WhichElementTemplate.Text,
                    TemplateAttributes =
                        (from CCBoxItem item in WhichAttributeTemplates.CheckedItems where item.Name != "Select All" select item.Name)
                        .ToArray(),
                    TemplateElementPaths =
                        (from CCBoxItem item in WhichElements.CheckedItems where item.Name != "Select All" select item.Name)
                        .ToArray(),
                    OutputTemplateAttribute =
                        lstWriteBackAttributeText.ToArray(),
                    EventFrameTemplateNames =
                        EventFrameText.ToArray(),
                    EventFrameUploadedTime =
                        EventFrameUploadedTimeText.ToArray(),
                    AFServerTimeZoneId = afServerTimeZone.Id
                },
                HistoricalDataAccess = new HistoricalConfiguration
                {
                    Enabled = true,
                    StartTime = historicalStartTime,
                    EndTime = historicalEndTime,
                    PageSize = historicalPageSize,
                    Override = isOverrideHistoricalData
                },
                StreamingDataAccess = new StreamingConfiguration
                {
                    CollectionIntervalMs = collectInteval
                },
                FalkonryConfiguration = new FalkonryConfiguration
                {
                    Host = host,
                    Token = token,
                    TimeFormat = "iso_8601",
                    Datastream = dataStreamName,
                    BatchLimit = batchLimit,
                    AssessmentId = AssessmentIdText.ToArray(),
                    AssessmentIdForFacts = AssessmentIdText.ToArray(),
                    Backfill = false,
                    SourceId = "new_datastream",
                    assessmentList = new List<Assessment>()
                }
            };
            _selectedConfig.Configuration = piConfiguration;
            Observable.Return(_selectedConfig)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .ObserveOn(NewThreadScheduler.Default)
                    .Select(x =>
                    {
                        // Checking wheather Datastream with the same name exists in the Pi Agent 
                        if (x.Configuration.FalkonryConfiguration.SourceId == "new_datastream" && _configFromFile.ConfigurationItems.Count > 0)
                        {
                            if (_configFromFile.ConfigurationItems.Where(y => (y.Configuration.FalkonryConfiguration.SourceId!= "new_datastream") && (y.Configuration.FalkonryConfiguration.Datastream.ToLower().Equals(dataStreamName.ToLower()))).Count()>0)
                            {
                                Exception error = new Exception();
                                error.Data.Add("title", "Cannot save.");
                                error.Data.Add("msg", $"Cannot save your Datastream. Datastream with name :{dataStreamName} already exists in Pi Agent");
                                throw error;
                            }
                        }
                        return x;
                    })
                    .Select(x =>
                    {
                        refreshLayer.tearDown();
                        // Authenticate user 
                        if (!FalkonryHelper.AuthenticateConnectionDetails(host, token))
                        {
                            Exception error = new Exception();
                            error.Data.Add("title", "Cannot save.");
                            error.Data.Add("msg", "Cannot save your Datastream. Are your Falkonry connection details correct?");
                            throw error;
                        }
                        return x;
                    })
                    .Select(x =>
                    {
                        // Checking wheather Datastream with same name exists on the server or not. 
                        var datastreams = new System.Collections.Generic.List<Datastream>();
                        datastreams = FalkonryHelper.GetDatastreams(x.Configuration.FalkonryConfiguration);
                        var existingDatastream = datastreams.Find(d => d.Name.ToLower().Equals(dataStreamName.ToLower()));
                        if (existingDatastream != null)
                        {
                            Exception error = new Exception();
                            error.Data.Add("title", "Cannot save.");
                            error.Data.Add("msg", $"Cannot save your Datastream. Datastream with name :{dataStreamName} already exists in Falkonry");
                            throw error;
                        }
                        return x;
                    })
                    .Select(x =>
                    {
                        // Dummy populations of temp variables 
                        for (int k = 0; k < assessmentCount; k++)
                        {
                            AssessmentIdText[k] = "";
                            lstWriteBackAttributeText[k] = "";
                            EventFrameText[k] = "";
                            EventFrameUploadedTimeText[k] = "";
                        }
                        return x;
                    })
                    .ObserveOnDispatcher()
                    .Select(x =>
                    {
                        // populating eventframe template names 
                        for (j = 0; j < WhichEventFrameTemplate.CheckedItems.Count; j++)
                        {
                            if (((CCBoxItem)(WhichEventFrameTemplate.CheckedItems[j])).Name != "Select All")
                            {
                                EventFrameText[0] += ((CCBoxItem)(WhichEventFrameTemplate.CheckedItems[j])).Name;
                                EventFrameUploadedTimeText[i] += "";

                                if (j + 1 != WhichEventFrameTemplate.CheckedItems.Count)
                                {
                                    EventFrameText[0] += ",";
                                    EventFrameUploadedTimeText[0] += ",";
                                }
                            }
                        }
                        return x;
                    })
                    .Select(x =>
                    {
                        // populating event frames
                        piConfiguration.AfConfiguration.EventFrameTemplateNames = EventFrameText.ToArray();
                        piConfiguration.AfConfiguration.EventFrameUploadedTime = EventFrameUploadedTimeText.ToArray();
                        return x;
                    })
                    .ObserveOn(NewThreadScheduler.Default)
                    .Select(x =>
                    {
                        // create Datastream Api call. Add New Config Element in the Configurations element
                        try
                        {
                            // create Datastream call is typical use case of leaking internal Data structure of the object. this you should not do.
                            // see line number 155 of FalkonryHelper.cs class
                            Datastream dataStream = FalkonryHelper.CreateDatastream(x);
                            _selectedConfig.BatchIdentifier = x.BatchIdentifier;
                            _selectedConfig.Configuration.BatchIdentifier = x.BatchIdentifier;
                            List<Assessment> tmpAssessment = FalkonryHelper.GetAssessments(x.Configuration.FalkonryConfiguration);
                            x.Configuration.FalkonryConfiguration.assessmentList.AddRange(tmpAssessment.Where(z => z.Datastream.Equals(x.Configuration.FalkonryConfiguration.SourceId)));
                            int index = 0;
                            x.Configuration.FalkonryConfiguration.assessmentList.All(y =>
                            {
                                x.Configuration.FalkonryConfiguration.AssessmentId[index] = y.Id;
                                x.Configuration.FalkonryConfiguration.AssessmentIdForFacts[index++] = y.Id;
                                return true;
                            });
                            x.Configuration.FalkonryConfiguration.SourceId = dataStream.Id;
                        }
                        catch (Exception dserror)
                        {
                            Log.Error($"Error while creating datastream - {dserror.Message}");
                            Exception error = new Exception();
                            error.Data.Add("title", "Error");
                            error.Data.Add("msg", $"Cannot create Datastream. {dserror.Message}");
                            throw error;
                        }
                        return x;
                    })
                    .Select(x =>
                    {
                        // updating AFDatabase
                        x.Enabled = true;
                        x.Deleted = false;
                        x.Status = "INFO";
                        x.ErrorMessage = "Datastream creation is in progress.";
                        saveConfig("Save", piSystemName);
                        return x;
                    })
                    .ObserveOnDispatcher()
                    .Select(x =>
                    {
                        updateAssessmentList();
                        return x;
                    })
                    .Subscribe(
                        x => {
                            refreshLayer.setUp();
                            // successfull creation of datastream
                            if (_creatingNewDS) _creatingNewDS = false;
                            saving = false;
                            hideSaveUpdate();
                            hideDsLoader();
                            _selectedConfig.Enabled = true;
                            ListViewItem itm = new ListViewItem(piConfiguration.FalkonryConfiguration.Datastream);
                            itm.Name = x.Configuration.FalkonryConfiguration.SourceId + x.Configuration.FalkonryConfiguration.Host;
                            lstDatastreams.Items.Add(itm);
                            foreach (ListViewItem item in lstDatastreams.Items)
                            {
                                if (item.Text == _selectedConfig.Name)
                                    item.Selected = true;
                                else
                                    item.Selected = false;
                            }
                            Log.Debug($"Datastream created successfully:' Config Id : {x.Id} Datastream Id :{x.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {x.Configuration.FalkonryConfiguration.Datastream} Host: {x.Configuration.FalkonryConfiguration.Host} Token: {x.Configuration.FalkonryConfiguration.Token}'");
                        },
                        x => {
                            // exception while creating datastream
                            refreshLayer.setUp();
                            saving = false;
                            try
                            {
                                if (x.Data != null && x.Data.Count > 0 && x.Data.Contains("title") && x.Data.Contains("msg"))
                                {
                                    Log.Error($"Save/Update failed {x.StackTrace}");
                                    MessageBox.Show(x.Data["msg"].ToString(), x.Data["title"].ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    Log.Error($"Can not Save/Update Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                                    MessageBox.Show($@"Cannot save your Datastream. {x.Message}!", @"Cannot save.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                hideDsLoader();

                            }
                            catch (Exception)
                            {
                                Log.Error($"Exception in On Error of Save/Update Subscriber {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                            }
                        });
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (saving)
                    return;
                saving = true;
                showDsLoader();
                if (txtFalkonryDatastream.Text.Trim() == "")
                {

                    MessageBox.Show(@"Please enter unique datastream name first. Unable to save the datastream configuration.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideDsLoader();
                    saving = false;
                    return;
                }

                if (WhichAttributeTemplates.Text.Trim() == "")
                {

                    MessageBox.Show(@"There are no AF Attributes selected. Unable to save the datastream configuration.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideDsLoader();
                    saving = false;
                    return;
                }

                if (WhichElements.Text.Trim() == "")
                {

                    MessageBox.Show(@"There are no AF Elements selected. Unable to save the datastream configuration.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideDsLoader();
                    saving = false;
                    return;
                }

                if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId != "new_datastream")
                {
                    int i = 0, j = 0;
                    string[] AssessmentIdText = new string[assessmentCount];
                    string[] lstWriteBackAttributeText = new string[assessmentCount];
                    EventFrameText = new string[assessmentCount];
                    string[] EventFrameUploadedTimeText = new string[assessmentCount];
                    string fixOutPutAttribute = lstWriteBackAttribute.Text;
                    TimeZoneInfo afServerTimeZone = WhichPISystem.PISystem.ServerTimeZone;
                    Hashtable eventFrameNameTimeHash = new Hashtable();
                    Log.Debug($"btnSave_Click: Mode - Update, datastream source id: {_selectedConfig.Configuration.FalkonryConfiguration.SourceId}");
                    Log.Debug($"btnSave_Click: Assessment Count - {assessmentCount}");
                    Hashtable assessmentBindingHash = new Hashtable();
                    Hashtable assessmentHash = new Hashtable();
                    Hashtable assessmentNameEventFrameHash = new Hashtable();
                    string piSystemName = WhichPISystem.PISystem.Name;
                    var piConfiguration = new PiConfiguration
                    {
                        AfConfiguration = new AfConfiguration
                        {
                            SystemName = _selectedConfig.Configuration.AfConfiguration.SystemName,
                            DatabaseName = _selectedConfig.Configuration.AfConfiguration.DatabaseName,
                            TemplateName = _selectedConfig.Configuration.AfConfiguration.TemplateName,
                            TemplateAttributes = _selectedConfig.Configuration.AfConfiguration.TemplateAttributes,
                            TemplateElementPaths = _selectedConfig.Configuration.AfConfiguration.TemplateElementPaths,
                            OutputTemplateAttribute =
                              lstWriteBackAttributeText.ToArray(),
                            EventFrameTemplateNames =
                              EventFrameText.ToArray(),
                            EventFrameUploadedTime =
                              EventFrameUploadedTimeText.ToArray(),
                            AFServerTimeZoneId = afServerTimeZone.Id
                        },
                        HistoricalDataAccess = new HistoricalConfiguration
                        {
                            Enabled = true,
                            StartTime = Utils.ConvertDateTimeToMilliseconds(StartDatePicker.Value),
                            EndTime = Utils.ConvertDateTimeToMilliseconds(EndDatePicker.Value),
                            PageSize = (int)pageSize.Value,
                            Override = chkOverride.Checked
                        },
                        StreamingDataAccess = new StreamingConfiguration
                        {
                            CollectionIntervalMs = (int)collectionInterval.Value
                        },
                        FalkonryConfiguration = new FalkonryConfiguration
                        {
                            Host = _selectedConfig.Configuration.FalkonryConfiguration.Host,
                            Token = _selectedConfig.Configuration.FalkonryConfiguration.Token,
                            TimeFormat = "iso_8601",
                            Datastream = _selectedConfig.Configuration.FalkonryConfiguration.Datastream,
                            BatchLimit = int.Parse(pageSize.Value.ToString(CultureInfo.InvariantCulture)),
                            AssessmentId = AssessmentIdText.ToArray(),
                            AssessmentIdForFacts = AssessmentIdText.ToArray(),
                            Backfill = false,
                            SourceId = _selectedConfig.Configuration.FalkonryConfiguration.SourceId,
                            assessmentList = new List<Assessment>()
                        }
                    };

                    Observable.Return(_selectedConfig)
                        .SubscribeOn(NewThreadScheduler.Default)
                        .ObserveOn(NewThreadScheduler.Default)
                        .Select(x =>
                        {
                            refreshLayer.tearDown();
                            // initialize with empty strings
                            for (int k = 0; k < assessmentCount; k++)
                            {
                                AssessmentIdText[k] = "";
                                lstWriteBackAttributeText[k] = "";
                                EventFrameText[k] = "";
                                EventFrameUploadedTimeText[k] = "";
                            }
                            return x;
                        })
                        .Select(x =>
                        {
                            // initailizing with actual value of output attribute and the assessmentID
                            lstWriteBackAttributeText[0] = fixOutPutAttribute;
                            AssessmentIdText[0] = x.Configuration.FalkonryConfiguration.AssessmentId[0];
                            return x;
                        })
                        .ObserveOnDispatcher()
                        .Select(x =>
                        {
                            var tmpValue = assessmentList.SelectedItem as dynamic;
                            if (tmpValue != null)
                            {
                                AssessmentIdText[0] = tmpValue.Value;
                            }
                            return x;
                        })
                        .Select(x =>
                        {
                            // initializing the 1st assessment event frames
                            for (j = 0; j < WhichEventFrameTemplate.CheckedItems.Count; j++)
                            {
                                if (((CCBoxItem)(WhichEventFrameTemplate.CheckedItems[j])).Name != "Select All")
                                {
                                    EventFrameText[0] += ((CCBoxItem)(WhichEventFrameTemplate.CheckedItems[j])).Name;
                                    if (eventFrameNameTimeHash.Contains(((CCBoxItem)(WhichEventFrameTemplate.CheckedItems[j])).Name))
                                        EventFrameUploadedTimeText[i] += eventFrameNameTimeHash[((CCBoxItem)(WhichEventFrameTemplate.CheckedItems[j])).Name];
                                    else
                                        EventFrameUploadedTimeText[i] += "";
                                    if (j + 1 != WhichEventFrameTemplate.CheckedItems.Count)
                                    {
                                        EventFrameText[0] += ",";
                                        EventFrameUploadedTimeText[0] += ",";
                                    }
                                }
                            }
                            // adding assessment,assessmentbinding and assessmentNameEventFramehash in there respective hashmaps
                            assessmentBindingHash.Add(lstWriteBackAttribute.Text, true);
                            assessmentHash.Add(assessmentList.Text, true);
                            assessmentNameEventFrameHash.Add(assessmentList.Text, EventFrameUploadedTimeText[0]);
                            return x;
                        })
                        .ObserveOnDispatcher()
                        .Select(x =>
                        {
                            for (i = 1; i < assessmentCount; i++)
                            {
                                CheckedComboBox eventFaremCcb = (CheckedComboBox)assessmentBody.Controls.Find(EVENTFRAMETEMPDROPDOWN + (i + 1), true)[0];
                                // string AssessmentId = ((ComboBox)panelAssessment.Controls[dynamicAssessmentStaticCount - 1 + dynamicAssessmentDropdownIndex + (i - 1) * dynamicAssessmentElementCount]).Text;
                                ComboBox assesmentComboBox = (ComboBox)assessmentBody.Controls.Find(DYNAASSESSDROPDOWN + (i + 1), true)[0];

                                string AssessmentId = (assesmentComboBox.SelectedItem as dynamic).Value;
                                string AssessmentName = assesmentComboBox.Text;
                                string outputTemplateAttribute = assessmentBody.Controls.Find(ATTRIBUTEOUTDROPDOWN + (i + 1), true)[0].Text;
                                bool isEventFrameAvailable = false;
                                AssessmentIdText[i] = AssessmentId;
                                lstWriteBackAttributeText[i] = outputTemplateAttribute;
                                if (x.Configuration.AfConfiguration.EventFrameTemplateNames != null &&
                                    x.Configuration.AfConfiguration.EventFrameTemplateNames.Count() > i &&
                                    x.Configuration.AfConfiguration.EventFrameUploadedTime != null &&
                                    x.Configuration.AfConfiguration.EventFrameUploadedTime.Count() > i)
                                {
                                    string[] eventFrameList = x.Configuration.AfConfiguration.EventFrameTemplateNames[i].Split(',');
                                    string[] eventFrameTimeList = x.Configuration.AfConfiguration.EventFrameUploadedTime[i].Split(',');
                                    for (int eventIndex = 0; eventIndex < eventFrameList.Count(); eventIndex++)
                                    {
                                        eventFrameNameTimeHash.Add(eventFrameList[eventIndex], eventFrameTimeList[eventIndex]);
                                    }
                                }
                                else if (x.Configuration.AfConfiguration.EventFrameTemplateNames != null &&
                                        x.Configuration.AfConfiguration.EventFrameTemplateNames.Count() > i)
                                {
                                    string[] eventFrameList = x.Configuration.AfConfiguration.EventFrameTemplateNames[i].Split(',');
                                    //string[] eventFrameTimeList = x.Configuration.AfConfiguration.EventFrameUploadedTime[i].Split(',');
                                    for (int eventIndex = 0; eventIndex < eventFrameList.Count(); eventIndex++)
                                    {
                                        eventFrameNameTimeHash.Add(eventFrameList[eventIndex], "");
                                    }
                                }


                                for (j = 0; j < eventFaremCcb.CheckedItems.Count; j++)
                                {
                                    isEventFrameAvailable = true;
                                    if (((CCBoxItem)(eventFaremCcb.CheckedItems[j])).Name != "Select All")
                                    {
                                        EventFrameText[i] += ((CCBoxItem)(eventFaremCcb.CheckedItems[j])).Name;
                                        if (eventFrameNameTimeHash.Contains(((CCBoxItem)(eventFaremCcb.CheckedItems[j])).Name))
                                            EventFrameUploadedTimeText[i] += eventFrameNameTimeHash[((CCBoxItem)(eventFaremCcb.CheckedItems[j])).Name];
                                        else
                                            EventFrameUploadedTimeText[i] += "";
                                        if (j + 1 != eventFaremCcb.CheckedItems.Count)
                                        {
                                            EventFrameText[i] += ",";
                                            EventFrameUploadedTimeText[i] += ",";
                                        }
                                    }
                                }
                                eventFrameNameTimeHash.Clear();

                                if ((string.IsNullOrEmpty(AssessmentName.Trim())) || (string.IsNullOrEmpty(outputTemplateAttribute) && !isEventFrameAvailable))
                                {
                                    Exception error = new Exception();
                                    error.Data.Add("title", "");
                                    error.Data.Add("msg", @"Please select proper values for assessment bindings. Unable to update the datastream configuration.");
                                    throw error;

                                }
                                if (!assessmentHash.Contains(AssessmentName))
                                {
                                    assessmentHash.Add(AssessmentName, true);
                                }
                                else
                                {
                                    Exception error = new Exception();
                                    error.Data.Add("title", "");
                                    error.Data.Add("msg", @"Duplicate assessment binding found, please select unique assessment bindings. Unable to update the datastream configuration.");
                                    throw error;
                                }

                                if (!assessmentBindingHash.Contains(outputTemplateAttribute))
                                {
                                    assessmentBindingHash.Add(outputTemplateAttribute, true);
                                }
                                else 
                                {
                                    Exception error = new Exception();
                                    error.Data.Add("title", "");
                                    error.Data.Add("msg", @"Duplicate assessment output attribute bindings found. Unable to update the datastream configuration.");
                                    throw error;
                                }
                            }
                            return x;
                        })
                        .Select(x =>
                        {
                            // Eabling historical data access if needed.
                            if (isTimeSettingChanged && (StartDatePicker.Value < (new DateTime(1970, 1, 1)).AddMilliseconds(x.Configuration.HistoricalDataAccess.StartTime).ToLocalTime() ||
                    EndDatePicker.Value < (new DateTime(1970, 1, 1)).AddMilliseconds(x.Configuration.HistoricalDataAccess.EndTime).ToLocalTime()))
                            {
                                x.Configuration.HistoricalDataAccess.Enabled = true;
                            }
                            return x;
                        })
                        .ObserveOn(NewThreadScheduler.Default)
                        .Select(x =>
                        {
                            piConfiguration.AfConfiguration.EventFrameTemplateNames = EventFrameText.ToArray();
                            piConfiguration.AfConfiguration.EventFrameUploadedTime = EventFrameUploadedTimeText.ToArray();
                            piConfiguration.AfConfiguration.OutputTemplateAttribute = lstWriteBackAttributeText.ToArray();
                            piConfiguration.FalkonryConfiguration.AssessmentId = AssessmentIdText.ToArray();
                            piConfiguration.FalkonryConfiguration.AssessmentIdForFacts = AssessmentIdText.ToArray();
                            Log.Info($"Setting assessmentList as {x.Configuration.FalkonryConfiguration.assessmentList} for Datastream :{piConfiguration.FalkonryConfiguration.Datastream} Id: {piConfiguration.FalkonryConfiguration.SourceId}");
                            piConfiguration.FalkonryConfiguration.assessmentList = x.Configuration.FalkonryConfiguration.assessmentList;
                            if (x.Configuration.FalkonryConfiguration.AssessmentId.Count() != piConfiguration.FalkonryConfiguration.AssessmentId.Count())
                            {
                                piConfiguration.HistoricalDataAccess.Enabled = true;
                            }
                            x.Enabled = true;
                            x.Configuration = piConfiguration;
                            return x;
                        })
                        .Select(x =>
                        {
                            saveConfig("Update", piSystemName);
                            return x;
                        })
                        .ObserveOnDispatcher()
                        .Subscribe(
                            x =>
                            {
                                // OnNext of Subscriber
                                refreshLayer.setUp();
                                saving = false;
                                hideSaveUpdate();
                                foreach (ListViewItem item in lstDatastreams.Items)
                                {
                                    if (item.Text == _selectedConfig.Name)
                                        item.Selected = true;
                                    else
                                        item.Selected = false;
                                }
                                Log.Debug($"Datastream Updated successfully:' Config Id : {x.Id} Datastream Id :{x.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {x.Configuration.FalkonryConfiguration.Datastream}'");

                            },
                            x =>
                            {
                                // Error in Observable Pipeline
                                refreshLayer.setUp();
                                saving = false;
                                try
                                {
                                    if (x.Data != null && x.Data.Count > 0 && x.Data.Contains("title") && x.Data.Contains("msg"))
                                    {
                                        Log.Error($"Update failed {x.StackTrace}");
                                        MessageBox.Show(x.Data["msg"].ToString(), x.Data["title"].ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else
                                    {
                                        Log.Error($"Can not Save/Update Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                                        MessageBox.Show($@"Cannot save your Datastream. {x.Message}!", @"Cannot save.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    hideDsLoader();

                                }
                                catch (Exception)
                                {
                                    Log.Error($"Exception in On Error of Save/Update Subscriber {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                                }
                            }
                        );



                }
                else
                {
                    saveObservalePipeline();
                }


            }
            catch (Exception exception)
            {
                saving = false;
                if (exception.Data != null && exception.Data.Count > 0 && exception.Data.Contains("title") && exception.Data.Contains("msg"))
                {
                    Log.Error($"Save/Update failed {exception.StackTrace}");
                    MessageBox.Show(exception.Data["msg"].ToString(), exception.Data["title"].ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Log.Error($"Could not save datastream :' Config Id : {_selectedConfig.Id} Datastream Id :{_selectedConfig.Configuration.FalkonryConfiguration.SourceId}, Error from Falkonry server: {exception.Message} ");
                    MessageBox.Show($@"Cannot save your Datastream. {exception.Message}!", @"Cannot save.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideDsLoader();

                }


            }
        }

        private void updateSelectedConfigFromDB(PiConfigurationItem _selectedConfig)
        {
            try
            {

                Configurations allCOnfigurations = AfHelper.RetrieveConfigurationFromPISystem(_selectedConfig.Configuration.AfConfiguration.SystemName);
                for (var i = 0; i < allCOnfigurations.ConfigurationItems.Count; i++)
                {
                    if (allCOnfigurations.ConfigurationItems[i].Id == _selectedConfig.Id && _configFromFile.ConfigurationItems[i].Configuration.FalkonryConfiguration.SourceId != null)
                    {
                        _selectedConfig = allCOnfigurations.ConfigurationItems[i];
                    }
                }

            }
            catch (Exception exception)
            {
                Log.Error($"updateSelectedConfigFromDB: Exception caught, Error in fetching latest datastreams from database. Error : {exception.Message}");
                MessageBox.Show($"Error: {exception.Message}", "Something went wrong!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }

        public void refreshConfigurationlist(IList<PiConfigurationItem> items)
        {
            if (btnSave.Visible || _creatingNewDS)
            {
                return;
            }
            Log.Info($"======================== Started Proceeding with New Configuration Items ===============================");
            if (_configFromFile != null)
            {
                _configFromFile.ConfigurationItems = (List<PiConfigurationItem>)items;
                initiateRefresh();
            }
            else
            {
                _configFromFile = new Configurations()
                {
                    ConfigurationItems = (List<PiConfigurationItem>)items
                };
                // First time loading configuration
                CollectConfiguration();
            }

           
            Log.Info($"========================== Ended with New Configuration Items ===========================================");
        }

        private void initiateRefresh()
        {
            lock (syncLock)
            {
                try
                {

                    if (_configFromFile.ConfigurationItems.Count > lstDatastreams.Items.Count)
                    {
                        // add new DS name in ListView
                        // practically this should not happen still we are keeping it as a precaution
                        IEnumerable<PiConfigurationItem> configItems = _configFromFile.ConfigurationItems.Where(x =>
                        {
                            if (lstDatastreams.Items.Find(x.Configuration.FalkonryConfiguration.SourceId + x.Configuration.FalkonryConfiguration.Host, false).Length > 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        });
                        configItems.All(x =>
                        {
                            ListViewItem listViewItm = new ListViewItem(x.Name);
                            listViewItm.Name = x.Configuration.FalkonryConfiguration.SourceId + x.Configuration.FalkonryConfiguration.Host;
                            lstDatastreams.Items.Add(listViewItm);
                            return true;
                        });
                    }
                    else if (_configFromFile.ConfigurationItems.Count < lstDatastreams.Items.Count)
                    {
                        // delete the DS name from ListView
                        List<ListViewItem> items = new List<ListViewItem>();
                        bool changeSelection = false;
                        foreach (ListViewItem item in lstDatastreams.Items)
                        {
                            bool present = false;
                            _configFromFile.ConfigurationItems.All(x =>
                            {
                                if (item.Name.Equals(x.Configuration.FalkonryConfiguration.SourceId + x.Configuration.FalkonryConfiguration.Host))
                                {
                                    present = true;
                                }
                                return true;
                            });
                            if (!present)
                            {
                                items.Add(item);
                                if (item.Selected)
                                {
                                    changeSelection = true;
                                }
                            }
                        }
                        foreach (ListViewItem itm in items)
                        {
                            lstDatastreams.Items.Remove(itm);
                        }
                        if (lstDatastreams.Items.Count > 0)
                        {
                            lstDatastreams.Select();
                            if (changeSelection)
                                lstDatastreams.Items[0].Selected = true;
                        }
                        else
                        {
                            errorBox.Text = "";
                            mainPanel.Hide();
                            lblNoDatastreams.Text = "No Datastreams present. Please create new datastream";
                            lblNoDatastreams.Show();
                        }
                    }

                    IEnumerable<PiConfigurationItem> selectedItems = _configFromFile.ConfigurationItems.Where(x => x.Configuration.FalkonryConfiguration.SourceId == _selectedConfig.Configuration.FalkonryConfiguration.SourceId);
                    if (selectedItems.Count() > 0)
                    {
                        PiConfigurationItem item = selectedItems.ElementAt(0);
                        PiConfigurationItem oldSelectedConfigReference = _selectedConfig;
                        _selectedConfig = item;
                        if (!item.Configuration.FalkonryConfiguration.Datastream.Equals(oldSelectedConfigReference.Configuration.FalkonryConfiguration.Datastream))
                        {
                            // show the new Datastream name
                            dsName.Text = item.Configuration.FalkonryConfiguration.Datastream;
                            ListViewItem[] listItems = lstDatastreams.Items.Find(item.Configuration.FalkonryConfiguration.SourceId + item.Configuration.FalkonryConfiguration.Host, false);
                            if (listItems != null && listItems.Length > 0)
                            {
                                listItems[0].Text = item.Configuration.FalkonryConfiguration.Datastream;
                            }
                        }
                        if ((item.Configuration.FalkonryConfiguration.AssessmentId != null && oldSelectedConfigReference.Configuration.FalkonryConfiguration.AssessmentId != null) &&
                            (item.Configuration.FalkonryConfiguration.AssessmentId.Length != oldSelectedConfigReference.Configuration.FalkonryConfiguration.AssessmentId.Length))
                        {
                            // refresh Assessment
                            reinitAssessmentBody();
                            updateAssessmentList();
                            waitForAssessmentsPopulateControl();
                            hideSaveUpdate();
                        }
                        // set text for information box
                        errorBox.Text = $"Datastream :'{item.Name}' \n {item.ErrorMessage}";
                        errorBox.ForeColor = System.Drawing.Color.Blue;
                        if (item.Status == "ERROR")
                            errorBox.ForeColor = System.Drawing.Color.Red;


                    }

                    _configFromFile.ConfigurationItems.All(x =>
                    {
                        ListViewItem[] listItems = lstDatastreams.Items.Find(x.Configuration.FalkonryConfiguration.SourceId + x.Configuration.FalkonryConfiguration.Host, false);
                        if (listItems != null && listItems.Length > 0)
                        {
                            if (listItems[0].Text != x.Configuration.FalkonryConfiguration.Datastream)
                            {
                                listItems[0].Text = x.Configuration.FalkonryConfiguration.Datastream;
                            }
                        }
                        return true;
                    });
                }
                catch(Exception e)
                {
                    Log.Fatal($"Exception in initiate Refresh Exception Message: {e.Message} \n Exception Stacktrace: {e.StackTrace}");
                }
            }
           
        }

        private void CollectConfiguration()
        {
            try
            {

                int firstValidConfigurationIndex = -1, iter = 0;
                lstDatastreams.View = View.List;
                lstDatastreams.HideSelection = false;
                lstDatastreams.Items.Clear();

                foreach (var config in _configFromFile.ConfigurationItems)
                {
                    if (!config.Deleted)
                    {
                        ListViewItem listViewItm = new ListViewItem(config.Name);
                        listViewItm.Name = config.Configuration.FalkonryConfiguration.SourceId + config.Configuration.FalkonryConfiguration.Host;
                        lstDatastreams.Items.Add(listViewItm);
                        if (firstValidConfigurationIndex == -1)
                        {
                            firstValidConfigurationIndex = iter;
                        }
                    }
                    iter++;
                }
                assessmentList.DisplayMember = "Text";
                assessmentList.ValueMember = "Value";
                if (lstDatastreams.Items.Count > 0)
                {
                    lstDatastreams.Sorting = SortOrder.Ascending;

                    bool selectFirstValidConfig = true;
                    if (_selectedConfig != null && _selectedConfig.Configuration != null && _selectedConfig.Configuration.FalkonryConfiguration != null)
                    {
                        foreach (ListViewItem item in lstDatastreams.Items)
                        {
                            if (_selectedConfig.Configuration.FalkonryConfiguration.Datastream != null && (item.Text.ToLower() == _selectedConfig.Configuration.FalkonryConfiguration.Datastream.ToLower()))
                            {
                                item.Selected = true;
                                selectFirstValidConfig = false;
                                break;
                            }
                        }
                    }

                    if (selectFirstValidConfig)
                    {
                        lstDatastreams.Items[0].Selected = true;
                        if (!_creatingNewDS)
                            _selectedConfig = _configFromFile.ConfigurationItems[firstValidConfigurationIndex];
                    }
                }
                else
                {
                    if (lstDatastreams.Enabled)
                    {
                        errorBox.Text = "";
                        mainPanel.Hide();
                        lblNoDatastreams.Text = "No Datastreams present. Please create new datastream";
                        lblNoDatastreams.Show();
                    }


                }
            }
            catch (Exception exception)
            {
                errorBox.Text = "";
                mainPanel.Hide();
                lblNoDatastreams.Text = "No Datastreams present. Please create new datastream";
                lblNoDatastreams.Show();
                Log.Error($"CollectConfiguration: Exception Message - {exception.Message} \n Exception StackTrace: {exception.StackTrace}");
                MessageBox.Show($"Error: {exception.Message}", "Something went wrong!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (exception.Message.Contains("user does not have rights"))
                {
                    if (System.Windows.Forms.Application.MessageLoop)
                    {
                        // WinForms app
                        System.Windows.Forms.Application.Exit();
                    }
                    else
                    {
                        // Console app
                        System.Environment.Exit(0);
                    }
                }

            }
            picAddNewDS.Enabled = true;
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            test = true;
            LoadFont();
            for (int i = 0; i < this.Controls.Count; i++)
            {
                Control control = this.Controls[i];
                if (control is Label)
                {
                    Label label = (Label)control;
                    alloc_font(font, label, label.Font.Size);
                }
            }

            subscriptions.setUpObservablePipeLine();
            mainPanel.Hide();
            picAddNewDS.Enabled = false;
            lblNoDatastreams.Text = "Please wait...";
            lblNoDatastreams.Show();
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fileVersionInfo.ProductVersion;
            _piOptions["header"] = "pi-agent" + Application.ProductVersion;
            assessmentMap = null;
            subscriptions.setUpObservablePipeLine();
            initializeHostTextChangeObservable();
            assessmentBody.AutoScroll = false;
            assessmentBody.HorizontalScroll.Visible = false;
            assessmentBody.HorizontalScroll.Maximum = 0;
            assessmentBody.HorizontalScroll.Enabled = false;
           
        }

        public PiConfigurationItem _selectedConfig;
        private Configurations _configFromFile;
        private PiConfigurationItem _previousConfig;


        private void readOnlyTextBox(System.Windows.Forms.TextBox textBox)
        {
            textBox.ReadOnly = true;
            // textBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            // textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            //textBox.ForeColor = System.Drawing.SystemColors.Window;
        }

        private void writeModeTextBox(System.Windows.Forms.TextBox textBox)
        {
            textBox.ReadOnly = false;
            //textBox.BackColor = System.Drawing.SystemColors.Window;
            // textBox.ForeColor = System.Drawing.SystemColors.MenuText;
            // textBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        }

        private void PopulateControls()
        {
            try
            {

                btnDelete.Visible = false;
               
                PiConfiguration piConfiguration = _selectedConfig.Configuration;
                mainPanel.Show();
                lblNoDatastreams.Hide();
               

                errorBox.Text = "";

                if (WhichPISystem.PISystems.Contains(piConfiguration.AfConfiguration.SystemName))
                    WhichPISystem.PISystem = WhichPISystem.PISystems[piConfiguration.AfConfiguration.SystemName];

                WhichPISystem.PISystem.Connect(true, Owner);
                if (WhichAFDatabase.AFDatabases.Contains(piConfiguration.AfConfiguration.DatabaseName))
                    WhichAFDatabase.AFDatabase = WhichAFDatabase.AFDatabases[piConfiguration.AfConfiguration.DatabaseName];
               
                foreach (var t in WhichElementTemplate.Items)
                {
                    if ((string)t != piConfiguration.AfConfiguration.TemplateName) continue;
                    WhichElementTemplate.Text = piConfiguration.AfConfiguration.TemplateName;
                    break;
                }

                txtFalkonryHost.Text = piConfiguration.FalkonryConfiguration.Host;
                txtFalkonryToken.Text = piConfiguration.FalkonryConfiguration.Token;
              
                piConfiguration.FalkonryConfiguration.TimeFormat = "iso_8601";

                btnTestFalkonry_Click(null, null);

                txtFalkonryDatastream.Text = piConfiguration.FalkonryConfiguration.Datastream;


                dsName.Text = piConfiguration.FalkonryConfiguration.Datastream;

                // setLinkColor(linkDatastream);

                int i = 0, j = 0, k = 0;
                WhichElements.Text = "";
                for (i = 0; i < piConfiguration.AfConfiguration.TemplateElementPaths.ToArray().Count(); i++)
                {
                    WhichElements.Text += piConfiguration.AfConfiguration.TemplateElementPaths.ToArray()[i];
                    for (j = 0; (!WhichElements.Text.Equals("") && j < WhichElements.Items.Count); j++)
                    {
                        if (((CCBoxItem)WhichElements.Items[j]).Name == (piConfiguration.AfConfiguration.TemplateElementPaths.ToArray()[i]))
                        {
                            WhichElements.SetItemChecked(j, true);
                        }
                    }
                    if (i + 1 != piConfiguration.AfConfiguration.TemplateElementPaths.ToArray().Count())
                    {
                        WhichElements.Text += ", ";
                    }
                }
                if ((string)WhichElementTemplate.SelectedValue == "No Template")
                    WhichElements_Leave(null, null);


                WhichAttributeTemplates.Text = "";
                for (i = 0; i < piConfiguration.AfConfiguration.TemplateAttributes.ToArray().Count(); i++)
                {
                    WhichAttributeTemplates.Text += piConfiguration.AfConfiguration.TemplateAttributes.ToArray()[i];
                    for (j = 0; (!WhichAttributeTemplates.Text.Equals("") && j < WhichAttributeTemplates.Items.Count); j++)
                    {
                        if (((CCBoxItem)WhichAttributeTemplates.Items[j]).Name == (piConfiguration.AfConfiguration.TemplateAttributes.ToArray()[i]))
                        {
                            WhichAttributeTemplates.SetItemChecked(j, true);
                        }
                    }
                    if (i + 1 != piConfiguration.AfConfiguration.TemplateAttributes.ToArray().Count())
                    {
                        WhichAttributeTemplates.Text += ", ";
                    }
                }

                batchIdentifier.Text = "";


                StartDatePicker.Value = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
                EndDatePicker.Value = new System.DateTime(1970, 1, 1, 0, 0, 0, 1);
                EndDatePicker.Value = (new DateTime(1970, 1, 1)).AddMilliseconds(piConfiguration.HistoricalDataAccess.EndTime).ToLocalTime();
                StartDatePicker.Value = (new DateTime(1970, 1, 1)).AddMilliseconds(piConfiguration.HistoricalDataAccess.StartTime).ToLocalTime();
                pageSize.Value = piConfiguration.HistoricalDataAccess.PageSize;
                chkOverride.Checked = false;
                chkStreaming.Checked = piConfiguration.StreamingDataAccess.Enabled;

                collectionInterval.Value = piConfiguration.StreamingDataAccess.CollectionIntervalMs;
                txtAssessment_Leave(null, null);

                /// Loading UI/UX for Old Datastream info
                if (piConfiguration.FalkonryConfiguration.SourceId != null && !piConfiguration.FalkonryConfiguration.SourceId.Equals("new_datastream"))
                {
                    txtFalkonryToken.Visible = false;
                    txtFalkonryHost.Visible = false;
                    Log.Info($"PopulateControls: Datastream: {piConfiguration.FalkonryConfiguration.Datastream} Id: {piConfiguration.FalkonryConfiguration.SourceId}");
                    btnAddNewAssessmentRow.Visible = true;
                    readOnlyTextBox(txtFalkonryHost);
                    readOnlyTextBox(txtFalkonryToken);
                    txtFalkonryDatastream.Visible = false;
                    linkDatastream.Visible = true;
                    btnDelete.Visible = true;
                    dsName.Text = piConfiguration.FalkonryConfiguration.Datastream;
                    //dsName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    WhichAFDatabase.Visible = false;
                    WhichPISystem.Visible = false;
                    livestreamStatusPanel.Visible = true;
                    WhichAttributeTemplates.Visible = false;
                    batchIdentifier.Visible = false;
                    WhichElementTemplate.Visible = false;
                    WhichElements.Visible = false;
                    lblPiReadOnly.Visible = true;
                    lblAfDbReadOnly.Visible = true;
                    lblAttributeReadOnly.Visible = true;
                    lblElementReadOnly.Visible = true;
                    batchIdentifierLabel.Visible = true;
                    lblTemplateReadOnly.Visible = true;
                    tokenlabel.Visible = true;
                    tokenlabel.Text = piConfiguration.FalkonryConfiguration.Token;
                    hostlabel.Visible = true;
                    hostlabel.Text = piConfiguration.FalkonryConfiguration.Host;
                    lblPiReadOnly.Text = WhichPISystem.Text;
                    lblAfDbReadOnly.Text = WhichAFDatabase.Text;
                    lblAttributeReadOnly.Text = WhichAttributeTemplates.Text;
                    lblElementReadOnly.Text = WhichElements.Text;
                    if (_selectedConfig.BatchIdentifier != null && !_selectedConfig.BatchIdentifier.Equals(""))
                    {
                        batchIdentifierLabel.Text = _selectedConfig.BatchIdentifier;
                    }
                    else
                    {
                        batchIdentifierLabel.Text = "N/A";
                    }
                    lblTemplateReadOnly.Text = WhichElementTemplate.Text;
                    reinitAssessmentBody();
                    updateAssessmentList();
                    waitForAssessmentsPopulateControl();

                    btnBackfillHistory.Show();
                    // btnStreaming.Show();
                    if (chkStreaming.Checked)
                    {
                        streamingOn.Show();
                        streamingOff.Hide();
                    }
                    else
                    {
                        streamingOn.Hide();
                        streamingOff.Show();
                    }

                    errorBox.Text = $"Datastream :'{_selectedConfig.Name}' \n {_selectedConfig.ErrorMessage}";
                    errorBox.ForeColor = System.Drawing.Color.Blue;
                    if (_selectedConfig.Status == "ERROR")
                        errorBox.ForeColor = System.Drawing.Color.Red;

                    hideSaveUpdate();
                }
                else
                {
                    // Loading UI/UX for New Datastream info
                    btnAddNewAssessmentRow.Visible = false;
                    writeModeTextBox(txtFalkonryHost);
                    writeModeTextBox(txtFalkonryToken);
                    //dsName.Text = "New Datastream";
                    reinitAssessmentBody();
                    assessmentCount = 1;
                    //dsName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    txtFalkonryDatastream.Visible = true;
                    txtFalkonryDatastream.Text = "Enter Datastream name";
                    tokenlabel.Visible = false;
                    hostlabel.Visible = false;
                    txtFalkonryToken.Visible = true;
                    txtFalkonryHost.Visible = true;
                    linkDatastream.Visible = false;
                    btnDelete.Visible = false;
                    livestreamStatusPanel.Visible = true;
                   
                    lblPiReadOnly.Visible = false;
                    lblAfDbReadOnly.Visible = false;
                    lblAttributeReadOnly.Visible = false;
                    lblElementReadOnly.Visible = false;
                    batchIdentifierLabel.Visible = false;
                    lblTemplateReadOnly.Visible = false;
                    btnCreateNewAssessment.Hide();
                    WhichAFDatabase.Visible = true;
                    WhichPISystem.Visible = true;
                    WhichAttributeTemplates.Visible = true;
                    WhichElementTemplate.Visible = true;
                    WhichElements.Visible = true;
                    batchIdentifier.Visible = true;

                    btnBackfillHistory.Hide();
                    //btnStreaming.Hide();
                    streamingOn.Show();
                    streamingOff.Show();
                    txtAssessmentIdOutput.Text = string.Join(",", piConfiguration.FalkonryConfiguration.AssessmentId);
                    //if (lstWriteBackAttribute.Items.Contains(piConfiguration.AfConfiguration.OutputTemplateAttribute[0])) lstWriteBackAttribute.Text = piConfiguration.AfConfiguration.OutputTemplateAttribute[0];
                    assessmentList.Hide();
                    txtAssessmentIdOutput.Show();
                   
                    enableSaveUpdate();
                }

                //txtAssessment_Leave(null, null);
                //txtAssessmentIdForFacts_Leave(null, null);

                _previousConfig = Clone(_selectedConfig);
                String serviceStatus = checkPiServiceStatus();

                if (serviceStatus == "Running" || serviceStatus == "Starting")
                {
                    piConnected.Show();
                    piDisconnected.Hide();
                }
                else
                {

                    piDisconnected.Show();
                    piConnected.Hide();
                }



            }

            catch (Exception exception)
            {
                btnDelete.Visible = true;
                Log.Error($"PopulateControls: Exception caught - {exception.Message}");
                Log.Error($"PopulateControls: Exception caught, Stack Trace - {exception.StackTrace}");
                MessageBox.Show($@"{exception.Message}", "Error");
            }
        }

        private void reinitAssessmentBody()
        {
            try
            {
                for (int i = 2; i <= assessmentCount; i++)
                { 
                    Control[] assessmentDropdownArr = assessmentBody.Controls.Find(DYNAASSESSDROPDOWN + i,true);
                    removeControls(assessmentDropdownArr);
                    removeControls(assessmentBody.Controls.Find(ADDNEWASSESSMENT + i, true));
                    removeControls(assessmentBody.Controls.Find(ATTRIBUTEOUTDROPDOWN + i,true));
                    removeControls(assessmentBody.Controls.Find(ADDNEWATTRIBUTE + i,true));
                    removeControls(assessmentBody.Controls.Find(EVENTFRAMETEMPDROPDOWN + i,true));
                    removeControls(assessmentBody.Controls.Find(DELETEASSESSMENT + i,true));
                }
            } catch (Exception e)
            {
                Log.Fatal("Exception in initializing assessment body", e);
            }
        }

        private void removeControls(Control[] array)
        {
            for (int j = 0; j < array.Length; j++)
            {
                assessmentBody.Controls.Remove(array[j]);
            }
        }

        private String waitForAssessmentsPopulateControl()
        {
            PiConfiguration piConfiguration = _selectedConfig.Configuration;
            lstWriteBackAttribute.Items.Clear();
            lstWriteBackAttribute.Sorted = true;
            int k = 0, i = 0;
            foreach (CCBoxItem avialableAttribute in WhichAttributeTemplates.Items)
            {
                if (!WhichAttributeTemplates.CheckedItems.Contains(avialableAttribute) && avialableAttribute.Name != "Select All")
                {
                    lstWriteBackAttribute.Items.Add(avialableAttribute.Name);
                }
            }
            if (lstWriteBackAttribute.Items.Count == 0)
            {
                lstWriteBackAttribute.Items.Add("");
            }
            int it = 0;
            assessmentCount = 1;
            panelAssessment.VerticalScroll.Value = 0;
           // btnAddNewAssessmentRow.Location = new System.Drawing.Point(15, 68);
          //  btnBackfillHistory.Location = new System.Drawing.Point(676, 68);

            List<string> AssessmentIdList = new List<string>(piConfiguration.FalkonryConfiguration.AssessmentId);
            List<string> outputList = new List<string>(piConfiguration.AfConfiguration.OutputTemplateAttribute);
            List<string> eventframeList = new List<string>(piConfiguration.AfConfiguration.EventFrameTemplateNames);
            int index = 0;
            foreach (string AssessmentId in piConfiguration.FalkonryConfiguration.AssessmentId)
            {
                if (assessmentMap != null && (AssessmentId == "" || assessmentMap[AssessmentId] == null))
                {
                    //AfHelper.UpdateState(config.Configuration.AfConfiguration.SystemName, config.Name, "INFO", $"Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                    index = AssessmentIdList.IndexOf(AssessmentId);
                    AssessmentIdList.RemoveAt(index);
                    outputList.RemoveAt(index);
                    eventframeList.RemoveAt(index);
                    Log.Info($"PopulateControl: Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
                }
            }
            piConfiguration.FalkonryConfiguration.AssessmentId = AssessmentIdList.ToArray();
            piConfiguration.AfConfiguration.OutputTemplateAttribute = outputList.ToArray();
            piConfiguration.AfConfiguration.EventFrameTemplateNames = eventframeList.ToArray();
            piConfiguration.FalkonryConfiguration.AssessmentIdForFacts = AssessmentIdList.ToArray();

            if (assessmentMap != null)
            {
                for (i = 0; i < piConfiguration.FalkonryConfiguration.AssessmentId.Count(); i++)
                {
                    ComboboxItem ccbItem = new ComboboxItem { Text = assessmentMap[piConfiguration.FalkonryConfiguration.AssessmentId[i]].ToString(), Value = piConfiguration.FalkonryConfiguration.AssessmentId[i] };

                    if (i == 0)
                    {
                        // Data binding for 1st row. i.e. Default assessment for the Datastream
                        if (lstWriteBackAttribute.Items.Contains(piConfiguration.AfConfiguration.OutputTemplateAttribute[0])) lstWriteBackAttribute.Text = piConfiguration.AfConfiguration.OutputTemplateAttribute[0];

                        if (piConfiguration.FalkonryConfiguration.AssessmentId[i] != "" && !assessmentList.Items.Contains(ccbItem))
                        {
                            assessmentList.Items.Add(ccbItem);
                        }
                        assessmentList.Show();
                        btnCreateNewAssessment.Show();
                        txtAssessmentIdOutput.Hide();
                        assessmentList.Text = assessmentMap[piConfiguration.FalkonryConfiguration.AssessmentId[i]].ToString();
                    }
                    else
                    {
                        btnAddNewAssessmentRow_Click(null, null);
                        ComboBox box = (ComboBox)assessmentBody.Controls.Find(DYNAASSESSDROPDOWN + assessmentCount, true)[0];
                        if (!box.Items.Contains(ccbItem))
                        {
                            box.Items.Add(ccbItem);
                        }

                        box.Text = assessmentMap[piConfiguration.FalkonryConfiguration.AssessmentId[i]].ToString();
                        Control outPutControl = assessmentBody.Controls.Find(ATTRIBUTEOUTDROPDOWN + assessmentCount, true)[0];
                        if (piConfiguration.AfConfiguration.OutputTemplateAttribute.Length > i)
                        {
                            outPutControl.Text = piConfiguration.AfConfiguration.OutputTemplateAttribute[i];
                        }
                        else
                        {
                            outPutControl.Text = "";
                        }

                    }
                }

               
                string[] eventFrameArrray = new string[piConfiguration.FalkonryConfiguration.AssessmentId.Count()];
                for (k = 0; i < eventFrameArrray.Count(); k++)
                {
                    eventFrameArrray[k] = "";
                }
                if (piConfiguration.AfConfiguration.EventFrameTemplateNames.Count() > 0 && piConfiguration.AfConfiguration.EventFrameTemplateNames[0] != null)
                {
                    WhichEventFrameTemplate.Text = piConfiguration.AfConfiguration.EventFrameTemplateNames[0];
                    eventFrameArrray = piConfiguration.AfConfiguration.EventFrameTemplateNames[0].Split((",").ToCharArray()).ToArray();
                }

                for (it = 0; it < WhichEventFrameTemplate.Items.Count; it++)
                {

                    if (eventFrameArrray.Contains(((CCBoxItem)(WhichEventFrameTemplate.Items[it])).Name))
                    {
                        WhichEventFrameTemplate.SetItemChecked(it, true);
                    }
                }
                if (!_creatingNewDS && piConfiguration.AfConfiguration.EventFrameTemplateNames.Count() > 0)
                {
                    for (int iEt = 1; iEt < piConfiguration.AfConfiguration.EventFrameTemplateNames.Count(); iEt++)
                    {
                        Control[] dummyCtrl = (assessmentBody.Controls.Find(EVENTFRAMETEMPDROPDOWN + (iEt + 1), true));
                        if (dummyCtrl.Length > 0)
                        {
                            CheckedComboBox eventFaremCcb = (CheckedComboBox)dummyCtrl[0];
                            if (piConfiguration.AfConfiguration.EventFrameTemplateNames[iEt] != null)
                            {
                                string[] eventFrameArrayDynamic = piConfiguration.AfConfiguration.EventFrameTemplateNames[iEt].Split((",").ToCharArray()).ToArray();
                                for (it = 0; it < eventFaremCcb.Items.Count; it++)
                                {
                                    if (eventFrameArrayDynamic.Contains(((CCBoxItem)eventFaremCcb.Items[it]).Name))
                                    {
                                        eventFaremCcb.SetItemChecked(it, true);
                                    }
                                }
                            }
                        }
                       
                    }
                }
            }
            return "";
        }

        public void checkUpdatedConfig(PiConfiguration config)
        {
            try
            {
                PiConfigurationItem item = _configFromFile.ConfigurationItems.Find(x => x.Configuration.FalkonryConfiguration.SourceId == config.FalkonryConfiguration.SourceId);
                if (item.Configuration.StreamingDataAccess.Enabled != config.StreamingDataAccess.Enabled)
                {
                    item.Configuration.StreamingDataAccess.Enabled = config.StreamingDataAccess.Enabled;
                    if (_selectedConfig.Id == item.Id)
                    {
                        // TODO :: Update live monitoring ui
                        if (item.Configuration.StreamingDataAccess.Enabled)
                        {
                            streamingOff.Hide();
                            streamingOn.Show();
                        }
                        else
                        {
                            streamingOn.Hide();
                            streamingOff.Show();
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e);
            }
        }


        private bool _addingNewConfig;

        private bool _creatingNewDS = false;


        private string GetAssessmentUrl(string AssessmentId)
        {
            var ret = "";

            try
            {
                //var falkonry = new FalkonryClient.Falkonry(txtFalkonryHost.Text, txtFalkonryToken.Text);
                //var assessment = FalkonryHelper.GetAssessments(_selectedConfig.Configuration.FalkonryConfiguration).FirstOrDefault(d => d.Id == AssessmentId);
                var assessment = FalkonryHelper.GetAssessment(_selectedConfig.Configuration.FalkonryConfiguration, AssessmentId);
                if (assessment == null) return ret;
                ret = $"{txtFalkonryHost.Text}/#/account/{assessment.Tenant}/datastream/{assessment.Datastream}/assessment/{assessment.Id}";
            }
            catch (Exception)
            {
                ret = "Assessment does not exist";
            }
            return ret;
        }

        private void updateAssessmentList()
        {
            try
            {
                //Log.Info("  Inside updateAssessmentList");
                string assessmentListText = assessmentList.Text;
                string assessmentListValue = "";
                assessmentList.Items.Clear();
                if (assessmentMap == null)
                    assessmentMap = new Hashtable();
                assessmentMap.Clear();
                foreach (Assessment assessmentObj in _selectedConfig.Configuration.FalkonryConfiguration.assessmentList)
                {
                    ComboboxItem item = new ComboboxItem { Text = assessmentObj.Name, Value = assessmentObj.Id };
                    assessmentList.Items.Add(item);

                    assessmentMap.Add(assessmentObj.Id, assessmentObj.Name);
                    if (assessmentObj.Name == assessmentListText)
                    {
                        assessmentListValue = assessmentObj.Id;
                    }
                }
                assessmentList.Text = assessmentListText;
                assessmentList.SelectedValue = assessmentListValue;

            }
            catch (Exception e)
            {
                Log.Error($"updateAssessmentList: Exception caught \n Stack Trace {e.StackTrace}");
                assessmentMap = null;
            }
        }

        private void btnTestFalkonry_Click(object sender, EventArgs eve)
        {
            if (txtFalkonryHost.Text == "")
            {
                MessageBox.Show(@"Invalid Falkonry Host Name", @"Invalid Falkonry Host Name", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            if (txtFalkonryToken.Text == "")
            {
                MessageBox.Show(@"Invalid Falkonry Token", @"Invalid Falkonry Token", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            try
            {
                Observable.Return(Tuple.Create(txtFalkonryHost.Text, txtFalkonryToken.Text))
                        .SubscribeOn(NewThreadScheduler.Default)
                        .ObserveOn(NewThreadScheduler.Default)
                        .Select(x =>
                        {
                            try
                            {
                                if (FalkonryHelper.AuthenticateConnectionDetails(x.Item1, x.Item2))
                                {
                                    return true;

                                }
                                else
                                {
                                    return false;
                                }
                            }
                            catch (Exception e)
                            {

                            }
                            return false;
                        })
                 .ObserveOnDispatcher()
                 .Subscribe(x => AuthenticateTofalkonry(x), x => { });

            }
            catch (Exception)
            {
                if (!_addingNewConfig)
                {
                    MessageBox.Show($@"Could not connect to Falkonry! Is your token correct?", @"Could not connect to Falkonry", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                    falkonryDisconnected.Show();
                    falkonryConnected.Hide();
                }
                else
                {

                }
            }
        }
        
        private void helpLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(ConfigProperties.Default.HelpLink);
        }

        private void pictureHelpIcon_Click(object sender, EventArgs e)
        {
            helpLinkClicked(null, null);


        }
        private void setLinkColor(System.Windows.Forms.LinkLabel linkObject)
        {
            Observable.Return<string>(txtFalkonryDatastream.Text)
                     .Select(x =>
                     {
                         Datastream datastream = null;
                         try
                         {
                             datastream = FalkonryHelper.GetDatastreams(_selectedConfig.Configuration.FalkonryConfiguration).FirstOrDefault(d => d.Name == x);

                         }
                         catch (Exception e)
                         {

                         }
                         return datastream;
                     })
                     .SubscribeOn(NewThreadScheduler.Default)
                     .ObserveOn(DispatcherScheduler.Current)
                     .Subscribe(
                         x => {
                             if (x != null)
                             {
                                 linkObject.Text = "Link To Datastream in Falkonry";
                                 linkObject.Links[0].Enabled = true;
                                 linkObject.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
                             }
                             else
                             {
                                 linkObject.Text = "Datastream does not exist";
                                 linkObject.Links[0].Enabled = false;
                                 linkObject.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(150)))), ((int)(((byte)(34)))));
                                 linkObject.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
                             }

                         },
                         x => {
                         });

        }
        private void txtFalkonryDatastreamLeave(object sender, EventArgs e)
        {
            try
            {
                if (btnSave.Visible)
                {
                    return;
                }
                WhichElementTemplate.DataSource = null;
                WhichAFDatabase.ClearCollection();
                // setLinkColor(linkDatastream);
                //lblDatastreamName.Text = txtFalkonryDatastream.Text;
                WhichElementTemplate.Items.Clear();
                WhichElementTemplate.Text = "";
                WhichAttributeTemplates.Items.Clear();
                WhichAttributeTemplates.Text = "";
                WhichElements.Items.Clear();
                WhichElements.Text = "";
                batchIdentifier.Items.Clear();
                batchIdentifier.Text = "";
                lstWriteBackAttribute.Items.Clear();
                lstWriteBackAttribute.Text = "";
                WhichEventFrameTemplate.Items.Clear();
                WhichEventFrameTemplate.Text = "";
                txtAssessmentIdOutput.Text = "";
                assessmentList.Text = "";
                assessmentList.Items.Clear();
                txtAssessment_Leave(null, null);

            }
            catch (Exception exception)
            {
                Log.Warn($"txtFalkonryDatastreamLeave: Exception - { exception.Message}");
                Log.Warn($"txtFalkonryDatastreamLeave: Stack Trace - { exception.StackTrace}");
                return;
            }
        }

        private void txtAssessment_Leave(object sender, EventArgs e)
        {
            try
            {
                // TODO :: Need to work on this.
                //picCreateNewOutput.Visible = true;
                //if (_previousConfig == null)
                //{
                //    return;
                //}
                //if (panelAssessment.Controls.Count > dynamicAssessmentStaticCount && panelAssessment.Controls.Count > 13)
                //{
                //    //Till 11 By default assessment details are there
                //    int panelCount = panelAssessment.Controls.Count;
                //    for (int i = dynamicAssessmentStaticCount; i < panelAssessment.Controls.Count;)
                //    {
                //        var panelObject = panelAssessment.Controls[i];
                //        panelObject.Dispose();
                //    }
                //    btnAddNewAssessmentRow.Location = new System.Drawing.Point(15, 68);
                //    //btnBackfillHistory.Location = new System.Drawing.Point(676, 68);
                //}

            }
            catch (Exception exception)
            {
                Log.Warn($"txtAssessment_Leave: Exception caught - {exception.Message}");
                Log.Warn($"txtAssessment_Leave: Exception , Stack Trace  - {exception.StackTrace}");
                MessageBox.Show($"Error: {exception.Message}", "Something went wrong!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                picDsLoader.Show();
                if (sender != null)
                {
                    DialogResult dialogResult = MessageBox.Show($"Are you sure, you want to delete the datastream : {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} ?", "Delete Datastream", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        picDsLoader.Hide();
                        return;
                       
                    }
                }
                ListViewItem deletedItem = null;

                foreach (ListViewItem item in lstDatastreams.Items)
                {
                    if (item.Selected)
                    {
                        deletedItem = item;
                        break;
                    }
                }
                if (deletedItem != null)
                {
                    String piSystemName = WhichPISystem.PISystem.Name;
                    btnDelete.Enabled = false;
                    btnBackfillHistory.Enabled = false;
                    livestreamStatusPanel.Enabled = false;
                    btnStreaming.Enabled = false;
                    Observable.Return(_selectedConfig)
                        .SubscribeOn(NewThreadScheduler.Default)
                        .ObserveOn(NewThreadScheduler.Default)
                        .Select(x =>
                        {
                            refreshLayer.tearDown();
                            // Delete Datastream from Falkonry back end
                            if (sender != null)
                            {
                                try
                                {
                                    FalkonryHelper.DeleteDatastream(x.Configuration.FalkonryConfiguration);
                                    x.Deleted = true;
                                    if (sender != null)
                                        saveConfig("Delete", piSystemName);
                                }
                                catch (Exception error)
                                {
                                    Log.Error($"btnDelete_click: Exception caught while deleting Datastream , {x.Configuration.FalkonryConfiguration.Datastream} - Exception {error.Message}");
                                    throw error;
                                }
                            }

                            return x;
                        })
                        .ObserveOnDispatcher()
                        .Subscribe(
                            x =>
                            {
                                btnDelete.Enabled = true;
                                btnBackfillHistory.Enabled = false;
                                livestreamStatusPanel.Enabled = false;
                                btnStreaming.Enabled = false;
                                if (x.Deleted)
                                {
                                    lstDatastreams.Items.Remove(deletedItem);
                                    if (lstDatastreams.Items.Count > 0)
                                    {
                                        foreach (ListViewItem item1 in lstDatastreams.Items)
                                        {
                                            item1.Selected = true;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (lstDatastreams.Enabled)
                                        {
                                            _selectedConfig = null;
                                            errorBox.Text = "";
                                            mainPanel.Hide();
                                            lblNoDatastreams.Text = "No Datastreams present. Please create new datastream";
                                            //picDsLoader.Hide();
                                            lblNoDatastreams.Show();
                                        }
                                    }
                                    picDsLoader.Hide();
                                    hideSaveUpdate();
                                    livestreamStatusPanel.Enabled = true;
                                    btnStreaming.Enabled = true;
                                    refreshLayer.setUp();
                                }
                            },
                            x =>
                            {
                                btnDelete.Enabled = true;
                                refreshLayer.setUp();
                                try
                                {
                                    if (x.Data != null && x.Data.Count > 0 && x.Data.Contains("title") && x.Data.Contains("msg"))
                                    {
                                        Log.Error($"Delete Datastream Failed {x.StackTrace}");
                                        MessageBox.Show(x.Data["msg"].ToString(), x.Data["title"].ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else if (x.Message == "Host unreachable")
                                    {
                                        Log.Error($"Can not Delete Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");

                                        DialogResult dialogResult = MessageBox.Show($"Failed to connect to host: {_selectedConfig.Configuration.FalkonryConfiguration.Host} \nPlease confirm if you would still like to delete the datastream : {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} ?\n\nNote: This will not delete the datastream from Falkonry Professional!", "Delete Datastream", MessageBoxButtons.YesNo);
                                       

                                        if (dialogResult == DialogResult.Yes)
                                        {
                                            saveConfig("Delete", piSystemName);
                                            
                                        }
                                    } else {
                                        Log.Error($"Can not Delete Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                                        MessageBox.Show($@"Cannot Delete your Datastream. {x.Message}!", @"Cannot Delete.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        picDsLoader.Hide();
                                    }
                                }
                                catch (Exception)
                                {
                                    Log.Error($"Exception in On Error of Save/Update Subscriber {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                                    picDsLoader.Hide();
                                }
                                hideSaveUpdate();
                            }
                        );
                }
            }
            catch (Exception exception)
            {

                Log.Fatal($"Exception caught while deleting datastream, Exception - {exception.Message}");
                Log.Fatal($"Exception , Stack Trace  - {exception.StackTrace}");
            }

        }

        public static T Clone<T>(T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        private void lstDatastreams_SelectedIndexChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            { 
                try
                {
                    var configItem = _configFromFile.ConfigurationItems.First(c => c.Configuration.FalkonryConfiguration.SourceId + c.Configuration.FalkonryConfiguration.Host == e.Item.Name);

                    _previousConfig = Clone(_selectedConfig);
                    _selectedConfig = configItem;

                    txtFalkonryDatastreamLeave(null, null);

                    CheckPiSystemConnection();

                    PopulateControls();
                }
                catch (Exception error)
                {
                    Log.Fatal($"Exception in listview Datastream selection change Exception Message: {error.Message} \n Exception StackTrace: {error.StackTrace}");
                }
            }

        }

       

        private void btnAddNewAssessmentRow_Click(object sender, EventArgs e)
        {
            try
            {


                enableSaveUpdate();
                PictureBox picDynamicAddNewAssessment = new PictureBox();
                PictureBox picDynamicAddNewOutput = new PictureBox();
                PictureBox dynamicAssessmentLoader = new PictureBox();
                PictureBox btnDynamicDeleteNewAssessment = new PictureBox();
                ComboBox lstDynamicAssessmentDropdown = new ComboBox();
                ComboBox lstDynamicWriteBackAttribute = new ComboBox();
                CheckedComboBox ccbDynamicEventFrameTemplate = new CheckedComboBox();
                int i = 0;
                int marginFromTop = 20;


                picCreateNewOutput.Visible = true;
                lstDynamicAssessmentDropdown.Location = new System.Drawing.Point(assessmentList.Location.X, (assessmentList.Location.Y + (assessmentCount * (assessmentList.Height + 7))));
                lstDynamicWriteBackAttribute.Location = new System.Drawing.Point(lstWriteBackAttribute.Location.X, (lstWriteBackAttribute.Location.Y + (assessmentCount * (lstWriteBackAttribute.Height + 7))));
                picDynamicAddNewAssessment.Location = new System.Drawing.Point(btnCreateNewAssessment.Location.X, lstDynamicAssessmentDropdown.Location.Y);
                picDynamicAddNewOutput.Location = new System.Drawing.Point(picCreateNewOutput.Location.X, picDynamicAddNewAssessment.Location.Y);
                ccbDynamicEventFrameTemplate.Location = new System.Drawing.Point(WhichEventFrameTemplate.Location.X, (WhichEventFrameTemplate.Location.Y + (assessmentCount * (WhichEventFrameTemplate.Height + 8))));

                assessmentCount++;

                if (assessmentCount == 2)
                    marginFromTop = 0;

                lstDynamicAssessmentDropdown.Font = assessmentList.Font;//new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lstDynamicAssessmentDropdown.FormattingEnabled = true;
                //lstDynamicAssessmentDropdown.Location = new System.Drawing.Point(17, 68 + marginFromTop);
                //lstDynamicAssessmentDropdown.Location = new System.Drawing.Point(assessmentList.Location.X, (assessmentList.Location.Y+(assessmentCount*assessmentList.Height)));
                lstDynamicAssessmentDropdown.Margin = new Padding(0, 5, 0, 5);
                lstDynamicAssessmentDropdown.Name = DYNAASSESSDROPDOWN;//"lstDynamicAssessmentDropdown";
                lstDynamicAssessmentDropdown.Name += assessmentCount;
                lstDynamicAssessmentDropdown.Size = assessmentList.Size;//new System.Drawing.Size(195, 24);
                lstDynamicAssessmentDropdown.TabIndex = 58;
                lstDynamicAssessmentDropdown.Tag = assessmentCount;
                lstDynamicAssessmentDropdown.SelectedIndexChanged += new System.EventHandler(this.assessmentList_SelectedIndexChanged);
                lstDynamicAssessmentDropdown.DropDownStyle = assessmentList.DropDownStyle;//System.Windows.Forms.ComboBoxStyle.DropDownList;
                lstDynamicAssessmentDropdown.DisplayMember = "Text";
                lstDynamicAssessmentDropdown.ValueMember = "Value";
                
                for (i = 0; i < _selectedConfig.Configuration.FalkonryConfiguration.assessmentList.Count; i++)
                {
                    ComboboxItem item = new ComboboxItem { Text = _selectedConfig.Configuration.FalkonryConfiguration.assessmentList.ElementAt(i).Name, Value = _selectedConfig.Configuration.FalkonryConfiguration.assessmentList.ElementAt(i).Id };
                    lstDynamicAssessmentDropdown.Items.Add(item);
                }

                //picDynamicAddNewAssessment.BackgroundImage = global::Falkonry.Integrator.PISystem.Configurator.Resources.ButtonBack___Rectangle;
                picDynamicAddNewAssessment.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_add;

                picDynamicAddNewAssessment.Name = ADDNEWASSESSMENT;//"picDynamicAddNewAssessment";
                picDynamicAddNewAssessment.Name += assessmentCount;
                // picDynamicAddNewAssessment.Padding = new System.Windows.Forms.Padding(7, 4, 3, 3);
                picDynamicAddNewAssessment.Size = btnCreateNewAssessment.Size;//new System.Drawing.Size(26, 26);
                picDynamicAddNewAssessment.SizeMode = btnCreateNewAssessment.SizeMode;
                picDynamicAddNewAssessment.TabIndex = 66;
                picDynamicAddNewAssessment.TabStop = false;
                picDynamicAddNewAssessment.Tag = assessmentCount;
                //picDynamicAddNewAssessment.Visible = false;
                picDynamicAddNewAssessment.Click += new System.EventHandler(this.btnAddNewAssessment_Click);
                picDynamicAddNewAssessment.MouseHover += new System.EventHandler(this.picAddNewAssessment_MouseHover);
            


                lstDynamicWriteBackAttribute.DropDownStyle = lstWriteBackAttribute.DropDownStyle;//System.Windows.Forms.ComboBoxStyle.DropDownList;
                lstDynamicWriteBackAttribute.Font = lstWriteBackAttribute.Font;//new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lstDynamicWriteBackAttribute.FormattingEnabled = true;

                lstDynamicWriteBackAttribute.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
                lstDynamicWriteBackAttribute.Name = ATTRIBUTEOUTDROPDOWN;//"lstDynamicWriteBackAttribute";
                lstDynamicWriteBackAttribute.Name += assessmentCount;
                lstDynamicWriteBackAttribute.Size = lstWriteBackAttribute.Size;//new System.Drawing.Size(195, 24);
                lstDynamicWriteBackAttribute.TabIndex = 54;
                lstDynamicWriteBackAttribute.Tag = assessmentCount;
                lstDynamicWriteBackAttribute.SelectedIndexChanged += new System.EventHandler(this.lstWriteBackAttribute_SelectedIndexChanged);
     
                for (i = 0; i < lstWriteBackAttribute.Items.Count; i++)
                {
                    lstDynamicWriteBackAttribute.Items.Add(lstWriteBackAttribute.Items[i]);
                }

                //picDynamicAddNewOutput.BackgroundImage = global::Falkonry.Integrator.PISystem.Configurator.Resources.ButtonBack___Rectangle;
                picDynamicAddNewOutput.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_add;
                picDynamicAddNewOutput.Name = ADDNEWATTRIBUTE;//"picDynamicAddNewOutput";
                picDynamicAddNewOutput.Name += assessmentCount;
                //picDynamicAddNewOutput.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
                picDynamicAddNewOutput.Size = picCreateNewOutput.Size;//new System.Drawing.Size(26, 26);
                picDynamicAddNewOutput.SizeMode = picCreateNewOutput.SizeMode;
                picDynamicAddNewOutput.TabIndex = 66;
                picDynamicAddNewOutput.TabStop = false;
                picDynamicAddNewOutput.Tag = assessmentCount;
                picDynamicAddNewOutput.Click += new System.EventHandler(this.picCreateNewOutput_Click);
                picDynamicAddNewOutput.MouseHover += new System.EventHandler(this.picAddNewOutput_MouseHover);



                dynamicAssessmentLoader.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.loading_circles;
                dynamicAssessmentLoader.Location = new System.Drawing.Point(ccbDynamicEventFrameTemplate.Location.X + ccbDynamicEventFrameTemplate.Width + 100, ccbDynamicEventFrameTemplate.Location.Y);
                dynamicAssessmentLoader.Name = ASSESSMENTLOADER;//"btnDynamicDeleteNewAssessment";
                dynamicAssessmentLoader.Name += assessmentCount;
                dynamicAssessmentLoader.Size = new System.Drawing.Size(25, 25);
                dynamicAssessmentLoader.SizeMode = btnCreateNewAssessment.SizeMode;
                dynamicAssessmentLoader.TabIndex = 61;
                dynamicAssessmentLoader.TabStop = false;
                dynamicAssessmentLoader.Tag = assessmentCount;
                dynamicAssessmentLoader.Visible = true;
                dynamicAssessmentLoader.Hide();

                ccbDynamicEventFrameTemplate.CheckOnClick = true;

                ccbDynamicEventFrameTemplate.DrawMode = WhichEventFrameTemplate.DrawMode;//System.Windows.Forms.DrawMode.OwnerDrawVariable;
                ccbDynamicEventFrameTemplate.DropDownHeight = WhichEventFrameTemplate.DropDownHeight;//1;
                ccbDynamicEventFrameTemplate.Font = WhichEventFrameTemplate.Font;//new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                ccbDynamicEventFrameTemplate.FormattingEnabled = true;
                //ccbDynamicEventFrameTemplate.IntegralHeight = false;

                ccbDynamicEventFrameTemplate.Name = EVENTFRAMETEMPDROPDOWN;//"ccbDynamicEventFrameTemplate";
                ccbDynamicEventFrameTemplate.Name += assessmentCount;
                ccbDynamicEventFrameTemplate.Size = WhichEventFrameTemplate.Size;//new System.Drawing.Size(242, 24);
                ccbDynamicEventFrameTemplate.TabIndex = 56;
                ccbDynamicEventFrameTemplate.ValueSeparator = ", ";
                ccbDynamicEventFrameTemplate.Click += new System.EventHandler(this.WhichEventFrameTemplate_Click);
                ccbDynamicEventFrameTemplate.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.WhichEventFrameTemplate_ItemCheck);
                ccbDynamicEventFrameTemplate.SelectedIndexChanged += new System.EventHandler(this.WhichEventFrameTemplate_SelectedIndexChanged);
                ccbDynamicEventFrameTemplate.DisplayMember = "Name";
                ccbDynamicEventFrameTemplate.MaxDropDownItems = 10;
                ccbDynamicEventFrameTemplate.ValueSeparator = ", ";
                ccbDynamicEventFrameTemplate.Tag = assessmentCount;



                //btnDynamicDeleteNewAssessment.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.loading_circles;
                btnDynamicDeleteNewAssessment.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_delete_1;
                btnDynamicDeleteNewAssessment.Location = new System.Drawing.Point(ccbDynamicEventFrameTemplate.Location.X + ccbDynamicEventFrameTemplate.Width + 5, ccbDynamicEventFrameTemplate.Location.Y);
                btnDynamicDeleteNewAssessment.Name = DELETEASSESSMENT;//"btnDynamicDeleteNewAssessment";
                btnDynamicDeleteNewAssessment.Name += assessmentCount;
                btnDynamicDeleteNewAssessment.Size = btnCreateNewAssessment.Size;//new System.Drawing.Size(24, 24);
                btnDynamicDeleteNewAssessment.SizeMode = btnCreateNewAssessment.SizeMode;
                btnDynamicDeleteNewAssessment.TabIndex = 61;
                btnDynamicDeleteNewAssessment.TabStop = false;
                btnDynamicDeleteNewAssessment.Tag = assessmentCount;
                btnDynamicDeleteNewAssessment.Click += new System.EventHandler(btnDeleteAssessment_Click);
                btnDynamicDeleteNewAssessment.MouseHover += new System.EventHandler(this.picDeleteAssessmentRow_MouseHover);





                for (i = 0; i < WhichEventFrameTemplate.Items.Count; i++)
                {
                    ccbDynamicEventFrameTemplate.Items.Add(WhichEventFrameTemplate.Items[i]);
                }

                //btnAddNewAssessmentRow.Location = new System.Drawing.Point(btnAddNewAssessmentRow.Location.X, lstDynamicWriteBackAttribute.Location.Y);

                assessmentBody.Controls.Add(lstDynamicAssessmentDropdown);
                assessmentBody.Controls.Add(picDynamicAddNewAssessment);
                assessmentBody.Controls.Add(lstDynamicWriteBackAttribute);
                assessmentBody.Controls.Add(picDynamicAddNewOutput);
                assessmentBody.Controls.Add(ccbDynamicEventFrameTemplate);
                assessmentBody.Controls.Add(dynamicAssessmentLoader);
                assessmentBody.Controls.Add(btnDynamicDeleteNewAssessment);
              
                assessmentBody.VerticalScroll.Value = assessmentBody.VerticalScroll.Maximum;
               

            }
            catch (Exception exception)
            {
                Log.Error($"Exception caught in btnAddNewAssessmentRow_Click: {exception.Message}");
            }
        }


        private void btnCreateNewOutput_Click(object sender, EventArgs e)
        {
            try
            {
                if (((Button)sender).Tag != null)
                    assessmentIndexInAction = (int)((Button)sender).Tag;
                else
                    assessmentIndexInAction = 1;
                showAssessmentLoader();
                var nameOfAttribute = Microsoft.VisualBasic.Interaction.InputBox("Please enter a new name for the AF Attribute Template.", "AF Attribute Template Name", "");
                if (nameOfAttribute == "")
                {
                    MessageBox.Show("No name was entered for the AF Attribute.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideAssessmentLoader();
                    return;
                }

                var attributeConfigStringName = Microsoft.VisualBasic.Interaction.InputBox("Please enter the PI Point name. \n You can use AF Substitution Parameters for the name!", "PI Point Name.", @"\\%Server%\%Element%.%Attribute%");
                if (attributeConfigStringName == "")
                {
                    MessageBox.Show("No name was entered for the PI Point.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideAssessmentLoader();
                    return;
                }

                if (WhichElementTemplate.Items.Count == 0 || WhichElementTemplate.SelectedValue == null)
                {
                    MessageBox.Show("No AF Element Template is selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideAssessmentLoader();
                    return;
                }


                ComboBox writeBackComboBox;
                if (assessmentIndexInAction == 1)
                {
                    writeBackComboBox = lstWriteBackAttribute;
                }
                else
                {
                    writeBackComboBox = (ComboBox)assessmentBody.Controls.Find(ADDNEWATTRIBUTE + assessmentIndexInAction, true)[0];
                }

                var nameofElementTemplate = (string)WhichElementTemplate.SelectedValue;

                var db = WhichAFDatabase.AFDatabase;
                if (db == null)
                {
                    return;
                }
                //afallElements = AFElement.FindElements(WhichAFDatabase.AFDatabase, null, "*", AFSearchField.Name, true, AFSortField.Name, AFSortOrder.Ascending, int.MaxValue);
                if ((string)WhichElementTemplate.SelectedValue == "No Template" && WhichElements.CheckedItems.Count > 0)
                {
                    for (int n = 0; n < WhichElements.CheckedItems.Count; n++)
                    {
                        int it = 0;
                        for (it = 0; it < afallElements.Count; it++)
                        {
                            string elementName = ((CCBoxItem)(WhichElements.CheckedItems[n])).Name;
                            if ((afallElements[it]).Name == (elementName.Substring(elementName.LastIndexOf('\\') + 1)))
                            {
                                break;
                            }
                        }
                        if (afallElements[it].Template != null)
                            afallElements[it].Template.AllowElementToExtend = true;
                        afallElements[it].Attributes.Add(nameOfAttribute);
                        var theAttribute = afallElements[it].Attributes[nameOfAttribute];
                        if (afallElements[it].Attributes[nameOfAttribute] == null)
                        {
                            if (afallElements[it].IsDirty)
                                afallElements[it].UndoCheckOut(true);

                            MessageBox.Show("Unable to create the AF Attribute.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            hideAssessmentLoader();
                            return;
                        }
                        afallElements[it].Attributes[nameOfAttribute].DataReferencePlugIn = db.PISystem.DataReferencePlugIns["PI Point"];
                        afallElements[it].Attributes[nameOfAttribute].ConfigString = $"{attributeConfigStringName};ReadOnly=False;ptclassname=classic;pointtype=String;pointsource=Falkonry-Output";
                        afallElements[it].CheckIn();
                        db.Refresh();

                        afallElements[it].Attributes[nameOfAttribute].DataReference.CreateConfig();
                    }
                    

                    //DB check in part is not handled
                }
                else
                {
                    db.ElementTemplates[nameofElementTemplate].AttributeTemplates.Add(nameOfAttribute);
                    var theAttribute = db.ElementTemplates[nameofElementTemplate].AttributeTemplates[nameOfAttribute];
                    if (theAttribute == null)
                    {
                        if (db.ElementTemplates[nameofElementTemplate].IsDirty)
                            db.ElementTemplates[nameofElementTemplate].UndoCheckOut(true);

                        MessageBox.Show("Unable to create the AF Attribute.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        hideAssessmentLoader();
                        return;
                    }

                    theAttribute.DataReferencePlugIn = db.PISystem.DataReferencePlugIns["PI Point"];
                    theAttribute.ConfigString = $"{attributeConfigStringName};ReadOnly=False;ptclassname=classic;pointtype=String;pointsource=Falkonry-Output";
                    theAttribute.ElementTemplate.CheckIn();


                    db.Refresh();
                    var foundAttributes = AFElement.FindElementsByTemplate(db, null, theAttribute.ElementTemplate, true, AFSortField.Name, AFSortOrder.Ascending, 10000).Select(elem => elem.Attributes[theAttribute.Name]);
                    if (foundAttributes.Count() == 0)
                    {
                        MessageBox.Show("Did not find any instances of the AF Attribute created, cannot create underlying PI Points.", "Unable to create PI Points.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    foundAttributes.ToList().ForEach(a => a.DataReference.CreateConfig());
                    //db.CheckIn();
                }

                MessageBox.Show("AF attributes created for Assessments on all selected Entities", "AF Attributes created.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                hideAssessmentLoader();
                db.CheckIn();

                int i = WhichAttributeTemplates.Items.Count;
                CCBoxItem item = new CCBoxItem(nameOfAttribute, i + 1);
                WhichAttributeTemplates.Items.Add(item);
                i = 0;
                writeBackComboBox.Items.Clear();

                foreach (CCBoxItem avialableAttribute in WhichAttributeTemplates.Items)
                {
                    if (!WhichAttributeTemplates.CheckedItems.Contains(avialableAttribute) && avialableAttribute.Name != "Select All")
                    {
                        writeBackComboBox.Items.Add(avialableAttribute.Name);
                    }
                }
                if (writeBackComboBox.Items.Contains(nameOfAttribute)) writeBackComboBox.Text = nameOfAttribute;
                if (writeBackComboBox.Items.Count == 0)
                {
                    writeBackComboBox.Items.Add("");
                }
                hideAssessmentLoader();

            }
            catch (Exception exception)
            {
                hideAssessmentLoader();
                MessageBox.Show($"Something went wrong creating the AF Attributes. Error: {exception.Message}", "Error creating AF Attributes.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                try
                {
                    if (WhichAFDatabase.AFDatabase.IsDirty) WhichAFDatabase.AFDatabase.UndoCheckOut(true);
                }
                catch (Exception)
                {

                }
            }
        }

        private void txtFalkonryDatastream_TextChanged(object sender, EventArgs e)
        {
            txtAssessmentIdOutput.Text = txtFalkonryDatastream.Text;
            assessmentList.Text = txtFalkonryDatastream.Text;
            _selectedConfig.Name = txtFalkonryDatastream.Text;
        }



        private void lblAutoUpdSettings_Click(object sender, EventArgs e)
        {
            if (pageSizeLabel.Visible)
            {
                pageSizeLabel.Visible = false;
                pageSize.Visible = false;
                lblCollectionInterval.Visible = false;
                collectionInterval.Visible = false;
            }
            else
            {
                pageSizeLabel.Visible = true;
                pageSize.Visible = true;
                lblCollectionInterval.Visible = true;
                collectionInterval.Visible = true;
            }

        }


        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void WhichAttributeTemplates_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //MessageBox.Show("Hello");
            try
            {
                if (((CheckedListBox)sender).SelectedItem != null && ((CCBoxItem)((CheckedListBox)sender).SelectedItem).Name == "Select All")
                {
                    bool isCheck = true;
                    if (e.Index == 0)
                    {
                        if (e.NewValue.ToString() == "Unchecked")
                        {
                            isCheck = false;
                        }
                        WhichAttributeTemplates.Text = "";
                        for (int i = 1; i < WhichAttributeTemplates.Items.Count; i++)
                        {
                            WhichAttributeTemplates.SetItemChecked(i, isCheck);
                        }
                    }
                }
                else
                {
                    if (e.Index > 0 && WhichAttributeTemplates.Items.Count > 0 && ((CCBoxItem)WhichAttributeTemplates.Items[0]).Name == "Select All" && WhichAttributeTemplates.GetItemChecked(0))
                    {
                        WhichAttributeTemplates.SetItemChecked(0, false);
                    }
                }

            }
            catch (Exception)
            {

                return;
            }

        }

        private void WhichElements_Leave(object sender, EventArgs e)
        {
            if ((string)WhichElementTemplate.SelectedValue != "No Template" && WhichElements.Items.Count > 0 && WhichElements.GetItemChecked(0))
            {
                WhichElements.SetItemChecked(0, true);
            }
            //For No Template
            if ((string)WhichElementTemplate.SelectedValue == "No Template" && WhichElements.CheckedItems.Count > 0)
            {
                WhichAttributeTemplates.Text = "";
                WhichAttributeTemplates.Items.Clear();
                WhichAttributeTemplates.Sorted = true;
                batchIdentifier.Text = "";
                batchIdentifier.Items.Clear();
                batchIdentifier.Sorted = true;
                int k = 0;
                int firstElementIndex = 0;

                // afallElements = AFElement.FindElements(WhichAFDatabase.AFDatabase, null, "*", AFSearchField.Name, true, AFSortField.Name, AFSortOrder.Ascending, int.MaxValue);
                for (k = 0; k < afallElements.Count; k++)
                {
                    string elementName = ((CCBoxItem)(WhichElements.CheckedItems[firstElementIndex])).Name;
                    if ((afallElements[k]).Name == (elementName.Substring(elementName.LastIndexOf('\\') + 1)))
                    {
                        break;
                    }
                }
                var firstElementsAttributes = (afallElements[k]).Attributes;
                firstElementsAttributes.Sort();
                AFAttribute[] noTemplateAttributes = new AFAttribute[(afallElements[k]).Attributes.Count];

                if (WhichElements.CheckedItems.Count > 1)
                {
                    int m = 0;
                    foreach (var attribute in firstElementsAttributes)
                    {
                        for (int it = firstElementIndex + 1; it < WhichElements.CheckedItems.Count; it++)
                        {
                            for (k = 0; k < afallElements.Count; k++)
                            {
                                string elementName = ((CCBoxItem)(WhichElements.CheckedItems[it])).Name;
                                if ((afallElements[k]).Name == (elementName.Substring(elementName.LastIndexOf('\\') + 1)))
                                {
                                    break;
                                }
                            }

                            int n = 0;
                            for (n = 0; n < afallElements[k].Attributes.Count; n++)
                            {
                                if (afallElements[k].Attributes[n].Name == attribute.Name)
                                    break;
                            }
                            if (n == afallElements[k].Attributes.Count)
                            {
                                break;
                            }
                            if (it == WhichElements.CheckedItems.Count - 1)
                            {
                                //noTemplateAttributes.SetValue(attribute, 0);
                                noTemplateAttributes[m++] = attribute;
                            }
                        }
                    }
                }
                else
                {
                    //firstElementsAttributes.CopyTo()
                    k = 0;
                    foreach (var attributeTemplate in firstElementsAttributes)
                    {
                        //noTemplateAttributeTemplate.SetValue(attributeTemplate, 0);
                        noTemplateAttributes[k++] = attributeTemplate;
                    }
                }
                for (k = 0; k < noTemplateAttributes.Count(); k++)
                {
                    if (noTemplateAttributes[k] != null)
                    {
                        CCBoxItem item = new CCBoxItem(noTemplateAttributes[k].Name, k);
                        WhichAttributeTemplates.Items.Add(item);
                        batchIdentifier.Items.Add(item);
                    }
                }


                WhichAttributeTemplates.DisplayMember = "Name";
                WhichAttributeTemplates.MaxDropDownItems = 10;
                WhichAttributeTemplates.ValueSeparator = ", ";

                batchIdentifier.DisplayMember = "Name";
                batchIdentifier.MaxDropDownItems = 10;
                // batchIdentifier.ValueSeparator = ", ";

                var currentAttribute = lstWriteBackAttribute.Text;
                lstWriteBackAttribute.Items.Clear();
                lstWriteBackAttribute.Sorted = true;
                foreach (CCBoxItem avialableAttribute in WhichAttributeTemplates.Items)
                {
                    lstWriteBackAttribute.Items.Add(avialableAttribute.Name);
                }
                if (lstWriteBackAttribute.Items.Contains(currentAttribute)) lstWriteBackAttribute.Text = currentAttribute;
                if (lstWriteBackAttribute.Items.Count == 0)
                {
                    lstWriteBackAttribute.Items.Add("");
                }
            }
        }

        private void WhichElements_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //MessageBox.Show("Hello");
            try
            {
                bool isCheck = true;
                if (sender == null || sender.Equals("")) return;
                if (!(((CheckedListBox)sender).SelectedItem == null || ((CheckedListBox)sender).SelectedItem.Equals("")) && ((CCBoxItem)((CheckedListBox)sender).SelectedItem).Name == "Select All")
                {
                    if (e.Index == 0)
                    {
                        if (e.NewValue.ToString() == "Unchecked")
                        {
                            isCheck = false;
                        }
                        WhichElements.Text = "";
                        for (int i = 1; i < WhichElements.Items.Count; i++)
                        {
                            WhichElements.SetItemChecked(i, isCheck);
                        }
                    }
                }
                else
                {
                    if (e.Index > 0 && WhichElements.Items.Count > 0 && ((CCBoxItem)WhichElements.Items[0]).Name == "Select All" && WhichElements.GetItemChecked(0))
                    {
                        WhichElements.SetItemChecked(0, false);
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error($"WhichElements_ItemCheck: Exception caught - {exception.Message}");
                Log.Error($"WhichElements_ItemCheck: Exception caught, Stack Trace - {exception.StackTrace}");
                return;
            }

        }

        private void WhichEventFrameTemplate_Click(object sender, EventArgs e)
        {
            if (((CheckedComboBox)sender).Tag != null)
                assessmentIndexInAction = (int)((CheckedComboBox)sender).Tag;
            else
                assessmentIndexInAction = 1;
        }
        private void WhichEventFrameTemplate_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //MessageBox.Show("Hello");
            try
            {
                int i = 0;
                if (((CheckedListBox)sender).SelectedItem != null && ((CCBoxItem)((CheckedListBox)sender).SelectedItem).Name == "Select All")
                {

                    bool isCheck = true;
                    if (e.Index == 0)
                    {
                        if (e.NewValue.ToString() == "Unchecked")
                        {
                            isCheck = false;
                        }
                        enableSaveUpdate();
                        if (assessmentIndexInAction == 1)
                        {
                            WhichEventFrameTemplate.Text = "";
                            for (i = 1; i < WhichEventFrameTemplate.Items.Count; i++)
                            {
                                WhichEventFrameTemplate.SetItemChecked(i, isCheck);
                            }
                        }
                        else
                        {
                            CheckedComboBox eventFaremCcb = (CheckedComboBox)(assessmentBody.Controls.Find(EVENTFRAMETEMPDROPDOWN + assessmentIndexInAction, true)[0]);
                            eventFaremCcb.Text = "";
                            for (i = 1; i < eventFaremCcb.Items.Count; i++)
                            {
                                eventFaremCcb.SetItemChecked(i, isCheck);
                            }
                        }

                    }
                }
                else
                {
                    CheckedComboBox eventFaremCcb;
                    if (assessmentIndexInAction == 1)
                    {
                        eventFaremCcb = WhichEventFrameTemplate;
                    }
                    else
                    {
                        eventFaremCcb = (CheckedComboBox)((CheckedComboBox)(assessmentBody.Controls.Find(EVENTFRAMETEMPDROPDOWN + assessmentIndexInAction, true)[0]));
                    }
                    if (e.Index > 0 && eventFaremCcb.Items.Count > 0 && eventFaremCcb.GetItemChecked(0))
                    {
                        eventFaremCcb.SetItemChecked(0, false);
                    }
                }
                enableSaveUpdate();
            }
            catch (Exception)
            {

                return;
            }

        }

        private void WhichAttributeTemplates_Leave(object sender, EventArgs e)
        {


            lstWriteBackAttribute.Items.Clear();
            foreach (CCBoxItem avialableAttribute in WhichAttributeTemplates.Items)
            {
                if (!WhichAttributeTemplates.CheckedItems.Contains(avialableAttribute) && avialableAttribute.Name != "Select All")
                {
                    lstWriteBackAttribute.Items.Add(avialableAttribute.Name);
                }
            }
            if (lstWriteBackAttribute.Items.Count == 0)
            {
                lstWriteBackAttribute.Items.Add("");
            }
            this.ActiveControl = null;
            return;
        }

        // Element Template Selection Changed.
        private void WhichElementTemplate_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            WhichAttributeTemplates.Text = "";
            WhichAttributeTemplates.Items.Clear();
            batchIdentifier.Text = "";
            batchIdentifier.Items.Clear();
            WhichElements.Text = "";
            WhichElements.Items.Clear();
            int i = 0;
            if (WhichElementTemplate.SelectedValue == null) return;


            WhichElements.Items.Clear();

            try
            {
                if ((string)WhichElementTemplate.SelectedValue == "No Template")
                {
                    //foreach (var element in AFElement.FindElementsByTemplate(WhichAFDatabase.AFDatabase, null, WhichAFDatabase.AFDatabase.ElementTemplates[(string)WhichElementTemplate.SelectedValue], true, AFSortField.Name, AFSortOrder.Ascending, 10000))
                    foreach (var element in afallElements)
                    {
                        CCBoxItem item = new CCBoxItem(element.GetPath(WhichAFDatabase.AFDatabase), i++);
                        WhichElements.Items.Add(item);
                    }

                }
                else
                {
                    CCBoxItem selectAllItem = new CCBoxItem("Select All", i++);
                    WhichAttributeTemplates.Items.Add(selectAllItem);
                    CCBoxItem selectOne = new CCBoxItem("Select Anyone", 0);
                    batchIdentifier.Items.Add(selectOne);
                    CCBoxItem selectAllItemElements = new CCBoxItem("Select All", i++);
                    WhichElements.Items.Add(selectAllItemElements);
                    WhichAFDatabase.AFDatabase.ElementTemplates[(string)WhichElementTemplate.SelectedValue].AttributeTemplates.Sort();
                    foreach (var attributeTemplate in WhichAFDatabase.AFDatabase.ElementTemplates[(string)WhichElementTemplate.SelectedValue].AttributeTemplates)
                    {
                        CCBoxItem item = new CCBoxItem(attributeTemplate.Name, i++);
                        WhichAttributeTemplates.Items.Add(item);
                        batchIdentifier.Items.Add(item);

                    }
                    WhichAttributeTemplates.DisplayMember = "Name";
                    WhichAttributeTemplates.MaxDropDownItems = 10;
                    WhichAttributeTemplates.ValueSeparator = ", ";
                    batchIdentifier.DisplayMember = "Name";
                    batchIdentifier.MaxDropDownItems = 10;
                    //batchIdentifier.ValueSeparator = ", ";
                    foreach (var element in AFElement.FindElementsByTemplate(WhichAFDatabase.AFDatabase, null, WhichAFDatabase.AFDatabase.ElementTemplates[(string)WhichElementTemplate.SelectedValue], true, AFSortField.Name, AFSortOrder.Ascending, 10000))
                    {
                        CCBoxItem item = new CCBoxItem(element.GetPath(WhichAFDatabase.AFDatabase), i++);
                        WhichElements.Items.Add(item);
                    }
                    WhichElements.DisplayMember = "Name";
                    WhichElements.MaxDropDownItems = 10;
                    WhichElements.ValueSeparator = ", ";
                    var currentAttribute = lstWriteBackAttribute.Text;
                    lstWriteBackAttribute.Items.Clear();
                    lstWriteBackAttribute.Sorted = true;
                    foreach (CCBoxItem avialableAttribute in WhichAttributeTemplates.Items)
                    {
                        lstWriteBackAttribute.Items.Add(avialableAttribute.Name);
                        Log.Debug($"lstWriteBackAttribute item { avialableAttribute }");
                    }
                    if (lstWriteBackAttribute.Items.Contains(currentAttribute)) lstWriteBackAttribute.Text = currentAttribute;
                    if (lstWriteBackAttribute.Items.Count == 0)
                    {
                        lstWriteBackAttribute.Items.Add("");
                    }

                }
            }

            catch(Exception ex)
            {
                Log.Error($"On template selection change Exception Message {ex.Message} ");
                Log.Fatal($" Exception StackTrace {ex.StackTrace}");
            }
        }


        private void txtFalkonryToken_TextChanged(object sender, EventArgs e)
        {
            checkFalkonryStatus();
        }

        class Auth
        {
            TextBox txtHost;
            TextBox txtToken;
            public Auth(ConfigurationForm form)
            {
                form.txtFalkonryHost = txtHost;
                form.txtFalkonryToken = txtToken;
            }
            public Func<bool, bool> getDelegate()
            {
                return new Func<bool, bool>(checkHostToken);
            }

            public bool checkHostToken(bool yes)
            {

                if (FalkonryHelper.AuthenticateConnectionDetails(this.txtHost.Text, this.txtToken.Text))
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
        }

        private bool AuthenticateTofalkonry(bool yes)
        {
            if (yes == true)
            {
                falkonryConnected.Show();
                falkonryDisconnected.Hide();
            }
            else
            {
                falkonryConnected.Hide();
                falkonryDisconnected.Show();
            }
            return yes;
        }

        private void initializeHostTextChangeObservable()
        {
            #region HOST Text Change
            var textchanges = Observable.FromEventPattern<EventHandler, EventArgs>
                      (
                       h => txtFalkonryHost.TextChanged += h,
                       h => txtFalkonryHost.TextChanged -= h
                      )
                      .Select(x =>
                      {
                          return Tuple.Create(((TextBox)x.Sender).Text, txtFalkonryToken.Text);
                      })
                      .SubscribeOn(NewThreadScheduler.Default)
                      .ObserveOn(NewThreadScheduler.Default)
                      .Throttle(TimeSpan.FromMilliseconds(300));

            textchanges
                  .Select(x =>
                  {
                      try
                      {
                          if (FalkonryHelper.AuthenticateConnectionDetails(x.Item1, x.Item2))
                          {
                              return true;

                          }
                          else
                          {
                              return false;
                          }
                      }
                      catch (Exception e)
                      {

                      }
                      return false;
                  })
                 .ObserveOnDispatcher()
                 .Subscribe(x => AuthenticateTofalkonry(x), x => { });
            #endregion
            #region token text change
            var tokenTextChange = Observable.FromEventPattern<EventHandler, EventArgs>
                      (
                       h => txtFalkonryToken.TextChanged += h,
                       h => txtFalkonryToken.TextChanged -= h
                      )
                      .Select(x =>
                      {
                          return Tuple.Create(txtFalkonryHost.Text, ((TextBox)x.Sender).Text);
                      })
                      .SubscribeOn(NewThreadScheduler.Default)
                      .ObserveOn(NewThreadScheduler.Default)
                      .Throttle(TimeSpan.FromMilliseconds(300));

            tokenTextChange
                  .Select(x =>
                  {
                      try
                      {
                          if (FalkonryHelper.AuthenticateConnectionDetails(x.Item1, x.Item2))
                          {
                              return true;

                          }
                          else
                          {
                              return false;
                          }
                      }
                      catch (Exception e)
                      {

                      }
                      return false;
                  })
                 .ObserveOnDispatcher()
                 .Subscribe(x => AuthenticateTofalkonry(x), x => { });
            #endregion
        }

        private void btnBackfillHistory_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstWriteBackAttribute.Text == "")
                {
                    MessageBox.Show($@"Please select attribute for result.", @"Backfill Output History!", MessageBoxButtons.OK);
                    return;
                }
                if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId == "new_datastream_saved")
                {
                    updateSelectedConfigFromDB(_selectedConfig);
                    if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId == "new_datastream_saved")
                    {
                        MessageBox.Show($@"Datastream creation is in progress, please try after sometime!", @"Backfill Output History!", MessageBoxButtons.OK);
                        return;
                    }
                }
                if (backFillInitiationInProgress)
                {
                    MessageBox.Show($@"Datastream BackFill initiation is in progress, please try after sometime!", @"Backfill Output History!", MessageBoxButtons.OK);
                    return;
                }
                _selectedConfig.Configuration.FalkonryConfiguration.Backfill = true;
                _selectedConfig.Enabled = true;
                string startValue = StartDatePicker.Value.ToString();
                string endValue = EndDatePicker.Value.ToString();
                string piSystemName = WhichPISystem.PISystem.Name;
                showDsLoader();
                backFillInitiationInProgress = true;
                Observable.Return(_selectedConfig)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .ObserveOn(NewThreadScheduler.Default)
                    .Select(x =>
                    {
                        AfHelper.UpdateState(_selectedConfig.Configuration.AfConfiguration.SystemName, _selectedConfig.Name, "INFO", $"Backfill output data initiated for '{startValue}' to '{endValue}''!");
                        
                        return x;
                    })
                    .Select(x =>
                    {
                        saveConfig("", piSystemName);
                        return x;
                    })
                    .ObserveOnDispatcher()
                    .Subscribe(
                    x => {
                        backFillInitiationInProgress = false;
                        hideDsLoader();
                        MessageBox.Show($@"Backfill output data initiated for '{startValue}' to '{endValue}''!", @"Backfill Output History Requested!", MessageBoxButtons.OK);
                        Log.Info($"Backfill output data initiated for '{startValue}' to '{endValue}'");
                    }, 
                    x => {
                        backFillInitiationInProgress = false;
                        try
                        {
                            if (x.Data != null && x.Data.Count > 0 && x.Data.Contains("title") && x.Data.Contains("msg"))
                            {
                                Log.Error($"Save/Update failed {x.StackTrace}");
                                MessageBox.Show(x.Data["msg"].ToString(), x.Data["title"].ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                Log.Error($"Can not Save/Update Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                                MessageBox.Show($@"Cannot save your Datastream. {x.Message}!", @"Cannot save.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            

                        }
                        catch (Exception)
                        {
                            Log.Error($"Exception in On Error of Save/Update Subscriber {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                        }
                        hideDsLoader();
                    });
                         
               
               
                
                Log.Debug("  End of  btnBackfillHistory_Click");
            }
            catch (Exception ex)
            {
                Log.Error($" btnBackfillHistory_Click: Exception caught : {ex.Message}");
                Log.Error($" btnBackfillHistory_Click: Exception Stack Trace : {ex.StackTrace}");
                throw;
            }
        }

        private void btnCancel_click(object sender, EventArgs e)
        {
            if (saving)
            {
                showDsLoader();
                return;
            }
          
           
            try
            {

                errorBox.Text = "";
                isTimeSettingChanged = false;
                if (_creatingNewDS) _creatingNewDS = false;
                if (_selectedConfig == null && lstDatastreams.Items.Count == 0)
                {
                    mainPanel.Hide();
                    lblNoDatastreams.Text = "No Datastreams present. Please create new datastream";
                    lblNoDatastreams.Show();
                    //enableListAndButtons();
                    //lstDatastreams.SelectedIndexChanged += new System.EventHandler(this.lstDatastreams_SelectedIndexChanged);
                    return;
                }
                else
                {
                    mainPanel.Show();
                    lblNoDatastreams.Hide();
                }
                if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId.Equals("new_datastream"))
                {
                    if (lstDatastreams.Items.Count > 0)
                    {
                        foreach (ListViewItem item in lstDatastreams.Items)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                    else
                    {
                        _selectedConfig = null;
                        errorBox.Text = "";
                        mainPanel.Hide();
                        lblNoDatastreams.Text = "No Datastreams present. Please create new datastream";
                        lblNoDatastreams.Show();
                        picAddNewDS.Enabled = true;
                    }


                }
                else
                {
                    int i = 0;
                    Log.Info($"btnCancel_Click: Canceling existing datastream update, {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} ");
                    panelAssessment.VerticalScroll.Value = 0;
                    PopulateControls();

                    //lstDatastreams.SelectedIndexChanged += new System.EventHandler(this.lstDatastreams_SelectedIndexChanged);
                }

            }
            catch (Exception exception)
            {
                Log.Error($" Error in fetching Cancel. Error : {exception.Message}");

                return;
            }

        }

        private void txtAssessmentIdOutput_TextChanged(object sender, EventArgs e)
        {
            if (txtAssessmentIdOutput.Text != _selectedConfig.Configuration.FalkonryConfiguration.AssessmentId.ToString())
                enableSaveUpdate();
        }

        private void lstWriteBackAttribute_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstWriteBackAttribute.Text != _selectedConfig.Configuration.AfConfiguration.OutputTemplateAttribute.ToString())
                enableSaveUpdate();
        }

        public void enableSaveUpdate()
        {
            try
            {

                if (_previousConfig.Configuration.FalkonryConfiguration.SourceId == _selectedConfig.Configuration.FalkonryConfiguration.SourceId || _selectedConfig.Configuration.FalkonryConfiguration.SourceId.Equals("new_datastream"))
                {
                    btnSave.Visible = true;
                    picRight.Visible = true;
                    btnCancel.Visible = true;
                    picWrong.Visible = true;
                    btnBackfillHistory.Enabled = false;
                    //btnStreaming.Enabled = false;
                    // btnStreaming.Cursor = System.Windows.Forms.Cursors.No;
                    btnBackfillHistory.Cursor = System.Windows.Forms.Cursors.No;
                    if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId.Equals("new_datastream"))
                    {
                        btnSave.Text = "SAVE";
                    }
                    else
                    {
                        btnSave.Text = "UPDATE";
                    }
                    lstDatastreams.Enabled = false;
                    picAddNewDS.Enabled = false;
                    btnDelete.Hide();
                    //disableListAndButtons();
                }
            }
            catch (Exception ex)
            {

                return;
            }
        }

        private void hideSaveUpdate()
        {

            btnSave.Visible = false;
            picRight.Visible = false;
            btnCancel.Visible = false;
            picWrong.Visible = false;
            btnBackfillHistory.Enabled = true;
            //btnStreaming.Enabled = true;
            //btnStreaming.Cursor = System.Windows.Forms.Cursors.Default;
            btnBackfillHistory.Cursor = System.Windows.Forms.Cursors.Default;
            hideDsLoader();

            lstDatastreams.Enabled = true;
            picAddNewDS.Enabled = true;
            btnDelete.Show();
            //enableListAndButtons();

        }


        private void WhichEventFrameTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            string eventFrameText = "";
            int i = 0;
            for (var iEt = 0; iEt < _selectedConfig.Configuration.AfConfiguration.EventFrameTemplateNames.Count(); iEt++)
            {
                eventFrameText += _selectedConfig.Configuration.AfConfiguration.EventFrameTemplateNames[iEt];
                if (i + 1 != _selectedConfig.Configuration.AfConfiguration.EventFrameTemplateNames.Count() && !eventFrameText.Equals(""))
                {
                    eventFrameText += ", ";
                }
            }
            if (eventFrameText != WhichEventFrameTemplate.Text)
                enableSaveUpdate();
        }

        private void collectionInterval_ValueChanged(object sender, EventArgs e)
        {
            if (collectionInterval.Value != _selectedConfig.Configuration.StreamingDataAccess.CollectionIntervalMs)
            {
                enableSaveUpdate();
            }
        }

        private void pageSize_ValueChanged(object sender, EventArgs e)
        {
            if (pageSize.Value != _selectedConfig.Configuration.HistoricalDataAccess.PageSize)
            {
                enableSaveUpdate();
            }
        }

        private void btnStreaming_Click(object sender, EventArgs e)
        {
           
            try {
                showDsLoader();
                if (lstWriteBackAttribute.Text == "" && !_selectedConfig.Configuration.StreamingDataAccess.Enabled)
                {
                    MessageBox.Show($@"Please select attribute for result.", @"Live monitoring!", MessageBoxButtons.OK);
                    hideDsLoader();
                    return;
                }
                if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId == "new_datastream_saved")
                {

                    Observable.Return(_selectedConfig)
                        .SubscribeOn(NewThreadScheduler.Default)
                        .ObserveOn(NewThreadScheduler.Default)
                        .Select(x =>
                        {
                            refreshLayer.tearDown();
                            updateSelectedConfigFromDB(_selectedConfig);
                            return x;
                        })
                        .ObserveOnDispatcher()
                        .Subscribe(
                        x =>
                        {
                            if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId == "new_datastream_saved")
                            {
                                MessageBox.Show($@"Datastream creation is in progress, please try after sometime!", @"Live monitoring!", MessageBoxButtons.OK);
                                hideDsLoader();
                                return;
                            }
                            refreshLayer.setUp();
                        },
                        x =>
                        {
                            Log.Error($" btnStreaming_Click: Exception caught : {x.Message}");
                            Log.Error($" btnStreaming_Click: Exception Stack Trace : {x.StackTrace}");
                            MessageBox.Show($@"{x.Message}!", @"Live monitoring!", MessageBoxButtons.OK);
                            hideDsLoader();
                            refreshLayer.setUp();
                        });

                }
                else
                {
                    if (!_selectedConfig.Configuration.StreamingDataAccess.Enabled)
                    {
                        try
                        {
                            _selectedConfig.Configuration.StreamingDataAccess.Enabled = true;
                            _selectedConfig.Enabled = true;
                            chkStreaming.Checked = true;
                            streamingOn.Show();
                            streamingOff.Hide();
                            Observable.Return(_selectedConfig)
                               .SubscribeOn(NewThreadScheduler.Default)
                               .ObserveOn(NewThreadScheduler.Default)
                               .Select(x =>
                               {
                                   refreshLayer.tearDown();
                                   Helper.FalkonryHelper.onDatastream(_selectedConfig.Configuration.FalkonryConfiguration);
                                   AfHelper.UpdateState(_selectedConfig.Configuration.AfConfiguration.SystemName, _selectedConfig.Name, "INFO", "Live monitoring is turned on!");
                                   return x;
                               })
                               .ObserveOnDispatcher()
                               .Subscribe(
                               x =>
                               {
                                   
                                   Log.Info($"Live monitoring is turned on for Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} SourceId - {_selectedConfig.Configuration.FalkonryConfiguration.SourceId}");
                                   MessageBox.Show($@"Live monitoring is turned on!", @"Live monitoring!", MessageBoxButtons.OK);
                                   hideDsLoader();
                                   refreshLayer.setUp();
                               },
                               x =>
                               {
                                   try
                                   {
                                       _selectedConfig.Configuration.StreamingDataAccess.Enabled = false;
                                       _selectedConfig.Enabled = false;
                                       chkStreaming.Checked = true;
                                       chkStreaming.Checked = false;
                                       streamingOn.Hide();
                                       streamingOff.Show();
                                       Log.Error($"Exception while Turning on live streaming for Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} SourceId - {_selectedConfig.Configuration.FalkonryConfiguration.SourceId} , Message - {x.Message}");
                                       AfHelper.UpdateState(_selectedConfig.Configuration.AfConfiguration.SystemName, _selectedConfig.Name, "ERROR", x.Message);
                                       MessageBox.Show($@"{x.Message}!", @"Live monitoring!", MessageBoxButtons.OK);
                                       hideDsLoader();
                                   }
                                   catch (Exception error)
                                   {
                                       Log.Error($"Exception in onError Turning Live Stream On {error.StackTrace}");
                                       hideDsLoader();
                                   }
                                   refreshLayer.setUp();
                               });


                        }
                        catch (Exception x)
                        {
                            Log.Error($"Exception while Turning on live streaming for Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} SourceId - {_selectedConfig.Configuration.FalkonryConfiguration.SourceId} , Message - {x.Message}");
                            AfHelper.UpdateState(_selectedConfig.Configuration.AfConfiguration.SystemName, _selectedConfig.Name, "ERROR", x.Message);
                            MessageBox.Show($@"{x.Message}!", @"Live monitoring!", MessageBoxButtons.OK);
                            hideDsLoader();
                        }

                    }
                    else
                    {
                        try
                        {
                            streamingOn.Hide();
                            streamingOff.Show();
                            Observable.Return(_selectedConfig)
                                   .SubscribeOn(NewThreadScheduler.Default)
                                   .ObserveOn(NewThreadScheduler.Default)
                                   .Select(x =>
                                   {
                                       refreshLayer.tearDown();
                                       Helper.FalkonryHelper.offDatastream(_selectedConfig.Configuration.FalkonryConfiguration);
                                       AfHelper.UpdateState(_selectedConfig.Configuration.AfConfiguration.SystemName, _selectedConfig.Name, "INFO", "Live monitoring is turned off!");
                                       return x;
                                   })
                                   .ObserveOnDispatcher()
                                   .Subscribe(
                                   x =>
                                   {
                                       Log.Info($"Live monitoring is turned off for Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} SourceId - {_selectedConfig.Configuration.FalkonryConfiguration.SourceId}");
                                       MessageBox.Show($@"Live monitoring is turned off!", @"Live monitoring!", MessageBoxButtons.OK);
                                       hideDsLoader();
                                       refreshLayer.setUp();
                                   },
                                   x =>
                                   {
                                       try
                                       {
                                           _selectedConfig.Configuration.StreamingDataAccess.Enabled = true;
                                           _selectedConfig.Enabled = true;
                                           chkStreaming.Checked = true;
                                           streamingOn.Show();
                                           streamingOff.Hide();
                                          
                                           Log.Error($"Exception while Turning off live streaming for Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} SourceId - {_selectedConfig.Configuration.FalkonryConfiguration.SourceId} , Message - {x.Message}");
                                           AfHelper.UpdateState(_selectedConfig.Configuration.AfConfiguration.SystemName, _selectedConfig.Name, "ERROR", "Error in turning off live monitoring!");
                                           MessageBox.Show($@"{x.Message}!", @"Live monitoring!", MessageBoxButtons.OK);
                                           hideDsLoader();
                                       }
                                       catch (Exception error)
                                       {
                                           Log.Error($"Exception in onError Turning Live Stream Off {error.StackTrace}");
                                           hideDsLoader();
                                       }
                                       refreshLayer.setUp();
                                   });


                        }
                        catch (Exception exception)
                        {
                            Log.Error($"Exception while Turning off live streaming for Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} SourceId - {_selectedConfig.Configuration.FalkonryConfiguration.SourceId} , Message - {exception.Message}");
                            AfHelper.UpdateState(_selectedConfig.Configuration.AfConfiguration.SystemName, _selectedConfig.Name, "ERROR", "Error in turning off live monitoring!");
                            MessageBox.Show($@"{exception.Message}!", @"Live monitoring!", MessageBoxButtons.OK);
                            hideDsLoader();
                        }
                    }

                }

               // hideDsLoader();

            }
            catch (Exception ex)
            {
                Log.Error($" btnStreaming_Click: Exception caught : {ex.Message}");
                Log.Error($" btnStreaming_Click: Exception Stack Trace : {ex.StackTrace}");
                hideDsLoader();
            }

        }

        private void saveConfig(String msgType, String piSystemName)
        {
            try
            {
                Log.Info($"saveConfig: Datastream - {_selectedConfig.Configuration.FalkonryConfiguration.Datastream}, Datastream Id - {_selectedConfig.Configuration.FalkonryConfiguration.SourceId}");
                
                if(msgType == "Save")
                    _configFromFile.ConfigurationItems.Add(_selectedConfig);
                _selectedConfig.Name = _selectedConfig.Configuration.FalkonryConfiguration.Datastream;
                _configFromFile.ConfigurationItems.All(x =>
                {
                    if (x.Id == _selectedConfig.Id)
                    {
                        x.Configuration = _selectedConfig.Configuration;
                    }
                    return true;
                });
                Tuple<bool, Exception> tuple = Helper.AfHelper.StoreConfigurationForPISystem(piSystemName, _configFromFile);
                if (tuple.Item1)
                {
                    if (msgType == "Save")
                    {


                    }
                    else if (msgType == "Update")
                    {


                    }
                    else if (msgType == "Delete")
                    {


                    }
                }
                else
                {
                    Log.Error($" Error in Storing to database.");
                    Exception error = new Exception();
                    if (tuple.Item2 != null)
                    {
                        error.Data.Add("title", @"Not Saved.");
                        error.Data.Add("msg", tuple.Item2.Message);
                    }
                    else
                    {
                        error.Data.Add("title", @"Not Saved.");
                        error.Data.Add("msg", @"Error in storing configuration of datastream.");
                    }
                   
                    throw error;

                }
            }
            catch (Exception exception)
            {
                Log.Error($"saveconfig: Exception caught, Stack Trace - {exception.StackTrace}");
                Exception error = new Exception();
                if(exception.Data!=null && exception.Data.Contains("title") && exception.Data.Contains("msg"))
                {
                    error.Data.Add("title",exception.Data["title"]);
                    error.Data.Add("msg",exception.Data["msg"]);
                }
                else
                {
                    error.Data.Add("title", @"Not Saved.");
                    error.Data.Add("msg", $"Error: {exception.Message}");
                }
                throw error;
            }

        }

        private void btnAddNewAssessment_Click(object sender, EventArgs e)
        {
            if (((PictureBox)sender).Tag != null)
                assessmentIndexInAction = (int)((PictureBox)sender).Tag;
            else
                assessmentIndexInAction = 1;

            showAssessmentLoader();
            if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId == "new_datastream_saved")
            {
                MessageBox.Show("Cannot create assessment. Datastream creation is still in progress.", "Cannot create.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                hideAssessmentLoader();
                return;
            }
            refreshLayer.tearDown();
            var nameOfAssessment = Microsoft.VisualBasic.Interaction.InputBox("Please enter the name for new Assessment.", "New Assessment Name", "");
            if (nameOfAssessment == "")
            {
                MessageBox.Show("No name was entered for the new Assessment.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                refreshLayer.setUp();
                hideAssessmentLoader();
                return;
            }

           
            createAssessmentAsync(nameOfAssessment);
            
           
        }

        private void assessmentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                enableSaveUpdate();
            }
            catch (Exception)
            {

            }
        }


        private void chkOverride_CheckedChanged(object sender, EventArgs e)
        {
            enableSaveUpdate();
            _selectedConfig.Configuration.HistoricalDataAccess.Override = true;
            _selectedConfig.Enabled = true;
            return;
        }

        private void createAssessmentAsync(String nameOfAssessment)
        {
            saving = true;
            var assessments = new System.Collections.Generic.List<Assessment>();
            Assessment newAssessment = null;
            String host = txtFalkonryHost.Text;
            String token = txtFalkonryToken.Text;
            enableSaveUpdate();
            Observable.Return(_selectedConfig)
                .SubscribeOn(NewThreadScheduler.Default)
                .ObserveOn(NewThreadScheduler.Default)
                .Select(x =>
                {
                    try
                    {   // Validating tokena and getting asssemsnet list
                        if (FalkonryHelper.AuthenticateConnectionDetails(host, token))
                        {
                            assessments = FalkonryHelper.GetAssessments(x.Configuration.FalkonryConfiguration);
                        }
                        else
                        {
                            Exception error = new Exception();
                            error.Data.Add("title", "Cannot save.");
                            error.Data.Add("msg", "Cannot create assessment. Are your Falkonry connection details correct?");
                            throw error;
                        }
                    }
                    catch (Exception excep)
                    {
                        Exception error = new Exception();
                        error.Data.Add("title", "Cannot save.");
                        error.Data.Add("msg", "Cannot create assessment. Are your Falkonry connection details correct?");
                        throw error;
                    }
                    return x;
                })
                .Select(x =>
                {
                    // Check assessment name already exists
                    var existingAssessment = assessments.Find(a => a.Name.ToLower() == nameOfAssessment.ToLower() && x.Configuration.FalkonryConfiguration.SourceId == a.Datastream);
                    if (existingAssessment != null)
                    {
                        Exception error = new Exception();
                        error.Data.Add("title", "Cannot save.");
                        error.Data.Add("msg", $"Cannot save your Assessment.Assessment with name :{nameOfAssessment} already exists");
                        throw error;
                    }
                    return x;
                })
                .Select(x =>
                {
                    // creating new assessment
                     AssessmentRequest newAssessmentRequest = new AssessmentRequest();
                    newAssessmentRequest.Name = nameOfAssessment;
                    newAssessmentRequest.Datastream = x.Configuration.FalkonryConfiguration.SourceId;
                    try
                    {
                        newAssessment = FalkonryHelper.CreateAssessment(x.Configuration.FalkonryConfiguration, newAssessmentRequest);
                        if (x.Configuration.FalkonryConfiguration.assessmentList != null)
                        {
                            x.Configuration.FalkonryConfiguration.assessmentList.Add(newAssessment);
                        }
                        Log.Info($"btnAddNewAssessment_Click: New Assessment created - {nameOfAssessment}");
                    }
                    catch (Exception exception)
                    {
                        Exception error = new Exception();
                        error.Data.Add("title", "Cannot save.");
                        error.Data.Add("msg", $"Cannot save new Assessment. Error : {exception.Message}");
                        throw error;
                    }
                    return x;
                })
                .ObserveOnDispatcher()
                .Subscribe(
                x => {
                    saving = false;
                    refreshLayer.setUp();
                    ComboBox assessmentListObj;
                    ComboboxItem ccbItem;
                    if (assessmentIndexInAction == 1)
                    {
                        assessmentListObj = assessmentList;
                    }
                    else
                    {
                        assessmentListObj = (ComboBox)assessmentBody.Controls.Find(DYNAASSESSDROPDOWN + assessmentIndexInAction, true)[0];

                    }
                    if (newAssessment != null)
                    {
                        ccbItem = new ComboboxItem { Text = nameOfAssessment, Value = newAssessment.Id };
                        assessmentListObj.Items.Add(ccbItem);
                        assessmentListObj.Text = nameOfAssessment;
                    }

                    hideAssessmentLoader();
                },
                x =>
                {
                    saving = false;
                    refreshLayer.setUp();
                    hideAssessmentLoader();
                    try
                    {
                        if (x.Data != null && x.Data.Count > 0 && x.Data.Contains("title") && x.Data.Contains("msg"))
                        {
                            Log.Error($"Save/Update failed {x.StackTrace}");
                            MessageBox.Show(x.Data["msg"].ToString(), x.Data["title"].ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            Log.Error($"Can not Save/Update Datastream {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                            MessageBox.Show($@"Cannot save your Datastream. {x.Message}!", @"Cannot save.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                       

                    }
                    catch (Exception)
                    {
                        Log.Error($"Exception in On Error of Save/Update Subscriber {_selectedConfig.Configuration.FalkonryConfiguration.Datastream} {x.StackTrace}");
                    }
                }
                );
        }

        private void btnDeleteAssessment_Click(object sender, EventArgs e)
        {

            enableSaveUpdate();
            if (((PictureBox)sender).Tag != null)
                assessmentIndexInAction = (int)((PictureBox)sender).Tag;
            else
                assessmentIndexInAction = 1;
            int currentDeletionIndex = assessmentIndexInAction;
            ComboBox assessmentDropDown = (ComboBox)assessmentBody.Controls.Find(DYNAASSESSDROPDOWN + assessmentIndexInAction, true)[0];
           
            if (assessmentDropDown.Text != null && assessmentDropDown.Text != "")
            {
                DialogResult dialogResult = MessageBox.Show($"Are you sure, you want to delete the assessment binding : {assessmentDropDown.Text} ?", "Delete Assessment Binding", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
            if ((assessmentCount == 2 && assessmentIndexInAction == 2))
            {
                assessmentBody.VerticalScroll.Value = 0;
            }
            assessmentBody.Controls.RemoveByKey(DYNAASSESSDROPDOWN + assessmentIndexInAction);
            assessmentBody.Controls.RemoveByKey(ADDNEWASSESSMENT + assessmentIndexInAction);
            assessmentBody.Controls.RemoveByKey(ATTRIBUTEOUTDROPDOWN + assessmentIndexInAction);
            assessmentBody.Controls.RemoveByKey(ADDNEWATTRIBUTE + assessmentIndexInAction);
            assessmentBody.Controls.RemoveByKey(EVENTFRAMETEMPDROPDOWN + assessmentIndexInAction);
            assessmentBody.Controls.RemoveByKey(DELETEASSESSMENT + assessmentIndexInAction);

            if (currentDeletionIndex >= 2 && currentDeletionIndex <= assessmentCount)
            {
                int nextIndex = currentDeletionIndex + 1;
                for (int i = nextIndex; i <= assessmentCount; i++)
                {
                    var assessmentCombobox = assessmentBody.Controls.Find(DYNAASSESSDROPDOWN + i, true)[0];
                    var addAssessment = assessmentBody.Controls.Find(ADDNEWASSESSMENT + i, true)[0];

                    var attributeCombobox = assessmentBody.Controls.Find(ATTRIBUTEOUTDROPDOWN + i, true)[0];
                    var addAttribute = assessmentBody.Controls.Find(ADDNEWATTRIBUTE + i, true)[0];

                    var eventFrameDropDown = assessmentBody.Controls.Find(EVENTFRAMETEMPDROPDOWN + i, true)[0];
                    var deleteRow = assessmentBody.Controls.Find(DELETEASSESSMENT + i, true)[0];

                    assessmentCombobox.Location = new System.Drawing.Point(assessmentList.Location.X, (assessmentCombobox.Location.Y - (assessmentCombobox.Height + 7)));
                    attributeCombobox.Location = new System.Drawing.Point(lstWriteBackAttribute.Location.X, (attributeCombobox.Location.Y - (attributeCombobox.Height + 7)));
                    addAssessment.Location = new System.Drawing.Point(btnCreateNewAssessment.Location.X, assessmentCombobox.Location.Y);
                    addAttribute.Location = new System.Drawing.Point(picCreateNewOutput.Location.X, addAssessment.Location.Y);
                    eventFrameDropDown.Location = new System.Drawing.Point(WhichEventFrameTemplate.Location.X, (eventFrameDropDown.Location.Y - (eventFrameDropDown.Height + 8)));
                    deleteRow.Location = new System.Drawing.Point(eventFrameDropDown.Location.X + eventFrameDropDown.Width + 5, eventFrameDropDown.Location.Y);

                    int newIndex = i - 1;
                    assessmentCombobox.Name = DYNAASSESSDROPDOWN + newIndex;
                    attributeCombobox.Name = ATTRIBUTEOUTDROPDOWN + newIndex;
                    addAssessment.Name = ADDNEWASSESSMENT + newIndex;
                    addAttribute.Name = ADDNEWATTRIBUTE + newIndex;
                    eventFrameDropDown.Name = EVENTFRAMETEMPDROPDOWN + newIndex;
                    deleteRow.Name = DELETEASSESSMENT + newIndex;
                    deleteRow.Tag = newIndex;

                }
            }
            assessmentCount--;

        }

        private void showDsLoader()
        {
            picDsLoader.Show();
        }

        private void hideDsLoader()
        {
            picDsLoader.Hide();
        }



        private void showAssessmentLoader()
        {
            if (assessmentIndexInAction == 1)
            {
                 assessmentLoader.Show();
                 picCreateNewOutput.Hide();
            }
            else
            {
                assessmentBody.Controls.Find(DELETEASSESSMENT + assessmentIndexInAction, true)[0].Visible = false;
                assessmentBody.Controls.Find(ASSESSMENTLOADER + assessmentIndexInAction, true)[0].Visible = true;
            }
            
        }

        private void hideAssessmentLoader()
        {
            if (assessmentIndexInAction == 1)
            {
                assessmentLoader.Hide();
                picCreateNewOutput.Show();
            }
            else
            {
                assessmentBody.Controls.Find(DELETEASSESSMENT + assessmentIndexInAction, true)[0].Visible = true;
                assessmentBody.Controls.Find(ASSESSMENTLOADER + assessmentIndexInAction, true)[0].Visible = false;

            }

        }
        /**
            * 
            * Set UI and other stuff for new Datastream creation.
            */
        void picAddNewDS_Click(object sender, EventArgs e)
        {
            try
            {
                txtFalkonryDatastreamLeave(null, null);
                _addingNewConfig = true;
                _creatingNewDS = true;

                //var configItem = _configFromFile.ConfigurationItems.First(c => c.Name == WhichConfiguration.Text.Split(':', ':', ':', ':')[0].Trim());
                var newName = "";
                var newGuid = Guid.NewGuid();
                var enteredName = newName;
                if (_configFromFile == null)
                {
                    _configFromFile = new Configurations();
                    _configFromFile.ConfigurationItems = new List<PiConfigurationItem>();
                }
                string host, token;
                if (_selectedConfig != null)
                {
                    host = _selectedConfig.Configuration.FalkonryConfiguration.Host;
                    token = _selectedConfig.Configuration.FalkonryConfiguration.Token;
                }
                else
                {
                    host = "https://app.falkonry.ai";
                    token = "enter your token here";
                }
                string[] AssessmentIdText = new string[] { "" };
                string[] lstWriteBackAttributeText = new string[] { "" };
                pageSize.Value = 50000;
                collectionInterval.Value = 2000;

                if (WhichPISystem.PISystems.DefaultPISystem.Databases.Count == 0)
                {

                    MessageBox.Show("No Databases available, please add a database and try again.");
                    return;
                }
                else
                {

                    if (WhichPISystem.PISystems.DefaultPISystem.Databases.DefaultDatabase == null)
                    {

                        MessageBox.Show("No Default Database selected, please select a default database and try again.");
                        return;
                    }
                }

                //WhichPISystem.PISystems.DefaultPISystem.Databases.DefaultDatabase = null;
                _configFromFile.ConfigurationItems.Add(
                    new PiConfigurationItem()
                    {
                        Configuration = new PiConfiguration()
                        {
                            AfConfiguration = new AfConfiguration
                            {
                                SystemName = WhichPISystem.PISystems.DefaultPISystem.Name,
                                DatabaseName = WhichPISystem.PISystems.DefaultPISystem.Databases.DefaultDatabase.Name,
                                TemplateName = "",
                                TemplateAttributes = new string[] { "" },
                                TemplateElementPaths = new string[] { "" },
                                EventFrameTemplateNames = new string[] { "" },
                                OutputTemplateAttribute = new string[] { "" }
                            },
                            HistoricalDataAccess = new HistoricalConfiguration
                            {
                                Enabled = false,
                                EndTime = Utils.ConvertDateTimeToMilliseconds(DateTime.Now.AddYears(1)),
                                StartTime = Utils.ConvertDateTimeToMilliseconds(DateTime.Now),
                                PageSize = 50000,
                                Override = false
                            },
                            StreamingDataAccess = new StreamingConfiguration
                            {
                                Enabled = false,
                                CollectionIntervalMs = 2000
                            },
                            FalkonryConfiguration = new FalkonryConfiguration
                            {
                                Host = host,
                                Token = token,
                                TimeFormat = "iso_8601",
                                Datastream = enteredName,
                                BatchLimit = 50000,
                                AssessmentId = AssessmentIdText,
                                AssessmentIdForFacts = AssessmentIdText,
                                    //AssessmentId = enteredName.ToArray(),
                                    //AssessmentIdForFacts = enteredName,
                                    Backfill = false,
                                SourceId = "new_datastream"
                            }
                        },
                        Id = newGuid,
                        Name = enteredName,
                        Enabled = true,
                        Deleted = false,
                        Status = "INFO",
                        ErrorMessage = "Datastream creation is in progress."
                    }
                );
                var configItem =
                _configFromFile.ConfigurationItems.First(
                    c => c.Name == enteredName);

                configItem.Configuration.HistoricalDataAccess.EndTime = Utils.ConvertDateTimeToMilliseconds(DateTime.Now.AddSeconds(1));

                _selectedConfig = configItem;


                foreach (ListViewItem item in lstDatastreams.Items)
                {
                    item.Selected = false;
                }
                //lblDatastreamName.Text = _selectedConfig.Name;

                CheckPiSystemConnection();
                // linkDatastream.Visible = false;
                //txtAssessment_Leave(null, null);
                PopulateControls();

                _addingNewConfig = false;
            }
            catch (Exception exception)
            {
                Log.Warn($"picAddNewDS_Click: Exception - { exception.Message}");
                Log.Warn($"picAddNewDS_Click: Stack Trace - { exception.StackTrace}");
                MessageBox.Show($@"{exception.Message}", "Error");
            }

        }

        void picAddNewDS_MouseHover(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
                ToolTip1.SetToolTip(picAddNewDS, "Add New Datastream");
            }
            catch (Exception exception)
            {
                Log.Warn($"Exception - { exception.Message}");
                Log.Warn($"Stack Trace - { exception.StackTrace}");
                MessageBox.Show($@"{exception.Message} ", "Error");
            }

        }

        void picAddNewAssessment_MouseHover(object sender, EventArgs e)
        {

        }

        void picDeleteAssessmentRow_MouseHover(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
                //if (((PictureBox)sender).Tag != null)
                //    assessmentIndexInAction = (int)((PictureBox)sender).Tag;
                //else
                //    assessmentIndexInAction = 1;

                //PictureBox picDeleteNewAssessmentObj;
                //if (assessmentIndexInAction == 1)
                //{
                //    //picDeleteNewAssessmentObj = this.btnCreateNewAssessment;
                //}
                //else
                //{
                //    picDeleteNewAssessmentObj = (PictureBox)panelAssessment.Controls[dynamicAssessmentStaticCount - 1 + dynamicAssessmentDeleteIndex + (assessmentIndexInAction - 2) * dynamicAssessmentElementCount];
                //    ToolTip2.SetToolTip(picDeleteNewAssessmentObj, "Delete Assessment binding");
                //}


            }
            catch (Exception exception)
            {
                Log.Warn($"Exception - { exception.Message}");
                Log.Warn($"Stack Trace - { exception.StackTrace}");
                MessageBox.Show($@"{exception.Message} ", "Error");
            }

        }

        void picAddNewOutput_MouseHover(object sender, EventArgs e)
        {

        }


        void picCreateNewOutput_Click(object sender, EventArgs e)
        {

            try
            {

                if (((PictureBox)sender).Tag != null)
                    assessmentIndexInAction = (int)((PictureBox)sender).Tag;
                else
                    assessmentIndexInAction = 1;
               if (assessmentIndexInAction == 1)
                {
                    WhichAttributeTemplates_Leave(null, null);
                }
                showAssessmentLoader();
               
                var nameOfAttribute = Microsoft.VisualBasic.Interaction.InputBox("Please enter a new name for the AF Attribute Template.", "AF Attribute Template Name", "");
                if (nameOfAttribute == "")
                {
                    MessageBox.Show("No name was entered for the AF Attribute.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideAssessmentLoader();
                    
                    return;
                }

                var attributeConfigStringName = Microsoft.VisualBasic.Interaction.InputBox("Please enter the PI Point name. \n You can use AF Substitution Parameters for the name!", "PI Point Name.", @"\\%Server%\%Element%.%Attribute%");
                if (attributeConfigStringName == "")
                {
                    MessageBox.Show("No name was entered for the PI Point.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideAssessmentLoader();
                    
                    return;
                }

                if (WhichElementTemplate.Items.Count == 0 || WhichElementTemplate.SelectedValue == null)
                {
                    MessageBox.Show("No AF Element Template is selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    hideAssessmentLoader();
                   
                    return;
                }


                ComboBox writeBackComboBox;
                if (assessmentIndexInAction == 1)
                {
                    writeBackComboBox = lstWriteBackAttribute;
                }
                else
                {
                    writeBackComboBox = (ComboBox)assessmentBody.Controls.Find(ATTRIBUTEOUTDROPDOWN + assessmentIndexInAction, true)[0];
                }

                var nameofElementTemplate = (string)WhichElementTemplate.SelectedValue;

                var db = WhichAFDatabase.AFDatabase;
                if (db == null)
                {

                    return;
                }
                //afallElements = AFElement.FindElements(WhichAFDatabase.AFDatabase, null, "*", AFSearchField.Name, true, AFSortField.Name, AFSortOrder.Ascending, int.MaxValue);
                if ((string)WhichElementTemplate.SelectedValue == "No Template" && WhichElements.CheckedItems.Count > 0)
                {
                    for (int n = 0; n < WhichElements.CheckedItems.Count; n++)
                    {
                        int it = 0;
                        for (it = 0; it < afallElements.Count; it++)
                        {
                            string elementName = ((CCBoxItem)(WhichElements.CheckedItems[n])).Name;
                            if ((afallElements[it]).Name == (elementName.Substring(elementName.LastIndexOf('\\') + 1)))
                            {
                                break;
                            }
                        }
                        if (afallElements[it].Template != null)
                            afallElements[it].Template.AllowElementToExtend = true;
                        afallElements[it].Attributes.Add(nameOfAttribute);
                        var theAttribute = afallElements[it].Attributes[nameOfAttribute];
                        if (afallElements[it].Attributes[nameOfAttribute] == null)
                        {
                            if (afallElements[it].IsDirty)
                                afallElements[it].UndoCheckOut(true);

                            MessageBox.Show("Unable to create the AF Attribute.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            hideAssessmentLoader();
                           
                            return;
                        }
                        afallElements[it].Attributes[nameOfAttribute].DataReferencePlugIn = db.PISystem.DataReferencePlugIns["PI Point"];
                        afallElements[it].Attributes[nameOfAttribute].ConfigString = $"{attributeConfigStringName};ReadOnly=False;ptclassname=classic;pointtype=String;pointsource=Falkonry-Output";
                        afallElements[it].CheckIn();
                        db.Refresh();
                        afallElements[it].Attributes[nameOfAttribute].DataReference.CreateConfig();
                    }
                }
                else
                {
                    db.ElementTemplates[nameofElementTemplate].AttributeTemplates.Add(nameOfAttribute);
                    var theAttribute = db.ElementTemplates[nameofElementTemplate].AttributeTemplates[nameOfAttribute];
                    if (theAttribute == null)
                    {
                        if (db.ElementTemplates[nameofElementTemplate].IsDirty)
                            db.ElementTemplates[nameofElementTemplate].UndoCheckOut(true);

                        MessageBox.Show("Unable to create the AF Attribute.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        hideAssessmentLoader();
                       
                        return;
                    }

                    theAttribute.DataReferencePlugIn = db.PISystem.DataReferencePlugIns["PI Point"];
                    theAttribute.ConfigString = $"{attributeConfigStringName};ReadOnly=False;ptclassname=classic;pointtype=String;pointsource=Falkonry-Output";
                    theAttribute.ElementTemplate.CheckIn();


                    db.Refresh();
                    hideAssessmentLoader();
                   
                    var foundAttributes = AFElement.FindElementsByTemplate(db, null, theAttribute.ElementTemplate, true, AFSortField.Name, AFSortOrder.Ascending, 10000).Select(elem => elem.Attributes[theAttribute.Name]);
                    showAssessmentLoader();
                   
                    if (foundAttributes.Count() == 0)
                    {
                        MessageBox.Show("Did not find any instances of the AF Attribute created, cannot create underlying PI Points.", "Unable to create PI Points.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    foundAttributes.ToList().ForEach(a => a.DataReference.CreateConfig());
                    //db.CheckIn();
                }

                MessageBox.Show("AF attributes created for Assessments on all selected Entities", "AF Attributes created.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                hideAssessmentLoader();
                
                db.CheckIn();

                int i = WhichAttributeTemplates.Items.Count;
                CCBoxItem item = new CCBoxItem(nameOfAttribute, i + 1);
                WhichAttributeTemplates.Items.Add(item);
                i = 0;
                writeBackComboBox.Items.Clear();

                foreach (CCBoxItem avialableAttribute in WhichAttributeTemplates.Items)
                {
                    if (!WhichAttributeTemplates.CheckedItems.Contains(avialableAttribute) && avialableAttribute.Name != "Select All")
                    {
                        writeBackComboBox.Items.Add(avialableAttribute.Name);
                    }
                }
                if (writeBackComboBox.Items.Contains(nameOfAttribute)) writeBackComboBox.Text = nameOfAttribute;
                if (writeBackComboBox.Items.Count == 0)
                {
                    writeBackComboBox.Items.Add("");
                }
                hideAssessmentLoader();
                
            }
            catch (Exception exception)
            {
                hideAssessmentLoader();
               
                MessageBox.Show($"Something went wrong creating the AF Attributes. Error: {exception.Message}", "Error creating AF Attributes.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                try
                {
                    if (WhichAFDatabase.AFDatabase.IsDirty) WhichAFDatabase.AFDatabase.UndoCheckOut(true);
                }
                catch (Exception)
                {

                }
            }
        }

        void picCreateNewAssessment_Click(object sender, EventArgs e)
        {

        }

        DialogResult ShowInputDialog(ref string input, ref string description)
        {
            System.Drawing.Size size = new System.Drawing.Size(300, 180);
            Form inputBox = new Form();
            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = "Report Bug";
            inputBox.StartPosition = FormStartPosition.CenterParent;
            inputBox.ControlBox = false;
            inputBox.MinimizeBox = false;
            inputBox.MaximizeBox = false;

            System.Windows.Forms.Label emailLabel = new System.Windows.Forms.Label();
            emailLabel.Size = new System.Drawing.Size(48, 23);
            emailLabel.Location = new System.Drawing.Point(5, 10);
            emailLabel.Text = "Email";
            inputBox.Controls.Add(emailLabel);

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 80, 23);
            textBox.Location = new System.Drawing.Point(65, 10);
            textBox.Text = input;
            inputBox.Controls.Add(textBox);

            System.Windows.Forms.Label descriptionLabel = new System.Windows.Forms.Label();
            descriptionLabel.Size = new System.Drawing.Size(60, 23);
            descriptionLabel.Location = new System.Drawing.Point(5, 40);
            descriptionLabel.Text = "Description";
            inputBox.Controls.Add(descriptionLabel);

            System.Windows.Forms.TextBox descriptionBox = new TextBox();
            descriptionBox.Size = new System.Drawing.Size(size.Width - 80, 100);
            descriptionBox.Location = new System.Drawing.Point(65, 40);
            descriptionBox.Multiline = true;
            //descriptionBox.MinimumSize = new System.Drawing.Size(size.Width - 70, 100);
            descriptionBox.Text = description;
            inputBox.Controls.Add(descriptionBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 120 - 80, 150);
            //okButton.Click += new System.EventHandler();
            inputBox.Controls.Add(okButton);
            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 120, 150);
            inputBox.Controls.Add(cancelButton);
            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;
            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            description = descriptionBox.Text;
            return result;
        }
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        bool getAccountDetails(ref string account, ref string body)
        {
            try
            {
                Log.Debug("Inside getAccountDetails");
                string email = "Your Email", description = "";
                DialogResult dialogResult = ShowInputDialog(ref email, ref description);
                if (dialogResult == DialogResult.OK)
                {
                    if (IsValidEmail(email))
                    {
                        body += $"Email Id: {email} <br/>";
                        body += $"Description: {description} <br/>";
                        account = email;
                    }
                    else
                    {
                        Log.Debug($"getAccountDetails: Invalid Email id - {email}");
                        MessageBox.Show($"Please enter valid email id.", "Invalid Email", MessageBoxButtons.OK);
                        getAccountDetails(ref account, ref body);
                        return false;
                    }
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    Log.Debug("getAccountDetails: Canceled.");
                    return false;
                }
                Log.Debug("End getAccountDetails");
                return true;
            }
            catch (Exception exception)
            {
                Log.Warn($"Exception caught in getAccountDetails- {exception.Message}");
                Log.Warn($"Exception caught in getAccountDetails, Stack Trace- {exception.StackTrace}");
                MessageBox.Show($"{exception.Message}", "Error in Sending logs", MessageBoxButtons.OK);
                return false;
            }
        }

        void btnExportLogs_Click(object sender, EventArgs e)
        {
            try
            {
                Log.Info(" Inside btnExportLogs_Click ");
                string body = "";
                string account = "";
                if (lstDatastreams.Items.Count > 0 && _selectedConfig != null)
                {
                    try
                    {
                        if (_selectedConfig.Configuration.FalkonryConfiguration.SourceId.ToLower().Equals("new_datastream".ToLower()))
                        {
                            List<PiConfigurationItem> items = _configFromFile.ConfigurationItems.Where(x => !x.Configuration.FalkonryConfiguration.SourceId.ToLower().Equals("new_datastream".ToLower())).ToList();
                            if (items.Count > 0)
                            {
                                var response = FalkonryHelper.GetDatastream(items.ElementAt(0).Configuration.FalkonryConfiguration);
                                body += $"User Id: {response.CreatedBy} <br/>";
                                body += $"Account Id: {response.Tenant} <br/>";
                                account = response.Tenant;
                            }
                        }
                        else
                        {
                            var response = FalkonryHelper.GetDatastream(_selectedConfig.Configuration.FalkonryConfiguration);
                            body += $"User Id: {response.CreatedBy} <br/>";
                            body += $"Account Id: {response.Tenant} <br/>";
                            account = response.Tenant;
                        }
                       
                    }
                    catch (Exception)
                    {

                    }
                    body += $"Host: {_selectedConfig.Configuration.FalkonryConfiguration.Host} <br/>";
                    
                }
                
                if (!getAccountDetails(ref account, ref body))
                {
                   
                    return;
                }
                EmailService.sendMail(account, body);
              
            }
            catch (Exception exception)
            {
                Log.Warn($"Exception caught in btnExportLogs_Click- {exception.Message}");
                Log.Warn($"Exception caught in btnExportLogs_Click, Stack Trace- {exception.StackTrace}");
                MessageBox.Show($"{exception.Message}", "Error in exporting logs", MessageBoxButtons.OK);
            }
        }

        void batchIdentifier_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!saving)
                {
                    if (((ComboBox)sender).SelectedIndex != 0)
                    {
                        CCBoxItem selectedItem = (CCBoxItem)((ComboBox)sender).SelectedItem;


                        WhichAttributeTemplates.Items.Remove(selectedItem);
                        foreach (object item in ((ComboBox)sender).Items)
                        {
                            CCBoxItem batchItem = (CCBoxItem)item;
                            if (!batchItem.Name.Equals(selectedItem.Name) && batchItem.Value != 0)
                            {
                                bool present = false;
                                foreach (object attrObj in WhichAttributeTemplates.Items)
                                {
                                    CCBoxItem attrItem = (CCBoxItem)attrObj;
                                    if (batchItem.Name.Equals(attrItem.Name))
                                    {
                                        present = true;
                                        break;
                                    }
                                    else
                                    {
                                        present = false;
                                    }
                                }
                                if (!present)
                                    WhichAttributeTemplates.Items.Insert(batchItem.Value, batchItem);
                            }

                        }
                        WhichAttributeTemplates.Text = "";
                        foreach (object attrObj in WhichAttributeTemplates.CheckedItems)
                        {
                            CCBoxItem attrItem = (CCBoxItem)attrObj;
                            if (attrItem.Value != 0)
                            {
                                if (WhichAttributeTemplates.Text.Equals(""))
                                {
                                    WhichAttributeTemplates.Text = attrItem.Name;
                                }
                                else
                                {
                                    WhichAttributeTemplates.Text = WhichAttributeTemplates.Text + "," + attrItem.Name;
                                }
                            }


                        }
                        if (selectedItem.Value != 0 && _selectedConfig != null)
                        {
                            _selectedConfig.BatchIdentifier = selectedItem.Name;
                        }
                    }
                    else
                    {
                        // Add CCBoxItem which is not present in attribute item list
                        CCBoxItem selectedItem = (CCBoxItem)((ComboBox)sender).SelectedItem;
                        foreach (object item in ((ComboBox)sender).Items)
                        {
                            CCBoxItem batchItem = (CCBoxItem)item;
                            if (batchItem.Value != 0)
                            {
                                bool present = false;
                                foreach (object attrObj in WhichAttributeTemplates.Items)
                                {
                                    CCBoxItem attrItem = (CCBoxItem)attrObj;
                                    if (batchItem.Name.Equals(attrItem.Name))
                                    {
                                        present = true;
                                        break;
                                    }
                                    else
                                    {
                                        present = false;
                                    }
                                }
                                if (!present)
                                    WhichAttributeTemplates.Items.Insert(batchItem.Value, batchItem);
                            }
                        }
                        WhichAttributeTemplates.Text = "";
                        foreach (object attrObj in WhichAttributeTemplates.CheckedItems)
                        {
                            CCBoxItem attrItem = (CCBoxItem)attrObj;
                            if (attrItem.Value != 0)
                            {
                                if (WhichAttributeTemplates.Text.Equals(""))
                                {
                                    WhichAttributeTemplates.Text = attrItem.Name;
                                }
                                else
                                {
                                    WhichAttributeTemplates.Text = WhichAttributeTemplates.Text + "," + attrItem.Name;
                                }
                            }


                        }
                        if (selectedItem.Value == 0 && _selectedConfig != null)
                        {
                            // _selectedConfig.BatchIdentifier = selectedItem.Name;
                            _selectedConfig.BatchIdentifier = "";
                        }
                    }

                }

            }
            catch (Exception error)
            {

            }
        }


        void linkDatastream_Click(object sender, EventArgs e)
        {
            Observable.Return<string>(txtFalkonryDatastream.Text)
                    .Select(x =>
                    {
                        Datastream datastream = null;
                        try
                        {
                            datastream = FalkonryHelper.GetDatastreams(_selectedConfig.Configuration.FalkonryConfiguration).FirstOrDefault(d => d.Name == x);

                        }
                        catch (Exception error)
                        {
                            Log.Error($"Get Datastream call failed on click of Link {error.StackTrace}");
                        }
                        return datastream;
                    })
                    .SubscribeOn(NewThreadScheduler.Default)
                    .ObserveOn(DispatcherScheduler.Current)
                    .Subscribe(
                        x => {
                            if (x != null)
                            {
                                var link = $"{txtFalkonryHost.Text}/#/account/{x.Tenant}/datastream/{x.Id}";
                                System.Diagnostics.Process.Start(link);
                            }

                        },
                        x => {
                        });
        }

        void WhichPISystem_Load(object sender, EventArgs e)
        {

        }

        void WhichAFDatabase_Load(object sender, EventArgs e)
        {

        }

        void lblAttributeReadOnly_Click(object sender, EventArgs e)
        {
            AttributeDialog dialog = new AttributeDialog(this, lblAttributeReadOnly);
            dialog.Show();
        }

        void configPanel_Paint(object sender, PaintEventArgs e)
        {
            Control p = (Control)sender;
            Color[] shadow = new Color[3];
            shadow[0] = Color.FromArgb(181, 181, 181);
            shadow[1] = Color.FromArgb(195, 195, 195);
            shadow[2] = Color.FromArgb(211, 211, 211);
            Pen pen = new Pen(shadow[0]);
            using (pen)
            {
                Point pt = p.Location;
                pt.Y += p.Height;
                for (var sp = 0; sp < 3; sp++)
                {
                    pen.Color = shadow[sp];
                    //e.Graphics.DrawLine(pen, pt.X, pt.Y, pt.X + p.Width - 1, pt.Y);
                    e.Graphics.DrawLine(pen, pt.X + sp, pt.Y, pt.X + p.Width - 1 + sp, pt.Y);
                    e.Graphics.DrawLine(pen, p.Right + sp, p.Top + sp, p.Right + sp, p.Bottom + sp);
                    pt.Y++;
                }
            }
        }

        void raisedBuddonShadow(object sender, PaintEventArgs e)
        {
            Control p = (Control)sender;
            Color[] shadow = new Color[3];
            shadow[0] = Color.FromArgb(150, 150, 150);
            shadow[1] = Color.FromArgb(181, 181, 181);
            shadow[2] = Color.FromArgb(195, 195, 195);
            //shadow[3] = Color.FromArgb(211, 211, 211);
            //shadow[4] = Color.FromArgb(220, 220, 220);
            //shadow[5] = Color.FromArgb(230, 230, 230);
            Pen pen = new Pen(shadow[0]);
            using (pen)
            {
                Point pt = p.Location;
                pt.Y += p.Height;
                for (var sp = 0; sp < 3; sp++)
                {
                    pen.Color = shadow[sp];
                    e.Graphics.DrawLine(pen, pt.X + sp, pt.Y, pt.X + p.Width - 1 + sp, pt.Y);
                    e.Graphics.DrawLine(pen, p.Right + sp, p.Top + sp, p.Right + sp, p.Bottom + sp);
                    pt.Y++;
                }
            }
        }

        void batchIdentifier_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void dsName_Click(object sender, EventArgs e)
        {

        }

        void label11_Click(object sender, EventArgs e)
        {

        }

        void mainPanel_Paint(object sender, PaintEventArgs e)
        {

            Panel panel = (Panel)sender;
            Color[] shadow = new Color[3];
            shadow[0] = Color.FromArgb(181, 181, 181);
            shadow[1] = Color.FromArgb(195, 195, 195);
            shadow[2] = Color.FromArgb(211, 211, 211);
            Pen pen = new Pen(shadow[0]);
            IEnumerable<Panel> panels = panel.Controls.OfType<Panel>().Where(x => {
                if (!x.Visible)
                {
                    return false;
                }
                else
                {
                    // draw shadow
                    return true;
                }

            });
            using (pen)
            {
                foreach (Panel p in panels)
                {
                    Point pt = p.Location;
                    pt.Y += p.Height;
                    for (var sp = 0; sp < 3; sp++)
                    {
                        pen.Color = shadow[sp];
                        e.Graphics.DrawLine(pen, pt.X + sp, pt.Y, pt.X + p.Width - 1 + sp, pt.Y);
                        e.Graphics.DrawLine(pen, p.Right + sp, p.Top + sp, p.Right + sp, p.Bottom + sp);
                        pt.Y++;
                    }
                }
            }

        }

        void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }


        void pictureBox5_Click(object sender, EventArgs e)
        {
            AssessmentForm form2 = new AssessmentForm(this, _selectedConfig);
            form2.ShowDialog();
        }

        void downAssessment_Click(object sender, EventArgs e)
        {

        }

        void dsName_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(dsName, _selectedConfig.Name);
        }

        void downconfiguration_Click(object sender, EventArgs e)
        {


            if (test == true)
            {
                tableLayoutPanel1.Hide();
                this.downconfiguration.Image = Falkonry.Integrator.PISystem.Configurator.Resources.ic_up;
                test = false;
            }
            else
            {
                tableLayoutPanel1.Show();
                this.downconfiguration.Image = Falkonry.Integrator.PISystem.Configurator.Resources.ic_down;
                test = true;
            }

        }

        void picRight_Click(object sender, EventArgs e)
        {
            btnSave_Click(null, null);
        }

        void picWrong_Click(object sender, EventArgs e)
        {
            btnCancel_click(null, null);
        }

        void falkonryButtonPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        void pictureBox1_Click(object sender, EventArgs e)
        {

        }



        private void pictureBox6_Click(object sender, EventArgs e)
        {
            lblAutoUpdSettings_Click(null, null);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void lblElementReadOnly_Click(object sender, EventArgs e)
        {
            ElementDialog dialog = new ElementDialog(this,lblElementReadOnly);
            dialog.Show();
        }

        private void pictureHelpIcon_Click_1(object sender, EventArgs e)
        {
            helpLinkClicked(sender, null);
        }

        private void ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Log.Info($"Entering form closing");
            try
            {
                subscriptions.clearAllSubscriptions();
                observableLayer.tearDown();
                refreshLayer.tearDown();
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    // Console app
                    System.Environment.Exit(0);
                }
            }
            catch(Exception error)
            {
                Log.Fatal($"Exception in form closing Exception Message: {error.Message} \n Exception Stacktrace: {error.StackTrace}");
            }
            Log.Info($"Leaving form closing");
        }

        private void ConfigurationForm_SizeChanged(object sender, EventArgs e)
        {
            if (lstDatastreams.Items.Count > 0)
            {
                 mainPanel.Visible = false;
            mainPanel.Visible = true;
            }
           
        }





        //private void btnBackFillHistory_click(object sender, EventArgs e)
        //{

        //}
    }
}