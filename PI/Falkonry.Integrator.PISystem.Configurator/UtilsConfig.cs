﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Falkonry.Integrator.PISystem.Configurator
{
    class UtilsConfig
    {
        public static bool checkWritePermission()
        {
            
            string appFileName = Environment.GetCommandLineArgs()[0];
            string appPath = Path.GetDirectoryName(appFileName);
            string falkonryPath = Path.GetFullPath(Path.Combine(appPath, @"..\..\"));
            falkonryPath = falkonryPath.TrimEnd(new[] { '\\' });
            AuthorizationRuleCollection rules;
            bool isWriteAccess = true;
            try
            {
                try
                {
                    rules = Directory.GetAccessControl(falkonryPath)
                        .GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
                }
                catch (Exception ex)
                { //Possible UnauthorizedAccessException
                    throw new Exception("Error writing to folder " + falkonryPath + ". Verify that you have access to folder", ex);
                }

                var rulesCast = rules.Cast<FileSystemAccessRule>();
                if (rulesCast.Any(rule => rule.AccessControlType == AccessControlType.Deny)
                    || !rulesCast.Any(rule => rule.AccessControlType == AccessControlType.Allow))
                {
                    isWriteAccess = false;
                    throw new Exception("Error writing to folder " + falkonryPath + ". Verify that you have access to folder");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}", "Access Denied!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return isWriteAccess;
        }
    }
}
