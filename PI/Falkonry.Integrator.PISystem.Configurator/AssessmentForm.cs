﻿using CheckComboBoxTest;
using Falkonry.Integrator.PISystem.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Falkonry.Integrator.PISystem.Configurator
{
    public partial class AssessmentForm : Form
    {
        private ConfigurationForm form_parent;
        private PiConfigurationItem _selectedConfigs;
        Hashtable assessmentMap = new Hashtable();

        public AssessmentForm(ConfigurationForm form, PiConfigurationItem _selectedConfig)
        {
            InitializeComponent();
            form_parent = form;
            _selectedConfigs = _selectedConfig;
          
        }

        //private void updateOutputAttributeTemplate()
        //{
        //    comboBox2.Items.Clear();
        //    comboBox2.Sorted = true;
        //    foreach (CCBoxItem avialableAttribute in WhichAttributeTemplates.Items)
        //    {
        //        if (!WhichAttributeTemplates.CheckedItems.Contains(avialableAttribute) && avialableAttribute.Name != "Select All")
        //        {
        //            lstWriteBackAttribute.Items.Add(avialableAttribute.Name);
        //        }
        //    }
        //    if (lstWriteBackAttribute.Items.Count == 0)
        //    {
        //        lstWriteBackAttribute.Items.Add("");
        //    }
        //    List<string> outputList = new List<string>(piConfiguration.AfConfiguration.OutputTemplateAttribute);
        //    int index = 0;
        //    foreach (string AssessmentId in piConfiguration.FalkonryConfiguration.AssessmentId)
        //    {
        //        if (assessmentMap != null && (AssessmentId == "" || assessmentMap[AssessmentId] == null))
        //        {
        //            //AfHelper.UpdateState(config.Configuration.AfConfiguration.SystemName, config.Name, "INFO", $"Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
        //            index = AssessmentIdList.IndexOf(AssessmentId);
        //            AssessmentIdList.RemoveAt(index);
        //            outputList.RemoveAt(index);
                    
        //           // Log.Info($"PopulateControl: Assessment binding deleted for {AssessmentId} as it is not found in datastream.");
        //        }
        //    }
        //    piConfiguration.AfConfiguration.OutputTemplateAttribute = outputList.ToArray();

        //    if (assessmentMap != null)
        //    {
        //        for (i = 0; i < piConfiguration.FalkonryConfiguration.AssessmentId.Count(); i++)
        //        {
        //            var ccbItem = new { Text = assessmentMap[piConfiguration.FalkonryConfiguration.AssessmentId[i]].ToString(), Value = piConfiguration.FalkonryConfiguration.AssessmentId[i] };

        //            if (i == 0)
        //            {
        //                if (lstWriteBackAttribute.Items.Contains(piConfiguration.AfConfiguration.OutputTemplateAttribute[0])) lstWriteBackAttribute.Text = piConfiguration.AfConfiguration.OutputTemplateAttribute[0];

        //                if (piConfiguration.FalkonryConfiguration.AssessmentId[i] != "" && !assessmentList.Items.Contains(ccbItem))
        //                {
        //                    assessmentList.Items.Add(ccbItem);
        //                }
        //                assessmentList.Show();
        //                btnCreateNewAssessment.Show();
        //                txtAssessmentIdOutput.Hide();
        //                // assessmentList.Valu = piConfiguration.FalkonryConfiguration.AssessmentId[i];
        //                assessmentList.Text = assessmentMap[piConfiguration.FalkonryConfiguration.AssessmentId[i]].ToString();
        //            }
        //        }
        //    }
        //}
        private void updateAssessmentList(string datastreamId)
        {
            try
            {
                IDictionary<string, string> combo = new Dictionary<string, string>();
                combo.Clear();
                if (assessmentMap == null)
                    assessmentMap = new Hashtable();
                assessmentMap.Clear();

                //List<Assessment> assessments = FalkonryHelper.GetAssessments(_selectedConfigs.Configuration.FalkonryConfiguration);
                //foreach (Assessment assessmentObj in assessments)
                //{
                //    if (assessmentObj.Datastream.Equals(datastreamId))
                //    {
                //        combo.Add(assessmentObj.Name, assessmentObj.Id);

                //    }
                //}

                selectedAssessment.DataSource = new BindingSource(combo, null);
                selectedAssessment.DisplayMember = "Key";
                selectedAssessment.ValueMember = "Value";


            }
            catch (Exception e)
            {
               // Log.Error($"updateAssessmentList: Exception caught {e.Message}");
               // Log.Error($"updateAssessmentList: Exception caught, Stack Trace {e.StackTrace}");
                assessmentMap = null;
            }
        }

     
        private void addAssessmentcombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void AssessmentForm_Load(object sender, EventArgs e)
        {

            updateAssessmentList(_selectedConfigs.Configuration.FalkonryConfiguration.SourceId);
            //selectedAssessment.Items.Add("satuesada");
            //selectedAssessment.Items.Add("ds");
            //selectedAssessment.Items.Add("tre");
         
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
       
        private void selectedAssessment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
             
            }
            catch (Exception)
            {

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }

        private void addAssessment_Click(object sender, EventArgs e)
        {

        }

        //private void WhichEventFrameTemplate_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    string eventFrameText = "";
        //    int i = 0;
        //    for (var iEt = 0; iEt < _selectedConfigs.Configuration.AfConfiguration.EventFrameTemplateNames.Count(); iEt++)
        //    {
        //        eventFrameText += _selectedConfigs.Configuration.AfConfiguration.EventFrameTemplateNames[iEt];
        //        if (i + 1 != _selectedConfigs.Configuration.AfConfiguration.EventFrameTemplateNames.Count() && !eventFrameText.Equals(""))
        //        {
        //            eventFrameText += ", ";
        //        }
        //    }
        //    if (eventFrameText != WhichEventFrameTemplate.Text)
        //        form_parent.enableSaveUpdate();

        //}

        //private void WhichEventFrameTemplate_ItemCheck(object sender, ItemCheckEventArgs e)
        //{
        //    try
        //    {
        //        int i = 0;
        //        if (((CheckedListBox)sender).SelectedItem != null && ((CCBoxItem)((CheckedListBox)sender).SelectedItem).Name == "Select All")
        //        {

        //            bool isCheck = true;
        //            if (e.Index == 0)
        //            {
        //                if (e.NewValue.ToString() == "Unchecked")
        //                {
        //                    isCheck = false;
        //                }
        //                form_parent.enableSaveUpdate();
        //                if (assessmentIndexInAction == 1)
        //                {
        //                    WhichEventFrameTemplate.Text = "";
        //                    for (i = 1; i < WhichEventFrameTemplate.Items.Count; i++)
        //                    {
        //                        WhichEventFrameTemplate.SetItemChecked(i, isCheck);
        //                    }
        //                }
        //                else
        //                {
        //                    CheckedComboBox eventFaremCcb = (CheckedComboBox)(panel3.Controls[dynamicAssessmentStaticCount - 1 + dynamicAssessmentEventIndex + (assessmentIndexInAction - 2) * dynamicAssessmentElementCount]);
        //                    panel3.Controls[dynamicAssessmentStaticCount - 1 + dynamicAssessmentEventIndex + (assessmentIndexInAction - 2) * dynamicAssessmentElementCount].Text = "";
        //                    for (i = 1; i < eventFaremCcb.Items.Count; i++)
        //                    {
        //                        eventFaremCcb.SetItemChecked(i, isCheck);
        //                    }
        //                }

        //            }
        //        }
        //        else
        //        {
        //            CheckedComboBox eventFaremCcb;
        //            if (assessmentIndexInAction == 1)
        //            {
        //                eventFaremCcb = WhichEventFrameTemplate;
        //            }
        //            else
        //            {
        //                eventFaremCcb = (CheckedComboBox)(panel3.Controls[dynamicAssessmentStaticCount - 1 + dynamicAssessmentEventIndex + (assessmentIndexInAction - 2) * dynamicAssessmentElementCount]);
        //            }
        //            if (e.Index > 0 && eventFaremCcb.Items.Count > 0 && eventFaremCcb.GetItemChecked(0))
        //            {
        //                eventFaremCcb.SetItemChecked(0, false);
        //            }
        //        }

        //        form_parent.enableSaveUpdate();
        //    }
        //    catch (Exception)
        //    {

        //        return;
        //    }
        //}

        //private void WhichEventFrameTemplate_Click(object sender, EventArgs e)
        //{
        //    if (((CheckedComboBox)sender).Tag != null)
        //        assessmentIndexInAction = (int)((CheckedComboBox)sender).Tag;
        //    else
        //        assessmentIndexInAction = 1;
        //}
    }


}
