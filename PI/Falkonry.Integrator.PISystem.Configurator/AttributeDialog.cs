﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Falkonry.Integrator.PISystem.Configurator
{
    public partial class AttributeDialog : Form
    {
        public ConfigurationForm form_parent;
        public Label Attribute;

        public AttributeDialog(ConfigurationForm form, Label label)
        {
            InitializeComponent();
            form_parent = form;
            Attribute = label;
          
        }

        private void AttributeDialog_Load(object sender, EventArgs e)
        {
           
           
            textBox1.Text = Attribute.Text;
        }
    }
}
