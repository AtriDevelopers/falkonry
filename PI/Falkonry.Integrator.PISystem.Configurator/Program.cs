﻿using System;
using log4net;
using System.Windows.Forms;
using Falkonry.Integrator.PISystem.Helper;

namespace Falkonry.Integrator.PISystem.Configurator
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);


        [STAThread]
        private static void Main()
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();

                Log.Info($" =========== Starting up configurator. =========== ");
                try
                {
                    Log.Info($" Windows Operating system: {Environment.OSVersion.ToString()} ");
                    Log.Info($" Falkonry Agent Version: {Application.ProductVersion} ");
                    Log.Info($".Net version installed on the machine is : {Utils.Get45or451FromRegistry()}");
                    Log.Info($" Machine Name: {Environment.MachineName.ToString()} ");
                    Log.Info($" Timezone: {TimeZoneInfo.Local.StandardName} ");
                    Log.Info($"Is User administrator: {Utils.IsUserAdministrator()}");
                }
                catch (Exception exception)
                {
                    Log.Error($"Error in fetching machine details : " + exception);
                }

                try
                {
                    if (!UtilsConfig.checkWritePermission())
                    {
                        return;
                    }

                    if (Utils.CheckAgentUpdate(Application.ProductVersion))
                    {
                        DialogResult dialogResult = MessageBox.Show($"A new version of PI Agent is available, do you want to upgrade the PI Agent?", "Update Available", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            Log.Debug("YES, User approved installation of latest PI Agent");
                            Utils.InstallLatestAgent();
                            return;
                        }
                        else
                        {
                            Log.Debug("NO, User denied upgrade to latest PI Agent");
                        }
                    }
                }
                catch (Exception exception)
                {
                    Log.Error($"Error in Checking update for Falkonry Agent  : " + exception.Message);
                }
                Utils.DeleteAgentMsi();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new ConfigurationForm());
                System.Environment.Exit(0);
                Log.Info($" =========== Stopping the configurator  ===========");
            }
            catch (Exception exception)
            {
                Log.Debug($"Exception caught in Configurator Main - {exception.Message}");
                Log.Debug($"Exception caught in Configurator Main, Stack Trace - {exception.StackTrace}");
                DialogResult dialogResult = MessageBox.Show($"Oops! There seems to have been a problem! Don't worry. We will try our best to keep your datastreams warm and cooking! Would you like to report this incident to Falkonry ? ", "Error", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    EmailService.sendMail();
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }
    }
}
