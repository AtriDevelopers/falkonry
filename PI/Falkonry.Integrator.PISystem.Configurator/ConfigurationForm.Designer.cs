﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace Falkonry.Integrator.PISystem.Configurator
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
            this.openConfigFile = new System.Windows.Forms.OpenFileDialog();
            this.lblLogo = new System.Windows.Forms.Label();
            this.lstDatastreams = new System.Windows.Forms.ListView();
            this.lblDatastreams = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.errorBox = new System.Windows.Forms.RichTextBox();
            this.chkStreaming = new System.Windows.Forms.CheckBox();
            this.panelAssessment = new System.Windows.Forms.Panel();
            this.assessmentOrangeStrip = new System.Windows.Forms.Panel();
            this.assessmentDescriptionStrip = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.assessmentBody = new System.Windows.Forms.Panel();
            this.assessmentLoader = new System.Windows.Forms.PictureBox();
            this.btnBackfillHistory = new System.Windows.Forms.Button();
            this.btnAddNewAssessmentRow = new System.Windows.Forms.Button();
            this.newAssessmentLoader = new System.Windows.Forms.PictureBox();
            this.txtAssessmentIdOutput = new System.Windows.Forms.TextBox();
            this.picCreateNewOutput = new System.Windows.Forms.PictureBox();
            this.assessmentLabel = new System.Windows.Forms.Label();
            this.eventFrameFactLabel = new System.Windows.Forms.Label();
            this.WhichEventFrameTemplate = new CheckComboBoxTest.CheckedComboBox();
            this.btnCreateNewAssessment = new System.Windows.Forms.PictureBox();
            this.lstWriteBackAttribute = new System.Windows.Forms.ComboBox();
            this.assessmentList = new System.Windows.Forms.ComboBox();
            this.attributeResultLabel = new System.Windows.Forms.Label();
            this.downHistory = new System.Windows.Forms.PictureBox();
            this.assessmentBindingTitle = new System.Windows.Forms.Label();
            this.chkOverride = new System.Windows.Forms.CheckBox();
            this.collectionInterval = new System.Windows.Forms.NumericUpDown();
            this.lblCollectionInterval = new System.Windows.Forms.Label();
            this.lblAutoUpdSetting = new System.Windows.Forms.LinkLabel();
            this.pageSize = new System.Windows.Forms.NumericUpDown();
            this.pageSizeLabel = new System.Windows.Forms.Label();
            this.StartDatePicker = new System.Windows.Forms.DateTimePicker();
            this.EndDatePicker = new System.Windows.Forms.DateTimePicker();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.configSectionPanel = new System.Windows.Forms.Panel();
            this.downconfiguration = new System.Windows.Forms.PictureBox();
            this.configSectionOrangeStrip = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.elementsLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAttributeTemplates = new System.Windows.Forms.Label();
            this.lblToken = new System.Windows.Forms.Label();
            this.lblElementTemplates = new System.Windows.Forms.Label();
            this.lblHostName = new System.Windows.Forms.Label();
            this.lblSelectConfiguration = new System.Windows.Forms.Label();
            this.lblAFDatabase = new System.Windows.Forms.Label();
            this.elementsPanel = new System.Windows.Forms.Panel();
            this.WhichElements = new CheckComboBoxTest.CheckedComboBox();
            this.lblElementReadOnly = new System.Windows.Forms.Label();
            this.templatePanel = new System.Windows.Forms.Panel();
            this.lblTemplateReadOnly = new System.Windows.Forms.Label();
            this.WhichElementTemplate = new System.Windows.Forms.ComboBox();
            this.attributePanel = new System.Windows.Forms.Panel();
            this.lblAttributeReadOnly = new System.Windows.Forms.Label();
            this.WhichAttributeTemplates = new CheckComboBoxTest.CheckedComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.afDatabasePanel = new System.Windows.Forms.Panel();
            this.lblAfDbReadOnly = new System.Windows.Forms.Label();
            this.WhichAFDatabase = new OSIsoft.AF.UI.AFDatabasePicker();
            this.piSystemPanel = new System.Windows.Forms.Panel();
            this.WhichPISystem = new OSIsoft.AF.UI.PISystemPicker();
            this.lblPiReadOnly = new System.Windows.Forms.Label();
            this.txtFalkonryTokenPanel = new System.Windows.Forms.Panel();
            this.tokenlabel = new System.Windows.Forms.Label();
            this.txtFalkonryToken = new System.Windows.Forms.TextBox();
            this.txtFalkonryHostPanel = new System.Windows.Forms.Panel();
            this.hostlabel = new System.Windows.Forms.Label();
            this.txtFalkonryHost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.batchIdentifierTag = new System.Windows.Forms.Label();
            this.batchIdentifierPanel = new System.Windows.Forms.Panel();
            this.batchIdentifierLabel = new System.Windows.Forms.Label();
            this.batchIdentifier = new System.Windows.Forms.ComboBox();
            this.configInformationStrip = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.txtFalkonryDatastream = new System.Windows.Forms.TextBox();
            this.dsName = new System.Windows.Forms.Label();
            this.picWrong = new System.Windows.Forms.PictureBox();
            this.picDsLoader = new System.Windows.Forms.PictureBox();
            this.linkDatastream = new System.Windows.Forms.PictureBox();
            this.btnDelete = new System.Windows.Forms.PictureBox();
            this.livestreamStatusPanel = new System.Windows.Forms.Panel();
            this.btnStreaming = new System.Windows.Forms.Label();
            this.streamingOff = new System.Windows.Forms.PictureBox();
            this.streamingOn = new System.Windows.Forms.PictureBox();
            this.picExportLogsLoader = new System.Windows.Forms.PictureBox();
            this.piConnectionPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.piConnected = new System.Windows.Forms.PictureBox();
            this.piDisconnected = new System.Windows.Forms.PictureBox();
            this.historyCardPanel = new System.Windows.Forms.Panel();
            this.historyBodyPanel = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.historyWindowDescriptionStrip = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.HistoryWindowLable = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.historicalOrgangeStrip = new System.Windows.Forms.Panel();
            this.falkonryButtonPanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.falkonryDisconnected = new System.Windows.Forms.PictureBox();
            this.falkonryConnected = new System.Windows.Forms.PictureBox();
            this.picRight = new System.Windows.Forms.PictureBox();
            this.lblNoDatastreams = new System.Windows.Forms.Label();
            this.labelFooter = new System.Windows.Forms.Label();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.messageSidePanel = new System.Windows.Forms.Panel();
            this.importantMessageLable = new System.Windows.Forms.Label();
            this.dataStreamsSidePanel = new System.Windows.Forms.Panel();
            this.picAddNewDS = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dummyAssessment = new System.Windows.Forms.PictureBox();
            this.pictureHelpIcon = new System.Windows.Forms.PictureBox();
            this.linkHelp = new System.Windows.Forms.LinkLabel();
            this.btnExportLogs = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.footerpanel = new System.Windows.Forms.Panel();
            this.panelAssessment.SuspendLayout();
            this.assessmentDescriptionStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.assessmentBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.assessmentLoader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newAssessmentLoader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCreateNewOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateNewAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.collectionInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageSize)).BeginInit();
            this.configSectionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.downconfiguration)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.elementsPanel.SuspendLayout();
            this.templatePanel.SuspendLayout();
            this.attributePanel.SuspendLayout();
            this.afDatabasePanel.SuspendLayout();
            this.piSystemPanel.SuspendLayout();
            this.txtFalkonryTokenPanel.SuspendLayout();
            this.txtFalkonryHostPanel.SuspendLayout();
            this.batchIdentifierPanel.SuspendLayout();
            this.configInformationStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWrong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDsLoader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkDatastream)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).BeginInit();
            this.livestreamStatusPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.streamingOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamingOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExportLogsLoader)).BeginInit();
            this.piConnectionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.piConnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.piDisconnected)).BeginInit();
            this.historyCardPanel.SuspendLayout();
            this.historyBodyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.historyWindowDescriptionStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.falkonryButtonPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.falkonryDisconnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.falkonryConnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.messageSidePanel.SuspendLayout();
            this.dataStreamsSidePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAddNewDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dummyAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureHelpIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.footerpanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // openConfigFile
            // 
            this.openConfigFile.FileName = "Falkonry.Integrator.PISystem.Config.json";
            this.openConfigFile.Filter = "Config File|Falkonry.Integrator.PISystem.Config.json";
            // 
            // lblLogo
            // 
            this.lblLogo.AutoSize = true;
            this.lblLogo.BackColor = System.Drawing.Color.Transparent;
            this.lblLogo.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogo.ForeColor = System.Drawing.Color.White;
            this.lblLogo.Location = new System.Drawing.Point(33, 7);
            this.lblLogo.Margin = new System.Windows.Forms.Padding(0);
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Size = new System.Drawing.Size(159, 22);
            this.lblLogo.TabIndex = 24;
            this.lblLogo.Text = "Falkonry PI Agent";
            this.lblLogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lstDatastreams
            // 
            this.lstDatastreams.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.lstDatastreams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDatastreams.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lstDatastreams.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstDatastreams.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDatastreams.ForeColor = System.Drawing.Color.Black;
            this.lstDatastreams.FullRowSelect = true;
            this.lstDatastreams.LabelWrap = false;
            this.lstDatastreams.Location = new System.Drawing.Point(4, 30);
            this.lstDatastreams.Margin = new System.Windows.Forms.Padding(0);
            this.lstDatastreams.MultiSelect = false;
            this.lstDatastreams.Name = "lstDatastreams";
            this.lstDatastreams.ShowGroups = false;
            this.lstDatastreams.Size = new System.Drawing.Size(169, 318);
            this.lstDatastreams.TabIndex = 29;
            this.lstDatastreams.UseCompatibleStateImageBehavior = false;
            this.lstDatastreams.View = System.Windows.Forms.View.List;
            this.lstDatastreams.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstDatastreams_SelectedIndexChanged);
            // 
            // lblDatastreams
            // 
            this.lblDatastreams.AutoSize = true;
            this.lblDatastreams.BackColor = System.Drawing.Color.Transparent;
            this.lblDatastreams.Font = new System.Drawing.Font("Arial", 11.5F);
            this.lblDatastreams.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblDatastreams.Location = new System.Drawing.Point(3, 5);
            this.lblDatastreams.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDatastreams.Name = "lblDatastreams";
            this.lblDatastreams.Size = new System.Drawing.Size(98, 18);
            this.lblDatastreams.TabIndex = 30;
            this.lblDatastreams.Text = "Datastreams";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "ic_help.png");
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Location = new System.Drawing.Point(0, 601);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 6, 0);
            this.statusStrip1.Size = new System.Drawing.Size(834, 0);
            this.statusStrip1.TabIndex = 44;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.Visible = false;
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // errorBox
            // 
            this.errorBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.errorBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.errorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorBox.ForeColor = System.Drawing.Color.Lime;
            this.errorBox.Location = new System.Drawing.Point(4, 15);
            this.errorBox.Margin = new System.Windows.Forms.Padding(2);
            this.errorBox.Name = "errorBox";
            this.errorBox.ReadOnly = true;
            this.errorBox.Size = new System.Drawing.Size(166, 150);
            this.errorBox.TabIndex = 51;
            this.errorBox.Text = "";
            // 
            // chkStreaming
            // 
            this.chkStreaming.AutoSize = true;
            this.chkStreaming.Location = new System.Drawing.Point(375, 8);
            this.chkStreaming.Margin = new System.Windows.Forms.Padding(2);
            this.chkStreaming.Name = "chkStreaming";
            this.chkStreaming.Size = new System.Drawing.Size(71, 17);
            this.chkStreaming.TabIndex = 59;
            this.chkStreaming.Text = "streaming";
            this.chkStreaming.UseVisualStyleBackColor = true;
            this.chkStreaming.Visible = false;
            // 
            // panelAssessment
            // 
            this.panelAssessment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelAssessment.AutoScroll = true;
            this.panelAssessment.BackColor = System.Drawing.Color.White;
            this.panelAssessment.Controls.Add(this.assessmentOrangeStrip);
            this.panelAssessment.Controls.Add(this.assessmentDescriptionStrip);
            this.panelAssessment.Controls.Add(this.assessmentBody);
            this.panelAssessment.Controls.Add(this.downHistory);
            this.panelAssessment.Controls.Add(this.assessmentBindingTitle);
            this.panelAssessment.Location = new System.Drawing.Point(9, 370);
            this.panelAssessment.Margin = new System.Windows.Forms.Padding(0);
            this.panelAssessment.MinimumSize = new System.Drawing.Size(610, 143);
            this.panelAssessment.Name = "panelAssessment";
            this.panelAssessment.Size = new System.Drawing.Size(612, 143);
            this.panelAssessment.TabIndex = 61;
            this.panelAssessment.Paint += new System.Windows.Forms.PaintEventHandler(this.configPanel_Paint);
            // 
            // assessmentOrangeStrip
            // 
            this.assessmentOrangeStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.assessmentOrangeStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(144)))), ((int)(((byte)(36)))));
            this.assessmentOrangeStrip.Location = new System.Drawing.Point(0, 0);
            this.assessmentOrangeStrip.Margin = new System.Windows.Forms.Padding(0);
            this.assessmentOrangeStrip.Name = "assessmentOrangeStrip";
            this.assessmentOrangeStrip.Size = new System.Drawing.Size(612, 2);
            this.assessmentOrangeStrip.TabIndex = 70;
            // 
            // assessmentDescriptionStrip
            // 
            this.assessmentDescriptionStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.assessmentDescriptionStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.assessmentDescriptionStrip.Controls.Add(this.pictureBox4);
            this.assessmentDescriptionStrip.Controls.Add(this.label12);
            this.assessmentDescriptionStrip.Location = new System.Drawing.Point(0, 24);
            this.assessmentDescriptionStrip.Margin = new System.Windows.Forms.Padding(0);
            this.assessmentDescriptionStrip.Name = "assessmentDescriptionStrip";
            this.assessmentDescriptionStrip.Size = new System.Drawing.Size(612, 0);
            this.assessmentDescriptionStrip.TabIndex = 69;
            this.assessmentDescriptionStrip.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_information;
            this.pictureBox4.Location = new System.Drawing.Point(11, 0);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(20, 20);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 46;
            this.pictureBox4.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoEllipsis = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(34, -1);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.label12.Size = new System.Drawing.Size(345, 20);
            this.label12.TabIndex = 43;
            this.label12.Text = "Assessment binding description";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // assessmentBody
            // 
            this.assessmentBody.AutoScroll = true;
            this.assessmentBody.AutoSize = true;
            this.assessmentBody.Controls.Add(this.assessmentLoader);
            this.assessmentBody.Controls.Add(this.btnBackfillHistory);
            this.assessmentBody.Controls.Add(this.btnAddNewAssessmentRow);
            this.assessmentBody.Controls.Add(this.newAssessmentLoader);
            this.assessmentBody.Controls.Add(this.txtAssessmentIdOutput);
            this.assessmentBody.Controls.Add(this.picCreateNewOutput);
            this.assessmentBody.Controls.Add(this.assessmentLabel);
            this.assessmentBody.Controls.Add(this.eventFrameFactLabel);
            this.assessmentBody.Controls.Add(this.WhichEventFrameTemplate);
            this.assessmentBody.Controls.Add(this.btnCreateNewAssessment);
            this.assessmentBody.Controls.Add(this.lstWriteBackAttribute);
            this.assessmentBody.Controls.Add(this.assessmentList);
            this.assessmentBody.Controls.Add(this.attributeResultLabel);
            this.assessmentBody.Location = new System.Drawing.Point(0, 24);
            this.assessmentBody.Margin = new System.Windows.Forms.Padding(0);
            this.assessmentBody.Name = "assessmentBody";
            this.assessmentBody.Size = new System.Drawing.Size(610, 119);
            this.assessmentBody.TabIndex = 49;
            // 
            // assessmentLoader
            // 
            this.assessmentLoader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.assessmentLoader.BackColor = System.Drawing.Color.Transparent;
            this.assessmentLoader.Cursor = System.Windows.Forms.Cursors.PanSE;
            this.assessmentLoader.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.loading_circles1;
            this.assessmentLoader.Location = new System.Drawing.Point(561, 59);
            this.assessmentLoader.Margin = new System.Windows.Forms.Padding(0);
            this.assessmentLoader.Name = "assessmentLoader";
            this.assessmentLoader.Size = new System.Drawing.Size(25, 25);
            this.assessmentLoader.TabIndex = 75;
            this.assessmentLoader.TabStop = false;
            this.assessmentLoader.Visible = false;
            // 
            // btnBackfillHistory
            // 
            this.btnBackfillHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBackfillHistory.BackColor = System.Drawing.Color.Silver;
            this.btnBackfillHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBackfillHistory.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBackfillHistory.FlatAppearance.BorderSize = 0;
            this.btnBackfillHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackfillHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackfillHistory.ForeColor = System.Drawing.Color.White;
            this.btnBackfillHistory.Location = new System.Drawing.Point(111, 9);
            this.btnBackfillHistory.Margin = new System.Windows.Forms.Padding(2);
            this.btnBackfillHistory.MaximumSize = new System.Drawing.Size(74, 18);
            this.btnBackfillHistory.Name = "btnBackfillHistory";
            this.btnBackfillHistory.Size = new System.Drawing.Size(74, 18);
            this.btnBackfillHistory.TabIndex = 74;
            this.btnBackfillHistory.Text = "BackFill History";
            this.btnBackfillHistory.UseVisualStyleBackColor = false;
            this.btnBackfillHistory.Click += new System.EventHandler(this.btnBackfillHistory_Click);
            // 
            // btnAddNewAssessmentRow
            // 
            this.btnAddNewAssessmentRow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddNewAssessmentRow.BackColor = System.Drawing.Color.Silver;
            this.btnAddNewAssessmentRow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddNewAssessmentRow.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAddNewAssessmentRow.FlatAppearance.BorderSize = 0;
            this.btnAddNewAssessmentRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewAssessmentRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNewAssessmentRow.ForeColor = System.Drawing.Color.White;
            this.btnAddNewAssessmentRow.Location = new System.Drawing.Point(15, 9);
            this.btnAddNewAssessmentRow.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddNewAssessmentRow.MaximumSize = new System.Drawing.Size(74, 18);
            this.btnAddNewAssessmentRow.Name = "btnAddNewAssessmentRow";
            this.btnAddNewAssessmentRow.Size = new System.Drawing.Size(74, 18);
            this.btnAddNewAssessmentRow.TabIndex = 73;
            this.btnAddNewAssessmentRow.Text = "New";
            this.btnAddNewAssessmentRow.UseVisualStyleBackColor = false;
            this.btnAddNewAssessmentRow.Click += new System.EventHandler(this.btnAddNewAssessmentRow_Click);
            // 
            // newAssessmentLoader
            // 
            this.newAssessmentLoader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.newAssessmentLoader.BackColor = System.Drawing.Color.Transparent;
            this.newAssessmentLoader.Cursor = System.Windows.Forms.Cursors.PanSE;
            this.newAssessmentLoader.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.loading_circles1;
            this.newAssessmentLoader.Location = new System.Drawing.Point(308, 10);
            this.newAssessmentLoader.Margin = new System.Windows.Forms.Padding(0);
            this.newAssessmentLoader.Name = "newAssessmentLoader";
            this.newAssessmentLoader.Size = new System.Drawing.Size(25, 25);
            this.newAssessmentLoader.TabIndex = 65;
            this.newAssessmentLoader.TabStop = false;
            this.newAssessmentLoader.Visible = false;
            // 
            // txtAssessmentIdOutput
            // 
            this.txtAssessmentIdOutput.Location = new System.Drawing.Point(10, 61);
            this.txtAssessmentIdOutput.Margin = new System.Windows.Forms.Padding(2);
            this.txtAssessmentIdOutput.Name = "txtAssessmentIdOutput";
            this.txtAssessmentIdOutput.ReadOnly = true;
            this.txtAssessmentIdOutput.Size = new System.Drawing.Size(121, 20);
            this.txtAssessmentIdOutput.TabIndex = 0;
            this.txtAssessmentIdOutput.TextChanged += new System.EventHandler(this.txtAssessmentIdOutput_TextChanged);
            // 
            // picCreateNewOutput
            // 
            this.picCreateNewOutput.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_add;
            this.picCreateNewOutput.Location = new System.Drawing.Point(314, 62);
            this.picCreateNewOutput.Margin = new System.Windows.Forms.Padding(0);
            this.picCreateNewOutput.Name = "picCreateNewOutput";
            this.picCreateNewOutput.Size = new System.Drawing.Size(20, 20);
            this.picCreateNewOutput.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCreateNewOutput.TabIndex = 66;
            this.picCreateNewOutput.TabStop = false;
            this.picCreateNewOutput.Click += new System.EventHandler(this.picCreateNewOutput_Click);
            this.picCreateNewOutput.MouseHover += new System.EventHandler(this.picAddNewOutput_MouseHover);
            // 
            // assessmentLabel
            // 
            this.assessmentLabel.AutoSize = true;
            this.assessmentLabel.BackColor = System.Drawing.Color.Transparent;
            this.assessmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessmentLabel.ForeColor = System.Drawing.Color.Black;
            this.assessmentLabel.Location = new System.Drawing.Point(9, 44);
            this.assessmentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.assessmentLabel.Name = "assessmentLabel";
            this.assessmentLabel.Size = new System.Drawing.Size(84, 17);
            this.assessmentLabel.TabIndex = 63;
            this.assessmentLabel.Text = "Assessment";
            this.assessmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // eventFrameFactLabel
            // 
            this.eventFrameFactLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventFrameFactLabel.AutoSize = true;
            this.eventFrameFactLabel.BackColor = System.Drawing.Color.Transparent;
            this.eventFrameFactLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventFrameFactLabel.ForeColor = System.Drawing.Color.Black;
            this.eventFrameFactLabel.Location = new System.Drawing.Point(361, 42);
            this.eventFrameFactLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.eventFrameFactLabel.Name = "eventFrameFactLabel";
            this.eventFrameFactLabel.Size = new System.Drawing.Size(197, 17);
            this.eventFrameFactLabel.TabIndex = 50;
            this.eventFrameFactLabel.Text = "Event frame template for facts";
            this.eventFrameFactLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WhichEventFrameTemplate
            // 
            this.WhichEventFrameTemplate.CheckOnClick = true;
            this.WhichEventFrameTemplate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.WhichEventFrameTemplate.DropDownHeight = 1;
            this.WhichEventFrameTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichEventFrameTemplate.FormattingEnabled = true;
            this.WhichEventFrameTemplate.IntegralHeight = false;
            this.WhichEventFrameTemplate.Location = new System.Drawing.Point(364, 59);
            this.WhichEventFrameTemplate.Margin = new System.Windows.Forms.Padding(0);
            this.WhichEventFrameTemplate.MaxDropDownItems = 10;
            this.WhichEventFrameTemplate.Name = "WhichEventFrameTemplate";
            this.WhichEventFrameTemplate.Size = new System.Drawing.Size(188, 25);
            this.WhichEventFrameTemplate.TabIndex = 56;
            this.WhichEventFrameTemplate.ValueSeparator = ", ";
            this.WhichEventFrameTemplate.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.WhichEventFrameTemplate_ItemCheck);
            this.WhichEventFrameTemplate.SelectedIndexChanged += new System.EventHandler(this.WhichEventFrameTemplate_SelectedIndexChanged);
            this.WhichEventFrameTemplate.Click += new System.EventHandler(this.WhichEventFrameTemplate_Click);
            // 
            // btnCreateNewAssessment
            // 
            this.btnCreateNewAssessment.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_add;
            this.btnCreateNewAssessment.Location = new System.Drawing.Point(139, 62);
            this.btnCreateNewAssessment.Margin = new System.Windows.Forms.Padding(0);
            this.btnCreateNewAssessment.Name = "btnCreateNewAssessment";
            this.btnCreateNewAssessment.Size = new System.Drawing.Size(20, 20);
            this.btnCreateNewAssessment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCreateNewAssessment.TabIndex = 67;
            this.btnCreateNewAssessment.TabStop = false;
            this.btnCreateNewAssessment.Visible = false;
            this.btnCreateNewAssessment.Click += new System.EventHandler(this.btnAddNewAssessment_Click);
            // 
            // lstWriteBackAttribute
            // 
            this.lstWriteBackAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstWriteBackAttribute.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstWriteBackAttribute.FormattingEnabled = true;
            this.lstWriteBackAttribute.Location = new System.Drawing.Point(182, 60);
            this.lstWriteBackAttribute.Margin = new System.Windows.Forms.Padding(2);
            this.lstWriteBackAttribute.Name = "lstWriteBackAttribute";
            this.lstWriteBackAttribute.Size = new System.Drawing.Size(125, 26);
            this.lstWriteBackAttribute.TabIndex = 54;
            this.lstWriteBackAttribute.SelectedIndexChanged += new System.EventHandler(this.lstWriteBackAttribute_SelectedIndexChanged);
            // 
            // assessmentList
            // 
            this.assessmentList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.assessmentList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessmentList.FormattingEnabled = true;
            this.assessmentList.Location = new System.Drawing.Point(10, 61);
            this.assessmentList.Margin = new System.Windows.Forms.Padding(2);
            this.assessmentList.Name = "assessmentList";
            this.assessmentList.Size = new System.Drawing.Size(121, 26);
            this.assessmentList.TabIndex = 62;
            this.assessmentList.Visible = false;
            this.assessmentList.SelectedIndexChanged += new System.EventHandler(this.assessmentList_SelectedIndexChanged);
            // 
            // attributeResultLabel
            // 
            this.attributeResultLabel.AutoSize = true;
            this.attributeResultLabel.BackColor = System.Drawing.Color.Transparent;
            this.attributeResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attributeResultLabel.ForeColor = System.Drawing.Color.Black;
            this.attributeResultLabel.Location = new System.Drawing.Point(186, 44);
            this.attributeResultLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.attributeResultLabel.Name = "attributeResultLabel";
            this.attributeResultLabel.Size = new System.Drawing.Size(121, 17);
            this.attributeResultLabel.TabIndex = 64;
            this.attributeResultLabel.Text = "Attribute for result";
            this.attributeResultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // downHistory
            // 
            this.downHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.downHistory.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_down;
            this.downHistory.InitialImage = null;
            this.downHistory.Location = new System.Drawing.Point(593, 7);
            this.downHistory.Margin = new System.Windows.Forms.Padding(0);
            this.downHistory.Name = "downHistory";
            this.downHistory.Size = new System.Drawing.Size(16, 17);
            this.downHistory.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.downHistory.TabIndex = 70;
            this.downHistory.TabStop = false;
            this.downHistory.Visible = false;
            // 
            // assessmentBindingTitle
            // 
            this.assessmentBindingTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessmentBindingTitle.Location = new System.Drawing.Point(8, 3);
            this.assessmentBindingTitle.Margin = new System.Windows.Forms.Padding(0);
            this.assessmentBindingTitle.Name = "assessmentBindingTitle";
            this.assessmentBindingTitle.Size = new System.Drawing.Size(132, 20);
            this.assessmentBindingTitle.TabIndex = 68;
            this.assessmentBindingTitle.Text = "Assesment Bindings";
            this.assessmentBindingTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkOverride
            // 
            this.chkOverride.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkOverride.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOverride.Location = new System.Drawing.Point(404, 9);
            this.chkOverride.Margin = new System.Windows.Forms.Padding(0);
            this.chkOverride.Name = "chkOverride";
            this.chkOverride.Size = new System.Drawing.Size(206, 19);
            this.chkOverride.TabIndex = 36;
            this.chkOverride.Text = "Override input and facts data";
            this.chkOverride.UseVisualStyleBackColor = true;
            this.chkOverride.CheckedChanged += new System.EventHandler(this.chkOverride_CheckedChanged);
            // 
            // collectionInterval
            // 
            this.collectionInterval.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.collectionInterval.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.collectionInterval.Location = new System.Drawing.Point(509, 80);
            this.collectionInterval.Margin = new System.Windows.Forms.Padding(2);
            this.collectionInterval.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.collectionInterval.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.collectionInterval.Name = "collectionInterval";
            this.collectionInterval.Size = new System.Drawing.Size(68, 23);
            this.collectionInterval.TabIndex = 34;
            this.collectionInterval.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.collectionInterval.Visible = false;
            this.collectionInterval.ValueChanged += new System.EventHandler(this.collectionInterval_ValueChanged);
            // 
            // lblCollectionInterval
            // 
            this.lblCollectionInterval.AutoSize = true;
            this.lblCollectionInterval.BackColor = System.Drawing.Color.Transparent;
            this.lblCollectionInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollectionInterval.Location = new System.Drawing.Point(270, 80);
            this.lblCollectionInterval.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCollectionInterval.Name = "lblCollectionInterval";
            this.lblCollectionInterval.Size = new System.Drawing.Size(271, 17);
            this.lblCollectionInterval.TabIndex = 35;
            this.lblCollectionInterval.Text = "Collection interval for live monitoring (ms):";
            this.lblCollectionInterval.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCollectionInterval.Visible = false;
            // 
            // lblAutoUpdSetting
            // 
            this.lblAutoUpdSetting.AutoSize = true;
            this.lblAutoUpdSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAutoUpdSetting.ForeColor = System.Drawing.Color.Black;
            this.lblAutoUpdSetting.LinkColor = System.Drawing.Color.Black;
            this.lblAutoUpdSetting.Location = new System.Drawing.Point(8, 45);
            this.lblAutoUpdSetting.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAutoUpdSetting.Name = "lblAutoUpdSetting";
            this.lblAutoUpdSetting.Size = new System.Drawing.Size(116, 17);
            this.lblAutoUpdSetting.TabIndex = 33;
            this.lblAutoUpdSetting.TabStop = true;
            this.lblAutoUpdSetting.Text = "Advance settings";
            this.lblAutoUpdSetting.Click += new System.EventHandler(this.lblAutoUpdSettings_Click);
            // 
            // pageSize
            // 
            this.pageSize.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageSize.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.pageSize.Location = new System.Drawing.Point(196, 78);
            this.pageSize.Margin = new System.Windows.Forms.Padding(2);
            this.pageSize.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.pageSize.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.pageSize.Name = "pageSize";
            this.pageSize.Size = new System.Drawing.Size(68, 23);
            this.pageSize.TabIndex = 31;
            this.pageSize.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.pageSize.Visible = false;
            this.pageSize.ValueChanged += new System.EventHandler(this.pageSize_ValueChanged);
            // 
            // pageSizeLabel
            // 
            this.pageSizeLabel.AutoSize = true;
            this.pageSizeLabel.BackColor = System.Drawing.Color.Transparent;
            this.pageSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageSizeLabel.Location = new System.Drawing.Point(5, 80);
            this.pageSizeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pageSizeLabel.Name = "pageSizeLabel";
            this.pageSizeLabel.Size = new System.Drawing.Size(210, 17);
            this.pageSizeLabel.TabIndex = 29;
            this.pageSizeLabel.Text = "Page size (Max event at a time):";
            this.pageSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.pageSizeLabel.Visible = false;
            // 
            // StartDatePicker
            // 
            this.StartDatePicker.CustomFormat = "dd-MMM-yyyy HH:mm:ss";
            this.StartDatePicker.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.StartDatePicker.Location = new System.Drawing.Point(60, 9);
            this.StartDatePicker.Margin = new System.Windows.Forms.Padding(2);
            this.StartDatePicker.MaxDate = new System.DateTime(3000, 1, 1, 0, 0, 0, 0);
            this.StartDatePicker.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.StartDatePicker.Name = "StartDatePicker";
            this.StartDatePicker.Size = new System.Drawing.Size(139, 23);
            this.StartDatePicker.TabIndex = 11;
            this.StartDatePicker.ValueChanged += new System.EventHandler(this.StartDatePicker_ValueChanged);
            // 
            // EndDatePicker
            // 
            this.EndDatePicker.CustomFormat = "dd-MMM-yyyy HH:mm:ss";
            this.EndDatePicker.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.EndDatePicker.Location = new System.Drawing.Point(249, 9);
            this.EndDatePicker.Margin = new System.Windows.Forms.Padding(2);
            this.EndDatePicker.MaxDate = new System.DateTime(3000, 1, 1, 0, 0, 0, 0);
            this.EndDatePicker.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.EndDatePicker.Name = "EndDatePicker";
            this.EndDatePicker.Size = new System.Drawing.Size(136, 23);
            this.EndDatePicker.TabIndex = 12;
            this.EndDatePicker.ValueChanged += new System.EventHandler(this.EndDatePicker_ValueChanged);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.BackColor = System.Drawing.Color.Transparent;
            this.lblStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartDate.Location = new System.Drawing.Point(10, 11);
            this.lblStartDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(46, 17);
            this.lblStartDate.TabIndex = 11;
            this.lblStartDate.Text = "Start :";
            this.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.BackColor = System.Drawing.Color.Transparent;
            this.lblEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndDate.Location = new System.Drawing.Point(204, 11);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(41, 17);
            this.lblEndDate.TabIndex = 13;
            this.lblEndDate.Text = "End :";
            this.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // configSectionPanel
            // 
            this.configSectionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configSectionPanel.AutoScroll = true;
            this.configSectionPanel.BackColor = System.Drawing.Color.White;
            this.configSectionPanel.Controls.Add(this.downconfiguration);
            this.configSectionPanel.Controls.Add(this.configSectionOrangeStrip);
            this.configSectionPanel.Controls.Add(this.tableLayoutPanel1);
            this.configSectionPanel.Controls.Add(this.configInformationStrip);
            this.configSectionPanel.Controls.Add(this.label9);
            this.configSectionPanel.Location = new System.Drawing.Point(8, 40);
            this.configSectionPanel.Margin = new System.Windows.Forms.Padding(0);
            this.configSectionPanel.Name = "configSectionPanel";
            this.configSectionPanel.Size = new System.Drawing.Size(612, 176);
            this.configSectionPanel.TabIndex = 37;
            this.configSectionPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.configPanel_Paint);
            // 
            // downconfiguration
            // 
            this.downconfiguration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.downconfiguration.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_down;
            this.downconfiguration.InitialImage = null;
            this.downconfiguration.Location = new System.Drawing.Point(611, 5);
            this.downconfiguration.Margin = new System.Windows.Forms.Padding(0);
            this.downconfiguration.Name = "downconfiguration";
            this.downconfiguration.Size = new System.Drawing.Size(16, 17);
            this.downconfiguration.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.downconfiguration.TabIndex = 47;
            this.downconfiguration.TabStop = false;
            this.downconfiguration.Visible = false;
            // 
            // configSectionOrangeStrip
            // 
            this.configSectionOrangeStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configSectionOrangeStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(144)))), ((int)(((byte)(36)))));
            this.configSectionOrangeStrip.Location = new System.Drawing.Point(0, 0);
            this.configSectionOrangeStrip.Margin = new System.Windows.Forms.Padding(0);
            this.configSectionOrangeStrip.Name = "configSectionOrangeStrip";
            this.configSectionOrangeStrip.Size = new System.Drawing.Size(629, 2);
            this.configSectionOrangeStrip.TabIndex = 44;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.32159F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.843843F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.83449F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.32159F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.843843F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.83465F));
            this.tableLayoutPanel1.Controls.Add(this.label14, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.label13, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.elementsLabel, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblAttributeTemplates, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblToken, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblElementTemplates, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblHostName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblSelectConfiguration, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblAFDatabase, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.elementsPanel, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.templatePanel, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.attributePanel, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.afDatabasePanel, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.piSystemPanel, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFalkonryTokenPanel, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtFalkonryHostPanel, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.batchIdentifierTag, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.batchIdentifierPanel, 5, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 20);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(612, 156);
            this.tableLayoutPanel1.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(413, 128);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 16);
            this.label14.TabIndex = 78;
            this.label14.Text = ":";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(108, 128);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 16);
            this.label13.TabIndex = 77;
            this.label13.Text = ":";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(413, 89);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 16);
            this.label8.TabIndex = 76;
            this.label8.Text = ":";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(108, 89);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 75;
            this.label7.Text = ":";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(108, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 16);
            this.label5.TabIndex = 73;
            this.label5.Text = ":";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // elementsLabel
            // 
            this.elementsLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.elementsLabel.AutoSize = true;
            this.elementsLabel.BackColor = System.Drawing.Color.Transparent;
            this.elementsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elementsLabel.ForeColor = System.Drawing.Color.Black;
            this.elementsLabel.Location = new System.Drawing.Point(307, 128);
            this.elementsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.elementsLabel.Name = "elementsLabel";
            this.elementsLabel.Size = new System.Drawing.Size(66, 17);
            this.elementsLabel.TabIndex = 8;
            this.elementsLabel.Text = "Elements";
            this.elementsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(413, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 16);
            this.label2.TabIndex = 72;
            this.label2.Text = ":";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAttributeTemplates
            // 
            this.lblAttributeTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAttributeTemplates.AutoSize = true;
            this.lblAttributeTemplates.BackColor = System.Drawing.Color.Transparent;
            this.lblAttributeTemplates.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttributeTemplates.ForeColor = System.Drawing.Color.Black;
            this.lblAttributeTemplates.Location = new System.Drawing.Point(2, 128);
            this.lblAttributeTemplates.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAttributeTemplates.Name = "lblAttributeTemplates";
            this.lblAttributeTemplates.Size = new System.Drawing.Size(102, 17);
            this.lblAttributeTemplates.TabIndex = 7;
            this.lblAttributeTemplates.Text = "  Attributes";
            this.lblAttributeTemplates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblToken
            // 
            this.lblToken.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblToken.AutoSize = true;
            this.lblToken.BackColor = System.Drawing.Color.Transparent;
            this.lblToken.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToken.ForeColor = System.Drawing.Color.Black;
            this.lblToken.Location = new System.Drawing.Point(2, 50);
            this.lblToken.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(102, 17);
            this.lblToken.TabIndex = 20;
            this.lblToken.Text = "  Token";
            this.lblToken.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblElementTemplates
            // 
            this.lblElementTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblElementTemplates.AutoSize = true;
            this.lblElementTemplates.BackColor = System.Drawing.Color.Transparent;
            this.lblElementTemplates.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElementTemplates.ForeColor = System.Drawing.Color.Black;
            this.lblElementTemplates.Location = new System.Drawing.Point(2, 89);
            this.lblElementTemplates.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblElementTemplates.Name = "lblElementTemplates";
            this.lblElementTemplates.Size = new System.Drawing.Size(102, 17);
            this.lblElementTemplates.TabIndex = 6;
            this.lblElementTemplates.Text = "  Template";
            this.lblElementTemplates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHostName
            // 
            this.lblHostName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHostName.AutoSize = true;
            this.lblHostName.BackColor = System.Drawing.Color.Transparent;
            this.lblHostName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHostName.ForeColor = System.Drawing.Color.Black;
            this.lblHostName.Location = new System.Drawing.Point(2, 2);
            this.lblHostName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHostName.Name = "lblHostName";
            this.lblHostName.Size = new System.Drawing.Size(102, 34);
            this.lblHostName.TabIndex = 19;
            this.lblHostName.Text = "  Falkonry Host";
            this.lblHostName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectConfiguration
            // 
            this.lblSelectConfiguration.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSelectConfiguration.AutoSize = true;
            this.lblSelectConfiguration.BackColor = System.Drawing.Color.Transparent;
            this.lblSelectConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectConfiguration.ForeColor = System.Drawing.Color.Black;
            this.lblSelectConfiguration.Location = new System.Drawing.Point(307, 11);
            this.lblSelectConfiguration.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSelectConfiguration.Name = "lblSelectConfiguration";
            this.lblSelectConfiguration.Size = new System.Drawing.Size(70, 17);
            this.lblSelectConfiguration.TabIndex = 21;
            this.lblSelectConfiguration.Text = "PI System";
            this.lblSelectConfiguration.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAFDatabase
            // 
            this.lblAFDatabase.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAFDatabase.AutoSize = true;
            this.lblAFDatabase.BackColor = System.Drawing.Color.Transparent;
            this.lblAFDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAFDatabase.ForeColor = System.Drawing.Color.Black;
            this.lblAFDatabase.Location = new System.Drawing.Point(307, 50);
            this.lblAFDatabase.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAFDatabase.Name = "lblAFDatabase";
            this.lblAFDatabase.Size = new System.Drawing.Size(90, 17);
            this.lblAFDatabase.TabIndex = 5;
            this.lblAFDatabase.Text = "AF Database";
            this.lblAFDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // elementsPanel
            // 
            this.elementsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.elementsPanel.Controls.Add(this.WhichElements);
            this.elementsPanel.Controls.Add(this.lblElementReadOnly);
            this.elementsPanel.Location = new System.Drawing.Point(430, 119);
            this.elementsPanel.Margin = new System.Windows.Forms.Padding(2);
            this.elementsPanel.Name = "elementsPanel";
            this.elementsPanel.Size = new System.Drawing.Size(174, 35);
            this.elementsPanel.TabIndex = 70;
            // 
            // WhichElements
            // 
            this.WhichElements.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.WhichElements.CheckOnClick = true;
            this.WhichElements.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.WhichElements.DropDownHeight = 1;
            this.WhichElements.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichElements.FormattingEnabled = true;
            this.WhichElements.IntegralHeight = false;
            this.WhichElements.Location = new System.Drawing.Point(0, 7);
            this.WhichElements.Margin = new System.Windows.Forms.Padding(2);
            this.WhichElements.Name = "WhichElements";
            this.WhichElements.Size = new System.Drawing.Size(174, 25);
            this.WhichElements.TabIndex = 31;
            this.WhichElements.ValueSeparator = ", ";
            this.WhichElements.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.WhichElements_ItemCheck);
            this.WhichElements.Leave += new System.EventHandler(this.WhichElements_Leave);
            // 
            // lblElementReadOnly
            // 
            this.lblElementReadOnly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblElementReadOnly.AutoEllipsis = true;
            this.lblElementReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.lblElementReadOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElementReadOnly.ForeColor = System.Drawing.Color.Black;
            this.lblElementReadOnly.Location = new System.Drawing.Point(0, 10);
            this.lblElementReadOnly.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblElementReadOnly.Name = "lblElementReadOnly";
            this.lblElementReadOnly.Size = new System.Drawing.Size(170, 18);
            this.lblElementReadOnly.TabIndex = 36;
            this.lblElementReadOnly.Text = "Elements";
            this.lblElementReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.lblElementReadOnly, "Click to see more elements");
            this.lblElementReadOnly.Visible = false;
            this.lblElementReadOnly.Click += new System.EventHandler(this.lblElementReadOnly_Click);
            // 
            // templatePanel
            // 
            this.templatePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.templatePanel.AutoSize = true;
            this.templatePanel.Controls.Add(this.lblTemplateReadOnly);
            this.templatePanel.Controls.Add(this.WhichElementTemplate);
            this.templatePanel.Location = new System.Drawing.Point(123, 84);
            this.templatePanel.Margin = new System.Windows.Forms.Padding(0);
            this.templatePanel.Name = "templatePanel";
            this.templatePanel.Size = new System.Drawing.Size(175, 33);
            this.templatePanel.TabIndex = 42;
            // 
            // lblTemplateReadOnly
            // 
            this.lblTemplateReadOnly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTemplateReadOnly.AutoSize = true;
            this.lblTemplateReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.lblTemplateReadOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemplateReadOnly.ForeColor = System.Drawing.Color.Black;
            this.lblTemplateReadOnly.Location = new System.Drawing.Point(-1, 8);
            this.lblTemplateReadOnly.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTemplateReadOnly.Name = "lblTemplateReadOnly";
            this.lblTemplateReadOnly.Size = new System.Drawing.Size(69, 18);
            this.lblTemplateReadOnly.TabIndex = 34;
            this.lblTemplateReadOnly.Text = "Template";
            this.lblTemplateReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTemplateReadOnly.Visible = false;
            // 
            // WhichElementTemplate
            // 
            this.WhichElementTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.WhichElementTemplate.BackColor = System.Drawing.SystemColors.Window;
            this.WhichElementTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WhichElementTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichElementTemplate.ForeColor = System.Drawing.SystemColors.WindowText;
            this.WhichElementTemplate.FormattingEnabled = true;
            this.WhichElementTemplate.ItemHeight = 16;
            this.WhichElementTemplate.Location = new System.Drawing.Point(1, 6);
            this.WhichElementTemplate.Margin = new System.Windows.Forms.Padding(0);
            this.WhichElementTemplate.Name = "WhichElementTemplate";
            this.WhichElementTemplate.Size = new System.Drawing.Size(174, 24);
            this.WhichElementTemplate.TabIndex = 2;
            this.WhichElementTemplate.SelectedIndexChanged += new System.EventHandler(this.WhichElementTemplate_SelectedIndexChanged_1);
            // 
            // attributePanel
            // 
            this.attributePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.attributePanel.Controls.Add(this.lblAttributeReadOnly);
            this.attributePanel.Controls.Add(this.WhichAttributeTemplates);
            this.attributePanel.Location = new System.Drawing.Point(123, 123);
            this.attributePanel.Margin = new System.Windows.Forms.Padding(0);
            this.attributePanel.Name = "attributePanel";
            this.attributePanel.Size = new System.Drawing.Size(175, 33);
            this.attributePanel.TabIndex = 70;
            // 
            // lblAttributeReadOnly
            // 
            this.lblAttributeReadOnly.AutoEllipsis = true;
            this.lblAttributeReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.lblAttributeReadOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttributeReadOnly.ForeColor = System.Drawing.Color.Black;
            this.lblAttributeReadOnly.Location = new System.Drawing.Point(-1, 8);
            this.lblAttributeReadOnly.Margin = new System.Windows.Forms.Padding(0);
            this.lblAttributeReadOnly.Name = "lblAttributeReadOnly";
            this.lblAttributeReadOnly.Size = new System.Drawing.Size(170, 18);
            this.lblAttributeReadOnly.TabIndex = 35;
            this.lblAttributeReadOnly.Text = "Attributes";
            this.lblAttributeReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.lblAttributeReadOnly, "Click to see more attributes");
            this.lblAttributeReadOnly.Visible = false;
            this.lblAttributeReadOnly.Click += new System.EventHandler(this.lblAttributeReadOnly_Click);
            // 
            // WhichAttributeTemplates
            // 
            this.WhichAttributeTemplates.BackColor = System.Drawing.SystemColors.Window;
            this.WhichAttributeTemplates.CheckOnClick = true;
            this.WhichAttributeTemplates.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.WhichAttributeTemplates.DropDownHeight = 1;
            this.WhichAttributeTemplates.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichAttributeTemplates.ForeColor = System.Drawing.SystemColors.WindowText;
            this.WhichAttributeTemplates.FormattingEnabled = true;
            this.WhichAttributeTemplates.IntegralHeight = false;
            this.WhichAttributeTemplates.ItemHeight = 20;
            this.WhichAttributeTemplates.Location = new System.Drawing.Point(1, 2);
            this.WhichAttributeTemplates.Margin = new System.Windows.Forms.Padding(2);
            this.WhichAttributeTemplates.Name = "WhichAttributeTemplates";
            this.WhichAttributeTemplates.Size = new System.Drawing.Size(174, 26);
            this.WhichAttributeTemplates.TabIndex = 30;
            this.WhichAttributeTemplates.ValueSeparator = ", ";
            this.WhichAttributeTemplates.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.WhichAttributeTemplates_ItemCheck);
            this.WhichAttributeTemplates.Leave += new System.EventHandler(this.WhichAttributeTemplates_Leave);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(413, 50);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 16);
            this.label6.TabIndex = 74;
            this.label6.Text = ":";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // afDatabasePanel
            // 
            this.afDatabasePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.afDatabasePanel.Controls.Add(this.lblAfDbReadOnly);
            this.afDatabasePanel.Controls.Add(this.WhichAFDatabase);
            this.afDatabasePanel.Location = new System.Drawing.Point(430, 41);
            this.afDatabasePanel.Margin = new System.Windows.Forms.Padding(2);
            this.afDatabasePanel.Name = "afDatabasePanel";
            this.afDatabasePanel.Size = new System.Drawing.Size(174, 35);
            this.afDatabasePanel.TabIndex = 42;
            // 
            // lblAfDbReadOnly
            // 
            this.lblAfDbReadOnly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblAfDbReadOnly.AutoSize = true;
            this.lblAfDbReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.lblAfDbReadOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAfDbReadOnly.ForeColor = System.Drawing.Color.Black;
            this.lblAfDbReadOnly.Location = new System.Drawing.Point(-1, 11);
            this.lblAfDbReadOnly.Margin = new System.Windows.Forms.Padding(0);
            this.lblAfDbReadOnly.Name = "lblAfDbReadOnly";
            this.lblAfDbReadOnly.Size = new System.Drawing.Size(90, 17);
            this.lblAfDbReadOnly.TabIndex = 33;
            this.lblAfDbReadOnly.Text = "AF Database";
            this.lblAfDbReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAfDbReadOnly.Visible = false;
            // 
            // WhichAFDatabase
            // 
            this.WhichAFDatabase.AccessibleDescription = "Database Picker";
            this.WhichAFDatabase.AccessibleName = "Database Picker";
            this.WhichAFDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichAFDatabase.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.WhichAFDatabase.Location = new System.Drawing.Point(1, 5);
            this.WhichAFDatabase.Margin = new System.Windows.Forms.Padding(0);
            this.WhichAFDatabase.Name = "WhichAFDatabase";
            this.WhichAFDatabase.ShowBegin = false;
            this.WhichAFDatabase.ShowDelete = false;
            this.WhichAFDatabase.ShowEnd = false;
            this.WhichAFDatabase.ShowFind = false;
            this.WhichAFDatabase.ShowImages = false;
            this.WhichAFDatabase.ShowList = false;
            this.WhichAFDatabase.ShowNavigation = false;
            this.WhichAFDatabase.ShowNew = false;
            this.WhichAFDatabase.ShowNext = false;
            this.WhichAFDatabase.ShowNoEntries = false;
            this.WhichAFDatabase.ShowPrevious = false;
            this.WhichAFDatabase.ShowProperties = false;
            this.WhichAFDatabase.Size = new System.Drawing.Size(165, 25);
            this.WhichAFDatabase.TabIndex = 1;
            this.WhichAFDatabase.SelectionChange += new OSIsoft.AF.UI.SelectionChangeEventHandler(this.WhichAFDatabase_SelectionChange);
            this.WhichAFDatabase.Load += new System.EventHandler(this.WhichAFDatabase_Load);
            // 
            // piSystemPanel
            // 
            this.piSystemPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.piSystemPanel.Controls.Add(this.WhichPISystem);
            this.piSystemPanel.Controls.Add(this.lblPiReadOnly);
            this.piSystemPanel.Location = new System.Drawing.Point(430, 2);
            this.piSystemPanel.Margin = new System.Windows.Forms.Padding(2);
            this.piSystemPanel.Name = "piSystemPanel";
            this.piSystemPanel.Size = new System.Drawing.Size(174, 35);
            this.piSystemPanel.TabIndex = 70;
            // 
            // WhichPISystem
            // 
            this.WhichPISystem.AccessibleDescription = "PI System Picker";
            this.WhichPISystem.AccessibleName = "PI System Picker";
            this.WhichPISystem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WhichPISystem.Cursor = System.Windows.Forms.Cursors.Default;
            this.WhichPISystem.EnableDelete = false;
            this.WhichPISystem.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhichPISystem.Location = new System.Drawing.Point(0, 8);
            this.WhichPISystem.LoginPromptSetting = OSIsoft.AF.UI.PISystemPicker.LoginPromptSettingOptions.Default;
            this.WhichPISystem.Margin = new System.Windows.Forms.Padding(2);
            this.WhichPISystem.Name = "WhichPISystem";
            this.WhichPISystem.ShowBegin = false;
            this.WhichPISystem.ShowDelete = false;
            this.WhichPISystem.ShowEnd = false;
            this.WhichPISystem.ShowFind = false;
            this.WhichPISystem.ShowImages = false;
            this.WhichPISystem.ShowNavigation = false;
            this.WhichPISystem.ShowNew = false;
            this.WhichPISystem.ShowNext = false;
            this.WhichPISystem.ShowPrevious = false;
            this.WhichPISystem.ShowProperties = false;
            this.WhichPISystem.Size = new System.Drawing.Size(174, 26);
            this.WhichPISystem.TabIndex = 0;
            this.WhichPISystem.ConnectionChange += new OSIsoft.AF.UI.ConnectionChangeEventHandler(this.WhichPISystem_ConnectionChange);
            this.WhichPISystem.Load += new System.EventHandler(this.WhichPISystem_Load);
            // 
            // lblPiReadOnly
            // 
            this.lblPiReadOnly.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblPiReadOnly.AutoSize = true;
            this.lblPiReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.lblPiReadOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPiReadOnly.ForeColor = System.Drawing.Color.Black;
            this.lblPiReadOnly.Location = new System.Drawing.Point(0, 11);
            this.lblPiReadOnly.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPiReadOnly.Name = "lblPiReadOnly";
            this.lblPiReadOnly.Size = new System.Drawing.Size(79, 18);
            this.lblPiReadOnly.TabIndex = 32;
            this.lblPiReadOnly.Text = "PI System:";
            this.lblPiReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblPiReadOnly.Visible = false;
            // 
            // txtFalkonryTokenPanel
            // 
            this.txtFalkonryTokenPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtFalkonryTokenPanel.Controls.Add(this.tokenlabel);
            this.txtFalkonryTokenPanel.Controls.Add(this.txtFalkonryToken);
            this.txtFalkonryTokenPanel.Location = new System.Drawing.Point(123, 45);
            this.txtFalkonryTokenPanel.Margin = new System.Windows.Forms.Padding(0);
            this.txtFalkonryTokenPanel.Name = "txtFalkonryTokenPanel";
            this.txtFalkonryTokenPanel.Size = new System.Drawing.Size(175, 33);
            this.txtFalkonryTokenPanel.TabIndex = 79;
            // 
            // tokenlabel
            // 
            this.tokenlabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tokenlabel.AutoSize = true;
            this.tokenlabel.BackColor = System.Drawing.Color.Transparent;
            this.tokenlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tokenlabel.ForeColor = System.Drawing.Color.Black;
            this.tokenlabel.Location = new System.Drawing.Point(-1, 8);
            this.tokenlabel.Margin = new System.Windows.Forms.Padding(0);
            this.tokenlabel.Name = "tokenlabel";
            this.tokenlabel.Size = new System.Drawing.Size(50, 18);
            this.tokenlabel.TabIndex = 33;
            this.tokenlabel.Text = "Token";
            this.tokenlabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFalkonryToken
            // 
            this.txtFalkonryToken.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtFalkonryToken.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFalkonryToken.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtFalkonryToken.Location = new System.Drawing.Point(0, 6);
            this.txtFalkonryToken.Margin = new System.Windows.Forms.Padding(0);
            this.txtFalkonryToken.Name = "txtFalkonryToken";
            this.txtFalkonryToken.Size = new System.Drawing.Size(174, 24);
            this.txtFalkonryToken.TabIndex = 23;
            this.txtFalkonryToken.Text = "Enter your token here";
            // 
            // txtFalkonryHostPanel
            // 
            this.txtFalkonryHostPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtFalkonryHostPanel.Controls.Add(this.hostlabel);
            this.txtFalkonryHostPanel.Controls.Add(this.txtFalkonryHost);
            this.txtFalkonryHostPanel.Location = new System.Drawing.Point(123, 6);
            this.txtFalkonryHostPanel.Margin = new System.Windows.Forms.Padding(0);
            this.txtFalkonryHostPanel.Name = "txtFalkonryHostPanel";
            this.txtFalkonryHostPanel.Size = new System.Drawing.Size(175, 33);
            this.txtFalkonryHostPanel.TabIndex = 80;
            // 
            // hostlabel
            // 
            this.hostlabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hostlabel.AutoSize = true;
            this.hostlabel.BackColor = System.Drawing.Color.Transparent;
            this.hostlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hostlabel.ForeColor = System.Drawing.Color.Black;
            this.hostlabel.Location = new System.Drawing.Point(-1, 8);
            this.hostlabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.hostlabel.Name = "hostlabel";
            this.hostlabel.Size = new System.Drawing.Size(40, 18);
            this.hostlabel.TabIndex = 33;
            this.hostlabel.Text = "Host";
            this.hostlabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.hostlabel.Visible = false;
            // 
            // txtFalkonryHost
            // 
            this.txtFalkonryHost.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtFalkonryHost.BackColor = System.Drawing.SystemColors.Window;
            this.txtFalkonryHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFalkonryHost.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtFalkonryHost.Location = new System.Drawing.Point(0, 6);
            this.txtFalkonryHost.Margin = new System.Windows.Forms.Padding(0);
            this.txtFalkonryHost.Name = "txtFalkonryHost";
            this.txtFalkonryHost.Size = new System.Drawing.Size(174, 24);
            this.txtFalkonryHost.TabIndex = 22;
            this.txtFalkonryHost.Text = "https://app.falkonry.ai";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(108, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 16);
            this.label1.TabIndex = 71;
            this.label1.Text = ":";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // batchIdentifierTag
            // 
            this.batchIdentifierTag.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.batchIdentifierTag.AutoSize = true;
            this.batchIdentifierTag.BackColor = System.Drawing.Color.Transparent;
            this.batchIdentifierTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batchIdentifierTag.ForeColor = System.Drawing.Color.Black;
            this.batchIdentifierTag.Location = new System.Drawing.Point(307, 89);
            this.batchIdentifierTag.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.batchIdentifierTag.Name = "batchIdentifierTag";
            this.batchIdentifierTag.Size = new System.Drawing.Size(102, 17);
            this.batchIdentifierTag.TabIndex = 38;
            this.batchIdentifierTag.Text = "Batch Identifier";
            this.batchIdentifierTag.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // batchIdentifierPanel
            // 
            this.batchIdentifierPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.batchIdentifierPanel.Controls.Add(this.batchIdentifierLabel);
            this.batchIdentifierPanel.Controls.Add(this.batchIdentifier);
            this.batchIdentifierPanel.Location = new System.Drawing.Point(430, 80);
            this.batchIdentifierPanel.Margin = new System.Windows.Forms.Padding(2);
            this.batchIdentifierPanel.Name = "batchIdentifierPanel";
            this.batchIdentifierPanel.Size = new System.Drawing.Size(174, 35);
            this.batchIdentifierPanel.TabIndex = 70;
            // 
            // batchIdentifierLabel
            // 
            this.batchIdentifierLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.batchIdentifierLabel.AutoEllipsis = true;
            this.batchIdentifierLabel.AutoSize = true;
            this.batchIdentifierLabel.BackColor = System.Drawing.Color.Transparent;
            this.batchIdentifierLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batchIdentifierLabel.ForeColor = System.Drawing.Color.Black;
            this.batchIdentifierLabel.Location = new System.Drawing.Point(0, 11);
            this.batchIdentifierLabel.Margin = new System.Windows.Forms.Padding(0);
            this.batchIdentifierLabel.Name = "batchIdentifierLabel";
            this.batchIdentifierLabel.Size = new System.Drawing.Size(112, 18);
            this.batchIdentifierLabel.TabIndex = 39;
            this.batchIdentifierLabel.Text = "Batch Identifier :";
            this.batchIdentifierLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // batchIdentifier
            // 
            this.batchIdentifier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.batchIdentifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.batchIdentifier.FormattingEnabled = true;
            this.batchIdentifier.Location = new System.Drawing.Point(0, 7);
            this.batchIdentifier.Margin = new System.Windows.Forms.Padding(2);
            this.batchIdentifier.Name = "batchIdentifier";
            this.batchIdentifier.Size = new System.Drawing.Size(173, 21);
            this.batchIdentifier.TabIndex = 40;
            this.batchIdentifier.SelectedIndexChanged += new System.EventHandler(this.batchIdentifier_SelectedIndexChanged);
            this.batchIdentifier.SelectedValueChanged += new System.EventHandler(this.batchIdentifier_SelectedValueChanged);
            // 
            // configInformationStrip
            // 
            this.configInformationStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configInformationStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.configInformationStrip.Controls.Add(this.pictureBox2);
            this.configInformationStrip.Controls.Add(this.label10);
            this.configInformationStrip.Location = new System.Drawing.Point(0, 23);
            this.configInformationStrip.Margin = new System.Windows.Forms.Padding(0);
            this.configInformationStrip.Name = "configInformationStrip";
            this.configInformationStrip.Size = new System.Drawing.Size(612, 0);
            this.configInformationStrip.TabIndex = 45;
            this.configInformationStrip.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_information;
            this.pictureBox2.Location = new System.Drawing.Point(11, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 19);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 46;
            this.pictureBox2.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoEllipsis = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(34, -1);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(345, 20);
            this.label10.TabIndex = 43;
            this.label10.Text = "Configuration description";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 3);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(132, 20);
            this.label9.TabIndex = 42;
            this.label9.Text = "Configuration";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPanel.AutoScroll = true;
            this.mainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(245)))));
            this.mainPanel.Controls.Add(this.txtFalkonryDatastream);
            this.mainPanel.Controls.Add(this.dsName);
            this.mainPanel.Controls.Add(this.picWrong);
            this.mainPanel.Controls.Add(this.picDsLoader);
            this.mainPanel.Controls.Add(this.linkDatastream);
            this.mainPanel.Controls.Add(this.btnDelete);
            this.mainPanel.Controls.Add(this.livestreamStatusPanel);
            this.mainPanel.Controls.Add(this.picExportLogsLoader);
            this.mainPanel.Controls.Add(this.piConnectionPanel);
            this.mainPanel.Controls.Add(this.historyCardPanel);
            this.mainPanel.Controls.Add(this.falkonryButtonPanel);
            this.mainPanel.Controls.Add(this.panelAssessment);
            this.mainPanel.Controls.Add(this.configSectionPanel);
            this.mainPanel.Controls.Add(this.picRight);
            this.mainPanel.ForeColor = System.Drawing.Color.Black;
            this.mainPanel.Location = new System.Drawing.Point(188, 33);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(0);
            this.mainPanel.MinimumSize = new System.Drawing.Size(620, 520);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(630, 520);
            this.mainPanel.TabIndex = 58;
            this.mainPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.mainPanel_Paint);
            // 
            // txtFalkonryDatastream
            // 
            this.txtFalkonryDatastream.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.txtFalkonryDatastream.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFalkonryDatastream.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFalkonryDatastream.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtFalkonryDatastream.Location = new System.Drawing.Point(8, 12);
            this.txtFalkonryDatastream.Margin = new System.Windows.Forms.Padding(0);
            this.txtFalkonryDatastream.Name = "txtFalkonryDatastream";
            this.txtFalkonryDatastream.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFalkonryDatastream.Size = new System.Drawing.Size(210, 26);
            this.txtFalkonryDatastream.TabIndex = 24;
            this.txtFalkonryDatastream.Text = "New Datastream";
            this.txtFalkonryDatastream.TextChanged += new System.EventHandler(this.txtFalkonryDatastream_TextChanged);
            // 
            // dsName
            // 
            this.dsName.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.dsName.AutoEllipsis = true;
            this.dsName.BackColor = System.Drawing.Color.Transparent;
            this.dsName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dsName.ForeColor = System.Drawing.Color.Black;
            this.dsName.Location = new System.Drawing.Point(11, 12);
            this.dsName.Margin = new System.Windows.Forms.Padding(0);
            this.dsName.Name = "dsName";
            this.dsName.Size = new System.Drawing.Size(210, 16);
            this.dsName.TabIndex = 54;
            this.dsName.Text = "New Datastream";
            this.dsName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsName.Click += new System.EventHandler(this.dsName_Click);
            this.dsName.MouseHover += new System.EventHandler(this.dsName_MouseHover);
            // 
            // picWrong
            // 
            this.picWrong.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_save;
            this.picWrong.Location = new System.Drawing.Point(304, 13);
            this.picWrong.Margin = new System.Windows.Forms.Padding(0);
            this.picWrong.Name = "picWrong";
            this.picWrong.Size = new System.Drawing.Size(17, 17);
            this.picWrong.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picWrong.TabIndex = 75;
            this.picWrong.TabStop = false;
            this.picWrong.Click += new System.EventHandler(this.picWrong_Click);
            // 
            // picDsLoader
            // 
            this.picDsLoader.BackColor = System.Drawing.Color.Transparent;
            this.picDsLoader.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.loading_circles;
            this.picDsLoader.Location = new System.Drawing.Point(262, 12);
            this.picDsLoader.Margin = new System.Windows.Forms.Padding(0);
            this.picDsLoader.Name = "picDsLoader";
            this.picDsLoader.Size = new System.Drawing.Size(25, 25);
            this.picDsLoader.TabIndex = 67;
            this.picDsLoader.TabStop = false;
            this.picDsLoader.Visible = false;
            // 
            // linkDatastream
            // 
            this.linkDatastream.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_data_straem_link;
            this.linkDatastream.Location = new System.Drawing.Point(222, 13);
            this.linkDatastream.Margin = new System.Windows.Forms.Padding(0);
            this.linkDatastream.Name = "linkDatastream";
            this.linkDatastream.Size = new System.Drawing.Size(17, 17);
            this.linkDatastream.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.linkDatastream.TabIndex = 69;
            this.linkDatastream.TabStop = false;
            this.linkDatastream.Click += new System.EventHandler(this.linkDatastream_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_delete_1;
            this.btnDelete.Location = new System.Drawing.Point(240, 12);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(17, 17);
            this.btnDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnDelete.TabIndex = 68;
            this.btnDelete.TabStop = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // livestreamStatusPanel
            // 
            this.livestreamStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.livestreamStatusPanel.BackColor = System.Drawing.Color.White;
            this.livestreamStatusPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.livestreamStatusPanel.Controls.Add(this.btnStreaming);
            this.livestreamStatusPanel.Controls.Add(this.streamingOff);
            this.livestreamStatusPanel.Controls.Add(this.streamingOn);
            this.livestreamStatusPanel.Location = new System.Drawing.Point(566, 13);
            this.livestreamStatusPanel.Margin = new System.Windows.Forms.Padding(2);
            this.livestreamStatusPanel.Name = "livestreamStatusPanel";
            this.livestreamStatusPanel.Size = new System.Drawing.Size(52, 15);
            this.livestreamStatusPanel.TabIndex = 72;
            this.livestreamStatusPanel.Click += new System.EventHandler(this.btnStreaming_Click);
            // 
            // btnStreaming
            // 
            this.btnStreaming.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStreaming.AutoSize = true;
            this.btnStreaming.BackColor = System.Drawing.Color.Transparent;
            this.btnStreaming.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStreaming.ForeColor = System.Drawing.Color.Black;
            this.btnStreaming.Location = new System.Drawing.Point(3, 1);
            this.btnStreaming.Margin = new System.Windows.Forms.Padding(0);
            this.btnStreaming.Name = "btnStreaming";
            this.btnStreaming.Size = new System.Drawing.Size(32, 15);
            this.btnStreaming.TabIndex = 73;
            this.btnStreaming.Text = "Live ";
            this.btnStreaming.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStreaming.Click += new System.EventHandler(this.btnStreaming_Click);
            // 
            // streamingOff
            // 
            this.streamingOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.streamingOff.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_red2;
            this.streamingOff.Location = new System.Drawing.Point(36, 4);
            this.streamingOff.Margin = new System.Windows.Forms.Padding(0);
            this.streamingOff.Name = "streamingOff";
            this.streamingOff.Size = new System.Drawing.Size(9, 9);
            this.streamingOff.TabIndex = 39;
            this.streamingOff.TabStop = false;
            // 
            // streamingOn
            // 
            this.streamingOn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.streamingOn.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_green;
            this.streamingOn.Location = new System.Drawing.Point(36, 4);
            this.streamingOn.Margin = new System.Windows.Forms.Padding(0);
            this.streamingOn.Name = "streamingOn";
            this.streamingOn.Size = new System.Drawing.Size(9, 9);
            this.streamingOn.TabIndex = 40;
            this.streamingOn.TabStop = false;
            this.streamingOn.Visible = false;
            // 
            // picExportLogsLoader
            // 
            this.picExportLogsLoader.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.loading_circles1;
            this.picExportLogsLoader.Location = new System.Drawing.Point(240, 12);
            this.picExportLogsLoader.Margin = new System.Windows.Forms.Padding(0);
            this.picExportLogsLoader.Name = "picExportLogsLoader";
            this.picExportLogsLoader.Size = new System.Drawing.Size(25, 25);
            this.picExportLogsLoader.TabIndex = 68;
            this.picExportLogsLoader.TabStop = false;
            this.picExportLogsLoader.Visible = false;
            // 
            // piConnectionPanel
            // 
            this.piConnectionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.piConnectionPanel.BackColor = System.Drawing.Color.White;
            this.piConnectionPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.piConnectionPanel.Controls.Add(this.label3);
            this.piConnectionPanel.Controls.Add(this.piConnected);
            this.piConnectionPanel.Controls.Add(this.piDisconnected);
            this.piConnectionPanel.Location = new System.Drawing.Point(330, 13);
            this.piConnectionPanel.Margin = new System.Windows.Forms.Padding(0);
            this.piConnectionPanel.Name = "piConnectionPanel";
            this.piConnectionPanel.Size = new System.Drawing.Size(90, 15);
            this.piConnectionPanel.TabIndex = 70;
            this.piConnectionPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.raisedBuddonShadow);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(1, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 14);
            this.label3.TabIndex = 32;
            this.label3.Text = "PI connection";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Paint += new System.Windows.Forms.PaintEventHandler(this.raisedBuddonShadow);
            // 
            // piConnected
            // 
            this.piConnected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.piConnected.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_green;
            this.piConnected.Location = new System.Drawing.Point(78, 4);
            this.piConnected.Margin = new System.Windows.Forms.Padding(0);
            this.piConnected.Name = "piConnected";
            this.piConnected.Size = new System.Drawing.Size(9, 9);
            this.piConnected.TabIndex = 37;
            this.piConnected.TabStop = false;
            // 
            // piDisconnected
            // 
            this.piDisconnected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.piDisconnected.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_red;
            this.piDisconnected.Location = new System.Drawing.Point(78, 4);
            this.piDisconnected.Margin = new System.Windows.Forms.Padding(0);
            this.piDisconnected.Name = "piDisconnected";
            this.piDisconnected.Size = new System.Drawing.Size(9, 9);
            this.piDisconnected.TabIndex = 36;
            this.piDisconnected.TabStop = false;
            // 
            // historyCardPanel
            // 
            this.historyCardPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyCardPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(254)))));
            this.historyCardPanel.Controls.Add(this.historyBodyPanel);
            this.historyCardPanel.Controls.Add(this.historyWindowDescriptionStrip);
            this.historyCardPanel.Controls.Add(this.HistoryWindowLable);
            this.historyCardPanel.Controls.Add(this.pictureBox7);
            this.historyCardPanel.Controls.Add(this.historicalOrgangeStrip);
            this.historyCardPanel.Location = new System.Drawing.Point(8, 224);
            this.historyCardPanel.Margin = new System.Windows.Forms.Padding(2);
            this.historyCardPanel.Name = "historyCardPanel";
            this.historyCardPanel.Size = new System.Drawing.Size(612, 139);
            this.historyCardPanel.TabIndex = 62;
            this.historyCardPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.configPanel_Paint);
            // 
            // historyBodyPanel
            // 
            this.historyBodyPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyBodyPanel.Controls.Add(this.lblStartDate);
            this.historyBodyPanel.Controls.Add(this.pictureBox6);
            this.historyBodyPanel.Controls.Add(this.EndDatePicker);
            this.historyBodyPanel.Controls.Add(this.lblEndDate);
            this.historyBodyPanel.Controls.Add(this.StartDatePicker);
            this.historyBodyPanel.Controls.Add(this.pageSizeLabel);
            this.historyBodyPanel.Controls.Add(this.collectionInterval);
            this.historyBodyPanel.Controls.Add(this.lblAutoUpdSetting);
            this.historyBodyPanel.Controls.Add(this.chkOverride);
            this.historyBodyPanel.Controls.Add(this.pageSize);
            this.historyBodyPanel.Controls.Add(this.lblCollectionInterval);
            this.historyBodyPanel.Location = new System.Drawing.Point(0, 24);
            this.historyBodyPanel.Margin = new System.Windows.Forms.Padding(2);
            this.historyBodyPanel.Name = "historyBodyPanel";
            this.historyBodyPanel.Size = new System.Drawing.Size(612, 115);
            this.historyBodyPanel.TabIndex = 49;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_dropdown;
            this.pictureBox6.Location = new System.Drawing.Point(125, 50);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(9, 9);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 48;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // historyWindowDescriptionStrip
            // 
            this.historyWindowDescriptionStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyWindowDescriptionStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.historyWindowDescriptionStrip.Controls.Add(this.pictureBox3);
            this.historyWindowDescriptionStrip.Controls.Add(this.label11);
            this.historyWindowDescriptionStrip.Location = new System.Drawing.Point(0, 24);
            this.historyWindowDescriptionStrip.Margin = new System.Windows.Forms.Padding(2);
            this.historyWindowDescriptionStrip.Name = "historyWindowDescriptionStrip";
            this.historyWindowDescriptionStrip.Size = new System.Drawing.Size(612, 0);
            this.historyWindowDescriptionStrip.TabIndex = 47;
            this.historyWindowDescriptionStrip.Visible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_information;
            this.pictureBox3.Location = new System.Drawing.Point(11, 0);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(20, 20);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 46;
            this.pictureBox3.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoEllipsis = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(34, -1);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(345, 20);
            this.label11.TabIndex = 43;
            this.label11.Text = "History Window Description";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HistoryWindowLable
            // 
            this.HistoryWindowLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HistoryWindowLable.Location = new System.Drawing.Point(8, 3);
            this.HistoryWindowLable.Margin = new System.Windows.Forms.Padding(0);
            this.HistoryWindowLable.Name = "HistoryWindowLable";
            this.HistoryWindowLable.Size = new System.Drawing.Size(132, 20);
            this.HistoryWindowLable.TabIndex = 46;
            this.HistoryWindowLable.Text = "History Window";
            this.HistoryWindowLable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.HistoryWindowLable.Click += new System.EventHandler(this.label11_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox7.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_down;
            this.pictureBox7.InitialImage = null;
            this.pictureBox7.Location = new System.Drawing.Point(594, 5);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 17);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 50;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Visible = false;
            // 
            // historicalOrgangeStrip
            // 
            this.historicalOrgangeStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historicalOrgangeStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(144)))), ((int)(((byte)(36)))));
            this.historicalOrgangeStrip.Location = new System.Drawing.Point(0, 0);
            this.historicalOrgangeStrip.Margin = new System.Windows.Forms.Padding(0);
            this.historicalOrgangeStrip.Name = "historicalOrgangeStrip";
            this.historicalOrgangeStrip.Size = new System.Drawing.Size(612, 2);
            this.historicalOrgangeStrip.TabIndex = 45;
            // 
            // falkonryButtonPanel
            // 
            this.falkonryButtonPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.falkonryButtonPanel.BackColor = System.Drawing.Color.White;
            this.falkonryButtonPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.falkonryButtonPanel.Controls.Add(this.label4);
            this.falkonryButtonPanel.Controls.Add(this.falkonryDisconnected);
            this.falkonryButtonPanel.Controls.Add(this.falkonryConnected);
            this.falkonryButtonPanel.Location = new System.Drawing.Point(432, 13);
            this.falkonryButtonPanel.Margin = new System.Windows.Forms.Padding(2);
            this.falkonryButtonPanel.Name = "falkonryButtonPanel";
            this.falkonryButtonPanel.Size = new System.Drawing.Size(122, 15);
            this.falkonryButtonPanel.TabIndex = 71;
            this.falkonryButtonPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.falkonryButtonPanel_Paint);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(1, 1);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 15);
            this.label4.TabIndex = 33;
            this.label4.Text = "Falkonry connection";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // falkonryDisconnected
            // 
            this.falkonryDisconnected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.falkonryDisconnected.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_red1;
            this.falkonryDisconnected.Location = new System.Drawing.Point(110, 4);
            this.falkonryDisconnected.Margin = new System.Windows.Forms.Padding(0);
            this.falkonryDisconnected.Name = "falkonryDisconnected";
            this.falkonryDisconnected.Size = new System.Drawing.Size(9, 9);
            this.falkonryDisconnected.TabIndex = 38;
            this.falkonryDisconnected.TabStop = false;
            // 
            // falkonryConnected
            // 
            this.falkonryConnected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.falkonryConnected.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_green;
            this.falkonryConnected.Location = new System.Drawing.Point(110, 4);
            this.falkonryConnected.Margin = new System.Windows.Forms.Padding(0);
            this.falkonryConnected.Name = "falkonryConnected";
            this.falkonryConnected.Size = new System.Drawing.Size(9, 9);
            this.falkonryConnected.TabIndex = 37;
            this.falkonryConnected.TabStop = false;
            // 
            // picRight
            // 
            this.picRight.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_right;
            this.picRight.Location = new System.Drawing.Point(287, 13);
            this.picRight.Margin = new System.Windows.Forms.Padding(0);
            this.picRight.Name = "picRight";
            this.picRight.Size = new System.Drawing.Size(17, 17);
            this.picRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRight.TabIndex = 74;
            this.picRight.TabStop = false;
            this.picRight.Click += new System.EventHandler(this.picRight_Click);
            // 
            // lblNoDatastreams
            // 
            this.lblNoDatastreams.Cursor = System.Windows.Forms.Cursors.PanSE;
            this.lblNoDatastreams.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDatastreams.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNoDatastreams.Location = new System.Drawing.Point(188, 79);
            this.lblNoDatastreams.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNoDatastreams.Name = "lblNoDatastreams";
            this.lblNoDatastreams.Size = new System.Drawing.Size(594, 29);
            this.lblNoDatastreams.TabIndex = 59;
            this.lblNoDatastreams.Text = "No Datastreams present. Please create new datastream";
            this.lblNoDatastreams.Visible = false;
            // 
            // labelFooter
            // 
            this.labelFooter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFooter.BackColor = System.Drawing.Color.Transparent;
            this.labelFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFooter.ForeColor = System.Drawing.Color.White;
            this.labelFooter.Location = new System.Drawing.Point(114, -1);
            this.labelFooter.Margin = new System.Windows.Forms.Padding(0);
            this.labelFooter.Name = "labelFooter";
            this.labelFooter.Size = new System.Drawing.Size(250, 16);
            this.labelFooter.TabIndex = 61;
            this.labelFooter.Text = "© 2017 Falkonry Inc. | Version: 1.1.0";
            this.labelFooter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbLogo
            // 
            this.pbLogo.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.logo;
            this.pbLogo.Location = new System.Drawing.Point(5, 5);
            this.pbLogo.Margin = new System.Windows.Forms.Padding(0);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(22, 23);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLogo.TabIndex = 23;
            this.pbLogo.TabStop = false;
            // 
            // messageSidePanel
            // 
            this.messageSidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.messageSidePanel.Controls.Add(this.importantMessageLable);
            this.messageSidePanel.Controls.Add(this.errorBox);
            this.messageSidePanel.Location = new System.Drawing.Point(7, 390);
            this.messageSidePanel.Margin = new System.Windows.Forms.Padding(0);
            this.messageSidePanel.Name = "messageSidePanel";
            this.messageSidePanel.Size = new System.Drawing.Size(173, 163);
            this.messageSidePanel.TabIndex = 70;
            // 
            // importantMessageLable
            // 
            this.importantMessageLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importantMessageLable.ForeColor = System.Drawing.Color.White;
            this.importantMessageLable.Location = new System.Drawing.Point(2, 0);
            this.importantMessageLable.Margin = new System.Windows.Forms.Padding(0);
            this.importantMessageLable.Name = "importantMessageLable";
            this.importantMessageLable.Size = new System.Drawing.Size(130, 15);
            this.importantMessageLable.TabIndex = 52;
            this.importantMessageLable.Text = "Activity Logs";
            // 
            // dataStreamsSidePanel
            // 
            this.dataStreamsSidePanel.Controls.Add(this.lblDatastreams);
            this.dataStreamsSidePanel.Controls.Add(this.picAddNewDS);
            this.dataStreamsSidePanel.Controls.Add(this.lstDatastreams);
            this.dataStreamsSidePanel.Location = new System.Drawing.Point(4, 34);
            this.dataStreamsSidePanel.Margin = new System.Windows.Forms.Padding(0);
            this.dataStreamsSidePanel.Name = "dataStreamsSidePanel";
            this.dataStreamsSidePanel.Size = new System.Drawing.Size(176, 351);
            this.dataStreamsSidePanel.TabIndex = 71;
            // 
            // picAddNewDS
            // 
            this.picAddNewDS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picAddNewDS.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_add_white;
            this.picAddNewDS.Location = new System.Drawing.Point(152, 3);
            this.picAddNewDS.Margin = new System.Windows.Forms.Padding(0);
            this.picAddNewDS.Name = "picAddNewDS";
            this.picAddNewDS.Size = new System.Drawing.Size(20, 20);
            this.picAddNewDS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picAddNewDS.TabIndex = 68;
            this.picAddNewDS.TabStop = false;
            this.picAddNewDS.Click += new System.EventHandler(this.picAddNewDS_Click);
            this.picAddNewDS.MouseHover += new System.EventHandler(this.picAddNewDS_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(144)))), ((int)(((byte)(36)))));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(-31, 770);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(0, 0);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(144)))), ((int)(((byte)(36)))));
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(-31, 770);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(0, 0);
            this.btnCancel.TabIndex = 45;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_click);
            // 
            // dummyAssessment
            // 
            this.dummyAssessment.Location = new System.Drawing.Point(0, 0);
            this.dummyAssessment.Margin = new System.Windows.Forms.Padding(2);
            this.dummyAssessment.Name = "dummyAssessment";
            this.dummyAssessment.Size = new System.Drawing.Size(0, 0);
            this.dummyAssessment.TabIndex = 73;
            this.dummyAssessment.TabStop = false;
            this.dummyAssessment.Visible = false;
            // 
            // pictureHelpIcon
            // 
            this.pictureHelpIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureHelpIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureHelpIcon.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_help;
            this.pictureHelpIcon.Location = new System.Drawing.Point(758, 10);
            this.pictureHelpIcon.Margin = new System.Windows.Forms.Padding(2);
            this.pictureHelpIcon.Name = "pictureHelpIcon";
            this.pictureHelpIcon.Size = new System.Drawing.Size(16, 17);
            this.pictureHelpIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureHelpIcon.TabIndex = 73;
            this.pictureHelpIcon.TabStop = false;
            this.pictureHelpIcon.Click += new System.EventHandler(this.pictureHelpIcon_Click_1);
            // 
            // linkHelp
            // 
            this.linkHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkHelp.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkHelp.LinkColor = System.Drawing.Color.White;
            this.linkHelp.Location = new System.Drawing.Point(775, 11);
            this.linkHelp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkHelp.Name = "linkHelp";
            this.linkHelp.Size = new System.Drawing.Size(37, 17);
            this.linkHelp.TabIndex = 72;
            this.linkHelp.TabStop = true;
            this.linkHelp.Text = "Help";
            this.linkHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.helpLinkClicked);
            // 
            // btnExportLogs
            // 
            this.btnExportLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportLogs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnExportLogs.FlatAppearance.BorderSize = 0;
            this.btnExportLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportLogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportLogs.ForeColor = System.Drawing.Color.Transparent;
            this.btnExportLogs.Location = new System.Drawing.Point(612, 8);
            this.btnExportLogs.Margin = new System.Windows.Forms.Padding(0);
            this.btnExportLogs.Name = "btnExportLogs";
            this.btnExportLogs.Size = new System.Drawing.Size(110, 18);
            this.btnExportLogs.TabIndex = 71;
            this.btnExportLogs.Text = "Report a problem\r\n\r\n";
            this.btnExportLogs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportLogs.UseVisualStyleBackColor = true;
            this.btnExportLogs.Click += new System.EventHandler(this.btnExportLogs_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Falkonry.Integrator.PISystem.Configurator.Resources.ic_report_a_problem;
            this.pictureBox1.Location = new System.Drawing.Point(597, 9);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 17);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 70;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.btnExportLogs_Click);
            // 
            // footerpanel
            // 
            this.footerpanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.footerpanel.BackColor = System.Drawing.Color.Transparent;
            this.footerpanel.Controls.Add(this.labelFooter);
            this.footerpanel.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.footerpanel.Location = new System.Drawing.Point(188, 554);
            this.footerpanel.Name = "footerpanel";
            this.footerpanel.Size = new System.Drawing.Size(537, 18);
            this.footerpanel.TabIndex = 75;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.ClientSize = new System.Drawing.Size(834, 573);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.lblLogo);
            this.Controls.Add(this.chkStreaming);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnExportLogs);
            this.Controls.Add(this.linkHelp);
            this.Controls.Add(this.pictureHelpIcon);
            this.Controls.Add(this.dummyAssessment);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.messageSidePanel);
            this.Controls.Add(this.dataStreamsSidePanel);
            this.Controls.Add(this.footerpanel);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.lblNoDatastreams);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(850, 600);
            this.Name = "ConfigurationForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration of Falkonry Integrator for the PI System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.SizeChanged += new System.EventHandler(this.ConfigurationForm_SizeChanged);
            this.panelAssessment.ResumeLayout(false);
            this.panelAssessment.PerformLayout();
            this.assessmentDescriptionStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.assessmentBody.ResumeLayout(false);
            this.assessmentBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.assessmentLoader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newAssessmentLoader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCreateNewOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateNewAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.collectionInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageSize)).EndInit();
            this.configSectionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.downconfiguration)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.elementsPanel.ResumeLayout(false);
            this.templatePanel.ResumeLayout(false);
            this.templatePanel.PerformLayout();
            this.attributePanel.ResumeLayout(false);
            this.afDatabasePanel.ResumeLayout(false);
            this.afDatabasePanel.PerformLayout();
            this.piSystemPanel.ResumeLayout(false);
            this.piSystemPanel.PerformLayout();
            this.txtFalkonryTokenPanel.ResumeLayout(false);
            this.txtFalkonryTokenPanel.PerformLayout();
            this.txtFalkonryHostPanel.ResumeLayout(false);
            this.txtFalkonryHostPanel.PerformLayout();
            this.batchIdentifierPanel.ResumeLayout(false);
            this.batchIdentifierPanel.PerformLayout();
            this.configInformationStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWrong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDsLoader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkDatastream)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).EndInit();
            this.livestreamStatusPanel.ResumeLayout(false);
            this.livestreamStatusPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.streamingOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamingOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExportLogsLoader)).EndInit();
            this.piConnectionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.piConnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.piDisconnected)).EndInit();
            this.historyCardPanel.ResumeLayout(false);
            this.historyBodyPanel.ResumeLayout(false);
            this.historyBodyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.historyWindowDescriptionStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.falkonryButtonPanel.ResumeLayout(false);
            this.falkonryButtonPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.falkonryDisconnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.falkonryConnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.messageSidePanel.ResumeLayout(false);
            this.dataStreamsSidePanel.ResumeLayout(false);
            this.dataStreamsSidePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAddNewDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dummyAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureHelpIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.footerpanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        //private void WhichAttributeTemplates_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
        //{

        //}

        #endregion
        private System.Windows.Forms.OpenFileDialog openConfigFile;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label lblLogo;
        private System.Windows.Forms.ListView lstDatastreams;
        private System.Windows.Forms.Label lblDatastreams;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.RichTextBox errorBox;
        private System.Windows.Forms.CheckBox chkStreaming;
        private CheckComboBoxTest.CheckedComboBox WhichEventFrameTemplate;
    
        private System.Windows.Forms.ComboBox lstWriteBackAttribute;
        private System.Windows.Forms.Label eventFrameFactLabel;
        private System.Windows.Forms.NumericUpDown collectionInterval;
        private System.Windows.Forms.Label lblCollectionInterval;
        private System.Windows.Forms.LinkLabel lblAutoUpdSetting;
        private System.Windows.Forms.NumericUpDown pageSize;
        private System.Windows.Forms.Label pageSizeLabel;
        private System.Windows.Forms.DateTimePicker StartDatePicker;
        private System.Windows.Forms.DateTimePicker EndDatePicker;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.Label lblElementReadOnly;
        private System.Windows.Forms.Label lblAttributeReadOnly;
        private System.Windows.Forms.Label lblTemplateReadOnly;
        private System.Windows.Forms.Label lblAfDbReadOnly;
        private System.Windows.Forms.Label lblPiReadOnly;
        private CheckComboBoxTest.CheckedComboBox WhichElements;
        private OSIsoft.AF.UI.PISystemPicker WhichPISystem;
        private System.Windows.Forms.Label elementsLabel;
        private System.Windows.Forms.Label lblSelectConfiguration;
        private System.Windows.Forms.Label lblAttributeTemplates;
        private System.Windows.Forms.ComboBox WhichElementTemplate;
        private System.Windows.Forms.Label lblElementTemplates;
        private System.Windows.Forms.TextBox txtFalkonryHost;
        private System.Windows.Forms.TextBox txtFalkonryToken;
        private System.Windows.Forms.Label lblToken;
        private System.Windows.Forms.Label lblHostName;
        private System.Windows.Forms.Label lblAFDatabase;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Label lblNoDatastreams;
        private System.Windows.Forms.Panel configSectionPanel;
        private System.Windows.Forms.Panel panelAssessment;
        private System.Windows.Forms.CheckBox chkOverride;
        private System.Windows.Forms.TextBox txtAssessmentIdOutput;
        private System.Windows.Forms.ComboBox assessmentList;
        private System.Windows.Forms.Label assessmentLabel;
        private System.Windows.Forms.Label attributeResultLabel;
        private System.Windows.Forms.PictureBox newAssessmentLoader;
        private System.Windows.Forms.PictureBox picCreateNewOutput;
        private System.Windows.Forms.Label labelFooter;
        private System.Windows.Forms.PictureBox picDsLoader;
        private System.Windows.Forms.PictureBox btnCreateNewAssessment;
        private System.Windows.Forms.PictureBox picAddNewDS;
        private System.Windows.Forms.PictureBox picExportLogsLoader;
        private System.Windows.Forms.Label batchIdentifierTag;
        private System.Windows.Forms.Label batchIdentifierLabel;
        private System.Windows.Forms.ComboBox batchIdentifier;
        private System.Windows.Forms.Panel historyCardPanel;
        private System.Windows.Forms.Panel messageSidePanel;
        private System.Windows.Forms.Label importantMessageLable;
        private System.Windows.Forms.Panel dataStreamsSidePanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel templatePanel;
        private System.Windows.Forms.Panel afDatabasePanel;
        private System.Windows.Forms.Panel piSystemPanel;
        private System.Windows.Forms.Panel attributePanel;
        private System.Windows.Forms.Panel elementsPanel;
        private System.Windows.Forms.Panel batchIdentifierPanel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel configSectionOrangeStrip;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel configInformationStrip;
        private System.Windows.Forms.Label HistoryWindowLable;
        private System.Windows.Forms.Panel historicalOrgangeStrip;
        private System.Windows.Forms.Panel historyWindowDescriptionStrip;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel assessmentOrangeStrip;
        private System.Windows.Forms.Panel assessmentDescriptionStrip;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label assessmentBindingTitle;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel historyBodyPanel;
        private System.Windows.Forms.Panel assessmentBody;
        private System.Windows.Forms.PictureBox downconfiguration;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox downHistory;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel falkonryButtonPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox falkonryDisconnected;
        private System.Windows.Forms.PictureBox falkonryConnected;
        private System.Windows.Forms.Panel piConnectionPanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox piConnected;
        private System.Windows.Forms.PictureBox piDisconnected;
        private System.Windows.Forms.PictureBox btnDelete;
        private System.Windows.Forms.PictureBox linkDatastream;
        private System.Windows.Forms.PictureBox picRight;
        private System.Windows.Forms.PictureBox picWrong;
        private System.Windows.Forms.Panel livestreamStatusPanel;
        private System.Windows.Forms.Label dsName;
        private System.Windows.Forms.Label btnStreaming;
        private System.Windows.Forms.PictureBox streamingOff;
        private System.Windows.Forms.PictureBox streamingOn;
        private System.Windows.Forms.TextBox txtFalkonryDatastream;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox dummyAssessment;
        private System.Windows.Forms.Button btnAddNewAssessmentRow;
        private System.Windows.Forms.Button btnBackfillHistory;
        private CheckComboBoxTest.CheckedComboBox WhichAttributeTemplates;
        private System.Windows.Forms.Panel txtFalkonryTokenPanel;
        private System.Windows.Forms.Label tokenlabel;
        private System.Windows.Forms.Panel txtFalkonryHostPanel;
        private System.Windows.Forms.Label hostlabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnExportLogs;
        private System.Windows.Forms.LinkLabel linkHelp;
        private System.Windows.Forms.PictureBox pictureHelpIcon;
        private System.Windows.Forms.PictureBox assessmentLoader;
        private System.Windows.Forms.Panel footerpanel;
        private OSIsoft.AF.UI.AFDatabasePicker WhichAFDatabase;
    }
}

