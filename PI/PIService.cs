﻿using System;
using System.Threading.Tasks;
using System.Collections;
using Falkonry.Integrator.PISystem.Helper;
using log4net;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using FalkonryClient.Helper.Models;
using System.Reflection;
using System.Net;

namespace Falkonry.Integrator.PISystem
{
    internal class PiService
    {
       
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private AFObservableLayer afObservableLayer;
        private AFSubscriptions afSubscriptions;
        private HistoricalPi _historical;
        private StreamingPi _streaming;
        private Timer _taskCheckTimer;


        private Dictionary<Guid, Task> _tasks;
        private Dictionary<Guid, StreamingPi> _pipes;
        private Dictionary<Guid, Dictionary<string, FalkonryReceiver>> _streamingTasks;

        private bool _startHasCompleted = false;
        private bool _previousConfigCheckRunning = false;
        public PiService()
        {
            afObservableLayer = new AFObservableLayer();
            afSubscriptions = new AFSubscriptions(afObservableLayer);
            //Task.Factory.StartNew(() =>
            //{
            _tasks = new Dictionary<Guid, Task>();
            _pipes = new Dictionary<Guid, StreamingPi>();
            _streamingTasks = new Dictionary<Guid, Dictionary<string, FalkonryReceiver>>();
            //_taskCheckTimer = new Timer(sender =>
            //{
            //    bool taskStillRunning = false;
            //    try
            //    {
            //        foreach (var task in _tasks)
            //        {
            //            //Log.Debug($"Configuration with Id: {task.Key} = Task Status: {task.Value.Status}");
            //            taskStillRunning = (task.Value.Status != TaskStatus.RanToCompletion);
            //            if (taskStillRunning) break;
            //        }
            //    }
            //    catch (Exception)
            //    {
            //    }

            //    if (!taskStillRunning && _startHasCompleted && !_previousConfigCheckRunning)
            //    {
            //        //CheckForConfigChanges();
            //    }
            //}, null, 0, 60000);
            //});
        }

        private void CheckForConfigChanges()
        {
            Log.Debug($"################START CheckForConfigChanges################.");
            _previousConfigCheckRunning = true;
            
            var newConfig = CollectConfiguration();
            if (!newConfig.Equals(ServiceConfigutations))
            {
                Log.Info($"CheckForConfigChanges: found changes, processing...");
                ServiceConfigutations = newConfig;
                foreach (var config in ServiceConfigutations.ConfigurationItems)
                {
                    // Check datastreamID, if it present then check if it present in falkonry if not then delete
                    //Log.Debug($"Checking Changes in datastream Object. ' Config Id : {config.Id} Datastream Id :{config.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {config.Configuration.FalkonryConfiguration.Datastream}'");

                    bool datastreamPresent = FalkonryHelper.syncWithFalkonry(config);
                    if(!datastreamPresent)
                    {
                        Log.Warn($"datastream not found at falkonry server. ' Config Id : {config.Id} Datastream Id :{config.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {config.Configuration.FalkonryConfiguration.Datastream}'");
                        config.Deleted = true;
                    }
                    // Deleting Configuration
                    if (config.Deleted)
                    {
                        Log.Info($"Deleting datastream : ' Config Id : {config.Id} Datastream Id :{config.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {config.Configuration.FalkonryConfiguration.Datastream}'");
                        if (_pipes.ContainsKey(config.Id))
                        {
                            _pipes[config.Id].Stop();
                            _pipes[config.Id].Dispose();
                            _pipes.Remove(config.Id);
                            Dictionary<string, FalkonryReceiver> map = _streamingTasks[config.Id];
                            foreach (KeyValuePair<string, FalkonryReceiver> entry in map)
                            {
                                ((FalkonryReceiver)entry.Value).StopListening();
                            }
                            _streamingTasks.Remove(config.Id);
                        }

                        if (_tasks.ContainsKey(config.Id))
                        {
                            lock (_tasks)
                            {
                                _tasks[config.Id].Dispose();
                                _tasks.Remove(config.Id);
                            }
                        }
                       try
                        {
                            Log.Debug($" Datastream to be deleted, message updated: ' Config Id : {config.Id} Datastream Id :{config.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {config.Configuration.FalkonryConfiguration.Datastream}'");
                            AfHelper.UpdateState(config.Configuration.AfConfiguration.SystemName, config.Name, "INFO", "Datastream Deleted");
                            AfHelper.SaveSingleConfig(config);
                        }
                        catch (Exception exception)
                        {
                            Log.Error($"Error in Deleting ' Config Id : {config.Id} Datastream Id :{config.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {config.Configuration.FalkonryConfiguration.Datastream}'.Error: { exception.Message}");
                            AfHelper.UpdateState(config.Configuration.AfConfiguration.SystemName, config.Name, "ERROR", $"Error in Deleting datastream");
                        }
                       
                    }
                    else
                    {
                        // New Configurations
                        if (!_tasks.ContainsKey(config.Id))
                        {
                            Log.Info($"New configuration found... ' Config Id : {config.Id} Datastream Id :{config.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {config.Configuration.FalkonryConfiguration.Datastream}'");
                            processConfiguration(config, false);
                        }
                        // Updated Configurations
                        else
                        {
                            if (config.Enabled)
                            {
                                Log.Info($"Updated configuration found... ' Config Id : {config.Id} Datastream Id :{config.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {config.Configuration.FalkonryConfiguration.Datastream}'...");
                                processConfiguration(config, false);
                            }
                        }
                    }
                }
            }
            Log.Debug($"################END CheckForConfigChanges################.");
            GC.Collect();
            _previousConfigCheckRunning = false;
        }

        private void processConfiguration(PiConfigurationItem serviceConfiguration, bool ignoreEnabled, bool runSynchronously = true)
        {
            try
            {
                Log.Info($"processConfiguration: datastream : ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                if (ignoreEnabled) serviceConfiguration.Enabled = true;

                if (serviceConfiguration.Enabled && _tasks.ContainsKey(serviceConfiguration.Id))
                {
                    Log.Debug($" releasing task and pipe for datastream : ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                    if (_pipes.ContainsKey(serviceConfiguration.Id))
                    {
                        _pipes[serviceConfiguration.Id].Stop();
                        _pipes[serviceConfiguration.Id].Dispose();
                        _pipes.Remove(serviceConfiguration.Id);
                        Dictionary<string, FalkonryReceiver> map = _streamingTasks[serviceConfiguration.Id];
                        foreach (KeyValuePair<string, FalkonryReceiver> entry in map)
                        {
                            ((FalkonryReceiver)entry.Value).StopListening();
                        }
                        _streamingTasks.Remove(serviceConfiguration.Id);

                    }

                    _tasks[serviceConfiguration.Id].Dispose();
                    _tasks.Remove(serviceConfiguration.Id);
                    //_streamingTasks.Remove(serviceConfiguration.Id);
                    GC.Collect();
                }

                Task t = new Task(() =>
                {
                    try
                    {
                        if (serviceConfiguration.Enabled)
                        {
                            //Log.Debug($" Creating new task and pipe for datastream : ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                            //AfHelper.UpdateState(serviceConfiguration.Configuration.AfConfiguration.SystemName, serviceConfiguration.Name, "OK", "Processing Configuration");
                            serviceConfiguration.Enabled = false;
                            //Log.Debug($" Processing Configuration for 'Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                            //Log.Debug($"Fetching the Attributes based on the search criteria... for datastream : {serviceConfiguration.Name}");

                            var attributes = AfHelper.FindElementAttributes(
                                serviceConfiguration.Configuration.AfConfiguration);
                            Log.Debug($"Attributes found = '{attributes.Count}'' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                            try
                            {
                                Datastream dataStream = null;
                                if (serviceConfiguration.Configuration.FalkonryConfiguration.SourceId != null && serviceConfiguration.Configuration.FalkonryConfiguration.SourceId !="new_datastream_saved")
                                    dataStream = FalkonryHelper.GetDatastream(serviceConfiguration.Configuration.FalkonryConfiguration);
                                else
                                    dataStream = FalkonryHelper.CreateDatastream(serviceConfiguration);


                                if (attributes.Count > 0 && serviceConfiguration.Configuration.HistoricalDataAccess != null)
                                {
                                    // Log.Debug($"Perform Historical Data Access = {serviceConfiguration.Configuration.HistoricalDataAccess.Enabled} for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                                    if (serviceConfiguration.Configuration.HistoricalDataAccess.Enabled)
                                    {
                                        bool historianDataAcess = serviceConfiguration.Configuration.HistoricalDataAccess.Enabled;
                                        bool overrideHistorianData = serviceConfiguration.Configuration.HistoricalDataAccess.Override;
                                        bool addEntityMeta = false;
                                        serviceConfiguration.Configuration.HistoricalDataAccess.Enabled = false;
                                        serviceConfiguration.Configuration.HistoricalDataAccess.Override = false;
                                        Log.Debug($"Creating Historical PI data access... for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                                        _historical = new HistoricalPi(serviceConfiguration.Configuration.HistoricalDataAccess, serviceConfiguration.Configuration.FalkonryConfiguration, attributes);
                                        // Input for Falkonry
                                        // Check that there isn't already data present...
                                        DateTime startTime; DateTime endTime;
                                        DateTime startTimeEvents = DateTime.Parse("01-Jan-1970");
                                        DateTime endTimeEvents = DateTime.Parse("01-Jan-1970");
                                        
                                        if (!overrideHistorianData)
                                        {
                                            try
                                            {
                                                startTimeEvents = Utils.ConvertMillisecondsToUTC(dataStream.Stats.EarliestDataPoint);
                                            }
                                            catch (Exception)
                                            {
                                            }

                                            try
                                            {
                                                endTimeEvents = Utils.ConvertMillisecondsToUTC(dataStream.Stats.LatestDataPoint);
                                            }
                                            catch (Exception)
                                            {
                                            }
                                        }
                                        else{
                                            startTime = Utils.ConvertMillisecondsToLOCAL(serviceConfiguration.Configuration.HistoricalDataAccess.StartTime);
                                            endTime = Utils.ConvertMillisecondsToLOCAL(serviceConfiguration.Configuration.HistoricalDataAccess.EndTime);

                                            Log.Debug($"Historical PI data access... Override : true. Selected time Range : {startTime} TO {endTime}. Data Present in Falkonry : {startTimeEvents} TO {endTimeEvents} for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                                            addEntityMeta = true;
                                            if (addEntityMeta)
                                                FalkonryHelper.AddEntityInformation(serviceConfiguration.Configuration.FalkonryConfiguration, attributes);
                                            _historical.StartBackfilling(
                                                new FalkonryFeeder(
                                                    serviceConfiguration.Configuration.FalkonryConfiguration,
                                                    false,
                                                    serviceConfiguration.Configuration.AfConfiguration
                                                    ),
                                                serviceConfiguration.Configuration.AfConfiguration,
                                                serviceConfiguration.Configuration.FalkonryConfiguration.AssessmentId.Length > 0,
                                                startTime,
                                                endTime);
                                            //if (addEntityMeta)
                                            //    FalkonryHelper.AddEntityInformation(serviceConfiguration.Configuration.FalkonryConfiguration, attributes);
                                        }

                                        TimeFrame
                                            datastreamTimeEvents = new TimeFrame(),
                                            configurationTimeEvents = new TimeFrame(),
                                            resultantTimeFrame_1 = new TimeFrame(),
                                            resultantTimeFrame_2 = new TimeFrame();

                                        datastreamTimeEvents.StartTime = startTimeEvents;
                                        datastreamTimeEvents.EndTime = endTimeEvents;

                                        configurationTimeEvents.StartTime = (new DateTime(1970, 1, 1)).AddMilliseconds(serviceConfiguration.Configuration.HistoricalDataAccess.StartTime).ToLocalTime();
                                        configurationTimeEvents.EndTime = (new DateTime(1970, 1, 1)).AddMilliseconds(serviceConfiguration.Configuration.HistoricalDataAccess.EndTime).ToLocalTime();

                                        resultantTimeFrame_1.StartTime = Utils.defaultDate;
                                        resultantTimeFrame_1.EndTime = Utils.defaultDate;

                                        resultantTimeFrame_2.StartTime = Utils.defaultDate;
                                        resultantTimeFrame_2.EndTime = Utils.defaultDate;

                                        Utils.ResultantTimeFrame(datastreamTimeEvents, configurationTimeEvents, resultantTimeFrame_1, resultantTimeFrame_2);


                                        if (!overrideHistorianData)
                                        {
                                            Log.Debug($"Historical PI data access... Override : false. Selected time Range : {serviceConfiguration.Configuration.HistoricalDataAccess.StartTime} TO {serviceConfiguration.Configuration.HistoricalDataAccess.EndTime}. Data Present in Falkonry : {startTimeEvents} TO {endTimeEvents} for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                                            if (resultantTimeFrame_1.StartTime != Utils.defaultDate && resultantTimeFrame_1.EndTime != Utils.defaultDate)
                                            {
                                                Log.Debug($"Historical PI data access... Override : false. Resultant Time Range {resultantTimeFrame_1.StartTime} To {resultantTimeFrame_1.EndTime} for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                                                addEntityMeta = true;
                                                if (addEntityMeta)
                                                    FalkonryHelper.AddEntityInformation(serviceConfiguration.Configuration.FalkonryConfiguration, attributes);
                                                _historical.StartBackfilling(
                                                    new FalkonryFeeder(
                                                        serviceConfiguration.Configuration.FalkonryConfiguration,
                                                        false,
                                                        serviceConfiguration.Configuration.AfConfiguration),
                                                    serviceConfiguration.Configuration.AfConfiguration,
                                                    serviceConfiguration.Configuration.FalkonryConfiguration.AssessmentId.Length > 0,
                                                    resultantTimeFrame_1.StartTime,
                                                    resultantTimeFrame_1.EndTime);
                                               
                                            }
                                            if (resultantTimeFrame_2.StartTime != Utils.defaultDate && resultantTimeFrame_2.EndTime != Utils.defaultDate)
                                            {
                                                Log.Debug($"Historical PI data access... Override : false. Resultant Time Range {resultantTimeFrame_2.StartTime} To {resultantTimeFrame_2.EndTime} for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                                                addEntityMeta = true;
                                                if (addEntityMeta)
                                                    FalkonryHelper.AddEntityInformation(serviceConfiguration.Configuration.FalkonryConfiguration, attributes);
                                                _historical.StartBackfilling(
                                                    new FalkonryFeeder(
                                                        serviceConfiguration.Configuration.FalkonryConfiguration,
                                                        false,
                                                        serviceConfiguration.Configuration.AfConfiguration),
                                                    serviceConfiguration.Configuration.AfConfiguration,
                                                    serviceConfiguration.Configuration.FalkonryConfiguration.AssessmentId.Length > 0,
                                                    resultantTimeFrame_2.StartTime,
                                                    resultantTimeFrame_2.EndTime);
                                            }
                                        }

                                        if (serviceConfiguration.Configuration.FalkonryConfiguration.AssessmentId.Length > 0)
                                        {
                                            _historical.StartUploadingFacts(new FalkonryFeeder(
                                                        serviceConfiguration.Configuration.FalkonryConfiguration,
                                                        false,
                                                        serviceConfiguration.Configuration.AfConfiguration),
                                                        serviceConfiguration.Configuration.AfConfiguration,
                                                         Utils.ConvertMillisecondsToLOCAL(serviceConfiguration.Configuration.HistoricalDataAccess.StartTime),
                                                         Utils.ConvertMillisecondsToLOCAL(serviceConfiguration.Configuration.HistoricalDataAccess.EndTime), overrideHistorianData);

                                        }
                                        //if (addEntityMeta)
                                        //    FalkonryHelper.AddEntityInformation(serviceConfiguration.Configuration.FalkonryConfiguration, attributes);
                                    }
                                    Log.Debug($"Perform Streaming Data Access =  {serviceConfiguration.Configuration.StreamingDataAccess.Enabled} for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");

                                    if (serviceConfiguration.Configuration.StreamingDataAccess.Enabled)
                                    {
                                        // Input for Falkonry

                                        Log.Info($"Configuring Streaming PI data access...for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
                                        Task.Factory.StartNew(() =>
                                        {
                                            _streaming = new StreamingPi(serviceConfiguration.Configuration.StreamingDataAccess, serviceConfiguration.Configuration.FalkonryConfiguration, attributes, serviceConfiguration.Configuration.AfConfiguration);
                                            _streaming.StartListening();
                                            if (!_pipes.ContainsKey(serviceConfiguration.Id))
                                            {
                                                _pipes.Add(serviceConfiguration.Id, _streaming);
                                            }
                                        }, TaskCreationOptions.AttachedToParent);
                                    }
                                    if (serviceConfiguration.Configuration.FalkonryConfiguration.AssessmentId.Length>0 && serviceConfiguration.Configuration.AfConfiguration.OutputTemplateAttribute.Length>0)
                                    {
                                        bool backFill = serviceConfiguration.Configuration.FalkonryConfiguration.Backfill;
                                        serviceConfiguration.Configuration.FalkonryConfiguration.Backfill = false;
                                        Task.Factory.StartNew(() =>
                                        {
                                            // Historical Output
                                            if (backFill)
                                            {
                                                var receiver = new FalkonryReceiver(serviceConfiguration.Configuration.FalkonryConfiguration, serviceConfiguration.Configuration.AfConfiguration, null);
                                                receiver.StartBackfill(Utils.ConvertMillisecondsToUTC(serviceConfiguration.Configuration.HistoricalDataAccess.StartTime), 
                                                    Utils.ConvertMillisecondsToUTC(serviceConfiguration.Configuration.HistoricalDataAccess.EndTime));
                                            }
                                            // Streaming Output
                                           
                                            if (serviceConfiguration.Configuration.StreamingDataAccess.Enabled)
                                            {
                                                Dictionary<string, FalkonryReceiver> _assessmentMap = new Dictionary<string, FalkonryReceiver>();
                                                Log.Debug($"Checking for Falkonry Receiver for Streaming Output...for ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}' ");
                                                foreach(var AssessmentId in serviceConfiguration.Configuration.FalkonryConfiguration.AssessmentId)
                                                {
                                                    if(AssessmentId != "")
                                                    {
                                                        if (!_streamingTasks.ContainsKey(serviceConfiguration.Id))
                                                            _streamingTasks.Add(serviceConfiguration.Id, _assessmentMap);

                                                        Task.Factory.StartNew(() =>
                                                        {
                                                            if (!_assessmentMap.ContainsKey(serviceConfiguration.Configuration.FalkonryConfiguration.SourceId + "_" + AssessmentId))
                                                            {
                                                                FalkonryReceiver receiver = (new FalkonryReceiver(serviceConfiguration.Configuration.FalkonryConfiguration, serviceConfiguration.Configuration.AfConfiguration, AssessmentId));
                                                                receiver.StartListening();
                                                                _assessmentMap.Add(serviceConfiguration.Configuration.FalkonryConfiguration.SourceId + "_" + AssessmentId, receiver);
                                                            }
                                                            else
                                                            {
                                                                Log.Debug($"Streaming Receiver found for AssessmentId : {AssessmentId}for ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}' ");
                                                                return;
                                                            }
                                                        }, TaskCreationOptions.AttachedToParent);
                                                    }
                                                    else
                                                    {
                                                        Log.Debug($"Empty assessment name for Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}");
                                                    }
                                                }
                                            }
                                        }, TaskCreationOptions.AttachedToParent);
                                    }
                                }
                                else
                                {
                                    Log.Debug($"No AFAttributes found, nothing to perform... for   ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");

                                }
                            }
                            catch (Exception exception)
                            {
                                if(serviceConfiguration.Configuration.FalkonryConfiguration.SourceId != null && serviceConfiguration.Configuration.FalkonryConfiguration.SourceId != "new_datastream_saved")
                                {
                                    Log.Error($"Could not process for :  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'.Error : {exception.Message}");
                                }else
                                {
                                    Log.Error($"Could not create datastream : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}.Error : {exception.Message}");
                                }
                               
                            }
                           
                            try
                            {
                                // Why this code ?????
                                //SetConfiguration(ServiceConfigutations);Utils.ResultantTimeFrame
                                //AfHelper.UpdateEnabledFlag(serviceConfiguration.Configuration.AfConfiguration.SystemName, serviceConfiguration.Name, serviceConfiguration.Enabled);
                                //serviceConfiguration.Configuration.AfConfiguration.EventFrameUploadedTime = new string[serviceConfiguration.Configuration.FalkonryConfiguration.AssessmentId.Count()];
                                for (int k=0; k < serviceConfiguration.Configuration.AfConfiguration.EventFrameUploadedTime.Count(); k++)
                                {
                                    //serviceConfiguration.Configuration.AfConfiguration.EventFrameUploadedTime[k] = "";
                                    string[] eventTimeFrameList = serviceConfiguration.Configuration.AfConfiguration.EventFrameUploadedTime[k].Split(',');
                                    string[] eventFrameList = serviceConfiguration.Configuration.AfConfiguration.EventFrameTemplateNames[k].Split(',');
                                    for (int eventIndex = 0; eventIndex < eventTimeFrameList.Count(); eventIndex++)
                                    {
                                        if(eventFrameList[eventIndex] == "")
                                        {
                                            eventTimeFrameList[eventIndex] = "";
                                        }
                                        else if (eventTimeFrameList[eventIndex] == "")
                                        {
                                            eventTimeFrameList[eventIndex] = serviceConfiguration.Configuration.HistoricalDataAccess.StartTime.ToString();
                                            eventTimeFrameList[eventIndex] += "-";
                                            eventTimeFrameList[eventIndex] += serviceConfiguration.Configuration.HistoricalDataAccess.EndTime.ToString();
                                        }
                                        else
                                        {
                                            string[] timeFrame = eventTimeFrameList[eventIndex].Split('-');
                                            DateTime startTime = Utils.ConvertMillisecondsToLOCAL(Convert.ToInt64(timeFrame[0]));
                                            DateTime endTime = Utils.ConvertMillisecondsToLOCAL(Convert.ToInt64(timeFrame[1]));
                                            TimeFrame
                                                newTimeFrame = new TimeFrame(),
                                                existingTimeFrame = new TimeFrame(),
                                                resultantTimeFrame = new TimeFrame();

                                            newTimeFrame.StartTime = Utils.ConvertMillisecondsToLOCAL(serviceConfiguration.Configuration.HistoricalDataAccess.StartTime);
                                            newTimeFrame.EndTime = Utils.ConvertMillisecondsToLOCAL(serviceConfiguration.Configuration.HistoricalDataAccess.EndTime);

                                            existingTimeFrame.StartTime = startTime;
                                            existingTimeFrame.EndTime = endTime;

                                            Utils.AggregateTimeFrame(existingTimeFrame, newTimeFrame, resultantTimeFrame);

                                            eventTimeFrameList[eventIndex] = Utils.ConvertDateTimeToMilliseconds(resultantTimeFrame.StartTime).ToString();
                                            eventTimeFrameList[eventIndex] += "-";
                                            eventTimeFrameList[eventIndex] += Utils.ConvertDateTimeToMilliseconds(resultantTimeFrame.EndTime).ToString();
                                        }
                                    }
                                    serviceConfiguration.Configuration.AfConfiguration.EventFrameUploadedTime[k] = string.Join(",", eventTimeFrameList); 
                                }

                                AfHelper.SaveSingleConfig(serviceConfiguration);
                            }
                            catch (Exception e)
                            {
                                Log.Error($"Could not write config to database.  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}' Error : {e.Message}");
                                Log.Error($"Exception Stack Trace - . {e.StackTrace}");
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Log.Error($"Unhandled exception:  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}' Error : {exception.Message}");
                    }
                });

                if (runSynchronously)
                {
                    t.ContinueWith(status => Log.Debug("Task completed."));
                    t.RunSynchronously();
                }
                else
                {
                    t.Start();
                }
                if (!_tasks.ContainsKey(serviceConfiguration.Id))
                {
                    _tasks.Add(serviceConfiguration.Id, t);
                }
            }
            catch (Exception exception)
            {
                Log.Error($"Unhandled exception while processconfiguration: {exception.Message} for  ' Config Id : {serviceConfiguration.Id} Datastream Id :{serviceConfiguration.Configuration.FalkonryConfiguration.SourceId} Datastream Name : {serviceConfiguration.Configuration.FalkonryConfiguration.Datastream}'");
            }
        }
        public void Start()
        {
          
            try {
                afObservableLayer.setUp();
                afSubscriptions.setUpObservablePipeLine();
            } catch(Exception e)
            {
                Log.Fatal($" Exception in setting up Reactive Infrastructure ",e);
            }
            ServicePointManager.DefaultConnectionLimit = 50;

            _startHasCompleted = true;
        }
        public void Stop()
        {
            afSubscriptions.clearAllSubscriptions();
            afObservableLayer.tearDown();
        }
        private DateTime _configUpdated = DateTime.Parse("01-Jan-1970");
        private Configurations ServiceConfigutations { get; set; }
        private Configurations CollectConfiguration()
        {
            try
            {
                Configurations ret = new Configurations();
                int piSystemCount = 0;
                ret.ConfigurationItems = new List<PiConfigurationItem>();

                var systems = new OSIsoft.AF.PISystems();
                foreach (var system in systems)
                {
                    Log.Debug($" Fetching Configuration for System : {system.Name}");
                    try
                    {
                        var config = AfHelper.RetrieveConfigurationFromPISystem(system.Name);
                        piSystemCount++;
                        if (config != null)
                        {
                            ret.ConfigurationItems.AddRange(config.ConfigurationItems);
                        }
                    }
                    catch(Exception e)
                    {
                        Log.Error($" Exception caught in CollectConfiguration. Error : {e.Message}");
                        Log.Error($" Exception Stack Trace CollectConfiguration: {e.StackTrace}");
                    }
                    
                   
                }
                
                if (piSystemCount == 0)
                {
                    Log.Error($"CollectConfiguration: Not able to connect to any of the PISystem");
                    ret = null;
                }
                return ret;
            }
            catch(Exception e)
            {
                Log.Error($" Exception caught in CollectConfiguration. Error : {e.Message}");
                Log.Error($" Exception Stack Trace CollectConfiguration: {e.StackTrace}");
                return null;
                //throw;
            }
           
        }
    }
}