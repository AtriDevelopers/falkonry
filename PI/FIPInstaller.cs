﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Falkonry.Integrator.PISystem
{
    [RunInstaller(true)]
    public partial class FipInstaller : Installer
    {
        private ServiceInstaller _serviceInstaller;
        private ServiceProcessInstaller _processInstaller;

        public FipInstaller()
        {
            _processInstaller = new ServiceProcessInstaller();
            _processInstaller.Account = ServiceAccount.LocalSystem;

            _serviceInstaller = new ServiceInstaller();
            _serviceInstaller.StartType = ServiceStartMode.Automatic;
            _serviceInstaller.DisplayName = typeof(FipInstaller).Namespace;
            _serviceInstaller.ServiceName = typeof(FipInstaller).Namespace;
            _serviceInstaller.Installers.Clear();

            Installers.Add(_processInstaller);
            Installers.Add(_serviceInstaller);
        }
    }
}
