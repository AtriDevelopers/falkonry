﻿using System.Linq;
using Falkonry.Integrator.PISystem.Helper;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Data;
using OSIsoft.AF.PI;
using OSIsoft.AF.Time;
using log4net;
using System.Collections.Generic;
using System;
using System.Reflection;

namespace Falkonry.Integrator.PISystem
{
    internal class HistoricalPi
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        private HistoricalConfiguration Configuration { get; }
        private FalkonryConfiguration _falkonryConfiguration { get;  }

        private AFAttributeList ConfiguredAttributes { get; }

        public HistoricalPi(HistoricalConfiguration historicalConfiguration, FalkonryConfiguration falkonryConfig, AFAttributeList attributes)
        {
            Configuration = historicalConfiguration;
            ConfiguredAttributes = attributes;
            _falkonryConfiguration = falkonryConfig;
        }

        public void StartBackfilling(FalkonryFeeder falkonry, AfConfiguration afConfiguration, bool searchForFacts, DateTime startTime, DateTime endTime)
        {
            if (afConfiguration.AFServerTimeZoneId != TimeZoneInfo.Local.Id)
            {
                Log.Debug($"StartBackfilling: Local time zone - {TimeZoneInfo.Local.Id} is different from AFservertTimeZone - {afConfiguration.AFServerTimeZoneId}");
                Log.Debug($"StartBackfilling: Machine Time zone startTime - {startTime}");
                Log.Debug($"StartBackfilling: Machine Time zone endTime - {endTime}");
                var afServerzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(afConfiguration.AFServerTimeZoneId);
                startTime = TimeZoneInfo.ConvertTime(startTime, afServerzTimeZone);
                endTime = TimeZoneInfo.ConvertTime(endTime, afServerzTimeZone);
                Log.Debug($"StartBackfilling: AFServer Time zone startTime - {startTime}");
                Log.Debug($"StartBackfilling: AFServer Time zone endTime - {endTime}");
            }
            var historyPeriod = new AFTimeRange(startTime, endTime);
            var paging = new PIPagingConfiguration(PIPageType.EventCount, Configuration.PageSize);
            Log.Debug($"Retrieving Events for AFAttributes... Time Range : {startTime} TO {endTime} for 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}'");
            // Check for total number of events:
            var counts = ConfiguredAttributes.Data.Summary(historyPeriod, AFSummaryTypes.Count, AFCalculationBasis.EventWeighted, AFTimestampCalculation.Auto, paging);

            const int totalEventsThreshold = 10000000; const int denseAttributeThreshold = 1000000;
            long totalEvents = 0; var atLeastOneDenseAttribute = false;
            foreach(var count in counts)
            {
                if (!count[AFSummaryTypes.Count].IsGood) continue;
                totalEvents += (int)count[AFSummaryTypes.Count].Value;
                
                if ((int) count[AFSummaryTypes.Count].Value <= denseAttributeThreshold) continue;
                if (!atLeastOneDenseAttribute) atLeastOneDenseAttribute = true;
            }
            Log.Debug($"Details on Fetched attributes for 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}'");
            Log.Debug($"Total events across all attributes = '{totalEvents}'");
            Log.Debug($"Total events threshold across all attributes = '{totalEventsThreshold}'");
            Log.Debug($"At least one dense attribute = '{false}'");

            if (totalEvents == 0)
            {
                AfHelper.UpdateState(afConfiguration.SystemName, _falkonryConfiguration.Datastream, "ERROR", $"No input events found for {startTime} to {endTime} !");
                Log.Debug($"StartBackfilling: No input events found for time range {startTime} to {endTime}");
                return;
            }

            if (totalEvents > totalEventsThreshold)
            {
                foreach (var attribute in ConfiguredAttributes)
                {
                    var halfway = historyPeriod.Span.TotalMilliseconds / 2;

                    var newStart = historyPeriod.StartTime;
                    var newEnd = historyPeriod.StartTime.LocalTime.AddMilliseconds(halfway);
                    var newHistoryPeriod = new AFTimeRange(newStart, newEnd);

                    var smallBucket = attribute.Data.RecordedValues(newHistoryPeriod, AFBoundaryType.Inside, null, null, true, totalEventsThreshold);
                    var containerValues = new AFValues(); containerValues.AddRange(smallBucket);
                    var container = new List<AFValues> { containerValues };

                    Log.Debug($"Sending to Falkonry for Time {newStart} TO {newEnd} Count :{container.Count}: 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}'");

                    falkonry.SendEvents(container);
                    newStart = newEnd;
                    newEnd = historyPeriod.EndTime;
                    newHistoryPeriod = new AFTimeRange(newStart, newEnd);

                    smallBucket = attribute.Data.RecordedValues(newHistoryPeriod, AFBoundaryType.Inside, null, null, true, totalEventsThreshold);
                    containerValues = new AFValues(); containerValues.AddRange(smallBucket);
                    container = new List<AFValues> { containerValues };
                    Log.Debug($"Sending to Falkonry for Time {newStart} TO {newEnd} Count :{container.Count}: 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}'");
                    //Log.Debug($"Sending to Falkonry for Time {newStart} TO {newEnd} Count :{container.Count}: 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}'");
                    falkonry.SendEvents(container);

                }
            }
            else
            {
                // Get everything in one go, the AF SDK handles a lot of this for us.
                var bigBucket = ConfiguredAttributes.Data.RecordedValues(historyPeriod, AFBoundaryType.Inside, "", true, paging).ToList();

                // Log the Event Count per Attribute, just so we can tune the service at a later date.
                //bigBucket.ForEach(att => Log.Debug($"Attribute '{att[0].Attribute.Name}', Events Retrieved = {att.Count}"));
                Log.Debug($"Sending to Falkonry for Time {historyPeriod.StartTime} TO {historyPeriod.EndTime} Count :{bigBucket.Count}: 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}'");
                falkonry.SendEvents(bigBucket);
            }

            // Search Event Frames for Facts
            if (!searchForFacts) return;

            
        }

        public void StartUploadingFacts(FalkonryFeeder falkonry, AfConfiguration afConfiguration, DateTime startTime, DateTime endTime, bool overrideHistorianData)
        {
            Log.Debug($"Searching for (Facts) Events... Time Range : {startTime} TO {endTime} 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}, overrideHistorianData - {overrideHistorianData}'");
            string[] eventFrameTemplateNames = afConfiguration.EventFrameTemplateNames;
            int eventIndex = 0;
            TimeFrame selectTimeFrame = new TimeFrame();
            selectTimeFrame.StartTime = startTime;
            selectTimeFrame.EndTime = endTime;
            for ( eventIndex=0; eventIndex < eventFrameTemplateNames.Count(); eventIndex++)
            {
                var events = AfHelper.FindEventFrames(
                       afConfiguration,
                       eventFrameTemplateNames[eventIndex].Split(','),
                       afConfiguration.EventFrameUploadedTime[eventIndex].Split(','),
                       selectTimeFrame,
                       overrideHistorianData
                       );
                Log.Debug($"Events found = '{events.Count}' Time Range : {startTime} TO {endTime} considering old data uploaded 'Datastream : {_falkonryConfiguration.Datastream} Id :{_falkonryConfiguration.SourceId}'");
                if (events.Count > 0) falkonry.SendFacts(events, eventIndex);
            }
        }

    }
}
