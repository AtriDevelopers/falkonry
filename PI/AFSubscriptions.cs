﻿using Falkonry.Integrator.PISystem.Functors;
using Falkonry.Integrator.PISystem.Helper;
using Falkonry.Integrator.PISystem.Subscribers;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falkonry.Integrator.PISystem
{
    public class AFSubscriptions
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private AFObservableLayer aflayer;
        private IDisposable historicalSubscription;
        private IDisposable configCreatedSubscription;
        private IDisposable configDeletedSubscription;
        private IDisposable configUpdatedSubscription;

        /**
* 
* Holds list of live streams curretly pushing data to falkonry back end.
*/
        private IDictionary<PiConfiguration, LiveStreamToFalkonry> liveStreams;



        /**
* Holds the PiConfig and FalkonryReceiver against AssessmentIds.
*/
        private IDictionary<string, Tuple<PiConfiguration, FalkonryReceiver>> falkonryReceivers;

        public AFSubscriptions(AFObservableLayer layer)
        {
            aflayer = layer;
            liveStreams = new Dictionary<PiConfiguration,LiveStreamToFalkonry>();
            falkonryReceivers = new Dictionary<string, Tuple<PiConfiguration, FalkonryReceiver>>();
        }

        public void setUpObservablePipeLine()
        {
            setUpConfigCreatedPipeline();
            setUpConfigDeletedPipeline();
            setUpConfigUpdatedPipeline();
            setUpHistoricalPipeLine();
        }

        private void setUpHistoricalPipeLine()
        {
            if (historicalSubscription == null)
            {
                historicalSubscription = aflayer.getObservableForHistoricalPush()
                .Select(new PushHistoricalData().getDelegate())
                .Subscribe(new PiConfigurationSubscriber());
            }
            else
            {
                Log.Info($"Historical Pipeline Already Set Up");
            }

        }

        internal void addLiveStreamInDictionary(LiveStreamToFalkonry liveStreamToFalkonry, PiConfiguration config)
        {
            liveStreams.Add(config, liveStreamToFalkonry);
        }

        public void AddFalkonryReceiverAgainstAssessmentId(Tuple<PiConfiguration, FalkonryReceiver> tuple, string assessmentId)
        {
            falkonryReceivers.Add(assessmentId, tuple);
            Log.Debug($"FalkonryReceiver Added in Dictionary for Datastream: {tuple.Item1.FalkonryConfiguration.Datastream} DSId: {tuple.Item1.FalkonryConfiguration.SourceId} AssessmentID: {assessmentId}");
        }

        internal void removeAllFalkonryReceiversForPiconfig(PiConfiguration config)
        {
            falkonryReceivers.Values.Where(x =>
            {
                Log.Debug($"Searching for FalkonryReceivers  Added in Dictionary for Datastream: {x.Item1.FalkonryConfiguration.Datastream} DSId: {x.Item1.FalkonryConfiguration.SourceId}");
                return x.Item1.Equals(config);
            }).Select(x => {
                Log.Debug($"FalkonryReceivers  Assessment Found in Dictionary for Datastream: {x.Item1.FalkonryConfiguration.Datastream} DSId: {x.Item1.FalkonryConfiguration.SourceId}");
                return x.Item2.getAssessmentId();
            }).
            ToList().All(x=> {
                falkonryReceivers.Remove(x);
                Log.Debug($"FalkonryReceiver REmove from in Dictionary for  AssessmentID: {x}");
                return true;
            });
            
        }

        internal void removeLiveStream(PiConfiguration config)
        {
            LiveStreamToFalkonry stream = null;
            liveStreams.TryGetValue(config, out stream);
            if (stream != null) 
            {
                stream.stopStreaming();
                liveStreams.Remove(config);
                Log.Info($"Live Streaming removed for Datastream: {config.FalkonryConfiguration.Datastream} Id: {config.FalkonryConfiguration.Datastream}");
            }

        }

        private void setUpConfigCreatedPipeline()
        {
            if (configCreatedSubscription == null)
            {
                configCreatedSubscription = aflayer.getObservableForConfigCreated()
                .Select(new FalkonryHistoricalOutput().getDelegate())
                 .Select(new LiveStreamToFalkonry(this).getDelegate())
                .Select(new FalkonryStreamingOutput(this).getDelegate())
                .Subscribe(new PiConfigurationSubscriber());
            } 
            else
            {
                Log.Info($"Historical Pipeline Already Set Up");
            }

        }

        private void setUpConfigDeletedPipeline()
        {
            if(configDeletedSubscription == null)
            {
                configDeletedSubscription = aflayer.getObservableForConfigDeleted()
                    .Select(new StopLiveStreaming(this).getDelegate())
                    .Select(new StopFalkonryOutputIngestion(this).getDelegate())
                    .Subscribe(new PiConfigurationSubscriber());
            }
        }

        private void setUpConfigUpdatedPipeline()
        {
            if (configUpdatedSubscription == null)
            {
                configUpdatedSubscription = aflayer.getObservableForConfigUpdated()
                    .Select(new StopLiveStreaming(this).getDelegate())
                    .Select(new StopFalkonryOutputIngestion(this).getDelegate())
                    .Select(new LiveStreamToFalkonry(this).getDelegate())
                    .Select(new FalkonryHistoricalOutput().getDelegate())
                    .Select(new FalkonryStreamingOutput(this).getDelegate())
                    .Subscribe(new PiConfigurationSubscriber());
            }
        }

        public void clearAllSubscriptions()
        {
            if(historicalSubscription!=null)
                historicalSubscription.Dispose();
            if (configCreatedSubscription != null)
                configCreatedSubscription.Dispose();
            if (configDeletedSubscription != null)
                configDeletedSubscription.Dispose();
            if (configUpdatedSubscription != null)
                configUpdatedSubscription.Dispose();

            liveStreams.All(x =>
            {
                bool yes = false;
                x.Value.stopStreaming();
                return yes;
            });
            liveStreams.Clear();

            falkonryReceivers.All(x =>
            {
                x.Value.Item2.StopListening();
                return true;
            });

            falkonryReceivers.Clear();
        }
    }
}
